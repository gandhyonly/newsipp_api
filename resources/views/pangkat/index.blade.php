@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>DASHBOARD</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Widgets -->
	    <div class="row clearfix">
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-blue hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">people</i>
	                </div>
	                <div class="content">
	                    <div class="text">JUMLAH</div>
	                    <div class="number count-to" data-from="0" data-to="{{$pangkatCount}}" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Widgets -->
	    

	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th width="200px">Nama Pangkat</th>
	                                <th width="">Golongan</th>
	                                <th width="">Singkatan</th>
	                                <th width="">Singkatan Tentara</th>
	                                <th width="">Jenis</th>
	                                <th>PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataPangkat as $pangkat)
	                            <tr style="font-size:12px;">
	                            	<td>{{$pangkat->pangkat_id}}</td>
	                            	<td>{{$pangkat->nama_pangkat}}</td>
	                            	<td>{{$pangkat->getNamaGolongan() }}</td>
	                            	<td>{{$pangkat->singkatan}}</td>
	                            	<td>{{$pangkat->singkatan_tentara}}</td>
	                            	<td>{{$pangkat->jenis}}</td>
	                            	<td>
										<a onclick="confirmDelete('{{ route('pangkat.destroy',$pangkat->pangkat_id) }}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
									</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                    {{ $dataPangkat->render() }}
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
	</div>
@endsection
@section('script')
	<script>
	$(function () {
	    //Widgets count
	    $('.count-to').countTo();
	});
	</script>
@endsection