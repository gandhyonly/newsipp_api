
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li {{ setActive('dashboard') }}>
                        <a href="{{route('dashboard.index')}}">
                            <i class="material-icons">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
<!--                     <li {{ setActive('personel') }}>
                        <a href="{{route('personel.create')}}">
                            <i class="material-icons">people</i>
                            <span>Input Personel</span>
                        </a>
                    </li> -->
<!--                     <li {{ setActive('personel') }}>
                        <a href="{{route('personel.index')}}">
                            <i class="material-icons">people</i>
                            <span>Personel</span>
                        </a>
                    </li> -->
                    <li {{ setActive('satuan') }} {{setActive('jenis-satuan')}}>
                        <a href="{{route('satuan.index')}}">
                            <i class="material-icons">location_on</i>
                            <span>Satuan</span>
                        </a>
                    </li>
                    <li {{ setActive('jabatan') }}>
                        <a href="{{route('jabatan.index')}}">
                            <i class="material-icons">stars</i>
                            <span>Jenis Jabatan</span>
                        </a>
                    </li>
                    <li {{ setActive('duplikasi-satuan') }}>
                        <a href="{{route('satuan.duplication.get')}}">
                            <i class="material-icons">stars</i>
                            <span>Duplikasi Satuan</span>
                        </a>
                    </li>
<!--                     <li {{ setActive('pangkat') }}>
                        <a href="{{route('pangkat.index')}}">
                            <i class="material-icons">stars</i>
                            <span>Pangkat</span>
                        </a>
                    </li>
                    <li {{ setActive('nivelering') }}>
                        <a href="{{route('nivelering.index')}}">
                            <i class="material-icons">stars</i>
                            <span>Nivelering</span>
                        </a>
                    </li> -->
                    <li class="header">SETTING</li>
                    @if(env('APP_MIGRATION','false')=='true')
                    <li>
                        <a href="{{route('migration.index')}}">
                            <i class="material-icons">autorenew</i>
                            <span>Migrasi</span>
                        </a>
                    </li>
                    @endif
                    <li>
                        <a href="{{route('user.logout')}}">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->