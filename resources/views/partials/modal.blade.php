<!-- Modal -->
<div class="modal" id="modalDelete" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-warning" style="width:30px;"></i> Pesan Konfirmasi</h4>
        </div>
      
        <div class="modal-body">
          <!-- form start -->
          <form method="POST" data-action='_LINK_' id="formDelete">
            <input type="hidden" name="_method" value="DELETE">
            {{ csrf_field() }}
            <div class="form-group">
              Apakah anda yakin akan menghapus data ini ?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn bg-red btn-lg btn-block waves-effect"> DELETE </button>
            </div>
          </form>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal" id="modalMigration" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
          <h4 class="modal-title" id="myModalLabel2"><i class="fa fa-warning" style="width:30px;"></i> Pesan Konfirmasi</h4>
        </div>
      
        <div class="modal-body">
          <!-- form start -->
          <form method="POST" data-action='_LINK_' id="formMigration">
            <input type="hidden" name="_method" value="POST">
            {{ csrf_field() }}
            <div class="form-group">
              Apakah anda yakin akan melakukan migrasi ?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn bg-red btn-lg btn-block waves-effect"> DELETE </button>
            </div>
          </form>
      </div>
    </div>
</div>