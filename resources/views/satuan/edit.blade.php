@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>
		    	@if($satuan->parent()->count() > 0)
		        <a href="{{route('satuan.show',$satuan->satuan_id)}}" class="btn btn-primary btn-xs">
	                    <i class="material-icons">keyboard_arrow_left</i>
		        </a>
		        @endif
		        Satuan Edit
		    </h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Filter -->
	    <div class="row clearfix" id="filter">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>EDIT SATUAN BARU</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{ route('satuan.update',$satuan->satuan_id) }}">
	                        {{ csrf_field() }}
							<input type="hidden" name="_method" value="PATCH">
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_satuan">NAMA SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="nama_satuan" name="nama_satuan" class="form-control" placeholder="Nama Satuan" type="text" value="{{$satuan->nama_satuan}}">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="tipe">PARENT SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" data-live-search="true" name="parent_id" id="parent_id">
		                                    	@if($satuan->parent()->first() != null)
			                                    	@if($satuan->parent()->first()->parent()->first()!=null)
			                                        	<option value="{{$satuan->parent()->first()->parent()->first()->satuan_id}}">{{$satuan->parent()->first()->parent()->first()->nama_satuan}}</option>
			                                    	@endif
		                                        <option value="{{$satuan->parent->satuan_id}}">{{$satuan->parent->nama_satuan}}</option>
		                                    	@endif
		                                    	@foreach($selectDataSatuanAtasan as $dataSatuanAtasan)
		                                        <option value="{{$dataSatuanAtasan->satuan_id}}">{{$dataSatuanAtasan->nama_satuan}}</option>
		                                    	@endforeach
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="tipe">TIPE SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="tipe" id="tipe">
		                                        <option value="">PILIH TIPE SATUAN</option>
		                                        <option value="STRUKTUR">STRUKTUR</option>
		                                        <option value="NONSTRUKTUR">NON STRUKTUR</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="jenis_satuan_id">JENIS SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" data-live-search="true" name="jenis_satuan_id" id="jenis_satuan_id">
		                                        <option value="">PILIH JENIS SATUAN</option>
	                                        	@foreach($selectDataJenisSatuan as $selectJenisSatuan)
		                                        <option value="{{ $selectJenisSatuan->jenis_satuan_id }}">{{ $selectJenisSatuan->nama_jenis_satuan }}</option>
		                                        @endforeach
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label>BUAT</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->
@endsection

@section('script')
	<script>
		$("#jenis_satuan_id").val("{{$satuan->jenis_satuan_id}}");
		$("#tipe").val("{{$satuan->tipe}}");
		$("#parent_id").val("{{$satuan->parent_id}}");
	</script>
@endsection