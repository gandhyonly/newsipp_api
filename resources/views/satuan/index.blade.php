@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>DASHBOARD</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Widgets -->
	    <div class="row clearfix">
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-blue hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">people</i>
	                </div>
	                <div class="content">
	                    <div class="text">SATUAN TERVERIFIKASI</div>
			            <div class="preloader" style="width:20px; height:20px;">
			                <div class="spinner-layer pl-white">
			                    <div class="circle-clipper left">
			                        <div class="circle"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="api-widget hidden">
		                    <div id="totalVerifiedSatuan">0</div> dari 
		                    <div id="totalSatuan">0</div>
			            </div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-green hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">check</i>
	                </div>
	                <div class="content">
	                    <div class="text">JENIS SATUAN</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahJenisSatuan}}" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-red hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">forum</i>
	                </div>
	                <div class="content">
	                    <div class="text">JENIS JABATAN</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahJabatan}}" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-orange hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">forum</i>
	                </div>
	                <div class="content">
	                    <div class="text">JABATAN TERVERIFIKASI</div>
			            <div class="preloader" style="width:20px; height:20px;">
			                <div class="spinner-layer pl-white">
			                    <div class="circle-clipper left">
			                        <div class="circle"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="api-widget hidden">
		                    <div id="totalVerifiedJabatan">0</div> dari 
		                    <div id="totalJabatan">0</div>
		                </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Widgets -->

	    <div class="row m-b-10">
	    	<div class="col-md-4"><a href="{{ route('satuan.create')}}" class="btn btn-success btn-block">Buat Satuan</a></div>
	    	<div class="col-md-4"><a href="{{ route('jenis-satuan.create')}}" class="btn btn-success btn-block">Buat Jenis Satuan</a></div>
	    </div>


	    <!-- Filter -->
	    <div class="row clearfix" id="filter">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>FILTER</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{route('satuan.filter')}}">
	                    	{{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_satuan">Nama Satuan</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="nama_satuan" name="nama_satuan" class="form-control" placeholder="Masukkan Nama Satuan" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="jenis_satuan_id">Jenis Satuan</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="jenis_satuan_id" id="jenis_satuan_id">
		                                        <option value="all">SEMUA JENIS SATUAN</option>
		                                        @foreach($dataJenisSatuan as $jenisSatuan)
		                                        <option value="{{$jenisSatuan->jenis_satuan_id}}">{{$jenisSatuan->nama_jenis_satuan}}</option>
		                                        @endforeach
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label><i class="material-icons">search</i> CARI</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->
	    
	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	            	<div class="header">
	            	<h2>Daftar Satuan</h2>
	            	</div>
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th width="350px">Nama Satuan</th>
	                                <th width="150px">Jumlah Satuan</th>
	                                <th width="200px">Tipe Satuan</th>
	                                <th width="100px">Status</th>
	                                <th width="100px">PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataSatuan as $satuan)
	                            <tr style="font-size:12px;">
	                            	<td>{{ $satuan->satuan_id }}</td>
	                            	<td>{{ $satuan->nama_satuan }}</td>
	                            	<td>{{ $satuan->childs()->count() }}</td>
	                            	<td>{{ $satuan->tipe }}</td>
	                            	<td>{!! $satuan->printStatus() !!}<br>
	                            		@if($satuan->verified_by==null)
	                            		<a href="{{route('satuan.verification',$satuan->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Verifikasi</a>
	                            		@else
	                            		<a href="{{route('satuan.verification',$satuan->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Tidak terverifikasi</a>
	                            		@endif
		                            </td>
	                            	<td>
		                            	<a href="{{route('satuan.show',$satuan->satuan_id)}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                    {{ $dataSatuan->render() }}
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->

	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	            	<div class="header">
	            	<h2>Jenis Satuan</h2>
	            	</div>
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th width="400px">Jenis Satuan</th>
	                                <th width="200px">Kategori Satuan</th>
	                                <th>Jumlah Satuan</th>
	                                <th>PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataJenisSatuan as $jenisSatuan)
	                            <tr style="font-size:12px;">
	                            	<td>{{ $jenisSatuan->jenis_satuan_id }}</td>
	                            	<td>{{ $jenisSatuan->nama_jenis_satuan }}</td>
	                            	<td>{{ $jenisSatuan->satuan()->count() }}</td>
	                            	<td>{{ $jenisSatuan->kategori }}</td>
	                            	<td>
		                            	<a href="{{route('jenis-satuan.edit',$jenisSatuan->jenis_satuan_id)}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
<!-- 	                            		<a onclick="confirmDelete('{{route('jenis-satuan.destroy',$jenisSatuan->jenis_satuan_id)}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a> -->
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                    {{ $dataSatuan->render() }}
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
	</div>
@endsection

@section('script')
	<script>
	$(function () {
	    //Widgets count
	    $('.count-to').countTo();
	});
	</script>
@endsection