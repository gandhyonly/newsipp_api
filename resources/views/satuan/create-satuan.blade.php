@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>
		    	@if($satuan->parent()->count() > 0)
		        <a href="{{route('satuan.show',$satuan->satuan_id)}}" class="btn btn-primary btn-xs">
	                    <i class="material-icons">keyboard_arrow_left</i>
		        </a>
		        @endif
		        Create Satuan
		    </h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Filter -->
	    <div class="row clearfix" id="filter">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>BUAT SATUAN BARU</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{route('satuan.store-satuan',$satuan->satuan_id)}}">
	                        {{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_satuan">NAMA SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="nama_satuan" name="nama_satuan" class="form-control" placeholder="Nama Satuan" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="tipe">TIPE SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="tipe" id="tipe">
		                                        <option value="">PILIH TIPE SATUAN</option>
		                                        <option value="STRUKTUR">STRUKTUR</option>
		                                        <option value="NONSTRUKTUR">NON STRUKTUR</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="jenis_satuan_id">JENIS SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" data-live-search="true" name="jenis_satuan_id" id="jenis_satuan_id">
		                                        <option value="">PILIH JENIS SATUAN</option>
	                                        	@foreach($selectDataJenisSatuan as $selectJenisSatuan)
		                                        <option value="{{ $selectJenisSatuan->jenis_satuan_id }}">{{ $selectJenisSatuan->nama_jenis_satuan }}</option>
		                                        @endforeach
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="jumlah_satuan">JUMLAH</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="jumlah_satuan" name="jumlah_satuan" class="form-control" placeholder="Jumlah" type="number" value="1">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="parent_id">PARENT SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
                                            		<input class="form-control" type="text" value="{{ $satuan->printPath($satuan->satuan_id)}}" disabled>
	                                            </div>
	                                        </div>
	                                        <input type="hidden" name="parent_id" value="{{$satuan->satuan_id}}">
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label>BUAT</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->
@endsection

@section('script')
	<script>
	$("#parent_id").val("{{$satuan->satuan_id}}");
	</script>
@endsection