@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>
		    	@if($satuan->parent()->count() > 0)
		        <a href="{{route('satuan.show',$satuan->parent_id)}}" class="btn btn-primary btn-xs">
	                    <i class="material-icons">keyboard_arrow_left</i>
		        </a>
		        @endif
			    Satuan View
			</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Widgets -->
	    <div class="row clearfix">
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-blue hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">people</i>
	                </div>
	                <div class="content">
	                    <div class="text">SATUAN TERVERIFIKASI</div>
			            <div class="preloader" style="width:20px; height:20px;">
			                <div class="spinner-layer pl-white">
			                    <div class="circle-clipper left">
			                        <div class="circle"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="api-widget hidden">
		                    <div id="totalVerifiedSatuan">0</div> dari 
		                    <div id="totalSatuan">0</div>
			            </div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-green hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">location_on</i>
	                </div>
	                <div class="content">
	                    <div class="text">SATWIL</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahSatwil}}" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-orange hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">group_work</i>
	                </div>
	                <div class="content">
	                    <div class="text">SATKER</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahSatker}}" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-red hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">star</i>
	                </div>
	                <div class="content">
	                    <div class="text">JABATAN TERVERIFIKASI</div>
			            <div class="preloader" style="width:20px; height:20px;">
			                <div class="spinner-layer pl-white">
			                    <div class="circle-clipper left">
			                        <div class="circle"></div>
			                    </div>
			                </div>
			            </div>
			            <div class="api-widget hidden">
		                    <div id="totalVerifiedJabatan">0</div> dari 
		                    <div id="totalJabatan">0</div>
		                </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Widgets -->

 	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	            <div class="card">
	            	<div class="header">
                        <small class="right">
                        	@if($satuan->satuan_id != 1)
                        		<a href="{{ route('satuan.edit',$satuan->satuan_id) }}">
                    			<button class="btn btn-xs bg-orange waves-effect" type="button" title="edit"><i class="material-icons">edit</i></button></a>
                        	@endif
                        	@if(auth('user')->user()->is_superadmin)
	                        	@if($satuan->satuan_id != 1)
	                        	<a onclick="confirmDelete('{{route('satuan.destroy',$satuan->satuan_id)}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
	                        	@endif
                        	@endif
                        </small>
                        <h2>Informasi Satuan </h2>
                        		
                    </div>
	            	<div class="body">
	            		<div class="row">
	            			<div class="col-md-12">
		            			<div class="information">
		            				<div class="information-label">Nama Satuan : </div>
		            				<div class="information-data">{{ $satuan->nama_satuan }}</div>
		            			</div>
		            			<div class="information">
		            				<div class="information-label">Tipe Satuan : </div>
		            				<div class="information-data">{{ $satuan->tipe }}</div>
		            			</div>
		            			<div class="information">
		            				<div class="information-label">Jenis Satuan : </div>
		            				<div class="information-data">{{ $satuan->jenisSatuan->nama_jenis_satuan }}</div>
		            			</div>
		            			<div class="information">
		            				<div class="information-label">Kategori : </div>
		            				<div class="information-data">{{ $satuan->jenisSatuan->kategori }}</div>
		            			</div>
		            			<div class="information">
		            				<div class="information-label">Status : </div>
		            				<div class="information-data">{!! $satuan->printStatus() !!}</div>
		            			</div>
		            			<div class="information">
                            		@if(Auth::user()->is_superadmin==1)
	                            		@if(!$satuan->is_verified)
	                            		<a href="{{route('satuan.verification',$satuan->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Verifikasi</a>
	                            		@else
	                            		<a href="{{route('satuan.verification',$satuan->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Tidak terverifikasi</a>
	                            		@endif
	                            	@endif
		            			</div>

	            			</div>
	            		</div>
	            	</div>
	            </div>
	        </div>
	        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
	            <div class="card">
	            	<div class="header">
                        <h2>Pilihan</h2>
                    </div>
	            	<div class="body">
	            		<div class="row">
	            			<div class="col-md-12">
	            				<a href="{{ route('satuan.create-satuan',$satuan->satuan_id)}}" class="btn btn-success btn-block">Tambah Satuan</a>
	            				<a href="{{ route('satuan.create-jabatan',$satuan->satuan_id)}}" class="btn btn-info btn-block">Tambah Jabatan</a>
	            			</div>
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows --> 

	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	            	<div class="header">
                        <h2>Satuan Kerja</h2>
                    </div>
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th></th>
	                                <th>ID</th>
	                                <th width="250px">Satuan</th>
	                                <th width="200px">Jumlah Satuan</th>
	                                <th width="200px">Jenis Satuan</th>
	                                <th width="100px">Status</th>
	                                <th width="150px">PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody class="sortable" data-entityname="satuan">
	                            @foreach($dataSatuanKerja as $satKerList)
	                            <tr style="font-size:12px;" data-itemId="{{{ $satKerList->satuan_id }}}">
        							<td class="sortable-handle"><span class="glyphicon glyphicon-sort"></span></td>
	                            	<td>{{ $satKerList->satuan_id }}</td>
	                            	<td>{{ $satKerList->nama_satuan }}</td>
	                            	<td>{{ $satKerList->childs()->count() }}</td>
	                            	<td>{{ $satKerList->jenisSatuan->nama_jenis_satuan }}</td>
	                            	<td>
	                            		{!! $satKerList->printStatus() !!}
	                            		<br>
	                            		@if(Auth::user()->is_superadmin==1)
		                            		@if($satKerList->verified_by==null)
		                            		<a href="{{route('satuan.verification',$satKerList->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Verifikasi</a>
		                            		@else
		                            		<a href="{{route('satuan.verification',$satKerList->satuan_id)}}" class="btn btn-danger" style="margin-top: 10px;">Tidak terverifikasi</a>
		                            		@endif
	                            		@endif
	                            	</td>
	                            	<td>
	                            		<a href="{{route('satuan.show',$satKerList->satuan_id)}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
	                            		<a href="{{ route('satuan.edit',$satKerList->satuan_id) }}"><button class="btn btn-xs bg-orange waves-effect" type="button" title="edit"><i class="material-icons">edit</i></button></a>
	                            		@if(Auth::user()->is_superadmin==1)
	                            		<a onclick="confirmDelete('{{route('satuan.destroy',$satKerList->satuan_id)}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
	                            		@endif
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	            	<div class="header">
                        <h2>Satuan Wilayah</h2>
                    </div>
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th></th>
	                                <th>ID</th>
	                                <th width="250px">Satuan</th>
	                                <th width="200px">Jumlah Satuan</th>
	                                <th width="200px">Tipe Satuan</th>
	                                <th width="200px">Jenis Satuan</th>
	                                <th width="100px">Status</th>
	                                <th width="150px">PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody class="sortable2" data-entityname="satuan">
	                            @foreach($dataSatuanWilayah as $satWilList)
	                            <tr style="font-size:12px;" data-itemId="{{{ $satWilList->satuan_id }}}">
        							<td class="sortable-handle"><span class="glyphicon glyphicon-sort"></span></td>
	                            	<td>{{ $satWilList->satuan_id }}</td>
	                            	<td>{{ $satWilList->nama_satuan }}</td>
	                            	<td>{{ $satWilList->childs()->count() }}</td>
	                            	<td>{{ $satWilList->tipe }}</td>
	                            	<td>{{ $satWilList->jenisSatuan->nama_jenis_satuan }}</td>
	                            	<td>{!! $satWilList->printStatus() !!}<br>
	                            		@if(Auth::user()->is_superadmin==1)
		                            		@if($satWilList->verified_by==null)
		                            		<a href="{{route('satuan.verification',$satWilList->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Verifikasi</a>
		                            		@else
		                            		<a href="{{route('satuan.verification',$satWilList->satuan_id)}}" class="btn btn-danger" style="margin-top: 10px;">Tidak terverifikasi</a>
		                            		@endif
		                            	@endif
	                            	</td>
	                            	<td>
	                            		<a href="{{route('satuan.show',$satWilList->satuan_id)}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
	                            		<a href="{{ route('satuan.edit',$satWilList->satuan_id) }}"><button class="btn btn-xs bg-orange waves-effect" type="button" title="edit"><i class="material-icons">edit</i></button></a>
	                            		@if(Auth::user()->is_superadmin==1)
	                            		<a onclick="confirmDelete('{{route('satuan.destroy',$satWilList->satuan_id)}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
		                            	@endif
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->


	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th width="50px">ID</th>
	                                <th>Jabatan</th>
	                                <th width="100px">DSP</th>
	                                <th width="250px">NIVELERING</th>
	                                <th width="250px">PANGKAT</th>
	                                <th width="250px">Atasan</th>
	                                <th width="150px">Jumlah Bawahan</th>
	                                <th width="150px">Verifikasi</th>
	                                <th width="100px">PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataSatuanJabatan as $satuanJabatan)
	                            <tr style="font-size:12px;">
	                            	<td>{{ $satuanJabatan->satuan_jabatan_id }}</td>
	                            	<td>{{ $satuanJabatan->jabatan->nama_jabatan }}</td>
	                            	<td>{{ $satuanJabatan->dsp }}</td>
	                            	<td>{{ $satuanJabatan->nivelering_id ? $satuanJabatan->nivelering->nama_nivelering : '' }}</td>
	                            	<td>{{ $satuanJabatan->listPangkat() }}</td>
	                            	@if($satuanJabatan->parent()->first() == null)
	                            	<td></td>
	                            	@else
	                            	<td>{{ $satuanJabatan->parent()->first()->fullJabatan() }}</td>
	                            	@endif

	                            	<td>
	                            		@if($satuanJabatan->error_message=='')
	                            		{!! $satuanJabatan->printStatus() !!}<br>

	                            		@if(Auth::user()->is_superadmin==1)
		                            		@if($satuanJabatan->verified_by==null)
		                            		<a href="{{route('satuan-jabatan.verification',[$satuan->satuan_id,$satuanJabatan->satuan_jabatan_id])}}" class="btn btn-success" style="margin-top: 10px;" data-method="POST" data-confirm="Apakah anda yakin data ini sudah benar?">Verifikasi</a>
		                            		<a type="button" class="btn btn-danger btn-jabatan-error" data-link="{{route('satuan-jabatan.data-error',[$satuan->satuan_id,$satuanJabatan->satuan_jabatan_id])}}" style="margin-top: 10px;">Data Salah</a>
		                            		@else
		                            		<a href="{{route('satuan-jabatan.verification',[$satuan->satuan_id,$satuanJabatan->satuan_jabatan_id])}}" class="btn btn-danger" style="margin-top: 10px;" data-method="POST" data-confirm="Apakah anda yakin data ini tidak benar?">Tidak Verifikasi</a>
		                            		@endif
		                            	@endif
	                            		@else
	                            		<label class="label bg-red">Error</label><br>
	                            		<span>Pesan Error :</span><br>
	                            		{{$satuanJabatan->error_mesasge}}	
	                            		@endif
	                            	</td>
	                            	<td>{{ $satuanJabatan->childs()->active()->count() }}</td>
	                            	<td>
	                            		<a href="{{route('satuan-jabatan.edit',[$satuan->satuan_id,$satuanJabatan->satuan_jabatan_id])}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
	                            		@if(Auth::user()->is_superadmin==1)
		                            	<a onclick="confirmDelete('{{ route('satuan-jabatan.destroy',[$satuan->satuan_id,$satuanJabatan->satuan_jabatan_id])}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
		                            	@endif
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
                        {!! $dataSatuanJabatan->render() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->

	</div>
@endsection
@section('script')
	<script>
/**
     *
     * @param type string 'insertAfter' or 'insertBefore'
     * @param entityName
     * @param id
     * @param positionId
     */
    var changePosition = function(requestData){
    	console.log(requestData);
        $.ajax({
            'url': '{{route('sort')}}',
            'type': 'POST',
            'data': requestData,
            'success': function(data) {
                if (data.success) {
                    console.log('Saved!');
                } else {
                    console.error(data.errors);
                }
            },
            'error': function(){
                console.error('Something wrong!');
            }
        });
    };

        $(document).ready(function(){
	        var $sortableTable = $('.sortable');
	        if ($sortableTable.length > 0) {
	            $sortableTable.sortable({
	                handle: '.sortable-handle',
	                axis: 'y',
	                update: function(a, b){

	                    var entityName = $(this).data('entityname');
	                    var $sorted = b.item;

	                    var $previous = $sorted.prev();
	                    var $next = $sorted.next();

	                    if ($previous.length > 0) {
	                        changePosition({
	                            parentId: $sorted.data('parentid'),
	                            type: 'moveAfter',
	                            entityName: entityName,
	                            id: $sorted.data('itemid'),
	                            positionEntityId: $previous.data('itemid')
	                        });
	                    } else if ($next.length > 0) {
	                        changePosition({
	                            parentId: $sorted.data('parentid'),
	                            type: 'moveBefore',
	                            entityName: entityName,
	                            id: $sorted.data('itemid'),
	                            positionEntityId: $next.data('itemid')
	                        });
	                    } else {
	                        console.error('Something wrong!');
	                    }
	                },
	                cursor: "move"
	            });
	        }
	        var $sortableTable2 = $('.sortable2');
	        if ($sortableTable2.length > 0) {
	            $sortableTable2.sortable({
	                handle: '.sortable-handle',
	                axis: 'y',
	                update: function(a, b){

	                    var entityName = $(this).data('entityname');
	                    var $sorted = b.item;

	                    var $previous = $sorted.prev();
	                    var $next = $sorted.next();

	                    if ($previous.length > 0) {
	                        changePosition({
	                            parentId: $sorted.data('parentid'),
	                            type: 'moveAfter',
	                            entityName: entityName,
	                            id: $sorted.data('itemid'),
	                            positionEntityId: $previous.data('itemid')
	                        });
	                    } else if ($next.length > 0) {
	                        changePosition({
	                            parentId: $sorted.data('parentid'),
	                            type: 'moveBefore',
	                            entityName: entityName,
	                            id: $sorted.data('itemid'),
	                            positionEntityId: $next.data('itemid')
	                        });
	                    } else {
	                        console.error('Something wrong!');
	                    }
	                },
	                cursor: "move"
	            });
	        }

            $('.btn-jabatan-error').click(function(){
                var link = $(this).data('link');
                console.log(link);
                swal({
                  title: "Pesan Error",
                  input: "text",
                  showCancelButton: true,
                  animation: "slide-from-top",
			      showLoaderOnConfirm: true,
                  inputPlaceholder: "Masukkan pesan error",
                  preConfirm: (error_message) => {
                  	console.log(error_message);
                        $.ajax({
                            type    : "POST",   
                            url     : link,
                            data    : {_token: "{{csrf_token()}}","error_message": error_message},
                            success: function(response){
                                swal({title:'Pesan Error',text:'Mengirim Pesan Error berhasil',confirm: {text: "OK",value: true,visible: true,className: "",closeModal: true}});
                                location.reload();
                            }
                        });
                  }
                });
            });
        });
	</script>
@endsection