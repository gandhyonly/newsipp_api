@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>Satuan View</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Widgets -->
	    <div class="row clearfix">
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-blue hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">people</i>
	                </div>
	                <div class="content">
	                    <div class="text">SATUAN</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahSatuan}}" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-green hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">check</i>
	                </div>
	                <div class="content">
	                    <div class="text">SATWIL</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahSatuanWilayah}}" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-orange hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">error</i>
	                </div>
	                <div class="content">
	                    <div class="text">SATKER</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahSatuanKerja}}" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Widgets -->

	    <!-- Filter -->
	    <div class="row clearfix" id="filter">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>FILTER</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{route('satuan.filter')}}">
	                    	{{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_satuan">Nama Satuan</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="nama_satuan" name="nama_satuan" class="form-control" placeholder="Masukkan Nama Satuan" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="jenis_satuan_id">Jenis Satuan</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="jenis_satuan_id" id="jenis_satuan_id">
		                                        <option value="all">SEMUA JENIS SATUAN</option>
		                                        @foreach($dataJenisSatuan as $jenisSatuan)
		                                        <option value="{{$jenisSatuan->jenis_satuan_id}}">{{$jenisSatuan->nama_jenis_satuan}}</option>
		                                        @endforeach
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label><i class="material-icons">search</i> CARI</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->

	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	            	<div class="header">
                        <h2>Satuan Kerja</h2>
                    </div>
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th width="250px">Satuan</th>
	                                <th width="200px">Jumlah Satuan</th>
	                                <th width="200px">Tipe Satuan</th>
	                                <th width="200px">Jenis Satuan</th>
	                                <th width="200px">Dari Satuan</th>
	                                <th>Status</th>
	                                <th>PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataSatuanKerja as $satKerList)
	                            <tr style="font-size:12px;">
	                            	<td>{{ $satKerList->satuan_id }}</td>
	                            	<td>{{ $satKerList->nama_satuan }}</td>
	                            	<td>{{ $satKerList->childs()->count() }}</td>
	                            	<td>{{ $satKerList->tipe }}</td>
	                            	<td>{{ $satKerList->jenisSatuan->nama_jenis_satuan }}</td>
	                            	<td>{{ $satKerList->printPath($satKerList->satuan_id) }}</td>
	                            	<td>
	                            		{!! $satKerList->printStatus() !!}
	                            		<br>
	                            		@if(!$satKerList->is_verified)
	                            		<a href="{{route('satuan.verification',$satKerList->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Verifikasi</a>
	                            		@else
	                            		<a href="{{route('satuan.verification',$satKerList->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Tidak terverifikasi</a>
	                            		@endif
	                            	</td>
	                            	<td>
	                            		<a href="{{route('satuan.show',$satKerList->satuan_id)}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
	                            		<a onclick="confirmDelete('{{route('satuan.destroy',$satKerList->satuan_id)}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	            	<div class="header">
                        <h2>Satuan Wilayah</h2>
                    </div>
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th width="250px">Satuan</th>
	                                <th width="200px">Jumlah Satuan</th>
	                                <th width="200px">Tipe Satuan</th>
	                                <th width="200px">Jenis Satuan</th>
	                                <th width="200px">Dari Satuan</th>
	                                <th>Status</th>
	                                <th>PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataSatuanWilayah as $satWilList)
	                            <tr style="font-size:12px;">
	                            	<td>{{ $satWilList->satuan_id }}</td>
	                            	<td>{{ $satWilList->nama_satuan }}</td>
	                            	<td>{{ $satWilList->childs()->count() }}</td>
	                            	<td>{{ $satWilList->tipe }}</td>
	                            	<td>{{ $satWilList->jenisSatuan->nama_jenis_satuan }}</td>
	                            	<td>{{ $satWilList->printPath($satWilList->satuan_id) }}</td>
	                            	<td>{!! $satWilList->printStatus() !!}<br>
	                            		@if(!$satWilList->is_verified)
	                            		<a href="{{route('satuan.verification',$satWilList->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Verifikasi</a>
	                            		@else
	                            		<a href="{{route('satuan.verification',$satWilList->satuan_id)}}" class="btn btn-success" style="margin-top: 10px;">Tidak terverifikasi</a>
	                            		@endif
	                            	</td>
	                            	<td>
	                            		<a href="{{route('satuan.show',$satWilList->satuan_id)}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
	                            		<a onclick="confirmDelete('{{route('satuan.destroy',$satWilList->satuan_id)}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
                        {!! $dataSatuanWilayah->render() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
	</div>
@endsection



@section('script')
	<script>
	$(function () {
	    //Widgets count
	    $('.count-to').countTo();
	});
	</script>
@endsection