@extends('layouts.default')

@section('content')
	<div class="container-fluid">
		<div class="block-header">
	        <h2>
		    	@if($satuan->parent()->count() > 0)
		        <a href="{{route('satuan.show',$satuan->satuan_id)}}" class="btn btn-primary btn-xs">
	                    <i class="material-icons">keyboard_arrow_left</i>
		        </a>
		        @endif
		        DASHBOARD
		    </h2>
		</div>

		@if(Session::has('success'))
			<div class="alert alert-success">{{Session::get('success')}}</div>
		@endif

		@if(Session::has('error'))
			<div class="alert alert-error">{{Session::get('error')}}</div>
		@endif

		<!-- Filter -->
		<div class="row clearfix" id="filter">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>RELASI SATUAN JABATAN BARU</h2>
					</div>
					<div class="body">
						<form id="form_validation" class="form-horizontal" method="POST" action="{{ route('satuan.store-jabatan',$satuan->satuan_id) }}">
							{{ csrf_field() }}
							<div class="row clearfix">
								<div class="col-md-12">
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="satuan_id">SATUAN</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<div class="form-group" style="margin:0px;">
												<div style="padding-left:12px;">
													<input id="nama_satuan" name="nama_satuan" class="form-control" readonly="readonly" type="text" value="{{$satuan->fullPrint()}}">
													<input type="hidden" name="satuan_id" value="{{$satuan->satuan_id}}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="jabatan_id">JABATAN</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" data-live-search="true" name="jabatan_id" id="jabatan_id">
												<option>PILIH JABATAN</option>
												@foreach($selectDataJabatan as $selectJabatan)
												<option value="{{ $selectJabatan->jabatan_id }}">{{ $selectJabatan->nama_jabatan }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="parent_satuan_jabatan_id">SATUAN JABATAN ATASAN</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" name="parent_satuan_jabatan_id" data-live-search="true" id="parent_satuan_jabatan_id">
												<option value="null">PILIH SATUAN JABATAN ATASAN</option>
												@foreach($selectDataAtasanJabatan as $selectAtasanJabatan)
												<option value="{{ $selectAtasanJabatan->satuan_jabatan_id }}">{{ $selectAtasanJabatan->fullJabatan() }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="parent_satuan_jabatan_id">STATUS JABATAN</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" name="status_jabatan_id" id="parent_satuan_jabatan_id">
												@foreach($selectDataStatusJabatan as $selectStatusJabatan)
												<option value="{{ $selectStatusJabatan->status_jabatan_id }}">{{ $selectStatusJabatan->status }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="pangkat_id">PANGKAT POLISI</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" data-live-search="true" name="pangkat[]" id="pangkat" multiple>
												<option value="null" selected="">TIDAK MEMILIKI PANGKAT POLISI</option>
												<option value="BA">BA</option>
												<option value="IP">IP</option>
												<option value="AKP">AKP</option>
												<option value="KP">KP</option>
												<option value="AKBP">AKBP</option>
												<option value="KBP">KBP</option>
												<option value="BRIGJEN">BRIGJEN</option>
												<option value="IRJEN">IRJEN</option>
												<option value="KOMJEN">KOMJEN</option>
												<option value="JENDERAL">JENDERAL</option>
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="pangkat_id">PANGKAT PNS</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" data-live-search="true" name="pangkat_id[]" id="pangkat_id" multiple>
												<option data-hidden="true">TIDAK MEMILIKI PANGKAT PNS</option>
												@foreach($selectDataPangkat as $selectPangkat)
													@if($selectPangkat->jenis=='PNS')
													<option value="{{ $selectPangkat->pangkat_id }}">{{ $selectPangkat->jenis }} {{ $selectPangkat->getNamaGolongan() }} ({{$selectPangkat->nama_pangkat}})</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="nivelering_id">NIVELERING</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" data-live-search="true" name="nivelering_id" id="nivelering_id">
												<option value="null">PILIH NIVELERING</option>
												@foreach($selectDataNivelering as $selectNivelering)
													<option value="{{ $selectNivelering->nivelering_id }}">{{ $selectNivelering->singkatan }} {{ $selectNivelering->nama_nivelering }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="code">DSP</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<div class="form-group" style="margin:0px;">
												<div class="form-line" style="padding-left:12px;">
													<input id="error_message" name="dsp" class="form-control" placeholder="Masukkan jumlah DSP" type="text">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-md-8 col-md-offset-4">
											<button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label>BUAT</label></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- End Filter -->
@endsection

@section('script')
	<script>
		$("#satuan_id").val("{{$satuan->satuan_id}}");
	</script>
@endsection