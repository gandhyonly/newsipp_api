@extends('layouts.default')

@section('content')
	<div class="container-fluid">
		<div class="block-header">
			<h2>DASHBOARD</h2>
		</div>

		@if(Session::has('success'))
			<div class="alert alert-success">{{Session::get('success')}}</div>
		@endif

		@if(Session::has('error'))
			<div class="alert alert-error">{{Session::get('error')}}</div>
		@endif

		<!-- Filter -->
		<div class="row clearfix" id="filter">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>RELASI SATUAN JABATAN BARU</h2>
					</div>
					<div class="body">
						<form id="form_validation" class="form-horizontal" method="POST" action="{{ route('satuan-jabatan.update',[$satuan->satuan_id,$satuanJabatan->satuan_jabatan_id]) }}">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="PATCH">
							<div class="row clearfix">
								<div class="col-md-12">
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="satuan_id">SATUAN</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<div class="form-group" style="margin:0px;">
												<div style="padding-left:12px;">
													<input id="nama_satuan" name="nama_satuan" class="form-control" readonly="readonly" type="text" value="{{$satuan->fullPrint()}}">
													<input type="hidden" name="satuan_id" value="{{$satuan->satuan_id}}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="jabatan_id">JABATAN</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" data-live-search="true" name="jabatan_id" id="jabatan_id">
												<option>PILIH JABATAN</option>
												@foreach($selectDataJabatan as $selectJabatan)
												<option value="{{ $selectJabatan->jabatan_id }}">{{ $selectJabatan->nama_jabatan }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="parent_satuan_jabatan_id">SATUAN JABATAN ATASAN</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" name="parent_satuan_jabatan_id" id="parent_satuan_jabatan_id">
												<option value="null">PILIH SATUAN JABATAN ATASAN</option>
												@foreach($selectDataAtasanJabatan as $selectAtasanJabatan)
												<option value="{{ $selectAtasanJabatan->satuan_jabatan_id }}">{{ $selectAtasanJabatan->fullJabatan() }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="status_jabatan_id">STATUS JABATAN</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" name="status_jabatan_id" id="status_jabatan_id">
												@foreach($selectDataStatusJabatan as $selectStatusJabatan)
												<option value="{{ $selectStatusJabatan->status_jabatan_id }}">{{ $selectStatusJabatan->status }}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="pangkat_id">PANGKAT POLISI</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" data-live-search="true" name="pangkat" id="pangkat">
												<option value="null">PILIH PANGKAT POLISI</option>
												<option value="BA">BA</option>
												<option value="IP">IP</option>
												<option value="AKP">AKP</option>
												<option value="KP">KP</option>
												<option value="AKBP">AKBP</option>
												<option value="KBP">KBP</option>
												<option value="BRIGJEN">BRIGJEN</option>
												<option value="IRJEN">IRJEN</option>
												<option value="KOMJEN">KOMJEN</option>
												<option value="JENDERAL">JENDERAL</option>
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="pangkat_id">PANGKAT PNS</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" data-live-search="true" name="pangkat_id[]" id="pangkat_id" multiple>
												@foreach($selectDataPangkat as $selectPangkat)
													@if($selectPangkat->jenis=='PNS')
													<option value="{{ $selectPangkat->pangkat_id }}">{{ $selectPangkat->jenis }} {{ $selectPangkat->getNamaGolongan() }} ({{$selectPangkat->nama_pangkat}})</option>
													@endif
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="nivelering_id">NIVELERING</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<select class="form-control show-tick" name="nivelering_id" id="nivelering_id">
												<option value="null">TIDAK MEMILIKI NIVELERING</option>
												@foreach($selectDataNivelering as $selectNivelering)
													<option value="{{ $selectNivelering->nivelering_id }}">{{ $selectNivelering->singkatan }} ({{ $selectNivelering->nama_nivelering }})</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
											<label for="code">DSP</label>
										</div>
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
											<div class="form-group" style="margin:0px;">
												<div class="form-line" style="padding-left:12px;">
													<input id="error_message" name="dsp" class="form-control" placeholder="Masukkan jumlah DSP" type="text" value="{{$satuanJabatan->dsp}}">
												</div>
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-md-8 col-md-offset-4">
											<button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label>BUAT</label></button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- End Filter -->


	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th width="50px">ID</th>
	                                <th width="250px">Jabatan</th>
	                                <th width="250px">DSP</th>
	                                <th width="250px">NIVELERING</th>
	                                <th width="250px">PANGKAT</th>
	                                <th width="150px">Jumlah Bawahan</th>
	                                <th width="100px">PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($satuanJabatan->childs()->active()->get() as $anakJabatan)
	                            <tr style="font-size:12px;">
	                            	<td>{{ $anakJabatan->satuan_jabatan_id }}</td>
	                            	<td>{{ $anakJabatan->jabatan->nama_jabatan }}</td>
	                            	<td>{{ $anakJabatan->dsp }}</td>
	                            	<td>{{ $anakJabatan->nivelering_id ? $anakJabatan->nivelering->nama_nivelering : '' }}</td>
	                            	<td>{{ $anakJabatan->listPangkat() }}</td>
	                            	<td>{{ $anakJabatan->childs()->count() }}</td>
	                            	<td>
	                            		<a href="{{route('satuan-jabatan.edit',[$satuan->satuan_id,$anakJabatan->satuan_jabatan_id])}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
	                            		@if(Auth::user()->is_superadmin==1)
		                            	<a onclick="confirmDelete('{{ route('satuan-jabatan.destroy',[$satuan->satuan_id,$anakJabatan->satuan_jabatan_id])}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
		                            	@endif
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
@endsection

@section('script')
	<script>
		$("#satuan_id").val("{{$satuanJabatan->satuan_id}}");
		$("#jabatan_id").val("{{$satuanJabatan->jabatan_id}}");
		$("#parent_satuan_jabatan_id").val("{{$satuanJabatan->parent_satuan_jabatan_id}}");
		$("#pangkat").val("{{$pangkatID}}");
		$("#pangkat_id").val([{!!$satuanJabatan->listPangkatPNSID()!!}]);
		$("#nivelering_id").val("{{$satuanJabatan->nivelering_id}}");
		$("#status_jabatan_id").val("{{$satuanJabatan->status_jabatan_id}}");
	</script>
@endsection