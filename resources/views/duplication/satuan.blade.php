@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>DUPLIKASI SATUAN</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif


	    <!-- Filter -->
	    <div class="row clearfix" id="filter">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>FORM DUPLIKASI</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{route('satuan.duplication.post')}}">
	                    	{{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_satuan">ID Sumber Duplikasi</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="sumber_duplikasi_id" name="sumber_duplikasi_id" class="form-control" placeholder="Masukkan ID Sumber Duplikasi" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_satuan">ID Tujuan Duplikasi</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="tujuan_duplikasi_id" name="tujuan_duplikasi_id" class="form-control" placeholder="Masukkan ID Tujuan Duplikasi" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_satuan">ID Kepala Jabatan</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="jabatan_id" name="jabatan_id" class="form-control" placeholder="Masukkan ID Kepala Jabatan" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label><i class="material-icons">search</i> CARI</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->
	</div>
@endsection