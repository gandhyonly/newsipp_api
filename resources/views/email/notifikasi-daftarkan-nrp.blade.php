<p>Kepada Yth : {{$helpdesk->nama_lengkap}} Nrp {{$helpdesk->nrp}}</p>

<p>Terima kasih atas kerjasamanya. 
</p>
<p>Data anda sudah kami proses, namun terjadi duplikasi NRP dengan personel Polri lainnya, sehingga anda mendapatkan NRP baru yaitu : {{$helpdesk->nrp_baru}}</p>
<p>Silahkan mendaftar menggunakan NRP baru tersebut kemudian isi form riwayat hidup</p>

<p>
Hormat kami, <br>
Helpdesk SSDM Polri<br>
Bag Infopers Ro Binkar SSDM Polri
</p>