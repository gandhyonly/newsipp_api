@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>DASHBOARD</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Filter -->
	    <div class="row clearfix" id="filter">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>EDIT JENIS SATUAN BARU</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{ route('jenis-satuan.update',$jenisSatuan->jenis_satuan_id)}}">
	                    	<input type="hidden" name="_method" value="PATCH">
	                        {{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_jenis_satuan">NAMA JENIS SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="nama_jenis_satuan" name="nama_jenis_satuan" class="form-control" placeholder="Nama Satuan" type="text" value="{{$jenisSatuan->nama_jenis_satuan}}">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="kategori">KATEGORI SATUAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="kategori" id="kategori">
		                                        <option value="SATUAN KERJA">SATUAN KERJA</option>
		                                        <option value="SATUAN WILAYAH">SATUAN WILAYAH</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label>SAVE</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->


	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	            	<div class="header">
	            	<h2>Daftar Satuan</h2>
	            	</div>
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
	                                <th width="350px">Nama Satuan</th>
	                                <th width="150px">Jumlah Satuan</th>
	                                <th width="150px">FullPrint</th>
	                                <th width="200px">Tipe Satuan</th>
	                                <th width="100px">Status</th>
	                                <th width="100px">PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataSatuan as $satuan)
	                            <tr style="font-size:12px;">
	                            	<td>{{ $satuan->satuan_id }}</td>
	                            	<td>{{ $satuan->nama_satuan }}</td>
	                            	<td>{{ $satuan->fullPrint() }}</td>
	                            	<td>{{ $satuan->childs()->count() }}</td>
	                            	<td>{{ $satuan->tipe }}</td>
	                            	<td>{!! $satuan->printStatus() !!}
		                            </td>
	                            	<td>
		                            	<a href="{{route('satuan.show',$satuan->satuan_id)}}" class="btn btn-xs bg-blue"><i class="material-icons">pageview</i></a>
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                    {{ $dataSatuan->render() }}
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
@endsection

@section('script')
	<script>
	$("#kategori").val("{{$jenisSatuan->kategori}}");
	</script>
@endsection