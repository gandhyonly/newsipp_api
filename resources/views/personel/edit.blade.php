@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>EDIT</h2>
	    </div>
	    <!-- Error Message -->
	    <div class="row clearfix" id="error-message">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>Daftar Data Error</h2>
	                </div>
	            	<div class="body">
	    				{!! $personel->error_message !!}
	            	</div>
	            </div>
            </div>
        </div>	
	    <!-- Form Edit -->
	    <div class="row clearfix" id="form-edit">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>Personel Data Form</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{route('personel.update',$personel->personel_id)}}">
	                        {{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nrp">NRP</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="nrp" name="nrp" class="form-control" placeholder="NRP Personel" type="text" value="{{$personel->nrp}}">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_lengkap">Nama Lengkap</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="nama_lengkap" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap Personel" type="text" value="{{$personel->nama_lengkap}}">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="tanggal_lahir">Tanggal Lahir</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="input-group" style="margin-bottom: 0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input type="text" id="tanggal_lahir" class="form-control date" placeholder="Ex: 30/07/2016" value="{{$personel->tanggal_lahir->format('d/m/Y')}}">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="status_personel_id">Status Personel</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="status_personel_id" id="status_personel_id">
		                                    	@foreach($dataStatusPersonel as $statusPersonel)
		                                        	<option value="{{$statusPersonel->status_personel_id}}">{{$statusPersonel->status}}</option>
		                                    	@endforeach
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="agama_id">Agama</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="agama_id" id="agama_id">
	                                        	<option value="">PILIH</option>
		                                    	@foreach($dataAgama as $agama)
		                                        	<option value="{{$agama->agama_id}}">{{$agama->nama}}</option>
		                                    	@endforeach
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="jenis_kelamin_id">Jenis Kelamin</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="jenis_kelamin_id" id="jenis_kelamin_id">
	                                        	<option value="">PILIH</option>
		                                    	@foreach($dataJenisKelamin as $jenisKelamin)
		                                        	<option value="{{$jenisKelamin->jenis_kelamin_id}}">{{$jenisKelamin->jenis}}</option>
		                                    	@endforeach
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="is_error">DATA ERROR</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="is_error" id="is_error">
	                                        	<option value="">PILIH</option>
	                                        	<option value="1">YA</option>
	                                        	<option value="0">TIDAK</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="is_duplicate">DATA DUPLIKAT</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="is_duplicate" id="is_duplicate">
	                                        	<option value="tes">PILIH</option>
	                                        	<option value="1">YA</option>
	                                        	<option value="0">TIDAK</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label><i class="material-icons">search</i> APPLY</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->
	</div>
@endsection

@section('script')
<script>
	$(function () {
	    $('.date').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' });
	    $("#jenis_kelamin_id").val("{{$personel->jenis_kelamin_id}}");
	    $("#agama_id").val("{{$personel->agama_id}}");
	    $("#is_duplicate").val("{{$personel->is_duplicate?"1":"0"}}");
	    $("#is_error").val("{{$personel->is_error?"1":"0"}}");
	});
</script>
@endsection