@extends('layouts.default')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Form Input DRH
                <small>Form Isian Untuk Melengkapi Daftar Riwayat Hidup</small>
            </h2>
        </div>
        <!-- Basic Example | Horizontal Layout -->

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>FORM DATA PRIBADI</h2>
                    </div>
                    <div class="body">
                        <div id="wizard_vertical">
                            <h2>Isi Biodata</h2>
                            <section>
                                <form>
                                    <div class="row clearfix">
                                        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
                                            <label class="form-label">NRP</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <button type="button" class="btn btn-primary btn-lg m-l-15 waves-effect">Cek
                                                NRP
                                            </button>
                                        </div>
                                    </div>

                                    <label class="form-label">Nama</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Tempat / Tanggal Lahir</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Jenis Kelamin</label>
                                    <div class="form-group">
                                        <select class="form-control show-tick">
                                            <option value=""> Jenis Kelamin</option>
                                            <option value="Pria">Pria</option>
                                            <option value="Wanita">Wanita</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Anak Ke</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Golongan Darah</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Agama</label>
                                    <div class="form-group">
                                        <select class="form-control show-tick">
                                            <option value="">-- Agama --</option>
                                            <option value="islam">Islam</option>
                                            <option value="kristen">Kristen</option>
                                            <option value="budha">Budha</option>
                                            <option value="hindu">Hindu</option>
                                            <option value="katholik">Katholik</option>
                                            <option value="kong hu cu">Kong Hu Cu</option>
                                        </select>
                                    </div>

                                    <label class="form-label">Status Marital</label>
                                    <div class="form-group">
                                        <select class="form-control show-tick">
                                            <option value="">-- Status Marital --</option>
                                            <option value="menikah">Menikah</option>
                                            <option value="lajang">Lajang</option>
                                            <option value="cerai">Cerai</option>
                                        </select>
                                    </div>


                                </form>
                            </section>

                            <h2>Kondisi Fisik</h2>
                            <section>
                                <form>
                                    <label class="form-label">Suku Bangsa</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select class="form-control show-tick" data-live-search="true">
                                                <option> Suku Bangsa</option>
                                                <option>Jawa</option>
                                                <option>Sumatra</option>
                                                <option>Kalimanta</option>
                                                <option>Sulawesi</option>
                                                <option>Maluku</option>
                                            </select>
                                        </div>
                                    </div>

                                    <label class="form-label">Warna Kulit</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select class="form-control show-tick" data-live-search="true">
                                                <option> Warna Kulit</option>
                                                <option>Putih</option>
                                                <option>Kuning</option>
                                                <option>Sawo</option>
                                                <option>Sawo Matang</option>
                                                <option>Hitam</option>
                                            </select>
                                        </div>
                                    </div>

                                    <label class="form-label">Jenis Rambut</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select class="form-control show-tick" data-live-search="true">
                                                <option> Jenis Rambut</option>
                                                <option>Ikal</option>
                                                <option>Lurus</option>
                                                <option>Berombak</option>
                                                <option>Gundul</option>
                                            </select>
                                        </div>
                                    </div>

                                    <label class="form-label">Warna Rambut</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <select class="form-control show-tick" data-live-search="true">
                                                <option> Warna Rambut</option>
                                                <option>Hitam</option>
                                                <option>Putih</option>
                                                <option>Kuning</option>
                                                <option>Biru</option>
                                            </select>
                                        </div>
                                    </div>

                                    <label class="form-label">Tinggi Badan</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Berat Badan</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Ukuran Topi</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Ukuran Sepatu</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Ukuran Celana</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Ukuran Baju</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>


                                </form>
                            </section>

                            <h2>Data Keluarga</h2>
                            <section>
                                <form>

                                    <label class="form-label">Nama Suami/Istri</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <h5>Data Anak</h5>

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="form-label">Nama Anak</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="username" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="form-label">Tempat / Tanggal Lahir</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="username" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="form-label">Jenis Kelamin</label>
                                            <select class="form-control show-tick">
                                                <option value=""> Jenis Kelamin</option>
                                                <option value="Pria">Pria</option>
                                                <option value="Wanita">Wanita</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2">
                                            <label class="form-label">Pendidikan</label>
                                            <select class="form-control show-tick">
                                                <option value=""> Pendidikan</option>
                                                <option value="Pria">Pelajar</option>
                                                <option value="Wanita">Mahasiswa</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="btn btn-primary">Tambah</div>
                                        </div>
                                    </div>

                                    <h5>Data Orang Tua / Saudara</h5>

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="form-label">Nama</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="username" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="form-label">Tempat / Tanggal Lahir</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="username" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="form-label">Jenis Kelamin</label>
                                            <select class="form-control show-tick">
                                                <option value=""> Jenis Kelamin</option>
                                                <option value="Pria">Pria</option>
                                                <option value="Wanita">Wanita</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2">
                                            <label class="form-label">Pekerjaan</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="username" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="btn btn-primary">Tambah</div>
                                        </div>
                                    </div>


                                </form>
                            </section>

                            <h2>Nomor Identitas</h2>
                            <section>
                                <form>
                                    <label class="form-label">Sidik Jari</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>
                                    <label class="form-label">Kartu ASABRI</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Tanda Kewenangan</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">Reg Penyidik</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">KTA</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>

                                    <label class="form-label">NPWP</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>


                                </form>
                            </section>

                            <h2>Pendidikan Umum</h2>
                            <section>
                                <form>

                                    <h5>Pendidikan Umum</h5>

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <label class="form-label">Macam Pendidikan</label>
                                            <div class="form-group">
                                                <select class="form-control show-tick" data-live-search="true">
                                                    <option> Macam Pendidikan</option>
                                                    <option>SD</option>
                                                    <option>SMP</option>
                                                    <option>SMA</option>
                                                    <option>Sarjana</option>
                                                    <option>Pasca Sarjana</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="form-label">Jurusan</label>
                                            <div class="form-group">
                                                <select class="form-control show-tick" data-live-search="true">
                                                    <option> Jurusan</option>
                                                    <option>Teknik Informatika</option>
                                                    <option>Akuntansi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-2">
                                            <label class="form-label">Status Kelulusan</label>
                                            <div class="form-group">
                                                <select class="form-control show-tick" data-live-search="true">
                                                    <option> Status Kelulusan</option>
                                                    <option>Lulus</option>
                                                    <option>Tidak Lulus</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <label class="form-label">Tahun Lulus</label>
                                            <div class="form-group">
                                                <select class="form-control show-tick" data-live-search="true">
                                                    <option> Tahun Lulus</option>
                                                    <option>1980</option>
                                                    <option>1981</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <label class="form-label">Lama </label>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control date" placeholder="Lama (Tahun)">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-1">
                                            <div class="btn btn-primary">Tambah</div>
                                        </div>
                                    </div>
                                </form>
                            </section>

                            <h2>Pendidikan POLRI</h2>
                            <section>
                                <form>
                                    <label class="form-label">Pendidikan Polri</label>
                                    <div class="form-group">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option> Dik POLRI Terakhir</option>
                                            <option>SESPIMTI</option>
                                            <option>LEMDIKPOL</option>
                                            <option>SESPIMPOL</option>
                                        </select>
                                    </div>
                                    <label class="form-label">Status Kelulusan</label>
                                    <div class="form-group">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option> Status Kelulusan</option>
                                            <option>Lulus</option>
                                            <option>Tidak Lulus</option>
                                        </select>
                                    </div>
                                    <label class="form-label">Angkatan</label>
                                    <div class="form-group">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option> Angkatan</option>
                                            <option>1980</option>
                                            <option>1981</option>
                                        </select>
                                    </div>
                                    <label class="form-label">Tahun Lulus</label>
                                    <div class="form-group">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option> Tahun Lulus</option>
                                            <option>1980</option>
                                            <option>1981</option>
                                        </select>
                                    </div>
                                    <label class="form-label">Lama (Bulan)</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control date" placeholder="Lama (bulan)">
                                            </div>
                                        </div>
                                    </div>

                                    <label class="form-label">Rank (bulan)</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control date" placeholder="Rank (bulan)">
                                            </div>
                                        </div>
                                    </div>
                                    <label class="form-label">Tempat Pendidikan</label>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control date"
                                                       placeholder="Tempat Pendidikan">
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </section>

                            <h2>Pendidikan Kejuruan</h2>
                            <section>
                                <label class="form-label">Pendidikan Kejuruan</label>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <select class="form-control show-tick">
                                            <option value=""> Bang Spes</option>
                                            <option value="Pria">Palan Labkrim</option>
                                            <option value="Wanita">Padas BRIMOB</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username" required>
                                                <label class="form-label">Tahun</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="btn btn-primary">Tambah</div>
                                    </div>
                                </div>

                            </section>

                            <h2>Data Tanda Jasa</h2>
                            <section>
                                <form>
                                    <label class="form-label">Tanda Jasa</label>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <select class="form-control show-tick">
                                                <option value=""> Tanda Jasa</option>
                                                <option value="Pria">GOM VII</option>
                                                <option value="Wanita">SL Kebaktian Sosial</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control" name="username" required>
                                                    <label class="form-label">T.M.T</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="btn btn-primary">Tambah</div>
                                        </div>
                                    </div>

                                </form>
                            </section>

                            <h2>Kemampuan Bahasa</h2>
                            <section>
                                <form>

                                    <h5>Bahasa Asing</h5>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <select class="form-control show-tick">
                                                <option value=""> Bahasa Asing</option>
                                                <option value="Pria">Inggris</option>
                                                <option value="Wanita">Mandarin</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4">
                                            <select class="form-control show-tick">
                                                <option value="">Aktif/Pasif</option>
                                                <option value="Pria">Aktif</option>
                                                <option value="Wanita">Pasif</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="btn btn-primary">Tambah</div>
                                        </div>
                                    </div>

                                    <h5>Bahasa Daerah</h5>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <select class="form-control show-tick">
                                                <option value=""> Bahasa Daerah</option>
                                                <option value="Pria">Jawa</option>
                                                <option value="Wanita">Batak</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4">
                                            <select class="form-control show-tick">
                                                <option value="">Aktif/Pasif</option>
                                                <option value="Pria">Aktif</option>
                                                <option value="Wanita">Pasif</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="btn btn-primary">Tambah</div>
                                        </div>
                                    </div>

                                </form>
                            </section>


                            <h2>Kemampuan Bela Diri / Olahraga</h2>
                            <section>
                                <h5>Kemampuan Bela Diri / Olahraga</h5>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <select class="form-control show-tick">
                                            <option value=""> Macam Kemampuan</option>
                                            <option value="Pria">Pencak Silat</option>
                                            <option value="Wanita">Karate</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username" required>
                                                <label class="form-label">Keterangan</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="btn btn-primary">Tambah</div>
                                    </div>
                                </div>

                            </section>

                            <h2>Kemampuan Bahasa</h2>
                            <section>
                                <form>

                                    <h5>Bahasa Asing</h5>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <select class="form-control show-tick">
                                                <option value=""> Bahasa Asing</option>
                                                <option value="Pria">Inggris</option>
                                                <option value="Wanita">Mandarin</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4">
                                            <select class="form-control show-tick">
                                                <option value="">Aktif/Pasif</option>
                                                <option value="Pria">Aktif</option>
                                                <option value="Wanita">Pasif</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="btn btn-primary">Tambah</div>
                                        </div>
                                    </div>

                                    <h5>Bahasa Daerah</h5>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <select class="form-control show-tick">
                                                <option value=""> Bahasa Daerah</option>
                                                <option value="Pria">Jawa</option>
                                                <option value="Wanita">Batak</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-4">
                                            <select class="form-control show-tick">
                                                <option value="">Aktif/Pasif</option>
                                                <option value="Pria">Aktif</option>
                                                <option value="Wanita">Pasif</option>
                                            </select>
                                        </div>

                                        <div class="col-lg-2">
                                            <div class="btn btn-primary">Tambah</div>
                                        </div>
                                    </div>

                                </form>
                            </section>


                            <h2>Riwayat Kepangkatan</h2>
                            <section>
                                <h5>Riwayat Kepangkatan</h5>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <select class="form-control show-tick">
                                            <option value=""> Pangkat</option>
                                            <option value="Pria">IPDA</option>
                                            <option value="Wanita">IPTU</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username" required>
                                                <label class="form-label">T.M.T</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="btn btn-primary">Tambah</div>
                                    </div>
                                </div>

                            </section>

                            <h2>Penugasan Luar Negeri</h2>
                            <section>
                                <h5>Penugasan Luar Negeri</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option>Negara Tempat Bertugas</option>
                                            <option>Amerika Serikat</option>
                                            <option>Thailand</option>
                                            <option>Singapura</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option>Macam Tugas</option>
                                            <option>Pelatihan Anti Terror</option>
                                            <option>Manajemen Anti Terror</option>
                                            <option>Assesment Center</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username" required>
                                                <label class="form-label">Mulai</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="btn btn-primary">Tambah</div>
                                    </div>
                                </div>

                            </section>

                            <h2>Penugasan OPS Kepolisian</h2>
                            <section>
                                <h5>Penugasan OPS Kepolisian</h5>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option>Nama Operasi</option>
                                            <option>Operasi 1</option>
                                            <option>Thailand</option>
                                            <option>Singapura</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option>Daerah Operasi</option>
                                            <option>Jakarta</option>
                                            <option>Bandung</option>
                                            <option>Banten</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username" required>
                                                <label class="form-label">Mulai</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username" required>
                                                <label class="form-label">Keterangan</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="btn btn-primary">Tambah</div>
                                    </div>
                                </div>

                            </section>



                            <h2>Kemampuan Brevet</h2>
                            <section>
                                <h5>Kemampuan Brevet</h5>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option value=""> Macam Brevet</option>
                                            <option value="Pria">BREVET BHAYANGKARA BAHARI UTAMA KEHORMATAN</option>
                                            <option value="Wanita">BREVET</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-4">
                                        <select class="form-control show-tick" data-live-search="true">
                                            <option value=""> Asal Peroleh</option>
                                            <option value="Pria">DITPOLAIR BAHARKAM POLRI</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-2">
                                        <div class="btn btn-primary">Tambah</div>
                                    </div>
                                </div>

                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Example | Vertical Layout -->

    </div>
@endsection

@section('script')
    <script src="/js/pages/forms/form-wizard.js"></script>
    <script src="/js/pages/forms/advanced-form-elements.js"></script>

    <script src="/js/pages/forms/basic-form-elements.js"></script>
@endsection