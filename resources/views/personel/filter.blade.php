@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>DASHBOARD</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Widgets -->
	    <div class="row clearfix">
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-blue hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">people</i>
	                </div>
	                <div class="content">
	                    <div class="text">JUMLAH</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahPersonel}}" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-green hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">check</i>
	                </div>
	                <div class="content">
	                    <div class="text">DATA BERSIH</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahDataBersih}}" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-orange hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">error</i>
	                </div>
	                <div class="content">
	                    <div class="text">DATA ERROR</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahDataError}}" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-red hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">forum</i>
	                </div>
	                <div class="content">
	                    <div class="text">DUPLICATE</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahDataDuplikat}}" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Widgets -->
	    <!-- Filter -->
	    <div class="row clearfix" id="filter">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>FILTER</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="GET" action="{{route('personel.filter')}}">
	                        {{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="display">TAMPILKAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="display" id="display">
		                                        <option value="all">SEMUA</option>
		                                        <option value="error_only">DATA ERROR SAJA</option>
		                                        <option value="duplicate_only">DATA DUPLIKAT SAJA</option>
		                                        <option value="both">DATA ERROR & DUPLIKAT</option>
		                                        <option value="clean">DATA BERSIH</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="sort_by">URUTKAN BERDASARKAN</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
		                                    <select class="form-control show-tick" name="sort_by" id="sort_by">
		                                        <option value="nrp">NRP</option>
		                                        <option value="nama_lengkap">Nama Lengkap</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="code">Pesan Error</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="error_message" name="error_message" class="form-control" placeholder="Masukkan Pesan Error" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="search_column">Cari</label>
	                                    </div>
	                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-7">
		                                    <select class="form-control show-tick" name="search_column" id="search_column">
		                                        <option value="nrp">NRP</option>
		                                        <option value="nama_lengkap">Nama Lengkap</option>
		                                        <option value="jabatan">Jabatan</option>
		                                    </select>
	                                    </div>
	                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="search_data" name="search_data" id="search_data" class="form-control" placeholder="Masukkan Data yang akan dicari" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label><i class="material-icons">search</i> APPLY</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->
	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th width="50px">ID</th>
	                                <th width="90px">NRP</th>
	                                <th width="150px">NAMA</th>
	                                <th width="150px">Jabatan</th>
	                                <th>STATUS</th>
	                                <th>PESAN ERROR</th>
	                                <th>PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataPersonel as $personel)
	                            <tr style="font-size:12px;">
	                            	<td>{{$personel->personel_id}}</td>
	                            	<td>{{$personel->nrp}}</td>
	                            	<td>{{$personel->nama_lengkap}}</td>
	                            	<td>{{$personel->jabatan}}</td>
	                            	<td>{!! $personel->statusList() !!}</td>
	                            	<td>{!! $personel->error_message !!}</td>
	                            	<td>{!! $personel->verificationButton() !!}<br> <a href="{{route('personel.edit',$personel->personel_id)}}" class="btn btn-xs bg-blue"><i class="material-icons">edit</i></a></td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                    {{ $dataPersonel->appends(Request::except('page'))->render() }}
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
	</div>
@endsection

@section('script')
<script>
	$(function () {
    	$("#display").val("{{$filterDisplay}}");
    	$("#sort_by").val("{{$filterSortBy}}");
    	$("#error_message").val("{{$filterErrorMessage}}");
    	$("#search_column").val("{{$filterSearchColumn}}");
    	$("#search_data").val("{{$filterSearchData}}");
	});
</script>
@endsection