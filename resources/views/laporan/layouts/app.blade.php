<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="_token" content="{{ csrf_token() }}">
        <title>LAPORAN VERIFIKATOR SIPP</title>
        <!-- Favicon-->
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        
        <!-- Bootstrap Core Css -->
        <link href="{{ asset('/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="{{ asset('/plugins/node-waves/waves.css') }}" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="{{ asset('/plugins/animate-css/animate.css') }}" rel="stylesheet" />

        <!-- Morris Chart Css-->
        <link href="{{ asset('/plugins/morrisjs/morris.css') }}" rel="stylesheet" />

        <!-- Bootstrap Select Css -->
        <link href="{{ asset('/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" />
        @yield('style')
        <!-- Custom Css -->
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="{{ asset('/css/themes/all-themes.css') }}" rel="stylesheet" />
        <link href="{{ asset('/css/sipp.css') }}" rel="stylesheet">
        <style>
            
        <style>
            .grid-actions {
                text-align: right;
            }

            .grid-actions .btn {
                margin-left: 16px;
            }
            .sortable-handle {
                cursor: move;
                width: 40px;
                color: #ddd;
            }
            .id-column {
                width: 40px;
            }

            /** notifications style */
            .alert {
                font-size: 14px;
            }
            .bootstrap-growl .close {
                margin-left: 10px;
            }

            /** forms */
        </style>
        </style>
    </head>

    <body class="theme-red">
        <!-- Page Loader -->
        <!--     
        <div class="page-loader-wrapper">
                <div class="loader">
                    <div class="preloader">
                        <div class="spinner-layer pl-red">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                    <p>Please wait...</p>
                </div>
        </div> -->
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Search Bar -->
        <div class="search-bar">
            <div class="search-icon">
                <i class="material-icons">search</i>
            </div>
            <input type="text" placeholder="START TYPING...">
            <div class="close-search">
                <i class="material-icons">close</i>
            </div>
        </div>
        <!-- #END# Search Bar -->
        @include('partials.topbar')
        <section>
            @include('partials.left-sidebar')
            @include('partials.right-sidebar')
        </section>

        <section class="content" style="min-height: 1500px;">
            @yield('content')
        </section>
        @include('partials.modal')

        <!-- Jquery Core Js -->
        <script src="{{ asset('/plugins/jquery/jquery.min.js') }}"></script>
          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="{{ asset('/plugins/bootstrap/js/bootstrap.js') }}"></script>

        <!-- Select Plugin Js -->
        <script src="{{ asset('/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="{{ asset('/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>

        <!-- Jquery Validation Plugin Css -->
        <script src="{{ asset('/plugins/jquery-validation/jquery.validate.js') }}"></script>

        <!-- JQuery Steps Plugin Js -->
        <script src="{{ asset('/plugins/jquery-steps/jquery.steps.js') }}"></script>

        <!-- Sweet Alert Plugin Js -->
        <script src="{{ asset('/plugins/sweetalert/sweetalert.min.js') }}"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="{{ asset('/plugins/node-waves/waves.js') }}"></script>

        <script src="{{ asset('/plugins/autosize/autosize.js') }}"></script>
        <!-- Jquery Input Mask Plugin Js -->
        <script src="{{ asset('/plugins/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>

        <!-- Jquery CountTo Plugin Js -->
        <script src="{{ asset('/plugins/jquery-countto/jquery.countTo.js') }}"></script>

        <!-- Morris Plugin Js -->
        <script src="{{ asset('/plugins/raphael/raphael.min.js') }}"></script>
        <script src="{{ asset('/plugins/morrisjs/morris.js') }}"></script>

        <!-- ChartJs -->
        <script src="{{ asset('/plugins/chartjs/Chart.bundle.js') }}"></script>

        @yield('script')

        <!-- Sparkline Chart Plugin Js -->
        <script src="{{ asset('/plugins/jquery-sparkline/jquery.sparkline.js') }}"></script>

            <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            </script>
        <!-- <script src="https://unpkg.com/sweetalert/dist/sweetaletrs.min.js') }}"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.6/sweetalert2.all.min.js"></script>
        <script src="{{ asset('/js/laravel.js')}}" type="text/javascript"></script>
        <script src="{{ asset('/js/newsipp.js') }}"></script>
        <!-- Custom Js -->
        <script src="{{ asset('/js/admin.js') }}"></script>
    </body>

</html>