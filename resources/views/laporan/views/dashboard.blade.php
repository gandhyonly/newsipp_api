@extends('laporan.layouts.app')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>DASHBOARD</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Widgets -->
	    <div class="row clearfix">
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-blue hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">people</i>
	                </div>
	                <div class="content">
	                    <div class="text">PERSONEL</div>
	                    <div class="number count-to" data-from="0" data-to="0" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Widgets -->
	</div>
@endsection

@section('script')
	<script>
	$(function () {
	    //Widgets count
	    $('.count-to').countTo();
	});
	</script>
@endsection