<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <table>
        <thead> 
        </thead>
        <tbody>
            <tr>
                <td>No.</td>    
                <td>Status</td>    
                <td>Nama Lengkap</td>    
                <td>NRP</td>    
                <td>NRP Baru</td>    
                <td>Satker Sekarang</td>    
                <td>No. Telp</td>    
                <td>Email</td>    
                <td>Satker Pertama</td>    
                <td>Nama Ibu</td>        
                <td>Tanggal Lahir</td>    
                <td>TMT Jabatan Pertama</td>
                <td>Pangkat</td>    
                <td>TMT Jabatan Terakhir</td>    
                <td>Tanggal Masuk Laporan</td>    
            </tr>
        @foreach ($helpdesk as $key => $data)
            
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $data->status }}</td>       
                <td>{{ $data->nama_lengkap }}</td>
                <td>{{ $data->nrp }}</td>
                <td>{{ $data->nrp_baru }}</td>
                <td>{{ $data->satuan_sekarang }} ({{$data->satker_sekarang_id}})</td>
                <td>{{ $data->handphone }}</td>
                <td>{{ $data->email }}</td>
                <td>{{ $data->satker_pertama }}</td>
                <td>{{ $data->nama_ibu }}</td>  
                <td>{{ $data->tanggal_lahir ? $data->tanggal_lahir->format('d M Y'):'' }}</td>
                <td>{{ $data->tmt_jabatan_pertama? $data->tmt_jabatan_pertama->format('d M Y'):'' }}</td>
                <td>{{ $data->pangkat_id ? $data->pangkat->singkatan:'' }}</td>
                <td>{{ $data->tmt_jabatan_terakhir ? $data->tmt_jabatan_terakhir->format('d M Y'):'' }}</td>  
                <td>{{ $data->created_at ? $data->created_at->format('d M Y - H:i:s'):'' }}</td>  
            </tr>
        @endforeach
        </tbody>

    </table>
  </body>
</html>
