@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>DASHBOARD</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif

	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Widgets -->
	    <div class="row clearfix">
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-blue hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">people</i>
	                </div>
	                <div class="content">
	                    <div class="text">PERSONEL</div>
	                    <div class="number count-to" data-from="0" data-to="0" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-light-green hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">location_on</i>
	                </div>
	                <div class="content">
	                    <div class="text">SATUAN</div>
	                    <div class="number count-to" data-from="0" data-to="0" data-speed="15" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-orange hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">stars</i>
	                </div>
	                <div class="content">
	                    <div class="text">JABATAN</div>
	                    <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-red hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">forum</i>
	                </div>
	                <div class="content">
	                    <div class="text">DUPLICATE</div>
	                    <div class="number count-to" data-from="0" data-to="0" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Widgets -->
	    <!-- migration -->
	    <div class="row clearfix" id="form-migration">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>Form Migrasi</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{route('migration.seed')}}">
	                        {{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-5 form-control-label">
	                                        <label for="status_personel_id">Database</label>
	                                    </div>
	                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-7">
		                                    <select class="form-control show-tick" name="database" id="database">
	                                        	<option value="newsipp_ssdm">LOCAL SSDM</option>
	                                        	<option value="newsipp_jateng">LOCAL JATENG</option>
	                                        	<option value="M_ssdm">M_ssdm</option>
	                                        	<option value="P_JATENG">P_JATENG</option>
	                                        	<option value="P_ACEH">P_ACEH</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-5 form-control-label">
	                                        <label for="status_personel_id">Table</label>
	                                    </div>
	                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-7">
		                                    <select class="form-control show-tick" name="table" id="table">
	                                        	<option value="PERSONEL">PERSONEL</option>
		                                    </select>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-10 col-md-offset-2">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label><i class="material-icons">search</i> MIGRASI</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End migration -->
	    <!-- Reset -->
	    <div class="row clearfix" id="form-reset">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>Reset Data</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="POST" action="{{route('migration.reset.personel')}}">
	                        {{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-md-12">
	                                        <button type="submit" class="btn bg-orange btn-lg btn-block waves-effect"><label><i class="material-icons">people</i> Reset Data Personel</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Reset -->
	</div>
@endsection

@section('script')
	<script>
	$(function () {
	    //Widgets count
	    $('.count-to').countTo();
	});
	</script>
@endsection