@extends('layouts.default')

@section('content')
	<div class="container-fluid">
	    <div class="block-header">
	        <h2>DASHBOARD</h2>
	    </div>

	    @if(Session::has('success'))
		    <div class="alert alert-success">{{Session::get('success')}}</div>
	    @endif
		
	    @if(Session::has('error'))
		    <div class="alert alert-error">{{Session::get('error')}}</div>
	    @endif

	    <!-- Widgets -->
	    <div class="row clearfix">
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-red hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">forum</i>
	                </div>
	                <div class="content">
	                    <div class="text">JABATAN DUPLIKAT</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahJabatan - $jumlahJabatanUnique }}" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	            <div class="info-box bg-red hover-expand-effect">
	                <div class="icon">
	                    <i class="material-icons">forum</i>
	                </div>
	                <div class="content">
	                    <div class="text">JABATAN</div>
	                    <div class="number count-to" data-from="0" data-to="{{$jumlahJabatan}}" data-speed="1000" data-fresh-interval="20"></div>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Widgets -->
	    
	    <!-- #END# Widgets -->
	    <div class="row m-b-10">
	    	<div class="col-md-4"><a href="{{ route('jabatan.create')}}" class="btn btn-success btn-block">Buat Jabatan Baru</a></div>
	    </div>

	
	    <!-- Filter -->
	    <div class="row clearfix" id="filter">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="header">
	                    <h2>FILTER</h2>
	                </div>
	                <div class="body">
	                    <form id="form_validation" class="form-horizontal" method="GET" action="{{route('jabatan.filter')}}">
	                    	{{ csrf_field() }}
	                        <div class="row clearfix">
	                            <div class="col-md-12">
	                                <div class="row clearfix">
	                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 form-control-label">
	                                        <label for="nama_jabatan">Nama Jabatan</label>
	                                    </div>
	                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-7">
	                                        <div class="form-group" style="margin:0px;">
	                                            <div class="form-line" style="padding-left:12px;">
	                                                <input id="nama_jabatan" name="nama_jabatan" class="form-control" placeholder="Masukkan Nama Jabatan" type="text">
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                                <div class="row clearfix">
	                                    <div class="col-md-8 col-md-offset-4">
	                                        <button type="submit" class="btn bg-green btn-lg btn-block waves-effect"><label><i class="material-icons">search</i> CARI</label></button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End Filter -->

	    <!-- Hover Rows -->
	    <div class="row clearfix">
	        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <div class="card">
	                <div class="body table-responsive">
	                    <table class="table table-hover" id="table-personel">
	                        <thead>
	                            <tr>
	                                <th width="100px">ID</th>
	                                <th width="400px">Nama Jabatan</th>
	                                <th width="200px">Jumlah</th>
	                                <th width="100px">PILIHAN</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($dataJabatan as $jabatan)
	                            <tr style="font-size:12px;">
	                            	<td>{{$jabatan->jabatan_id}}</td>
	                            	<td>{{$jabatan->nama_jabatan}}</td>
	                            	<td>{{$jabatan->satuanJabatan()->count()}}</td>
	                            	<td>
	                            		<a onclick="confirmDelete('{{route('jabatan.destroy',$jabatan->jabatan_id)}}')"><button class="btn btn-xs bg-red waves-effect" type="button" title="delete"><i class="material-icons">delete_forever</i></button></a>
	                            	</td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                    {{ $dataJabatan->render() }}
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- #END# Hover Rows -->
	</div>
@endsection
@section('script')
	<script>
	$(function () {
	    //Widgets count
	    $('.count-to').countTo();
	});
	</script>
@endsection