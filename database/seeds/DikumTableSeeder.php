<?php

use App\Dikum;
use Illuminate\Database\Seeder;

class DikumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dikum::create(['tingkat' => 'SD']);
        Dikum::create(['tingkat' => 'MI']);
        Dikum::create(['tingkat' => 'SMP']);
        Dikum::create(['tingkat' => 'MTS']);
        Dikum::create(['tingkat' => 'SMA']);
        Dikum::create(['tingkat' => 'SMK']);
        Dikum::create(['tingkat' => 'MA']);
        Dikum::create(['tingkat' => 'MK']);
        Dikum::create(['tingkat' => 'D1']);
        Dikum::create(['tingkat' => 'D2']);
        Dikum::create(['tingkat' => 'D3']);
        Dikum::create(['tingkat' => 'D4']);
        Dikum::create(['tingkat' => 'S1']);
        Dikum::create(['tingkat' => 'S1 - Profesi']);
        Dikum::create(['tingkat' => 'S2']);
        Dikum::create(['tingkat' => 'S2 - Profesi']);
        Dikum::create(['tingkat' => 'S3']);
    }
}
