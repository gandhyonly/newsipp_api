<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiUnitLantasPrarural extends Duplikasi
{
	function __construct()
	{
		$this->limit=7000;
		$this->tipeDuplikasi='Unit Lantas Prarural';
		// $this->sumberDuplikasi=12880;//POLDA ACEH > POLRESTA BANDA ACEH >POLSUBSEKTOR BLANG BINTANG (subsektor) 
		$this->sumberDuplikasi=6148061;//DUPLIKASI UNIT LANTAS METRO 
		$this->targetDuplikasi=Satuan::where('jenis_satuan_id',63)->get();
		// $this->targetDuplikasi=Satuan::where('satuan_id',182092)->get();
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->duplication();
    }
}
