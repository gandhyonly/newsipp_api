<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiUnitLantasTanpaJabatan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::selectSheets('Sheet1')->load(public_path('duplication/unit-lantas.xls'), function($reader){
            foreach($reader->get() as $row){
            	$parentSatuan=Satuan::find($row->satuan_id);
            	if($parentSatuan){
            		$flag=false;
			    	if($parentSatuan->jenis_satuan_id==46){
			    		$jenisSatuanID = 64;
			    		$flag = true;
                        continue;
			    	} else if($parentSatuan->jenis_satuan_id==47){
			    		$jenisSatuanID = 63;
			    		$flag = true;
                        continue;
			    	} else if($parentSatuan->jenis_satuan_id==44){
                        $jenisSatuanID = 74;
                        $flag = true;
                    } else if($parentSatuan->jenis_satuan_id==45){
                        $jenisSatuanID = 75;
                        $flag = true;
                    } else {
                        echo "$row->satuan_id (Jenis Satuan ID $parentSatuan->jenis_satuan_id)\n";
			    		$flag=false;
			    	}
			    	if($flag){
                        // $satuan = Satuan::create([
                        //     'nama_satuan' => 'UNIT LANTAS',
                        //     'tipe' => 'STRUKTUR',
                        //     'jenis_satuan_id' => $jenisSatuanID,
                        //     'parent_id' => $parentSatuan->satuan_id,
                        // ]);
                        // $satuan->posisi=$satuan->satuan_id;
                        // $satuan->save();
			    	} else {
			    		echo "$row->satuan_id (Jenis Satuan ID $parentSatuan->jenis_satuan_id)\n";
			    	}
            	} else {
            		echo "Satuan Tidak Ditemukan: $row->satuan_id\n";
            	}
	    	}
        });
    }
}
