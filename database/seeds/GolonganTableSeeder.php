<?php

use App\Golongan;
use Illuminate\Database\Seeder;

class GolonganTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Golongan::create(['nama_golongan' => 'I/A']);
        Golongan::create(['nama_golongan' => 'I/B']);
        Golongan::create(['nama_golongan' => 'I/C']);
        Golongan::create(['nama_golongan' => 'I/D']);
        Golongan::create(['nama_golongan' => 'II/A']);
        Golongan::create(['nama_golongan' => 'II/B']);
        Golongan::create(['nama_golongan' => 'II/C']);
    	Golongan::create(['nama_golongan' => 'II/D']);
        Golongan::create(['nama_golongan' => 'III/A']);
        Golongan::create(['nama_golongan' => 'III/B']);
        Golongan::create(['nama_golongan' => 'III/C']);
        Golongan::create(['nama_golongan' => 'III/D']);
        Golongan::create(['nama_golongan' => 'IV/A']);
        Golongan::create(['nama_golongan' => 'IV/B']);
        Golongan::create(['nama_golongan' => 'IV/C']);
        Golongan::create(['nama_golongan' => 'IV/D']);
        Golongan::create(['nama_golongan' => 'IV/E']);
    }
}
