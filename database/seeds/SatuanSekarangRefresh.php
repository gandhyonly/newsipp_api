<?php

use App\Member;
use App\Satuan;
use Illuminate\Database\Seeder;

class SatuanSekarangRefresh extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$count=0;
    	Member::chunk(100,function($members) use ($count){
    		foreach($members as $member){
    			$count++;
    			$member->satker_sekarang=Satuan::find($member->satuan_3)->nama_satuan.' > '.Satuan::find($member->satuan_2)->nama_satuan.' > '.Satuan::find($member->satuan_1)->nama_satuan;
    			$member->save();
    			echo "$count,";
    		}
    	});
    }
}
