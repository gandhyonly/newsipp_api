<?php

use App\Dikpol;
use App\JenisDikpol;
use App\TipeDikpol;
use Illuminate\Database\Seeder;

class PendidikanPolisiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    		$jenisDikpol = JenisDikpol::create(['nama_jenis_pendidikan' => 'DIKTUK']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'AKPOL']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'AKABRI']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'AKABRI A (KHUSUS LULUSAN TAHUN 1988)']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'AKABRI B (KHUSUS LULUSAN TAHUN 1988)']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SIPSS']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'PPSS']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SEPA']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SEPA MILSUK']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SEPA PK ABRI']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SEBA']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SETA']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'PAG']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'PAG TA']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SAG']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'DIKMABA']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'DIKMATA']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SECATA']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SECABA']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'DIKTUKBA']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SIPSUS']);
    		$jenisDikpol = JenisDikpol::create(['nama_jenis_pendidikan' => 'DIKBANG']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'AGOL']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SIP']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SECAPA']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SIP SUS']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'STIK PTIK']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'PTIK']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SESPIMMA']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SELAPA']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SESPIMMEN']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SESPIM']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SESPIMTI']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'SESPATI']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'LEMHANAS']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'DIKLAT PIM TK.I']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'DIKLAT PIM TK.II']);
    			$dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'DIKLAT PIM TK.III']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'DIKLAT PIM TK.IV']);
                $dikpol = Dikpol::create(['jenis_dikpol_id' => $jenisDikpol->jenis_dikpol_id, 'tingkat' => 'DIKTUKPA']);
    }
}
