<?php

use App\LokasiPenugasan;
use Illuminate\Database\Seeder;

class LokasiPenugasanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	LokasiPenugasan::create(['negara_id' => 83, 'nama_lembaga' => 'MABES POLRI', 'kota' => 'JAKARTA']);
        LokasiPenugasan::create(['negara_id' => 83, 'nama_lembaga' => 'POLDA JABAR', 'kota' => 'JAKARTA']);
        LokasiPenugasan::create(['negara_id' => 83, 'nama_lembaga' => 'POLDA JATIM', 'kota' => 'SURABAYA']);
        LokasiPenugasan::create(['negara_id' => 83, 'nama_lembaga' => 'POLDA JATENG', 'kota' => 'SEMARANG']);
        LokasiPenugasan::create(['negara_id' => 83, 'nama_lembaga' => 'POLDA SULSEL', 'kota' => 'MAKASSAR']);
    	LokasiPenugasan::create(['negara_id' => 83, 'nama_lembaga' => 'POLDA KALTIM', 'kota' => 'SAMARINDA']);
    }
}
