<?php

use App\Kota;
use App\Personel;
use Illuminate\Database\Seeder;

class TempatLahirChangetoKota extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$total = Personel::count();
    	foreach(Personel::get() as $key => $personel){
            $this->show_status($key+1, $total, $size=30);
            $kota = Kota::where('nama_kota',$personel->tempat_lahir)->first();
    		$personel->tempat_lahir_id =  $kota ? $kota->kota_id : null;
    		$personel->save();
    	}
    }

    private function show_status($done, $total, $size=30) {
        static $start_time;

        // if we go over our bound, just ignore it
        if($done > $total) return;

        if(empty($start_time)) $start_time=time();
        $now = time();

        $perc=(double)($done/$total);

        $bar=floor($perc*$size);

        $status_bar="\r[";
        $status_bar.=str_repeat("=", $bar);
        if($bar<$size){
            $status_bar.=">";
            $status_bar.=str_repeat(" ", $size-$bar);
        } else {
            $status_bar.="=";
        }

        $disp=number_format($perc*100, 0);

        $status_bar.="] $disp%  $done/$total";

        $rate = ($now-$start_time)/$done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);

        $elapsed = $now - $start_time;

        $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

        echo "$status_bar  ";

        flush();

        // when done, send a newline
        if($done == $total) {
            echo "\n";
        }

    }
}
