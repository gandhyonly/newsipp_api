<?php

use App\SatuanJabatan2;
use App\SatuanJabatan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MigrasiSatuanJabatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	function __construct()
	{
		$this->countSatuanJabatan = 1;
        $this->totalSatuanJabatan = SatuanJabatan2::count();
	}
    public function run()
    {
    	// $arraySatuanJabatan = [26];
        // $arraySatuanJabatan = [123801, 160817, 160864, 160906];
        $arraySatuanJabatan = [29204,31473,33268, 36213,36556,35966,36909,37317,37441,37474,37498,37516];

    	// sudah 17518,15249,26493,22098,23242,22441,23594,23118,23040,29180,23100,23073
    	// $arraySatuanJabatan = [7136,7162,7180,7211,7210,7212,7252,7253,7254];
    	
    	// $arraySatuanJabatan = [26,123801,160817,160864,160906,161680,160985,162348,163264,163544,163358,163425,163659,163702,163714,163725,163734];
    	foreach($arraySatuanJabatan as $satuanJabatanID){
	    	echo "$satuanJabatanID ";
	    	$satuanMabes = SatuanJabatan2::find($satuanJabatanID);
	    	$pangkatID = $satuanMabes->pangkat()->pluck('pangkat_jabatan.pangkat_id')->toArray();
	    	$newSatuanJabatan = SatuanJabatan::create([
	    		'satuan_jabatan_id' => $satuanMabes->satuan_jabatan_id,
	    		'keterangan' => $satuanMabes->keterangan,
	    		'dsp' => $satuanMabes->dsp,
	    		'parent_satuan_jabatan_id' => $satuanMabes->parent_satuan_jabatan_id,
	    		'jabatan_id' => $satuanMabes->jabatan_id,
	    		'satuan_id' => '3'.$satuanMabes->satuan_id,
	    		'status_jabatan_id' => $satuanMabes->status_jabatan_id,
	    		'nivelering_id' => $satuanMabes->nivelering_id
	    	]);
	    	echo "created.";
	    	$newSatuanJabatan->pangkat()->sync($pangkatID);
	    	echo "connected.\n";
	    	foreach($satuanMabes->childs as $jabatan){
	    		$this->jabatanChild($jabatan);
	    	}
    	}
    }
	public function jabatanChild($paramJabatan)
	{
    	echo "$paramJabatan->satuan_jabatan_id ";
    	$pangkatID = $paramJabatan->pangkat()->pluck('pangkat_jabatan.pangkat_id')->toArray();
    	$newSatuanJabatan = SatuanJabatan::create([
    		'satuan_jabatan_id' => $paramJabatan->satuan_jabatan_id,
    		'keterangan' => $paramJabatan->keterangan,
    		'dsp' => $paramJabatan->dsp,
    		'parent_satuan_jabatan_id' => $paramJabatan->parent_satuan_jabatan_id,
    		'jabatan_id' => $paramJabatan->jabatan_id,
    		'satuan_id' => '3'.$paramJabatan->satuan_id,
    		'status_jabatan_id' => $paramJabatan->status_jabatan_id,
    		'nivelering_id' => $paramJabatan->nivelering_id
    	]);
    	echo "created.";
    	$newSatuanJabatan->pangkat()->sync($pangkatID);
    	echo "connected.\n";
    	$this->countSatuanJabatan++;
		foreach($paramJabatan->childs as $jabatan){
			if($jabatan->childs==null){
				return null;
			}
			$this->jabatanChild($jabatan);
		}
	}
}
