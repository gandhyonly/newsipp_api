<?php

use App\StatusJabatan;
use Illuminate\Database\Seeder;

class StatusJabatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(StatusJabatan::count()==0){
            StatusJabatan::create([
                'status' => 'Aktif'
            ]);
            StatusJabatan::create([
                'status' => 'Tidak Aktif'
            ]);
        }
    }
}
