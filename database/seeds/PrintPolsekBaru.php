<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class PrintPolsekBaru extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$tests = Satuan::where('jenis_satuan_id',77)->get();
    	foreach($tests as $test){
    		echo $test->fullPrintBackward()."\n";
    	}
    }
}
