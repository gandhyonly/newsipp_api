<?php

use App\Hobi;
use Illuminate\Database\Seeder;

class HobiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Hobi::create(['hobi' => 'Berkebun']);
    	Hobi::create(['hobi' => 'Menari']);
    	Hobi::create(['hobi' => 'Bernyanyi']);
    	Hobi::create(['hobi' => 'Memasak']);
    	Hobi::create(['hobi' => 'Menulis']);
    	Hobi::create(['hobi' => 'Olahraga']);
    	Hobi::create(['hobi' => 'Fotografi']);
    	Hobi::create(['hobi' => 'Bermain game']);
    	Hobi::create(['hobi' => 'Menggambar/melukis']);
    	Hobi::create(['hobi' => 'Fashion']);
    	Hobi::create(['hobi' => 'Otomotif']);
    	Hobi::create(['hobi' => 'Traveling']);
    	Hobi::create(['hobi' => 'Mengajar']);
    	Hobi::create(['hobi' => 'Bermain Musik']);
    	Hobi::create(['hobi' => 'Kerajinan Tangan']);
    	Hobi::create(['hobi' => 'Kolektor']);
    	Hobi::create(['hobi' => 'Sulap']);
    	Hobi::create(['hobi' => 'Renang']);
    }
}
