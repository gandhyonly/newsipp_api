<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(KotaTableSeeder::class);
        $this->call(NegaraTableSeeder::class);
        $this->call(HelpdeskUserTableSeeder::class);
        // Pendidikan
        $this->call(DikumTableSeeder::class);
        $this->call(PendidikanPolisiTableSeeder::class);
        $this->call(DikbangspesTableSeeder::class);
        // end Pendidikan
        $this->call(JurusanTableSeeder::class);
        $this->call(KonsentrasiTableSeeder::class);
        $this->call(GelarTableSeeder::class);
        $this->call(BahasaTableSeeder::class);
        $this->call(TandaKehormatanTableSeeder::class);
        $this->call(HobiTableSeeder::class);
        $this->call(OlahragaTableSeeder::class);
        $this->call(PsikologiTableSeeder::class);
        // $this->call(KesehatanTableSeeder::class);
        $this->call(SIMTableSeeder::class);
        $this->call(AgamaTableSeeder::class);
        $this->call(StatusFormTableSeeder::class);
        $this->call(TingkatPenghargaanTableSeeder::class);
        $this->call(PenghargaanTableSeeder::class);
        $this->call(BrevetTableSeeder::class);
        $this->call(JenisBKOTableSeeder::class);
        $this->call(MisiTableSeeder::class);
        $this->call(JenisPenugasanTableSeeder::class);
        $this->call(LokasiPenugasanTableSeeder::class);

        // Admin
        $this->call(AdminTableSeeder::class);
        // end
        $this->call(UserTableSeeder::class);
        
        $this->call(JenisPekerjaanTableSeeder::class);
        $this->call(HubunganKeluargaTableSeeder::class);

        $this->call(AgamaTableSeeder::class);
        // $this->call(JenisKelaminTableSeeder::class);
        $this->call(StatusPersonelTableSeeder::class);
        // for demo only
        $this->call(OtherTableSeeder::class);
        // end demo
            $this->call(StatusJabatanTableSeeder::class);
            $this->call(GolonganTableSeeder::class);
            $this->call(NiveleringTableSeeder::class);
            $this->call(JabatanPenugasanTableSeeder::class);
            // 
            $this->call(PangkatTableSeeder::class);
            
            // $this->call(JenisSatuanTableSeeder::class);
            // $this->call(JabatanTableSeeder::class);
            // $this->call(SatuanTableSeeder::class);

        // $this->call(PersonelSementaraTableSeeder::class);
        // $this->call(HelpdeskTableSeeder::class);

    }
}
