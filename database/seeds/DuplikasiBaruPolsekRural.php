<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiBaruPolsekRural extends Duplikasi
{
	function __construct()
	{
		$this->limit=7000;
		$this->tipeDuplikasi='POLSEK RURAL BARU';
		// $this->sumberDuplikasi=10987;//POLDA ACEH > POLRESTA BANDA ACEH > POLSEK KUTA ALAM (rural)
		$this->sumberDuplikasi=7213574;//DUPLIKASI POLSEK RURAL
		$this->targetDuplikasi=Satuan::where('jenis_satuan_id',78)->get();
		// $this->targetDuplikasi=Satuan::where('jenis_satuan_id',46)->get();
		// $this->targetDuplikasi=Satuan::where('satuan_id',7213574)->get();
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->duplication();
		Satuan::where('jenis_satuan_id',78)->update(['jenis_satuan_id'=>46]);
    }
}
