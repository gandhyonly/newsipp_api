<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiBaruPolsekUrban extends Duplikasi
{
	function __construct()
	{
		$this->limit=7000;
		$this->tipeDuplikasi='POLSEK URBAN BARU';
		$this->sumberDuplikasi=5182089;//DUPLIKASI POLSEK URBAN BARU
		$this->targetDuplikasi=Satuan::where('jenis_satuan_id',85)->get();
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->duplication();
		Satuan::where('jenis_satuan_id',85)->update(['jenis_satuan_id'=>45]);
    }
}
