<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiUnitLantas extends Seeder
{
    public function run()
    {
        $this->call(DuplikasiUnitLantasMetro::class);
        $this->call(DuplikasiUnitLantasUrban::class);
        $this->call(DuplikasiUnitLantasRural::class);
        $this->call(DuplikasiUnitLantasPrarural::class);
    }
}
