<?php

use App\Brevet;
use Illuminate\Database\Seeder;

class BrevetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load(public_path('migration/brevet.csv'), function($reader){
            foreach($reader->get() as $row){
                Brevet::create(['nama_brevet' => $row->brevet]);
            }
        });
    }
}
