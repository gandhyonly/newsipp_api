<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiLemdiklat extends Duplikasi
{
	function __construct()
	{
		$this->limit=20000;
		$this->tipeDuplikasi='LEMDIKLAT';
		$this->sumberDuplikasi=1205;//Polda A Duplikasi
		// $this->sumberDuplikasi=5;//Polda Aceh
		// $this->targetDuplikasi=Satuan::where('jenis_satuan_id',57)->get();
		$this->targetDuplikasi=Satuan::where('satuan_id','>=',7210145)->where('satuan_id','<=',7210154)->get();
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatan=[];
		$this->arraySatuanJabatanBaru=[];
	}
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->duplication();
    }
}
