<?php

use App\Satuan;
use App\SatuanJabatan;
use Illuminate\Database\Seeder;

class DuplikasiJabatanBintara extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    function __construct()
    {
        // pati (2240) 37,38,39,40
        // pama (2238) 31,32,33
        // pamen (663) 34,35,36
        // Bintara (145) 25,26,27,28
        // Tamtama (809) 19,20,21,22,23,24
        $this->jabatanID = 145; 
        $this->pangkatID = [25,26,27,28];
        
    }
    public function run()
    {
        echo "Duplikasi Bintara \n";
        echo "Submabes \n";
        $satuanMabes = [1168, 1169, 1170, 1171, 1173, 1174, 1175, 1176, 1177, 1178, 1179, 1180, 1181, 1182, 1183, 1190, 1194, 1195, 1196, 1197, 1209, 1211, 1212, 1213, 3588 ];
        $this->duplikasi($satuanMabes);
        // Polda
        echo "POLDA \n";
        $satuanPolda = Satuan::whereIn('jenis_satuan_id',[55,56,57])->pluck('satuan_id')->toArray();
        $this->duplikasi($satuanPolda);
        // Polres dan SubPolda
        echo "Polres dan SubPolda \n";
        $this->duplikasiChilds($satuanPolda);
        $satuanPOLRES = Satuan::whereIn('jenis_satuan_id',[3,4,6,43])->pluck('satuan_id')->toArray();
        echo "POLSEK dan SUBPOLRES \n";
        $this->duplikasiChilds($satuanPOLRES);

    }
    public function duplikasi($satuanArray)
    {
        foreach($satuanArray as $satuanID){
            echo $satuanID."\n";
            $satuanJabatan = SatuanJabatan::create([
                'dsp' => 0,
                'satuan_id' => $satuanID,
                'jabatan_id' => $this->jabatanID,
                'status_jabatan_id' => 1,
            ]);
            $satuanJabatan->pangkat()->sync($this->pangkatID);
        }
    }
    public function duplikasiChilds($satuanArray)
    {
        foreach($satuanArray as $satuanID){
            foreach(Satuan::find($satuanID)->childs as $satuan){
                echo "$satuan->nama_satuan ($satuan->satuan_id)\n";
                $satuanJabatan = SatuanJabatan::create([
                    'dsp' => 0,
                    'satuan_id' => $satuan->satuan_id,
                    'jabatan_id' => $this->jabatanID,
                    'status_jabatan_id' => 1,
                ]);
                $satuanJabatan->pangkat()->sync($this->pangkatID);
            }
        }
    }
}
