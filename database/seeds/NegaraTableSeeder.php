<?php

use App\Negara;
use Illuminate\Database\Seeder;

class NegaraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load(public_path('migration/negara.csv'), function($reader){
            foreach($reader->get() as $row){
                Negara::create(['nama_negara' => $row->nama_negara]);
            }
        });
    }
}
