<?php

use App\Satuan2;
use App\Satuan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MigrasiSatuanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	function __construct()
	{
		$this->countSatuan = 1;
        $this->totalSatuan = Satuan2::count();
	}

    public function run()
    {

    	// $arraySatuan = [1167];
    	// $arraySatuan = [209760];
    	// $arraySatuan = [139658,182010,182029,182048];
    	// $arraySatuan = [26,123801,160817,160864,160906,161680,160985,162348,163264,163544,163358,163425,163659,163702,163714,163725,163734];
    	$arraySatuan = [182080,182081,182082,182083,182084,182085,182086,182088,182089,182090,182091,182092];
    	// $arraySatuan = [3182080,3182081,3182082,3182083,3182084,3182085,3182086,3182088,3182089,3182090,3182091,3182092];

    	foreach($arraySatuan as $satuanID){
	    	$satuanMabes = Satuan2::find($satuanID);
	    	Satuan::create([
	    		'satuan_id' => '3'.$satuanMabes->satuan_id,
	    		'nama_satuan' => $satuanMabes->nama_satuan,
	    		'tipe' => $satuanMabes->tipe,
	    		'jenis_satuan_id' => $satuanMabes->jenis_satuan_id,
	    		'parent_id' => $satuanMabes->parent_id,
	    		'posisi' => '3'.$satuanMabes->satuan_id
	    	]);
	    	echo "$satuanMabes->satuan_id\n";
	    	foreach($satuanMabes->childs as $satuan){
	    		$this->satuanChild($satuan);
	    	}
    	}
    }
	public function satuanChild($paramSatuan)
	{
    	Satuan::create([
    		'satuan_id' => '3'.$paramSatuan->satuan_id,
    		'nama_satuan' => $paramSatuan->nama_satuan,
    		'tipe' => $paramSatuan->tipe,
    		'jenis_satuan_id' => $paramSatuan->jenis_satuan_id,
    		'parent_id' => '3'.$paramSatuan->parent_id,
    		'posisi' => '3'.$paramSatuan->satuan_id
    	]);
    	echo "$paramSatuan->satuan_id\n";
		if($paramSatuan->childs==null){
			return null;
		}
    	$this->countSatuan++;
		foreach($paramSatuan->childs as $satuan){
			// $this->show_status($this->countSatuan++, $this->totalSatuan, $size=30);
			$this->satuanChild($satuan);
		}
	}
    private function show_status($done, $total, $size=30) {
	    static $start_time;

	    // if we go over our bound, just ignore it
	    if($done > $total) return;

	    if(empty($start_time)) $start_time=time();
	    $now = time();

	    $perc=(double)($done/$total);

	    $bar=floor($perc*$size);

	    $status_bar="\r[";
	    $status_bar.=str_repeat("=", $bar);
	    if($bar<$size){
	        $status_bar.=">";
	        $status_bar.=str_repeat(" ", $size-$bar);
	    } else {
	        $status_bar.="=";
	    }

	    $disp=number_format($perc*100, 0);

	    $status_bar.="] $disp%  $done/$total";

	    $rate = ($now-$start_time)/$done;
	    $left = $total - $done;
	    $eta = round($rate * $left, 2);

	    $elapsed = $now - $start_time;

	    $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

	    echo "$status_bar  ";

	    flush();

	    // when done, send a newline
	    if($done == $total) {
	        echo "\n";
	    }
	}
}
