<?php

use Illuminate\Database\Seeder;
use App\SatuanJabatan;
use App\Satuan;

class AttachAtasanKapolres extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$kelompokSatuanJabatan = SatuanJabatan::whereIn('jabatan_id',[280,281,282,2193])->where('keterangan','NOT LIKE','%DUPLIKASI%')->whereNull('parent_satuan_jabatan_id')->get();
		foreach($kelompokSatuanJabatan as $satuanJabatan){
            echo "$satuanJabatan->satuan_jabatan_id ";
			$satuan = Satuan::find($satuanJabatan->satuan_id);
            echo "$satuan->nama_satuan\n";
            echo "$satuan->parent->nama_satuan ";
			$satuanJabatanAtasan = SatuanJabatan::where('satuan_id',$satuan->parent_id)->orderBy('golongan_id')->first();
            $jabatan=$satuanJabatan->jabatan->nama_jabatan;
            echo "$jabatan\n";
			$satuanJabatan->parent_satuan_jabatan_id=$satuanJabatanAtasan->satuan_jabatan_id;
			$satuanJabatan->save();
		}
    }
}
