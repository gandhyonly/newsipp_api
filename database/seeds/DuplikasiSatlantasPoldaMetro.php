<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiSatlantasPoldaMetro extends Duplikasi
{
	function __construct()
	{
		$this->limit=7000;
		$this->tipeDuplikasi='DUPLIKASI SATLANTAS POLDA METRO';
		$this->sumberDuplikasi=4067688;//DUPLIKASI RUMKIT1
		$this->polresMetro=Satuan::where('jenis_satuan_id',43)->pluck('satuan_id')->toArray();
		$this->targetDuplikasi=Satuan::where('nama_satuan','SATLANTAS')->whereIn('parent_id',$this->polresMetro)->get();
		
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->duplication();
    }
}
