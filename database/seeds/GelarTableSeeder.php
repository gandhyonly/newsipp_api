<?php

use App\Gelar;
use App\Jurusan;
use Illuminate\Database\Seeder;

class GelarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	if(Gelar::count()==0){
	    	foreach(Jurusan::get() as $jurusan){
		    	Gelar::create(['jurusan_id' => $jurusan->jurusan_id,'gelar' => 'S.Kom.']);
		    	Gelar::create(['jurusan_id' => $jurusan->jurusan_id,'gelar' => 'M.Sc.']);
		    	Gelar::create(['jurusan_id' => $jurusan->jurusan_id,'gelar' => 'Dr.','gelar_posisi_sebelum' => true]);
	    	}
    	}
    }
}
