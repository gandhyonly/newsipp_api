<?php

use Illuminate\Database\Seeder;
use App\SatuanJabatan;
use App\Satuan;

class AttachAtasanKapolda extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$kelompokSatuanJabatan = SatuanJabatan::whereIn('jabatan_id',[276])->where('keterangan','NOT LIKE','%DUPLIKASI%')->whereNull('parent_satuan_jabatan_id')->get();
		foreach($kelompokSatuanJabatan as $satuanJabatan){
            echo "$satuanJabatan->satuan_jabatan_id ";
			$satuan = Satuan::find($satuanJabatan->satuan_id);
            echo "$satuan->nama_satuan";
			$satuanJabatanAtasan = SatuanJabatan::whereIn('jabatan_id',[845,276,280,281,282,283,286,843,2193,2208])->where('satuan_id',$satuan->parent_id)->first();
            $jabatan=$satuanJabatan->jabatan->nama_jabatan;
            echo "$jabatan\n";
			$satuanJabatan->parent_satuan_jabatan_id=$satuanJabatanAtasan->satuan_jabatan_id;
			$satuanJabatan->save();
		}
    }
}
