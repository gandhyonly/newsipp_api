<?php

use Illuminate\Database\Seeder;

class MigrasiTotalSatuanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(JabatanTableSeeder::class);
        $this->call(JenisSatuanTableSeeder::class);
        $this->call(MigrasiSatuanTableSeeder::class);
        $this->call(MigrasiSatuanJabatanTableSeeder::class);
        $this->call(SatuanJabatanNullTableSeeder::class);
    }
}
