<?php

use App\Kesehatan;
use Illuminate\Database\Seeder;

class KesehatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Kesehatan::create(['jenis' => 'Kesehatan 1']);
    	Kesehatan::create(['jenis' => 'Kesehatan 2']);
    	Kesehatan::create(['jenis' => 'Kesehatan 3']);
    	Kesehatan::create(['jenis' => 'Kesehatan 4']);
    	Kesehatan::create(['jenis' => 'Kesehatan 5']);
    }
}
