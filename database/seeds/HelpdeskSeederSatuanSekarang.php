<?php

use App\Helpdesk;
use Illuminate\Database\Seeder;

class HelpdeskSeederSatuanSekarang extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach(Helpdesk::whereNull('satuan_sekarang')->get() as $helpdesk){
    		$helpdesk->satuan_sekarang=$helpdesk->satuanSekarang->fullPrint();
    		$helpdesk->save();
    	}
    }
}
