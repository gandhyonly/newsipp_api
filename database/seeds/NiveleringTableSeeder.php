<?php

use App\Eselon;
use App\Nivelering;
use Illuminate\Database\Seeder;

class NiveleringTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eselon = Eselon::create(['nama_eselon' => 'IA']);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IA','eselon_id' => $eselon->eselon_id]);
        $eselon = Eselon::create(['nama_eselon' => 'IB']);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IB','eselon_id' => $eselon->eselon_id]);

        $eselon = Eselon::create(['nama_eselon' => 'IIA']);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IIA','eselon_id' => $eselon->eselon_id]);
        $eselon = Eselon::create(['nama_eselon' => 'IIB']);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IIB/1','eselon_id' => $eselon->eselon_id]);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IIB/2','eselon_id' => $eselon->eselon_id]);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IIB/3','eselon_id' => $eselon->eselon_id]);

        $eselon = Eselon::create(['nama_eselon' => 'IIIA']);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IIIA/1','eselon_id' => $eselon->eselon_id]);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IIIA/2','eselon_id' => $eselon->eselon_id]);
        $eselon = Eselon::create(['nama_eselon' => 'IIIB']);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IIIB/1','eselon_id' => $eselon->eselon_id]);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IIIB/2','eselon_id' => $eselon->eselon_id]);

        $eselon = Eselon::create(['nama_eselon' => 'IVA']);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IVA','eselon_id' => $eselon->eselon_id]);
        $eselon = Eselon::create(['nama_eselon' => 'IVB']);
            $nivelering = Nivelering::create(['nama_nivelering' => 'IVB','eselon_id' => $eselon->eselon_id]);
    }
}
