<?php

use App\HubunganKeluarga;
use Illuminate\Database\Seeder;

class HubunganKeluargaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(HubunganKeluarga::count()==0){
            HubunganKeluarga::create(['hubungan' => 'SAUDARA KANDUNG']); //1
            HubunganKeluarga::create(['hubungan' => 'AYAH KANDUNG']); //2
            HubunganKeluarga::create(['hubungan' => 'IBU KANDUNG']); //3
            HubunganKeluarga::create(['hubungan' => 'AYAH MERTUA']); //4 
            HubunganKeluarga::create(['hubungan' => 'IBU MERTUA']); //5
            HubunganKeluarga::create(['hubungan' => 'AYAH ANGKAT']); //6 
            HubunganKeluarga::create(['hubungan' => 'IBU ANGKAT']); //7
            HubunganKeluarga::create(['hubungan' => 'AYAH TIRI']); //8
            HubunganKeluarga::create(['hubungan' => 'IBU TIRI']); //9
            HubunganKeluarga::create(['hubungan' => 'ANAK TIRI']); //10
            HubunganKeluarga::create(['hubungan' => 'ANAK KANDUNG']); //11
            HubunganKeluarga::create(['hubungan' => 'ANAK ANGKAT']); //12
            HubunganKeluarga::create(['hubungan' => 'SUAMI']); //13
            HubunganKeluarga::create(['hubungan' => 'ISTRI']); //14
        }
    }
}
