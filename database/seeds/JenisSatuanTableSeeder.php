<?php

use App\JenisSatuan2;
use App\JenisSatuan;
use Illuminate\Database\Seeder;

class JenisSatuanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(JenisSatuan2::get() as $jenisSatuan){
            JenisSatuan::create([
                'jenis_satuan_id' => $jenisSatuan->jenis_satuan_id,
                'nama_jenis_satuan' => $jenisSatuan->nama_jenis_satuan,
                'kategori' => $jenisSatuan->kategori
            ]);
        }
    }
}
