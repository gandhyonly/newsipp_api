<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiBaruPolsubsektor extends Duplikasi
{
	function __construct()
	{
		$this->limit=7000;
		$this->tipeDuplikasi='POLSUBSEKTOR BARU';
		$this->sumberDuplikasi=5182092;//DUPLIKASI POLSUBSEKTOR
		$this->targetDuplikasi=Satuan::where('jenis_satuan_id',81)->get();
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->duplication();
		Satuan::where('jenis_satuan_id',81)->update(['jenis_satuan_id'=>48]);
    }
}
