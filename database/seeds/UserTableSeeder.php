<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::count()==0){
            User::create([
                'name'          => 'admin',
                'username'      => 'admin',
                'password'      => Hash::make('SIPP2017'),
                'api_token'     => 'Qw8WW0amC9TdkhobLhIYBUO8rNcPGCsH1SRdx99QxNXPr9L6jkldPNDVf0Sh',
            ]);
            User::create(['name' => 'Frentina', 'username' => '3333', 'password'=> Hash::make('admin'), 'api_token' => str_random(60), 'is_superadmin' => true ]);

            User::create([
                'name'          => 'Frentina',
                'username'      => '94031222',
                'password'      => Hash::make('frentina'),
                'api_token'     => str_random(60),
                'is_superadmin' => true
            ]);
            User::create([
                'name'          => 'Jonathan',
                'username'      => '9999',
                'password'      => Hash::make('jojo'),
                'api_token'     => str_random(60),
                'is_superadmin' => true
            ]);
            User::create([
                'name'          => 'Oriza Febrianto',
                'username'      => '95021134',
                'password'      => Hash::make('SIPPOriza'),
                'api_token'     => str_random(60),
                'is_superadmin' => true
            ]);
            User::create([
                'name'          => 'Rio Dwi Handoko',
                'username'      => '93031081',
                'password'      => Hash::make('SIPP1081'),
                'api_token'     => str_random(60),
                'is_superadmin' => true
            ]);
        }
    }
}
