<?php

use App\VerifikatorUser;
use Illuminate\Database\Seeder;

class VerifikatorUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	VerifikatorUser::create(['nama' => 'Verifikator 1 - SUBBAG PROGAR','nrp' => '10000001', 'password' => Hash::make('verifikator1'),'satuan_id' => 1,'tingkat' => 1,'api_token' => '10000001']);
        VerifikatorUser::create(['nama' => 'Verifikator 2 - RO RENMIN','nrp' => '10000002', 'password' => Hash::make('verifikator2'),'satuan_id' => 1,'tingkat' => 2,'api_token' => '10000002']);
        VerifikatorUser::create(['nama' => 'Verifikator 3 - POLDA JATENG','nrp' => '20000003', 'password' => Hash::make('verifikator3'),'satuan_id' => 1,'tingkat' => 3,'api_token' => '10000003']);
        VerifikatorUser::create(['nama' => 'Verifikator 4 - MABES','nrp' => '10000004', 'password' => Hash::make('verifikator4'),'satuan_id' => 1,'tingkat' => 4,'api_token' => '10000004']);
    }
}
