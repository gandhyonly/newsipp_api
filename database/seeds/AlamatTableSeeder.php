<?php

use App\Alamat;
use Illuminate\Database\Seeder;

class AlamatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Alamat::create([
    		'personel_id' => 1,
    		'alamat' => 'Jalan Melati, Cluster Sulawesi, Blok S10, Nomor 3, Kota Tangerang',
            'telepon' => '812379123',
    		'status_terakhir' => 1,
    		'tanggal_mulai' => '2017-01-01',
    		'tanggal_selesai' => '2018-01-01',
    	]);
    	Alamat::create([
    		'personel_id' => 1,
    		'alamat' => 'Jalan Jakarta, Cluster Kalimantan, Blok K3, Nomor 3, Kota Jakarta',
            'telepon' => '000000',
    		'status_terakhir' => 0,
    		'tanggal_mulai' => '2016-01-01',
    		'tanggal_selesai' => '2017-01-01',
    	]);
    	Alamat::create([
    		'personel_id' => 2,
    		'alamat' => 'Jalan Yogyakarta, Cluster Papua, Blok P3, Nomor 3, Kota Yogyakarta',
            'telepon' => '22222',
    		'status_terakhir' => 1,
    		'tanggal_mulai' => '2017-01-01',
    		'tanggal_selesai' => '2018-01-01',
    	]);
    }
}
