<?php

use Illuminate\Database\Seeder;

class HelpdeskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(App\Helpdesk::class, 200)->create();
    }
}
