<?php

use App\Fisik;
use App\Member;
use App\Personel;
use App\PersonelData;
use Illuminate\Database\Seeder;

class PersonelSementaraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Member::count()==0){
            $personel = Personel::createPersonel(['nrp'=>'1001','tanggal_lahir' => '2012-12-12','nama_ibu' => 'ibu', 'satker_pertama' => 'POLDA ACEH','tmt_jabatan_pertama' => '2012-12-12']);
            $member = Member::create([
                        'nrp'=>'1001',
                        'personel_id'=>$personel->personel_id,
                        'tanggal_lahir'=>'2012-12-12',
                        'password' => Hash::make('admin'),
                        'status' => 'INVALID',
                        'api_token' => '123781y2783h178edh1782h1hdo8h1y2381y238y2183',
                        'satuan_1' => 1,
                        'satuan_2' => 1,
                        'satuan_3' => 1,
                        'satuan_4' => 1
                    ]);

            $member->newStatusForm();
            $personel = Personel::createPersonel(['nrp'=>'1002','tanggal_lahir' => '2012-12-12','nama_ibu' => 'ibu', 'satker_pertama' => 'POLDA ACEH','tmt_jabatan_pertama' => '2012-12-12']);
            $member = Member::create([
                        'nrp'=>'1002',
                        'personel_id'=>$personel->personel_id,
                        'tanggal_lahir'=>'2012-12-12',
                        'password' => Hash::make('admin'),
                        'status' => 'INVALID',
                        'satuan_1' => 1,
                        'satuan_2' => 1,
                        'satuan_3' => 1,
                        'satuan_4' => 1
                    ]);

            $member->newStatusForm();
            $personel = Personel::createPersonel(['nrp'=>'1003','tanggal_lahir' => '2012-12-12','nama_ibu' => 'ibu', 'satker_pertama' => 'POLDA ACEH','tmt_jabatan_pertama' => '2012-12-12']);
            $member = Member::create([
                        'nrp'=>'1003',
                        'personel_id'=>$personel->personel_id,
                        'tanggal_lahir'=>'2012-12-12',
                        'password' => Hash::make('admin'),
                        'status' => 'INVALID',
                        'satuan_1' => 1,
                        'satuan_2' => 1,
                        'satuan_3' => 1,
                        'satuan_4' => 1
                    ]);

            $personel = Personel::createPersonel(['nrp'=>'1004','tanggal_lahir' => '2012-12-12','nama_ibu' => 'ibu', 'satker_pertama' => 'POLDA ACEH','tmt_jabatan_pertama' => '2012-12-12']);
            $member->newStatusForm();
            $member = Member::create([
                        'nrp'=>'1004',
                        'personel_id'=>$personel->personel_id,
                        'tanggal_lahir'=>'2012-12-12',
                        'password' => Hash::make('admin'),
                        'status' => 'INVALID',
                        'api_token' => 'pns',
                        'satuan_1' => 1,
                        'satuan_2' => 1,
                        'satuan_3' => 1,
                        'satuan_4' => 1
                    ]);

            $member->newStatusForm();
            for($i=5;$i<100;$i++){
                $personel = Personel::createPersonel(['nrp'=>'100'.$i,'tanggal_lahir' => '2012-12-12','nama_ibu' => 'ibu', 'satker_pertama' => 'POLDA ACEH','tmt_jabatan_pertama' => '2012-12-12']);
            }
        }

    }
}
