<?php

use App\HelpdeskUser;
use Illuminate\Database\Seeder;

class HelpdeskUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	HelpdeskUser::create(['nama' => 'Helpdesk 1','nrp' => '10001000', 'password' => bcrypt('helpdesk'), 'api_token' => '1111']);
    	HelpdeskUser::create(['nama' => 'Helpdesk 2','nrp' => '10001001', 'password' => bcrypt('helpdesk'), 'api_token' => str_random(60)]);
    	HelpdeskUser::create(['nama' => 'Helpdesk 3','nrp' => '10001002', 'password' => bcrypt('helpdesk'), 'api_token' => str_random(60)]);
    	HelpdeskUser::create(['nama' => 'Helpdesk 4','nrp' => '10001003', 'password' => bcrypt('helpdesk'), 'api_token' => str_random(60)]);
    	HelpdeskUser::create(['nama' => 'Helpdesk 5','nrp' => '10001004', 'password' => bcrypt('helpdesk'), 'api_token' => str_random(60)]);
    	HelpdeskUser::create(['nama' => 'Helpdesk 6','nrp' => '10001005', 'password' => bcrypt('helpdesk'), 'api_token' => str_random(60)]);
    	HelpdeskUser::create(['nama' => 'Helpdesk 7','nrp' => '10001006', 'password' => bcrypt('helpdesk'), 'api_token' => str_random(60)]);
    	HelpdeskUser::create(['nama' => 'Helpdesk 8','nrp' => '10001007', 'password' => bcrypt('helpdesk'), 'api_token' => str_random(60)]);
    }
}
