<?php

use App\Fisik;
use App\Personel;
use App\PersonelData;
use Illuminate\Database\Seeder;

class FixMemberError extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$test = 0;
    	Personel::chunk(100, function ($personels) use ($test){
    		echo $test."\n";
			foreach ($personels as $personel) {
				if(PersonelData::where('personel_id',$personel->personel_id)->count()==0){
					PersonelData::create(['personel_id'=>$personel->personel_id]);
				}
				if(Fisik::where('personel_id',$personel->personel_id)->count()==0){
					Fisik::create(['personel_id'=>$personel->personel_id]);
				}
			}
			$test+=100;
		});
		echo "FINISH";
    }
}
