<?php

use App\JenisPenugasan;
use Illuminate\Database\Seeder;

class JenisPenugasanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisPenugasan::create(['nama_jenis_penugasan' => 'PENUGASAN DALAM/LUAR NEGERI']);
    	JenisPenugasan::create(['nama_jenis_penugasan' => 'PENUGASAN LUAR STRUKTUR']);
    }
}
