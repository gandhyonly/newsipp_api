<?php

use Illuminate\Database\Seeder;

class AfterSatuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PersonelSementaraTableSeeder::class);
        $this->call(VerifikatorUserTableSeeder::class);
        $this->call(HelpdeskTableSeeder::class);
    }
}
