<?php

use Illuminate\Database\Seeder;

class DuplikasiSeluruhPolsek extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DuplikasiPolsekMetro::class);
        $this->call(DuplikasiPolsekUrban::class);
        $this->call(DuplikasiPolsekRural::class);
        $this->call(DuplikasiPolsekPrarural::class);
        $this->call(DuplikasiPolsekSubsektor::class);
    }
}
