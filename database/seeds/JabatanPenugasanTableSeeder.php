<?php

use App\Eselon;
use App\JabatanPenugasan;
use Illuminate\Database\Seeder;

class JabatanPenugasanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JabatanPenugasan::create(['nama_jabatan_penugasan' => 'Jabatan Luar Struktur 1', "eselon" => 'IVA', 'lokasi_penugasan_id' => 1]);
        JabatanPenugasan::create(['nama_jabatan_penugasan' => 'Jabatan Luar Struktur 2', "eselon" => 'IVA', 'lokasi_penugasan_id' => 2]);
        Excel::load(public_path('migration/jabatan.xlsx'), function($reader){
            foreach($reader->get() as $row){
                $eselonID=null;
                $eselon = Eselon::where('nama_eselon',$row->eselon)->first();
                if($eselon!=null){
                    $eselonID=$eselon->nama_eselon;
                }
                JabatanPenugasan::create(['nama_jabatan_penugasan' => $row->jabatan_penugasan, "eselon" => $eselonID, 'misi_id' => 1]);
            }
        });
    }
}
