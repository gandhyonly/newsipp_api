<?php

use App\Konsentrasi;
use Illuminate\Database\Seeder;

class KonsentrasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Konsentrasi::create(['nama_konsentrasi' => 'Pemograman', 'jurusan_id' => 1]);
    	Konsentrasi::create(['nama_konsentrasi' => 'Jaringan', 'jurusan_id' => 1]);
    	Konsentrasi::create(['nama_konsentrasi' => 'Analisa Bisnis', 'jurusan_id' => 2]);
    	Konsentrasi::create(['nama_konsentrasi' => 'Pemograman', 'jurusan_id' => 2]);
    	Konsentrasi::create(['nama_konsentrasi' => 'Jaringan', 'jurusan_id' => 3]);
    	Konsentrasi::create(['nama_konsentrasi' => 'Pemograman', 'jurusan_id' => 3]);
    }
}
