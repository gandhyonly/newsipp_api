<?php

use App\TandaKehormatan;
use Illuminate\Database\Seeder;

class TandaKehormatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Excel::load(public_path('migration/tanda_kehormatan.xlsx'), function($reader){
            foreach($reader->get() as $row){
                TandaKehormatan::create(['nama_tanda_kehormatan' => $row->tanda_kehormatan]);
            }
        });


    }
}
