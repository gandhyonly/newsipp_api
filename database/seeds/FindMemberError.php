<?php

use App\Member;
use App\MemberStatusForm;
use Illuminate\Database\Seeder;

class FindMemberError extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$i=0;
    	foreach(Member::where('member_id','<',3000)->orderBy('member_id','ASC')->get() as $member){
    		// if($member->tingkat>1 && $member->updated==false){
    		// 	$i++;
    		// 	echo $member->member_id."($member->nrp),tingkat=$member->tingkat => ".$member->statusForm->count()."\n";
    		// }
    		if($member->statusForm->count() < 20){
    			if(MemberStatusForm::where('member_id',$member->id)->count()<20){
    				MemberStatusForm::where('member_id',$member->id)->delete();
    				$i++;
    			}
    		}
    	}
    	echo "total = $i";
    }
}
