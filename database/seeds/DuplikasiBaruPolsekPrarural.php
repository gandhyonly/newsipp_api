<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiBaruPolsekPrarural extends Duplikasi
{
	function __construct()
	{
		$this->limit=7000;
		$this->tipeDuplikasi='POLSEK PRARURAL BARU';
		$this->sumberDuplikasi=7213576;//DUPLIKASI POLSEK PRARURAL
		$this->targetDuplikasi=Satuan::where('jenis_satuan_id',79)->get();
		// $this->targetDuplikasi=Satuan::where('satuan_id',7213576)->get();
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->duplication();
		Satuan::where('jenis_satuan_id',79)->update(['jenis_satuan_id'=>47]);
    }
}
