<?php

use App\Kota;
use Illuminate\Database\Seeder;

class KotaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        Excel::load(public_path('migration/kota.csv'), function($reader){
            foreach($reader->get() as $row){
                Kota::create(['nama_kota' => $row->nama_kota]);
            }
        });
    }
}
