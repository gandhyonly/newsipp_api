<?php

use App\Member;
use App\Personel;
use Illuminate\Database\Seeder;

class ConnectMemberToPersonel extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach(Member::whereNull('personel_id')->get() as $member){
    		if(Personel::where('nrp',$member->nrp)->count()==1){
    			$personel = Personel::where('nrp',$member->nrp)->first();
	    		$member->personel_id=$personel->personel_id;
	    		$member->save();
    		}
    	}
    }
}
