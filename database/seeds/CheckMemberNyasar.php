<?php

use App\Member;
use App\Satuan;
use Illuminate\Database\Seeder;

class CheckMemberNyasar extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$i=0;
    	$tempSatuan1='';
    	$tempSatuan2='';
    	foreach(Member::orderBy('satuan_1','ASC')->orderBy('satuan_2','ASC')->where('satuan_1','>=',11081)->where('satuan_1','<=',18041)->get() as $member){
    		if($member->satuan_1 == $tempSatuan1 && $member->satuan_2 != $tempSatuan2){
    			$satuanTemp1=Satuan::find($tempSatuan1);
    			$satuanTemp2=Satuan::find($tempSatuan2);
    			$satuan1=Satuan::find($member->satuan_1);
    			$satuan2=Satuan::find($member->satuan_2);
    			echo "$satuanTemp1->nama_satuan > $satuanTemp2->nama_satuan == ";
    			echo "$satuan1->nama_satuan ($satuan1->satuan_id) > $satuan2->nama_satuan ($satuan2->satuan_id)\n";
    			$i++;
    		}
    		$tempSatuan1=$member->satuan_1;
    		$tempSatuan2=$member->satuan_2;
    	}
    		echo $i;
    }
}
