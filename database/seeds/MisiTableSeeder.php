<?php

use App\Misi;
use Illuminate\Database\Seeder;

class MisiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Misi::create(['nama_misi' => 'Misi 1','kota' => 'JAKARTA','negara_id' => 83]);
    	Misi::create(['nama_misi' => 'Misi 2','kota' => 'JAKARTA','negara_id' => 83]);
    	Misi::create(['nama_misi' => 'Misi 3','kota' => 'JAKARTA','negara_id' => 83]);
    }
}
