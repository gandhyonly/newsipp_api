<?php

use App\TingkatPenghargaan;
use Illuminate\Database\Seeder;

class TingkatPenghargaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TingkatPenghargaan::create(['tingkat_penghargaan' => 'PRESIDEN']);
        TingkatPenghargaan::create(['tingkat_penghargaan' => 'KAPOLRI']);
        TingkatPenghargaan::create(['tingkat_penghargaan' => 'GUBERNUR']);
        TingkatPenghargaan::create(['tingkat_penghargaan' => 'BUPATI']);
    	TingkatPenghargaan::create(['tingkat_penghargaan' => 'KAPOLDA']);
    	TingkatPenghargaan::create(['tingkat_penghargaan' => 'KAPOLRES']);
    }
}
