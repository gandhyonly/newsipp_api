<?php

use App\Personel;
use App\PersonelKotor;
use Illuminate\Database\Seeder;

class MigrasiPersonelNamaIbu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach(Personel::get() as $personel){
    		$personelKotor = PersonelKotor::find($personel->personel_id);
    		if($personelKotor){
	    		$personel->nama_ibu=$personelKotor->nama_ibu;
	    		$personel->save();
	    		echo "$personel->personel_id";
    		}else {
    			echo "MASALAH (".$personel->personel_id.")\n";
    		}
    	}
    }
}
