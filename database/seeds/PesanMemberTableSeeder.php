<?php

use App\PesanMember;
use Illuminate\Database\Seeder;

class PesanMemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	PesanMember::create([
            'judul' => '<p>Judul Pesan</p>',
    		'pesan' => '<p>Pesan Percobaan</p>',
    		'verifikator_user_id' => 1,
    		'member_id' => 1,
    		'tingkat' => 1,
    	]);
    	PesanMember::create([
            'judul' => '<p>Judul Pesan 2</p>',
    		'pesan' => '<p>Pesan Percobaan 2</p>',
    		'verifikator_user_id' => 1,
    		'member_id' => 1,
    		'tingkat' => 1,
    	]);
    	PesanMember::create([
            'judul' => '<p>Judul Pesan 3</p>',
    		'pesan' => '<p>Pesan Percobaan 3</p><p>Pesan Percobaan 3</p><p>Pesan Percobaan 3</p>',
    		'verifikator_user_id' => 1,
    		'member_id' => 1,
    		'tingkat' => 1,
    	]);
    }
}
