<?php

use App\Personel;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PersonelBaruTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load(public_path('migration/personel/listpersonel-2.csv'), function($reader){
            $i=0;
            foreach($reader->get() as $row){
            	if(Personel::where('nrp',$row->nrp)->count() == 0){
			    	Personel::createPersonel([
			    		'nama_lengkap' => $row->nama_lengkap,
			    		'nrp' => $row->nrp,
			    		'tanggal_lahir' => Carbon::createFromFormat('Y-m-d', $row->tanggal_lahir)->toDateString(),
			    		'tmt_pertama' => Carbon::createFromFormat('Y-m-d', $row->tmt_pertama)->toDateString(),
                        'tmt_jabatan_pertama' => Carbon::createFromFormat('Y-m-d', $row->tmt_pertama)->toDateString(),
			    		'satker_pertama' => $row->satker_pertama
			    	]);
                    echo "$row->nrp\n";
            	} else {
            		echo "$i = $row->nrp \n";
            	}
            }
        });
    }
}
