<?php

use App\Other;
use App\OtherGroup;
use Illuminate\Database\Seeder;

class OtherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$group = OtherGroup::create(['name'=>'JENIS KELAMIN']); //1
    	Other::create(['value' => 'PRIA','other_group_id' => $group->id]);
    	Other::create(['value' => 'WANITA','other_group_id' => $group->id]);
    	$group = OtherGroup::create(['name'=>'GOLONGAN DARAH']); //2
        Other::create(['value' => 'A','other_group_id' => $group->id]);
        Other::create(['value' => 'B','other_group_id' => $group->id]);
        Other::create(['value' => 'AB','other_group_id' => $group->id]);
        Other::create(['value' => 'O','other_group_id' => $group->id]);
        Other::create(['value' => '-','other_group_id' => $group->id]);
        $group = OtherGroup::create(['name'=>'STATUS PERNIKAHAN']); //3
        Other::create(['value' => 'BELUM MENIKAH','other_group_id' => $group->id]);
        Other::create(['value' => 'DUDA','other_group_id' => $group->id]);
        Other::create(['value' => 'JANDA','other_group_id' => $group->id]);
        Other::create(['value' => 'MENIKAH','other_group_id' => $group->id]);
        $group = OtherGroup::create(['name'=>'WARNA KULIT']); //4
        Other::create(['value' => 'SAWO MATANG','other_group_id' => $group->id]);
        Other::create(['value' => 'PUTIH','other_group_id' => $group->id]);
        Other::create(['value' => 'KUNING','other_group_id' => $group->id]);
        Other::create(['value' => 'HITAM','other_group_id' => $group->id]);
        $group = OtherGroup::create(['name'=>'SUKU BANGSA']); //5:
            Excel::load(public_path('migration/suku_bangsa.csv'), function($reader) use ($group){
                foreach($reader->get() as $row){
                    Other::create(['value' => $row->suku_bangsa,'other_group_id' => $group->id]);
                }
            });
        $group = OtherGroup::create(['name'=>'JENIS RAMBUT']); //6
        Other::create(['value' => 'IKAL','other_group_id' => $group->id]);
        Other::create(['value' => 'LURUS','other_group_id' => $group->id]);
        Other::create(['value' => 'KERITING','other_group_id' => $group->id]);
        Other::create(['value' => 'GUNDUL','other_group_id' => $group->id]);
        $group = OtherGroup::create(['name'=>'WARNA RAMBUT']); //7
        Other::create(['value' => 'COKLAT','other_group_id' => $group->id]);
        Other::create(['value' => 'HITAM','other_group_id' => $group->id]);
        Other::create(['value' => 'KUNING','other_group_id' => $group->id]);
        $group = OtherGroup::create(['name'=>'WARNA MATA']); //8
        Other::create(['value' => 'COKLAT','other_group_id' => $group->id]);
        Other::create(['value' => 'HITAM','other_group_id' => $group->id]);
        Other::create(['value' => 'BIRU','other_group_id' => $group->id]);
        Other::create(['value' => 'HIJAU','other_group_id' => $group->id]);
        $group = OtherGroup::create(['name'=>'AKREDITASI']); //9
        Other::create(['value' => 'A','other_group_id' => $group->id]);
        Other::create(['value' => 'B','other_group_id' => $group->id]);
        Other::create(['value' => 'C','other_group_id' => $group->id]);
        $group = OtherGroup::create(['name'=>'STATUS KELUARGA']); //10
        Other::create(['value' => 'HIDUP','other_group_id' => $group->id]);
        Other::create(['value' => 'MENINGGAL','other_group_id' => $group->id]);

    }
}
