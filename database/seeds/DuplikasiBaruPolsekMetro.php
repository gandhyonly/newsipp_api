<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class DuplikasiBaruPolsekMetro extends Duplikasi
{
	function __construct()
	{
		$this->limit=7000;
		$this->tipeDuplikasi='POLSEK METRO BARU';
		$this->sumberDuplikasi=5182088;//DUPLIKASI POLSEK METRO
		$this->targetDuplikasi=Satuan::where('jenis_satuan_id',86)->get();
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->duplication();
		Satuan::where('jenis_satuan_id',86)->update(['jenis_satuan_id'=>44]);
    }
}
