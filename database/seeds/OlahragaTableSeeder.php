<?php

use App\Olahraga;
use Illuminate\Database\Seeder;

class OlahragaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Olahraga::create(['nama_olahraga' => 'Judo']);
    	Olahraga::create(['nama_olahraga' => 'Taekwondo']);
    	Olahraga::create(['nama_olahraga' => 'Sepak Bola']);
    	Olahraga::create(['nama_olahraga' => 'Bola Basket']);
    	Olahraga::create(['nama_olahraga' => 'Bulutangkis']);
    	Olahraga::create(['nama_olahraga' => 'Tennis']);
    	Olahraga::create(['nama_olahraga' => 'Tennis Meja']);
    	Olahraga::create(['nama_olahraga' => 'Renang']);
    	Olahraga::create(['nama_olahraga' => 'Lari']);
    	Olahraga::create(['nama_olahraga' => 'Marathon']);
    }
}
