<?php

use App\Fisik;
use App\HubunganKeluarga;
use App\KeluargaPersonel;
use App\PendidikanPersonel;
use App\Personel;
use App\PersonelData;
use App\RiwayatJabatan;
use App\RiwayatPangkat;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MigrasiPersonel2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$DBName='dbkotor';
    	$count=0;
    	$countPersonel=0;
    	$countDB=0;
  		// DB::statement("SET foreign_key_checks=0");
		// Personel::truncate();
		// PendidikanPersonel::truncate();
		// DB::statement("SET foreign_key_checks=1");
        $tempPersonelID='0a';
        $tempNrp='0a';
        $tempTanggalLahir='0a';
        $tempNamaLengkap='0a';
        $tempCountJabatan='0a';
		$countPersonelPerDB=0;
    	$errorAgama=false;
    	$countDB++;
    	echo "\n====================================================================";
    	echo "\nMigrasi Start";
    	echo "\n====================================================================\n";
        $daftarPersonel = DB::connection($DBName)->table('personel')
        ->select(
        	'personel_id',
        	'nama_lengkap',
        	'tempat_lahir',
        	'tanggal_lahir',
        	'jenis_kelamin',
        	'status_pernikahan',
        	'agama_id',
        	'satker_pertama',
        	'nama_ibu',
        	'tmt_pertama',
        	'tmt_jabatan_pertama',
        	'nrp',
        	'polisi',
        	'status_personel_id',
        	'database'
        )
        ->orderBy('nrp')
        ->orderBy('tanggal_lahir')
        ->orderBy('nama_lengkap')
        ->offset(423761)
        ->get();
        $totalPersonel = DB::connection($DBName)->table('personel')->count();
    	foreach($daftarPersonel as $key => $oldpersonel){
    		$countPersonelPerDB++;
    		$countPersonel++;
    		$this->show_status($countPersonelPerDB, $totalPersonel-423761, $size=30);

	        if(Personel::find($oldpersonel->personel_id)){
	        	continue;
	        }
	        $countJabatan = DB::connection($DBName)->table('riwayat_jabatan')
	        ->where('personel_id',$oldpersonel->personel_id)
	        ->count();


	        if($tempNrp==$oldpersonel->nrp && $tempTanggalLahir==$oldpersonel->tanggal_lahir){
	        	if($tempCountJabatan > $countJabatan){
	        		continue;
	        	} else {
	        		Personel::find($tempPersonelID)->delete();
	        	}
	        }
            $skip=false;
            $error=false;
            $agamaID=null;

    		$newpersonel = Personel::create([
                'personel_id' => $oldpersonel->personel_id,
                'nama_lengkap' => $oldpersonel->nama_lengkap,
                'tempat_lahir' => $oldpersonel->tempat_lahir,
                'tanggal_lahir' => $oldpersonel->tanggal_lahir,
                'jenis_kelamin' => $oldpersonel->jenis_kelamin,
                'tmt_pertama' => $oldpersonel->tmt_pertama,
                'tmt_jabatan_pertama' => $oldpersonel->tmt_pertama,
                'agama_id' => $oldpersonel->agama_id,
                'nrp' => $oldpersonel->nrp,
                'polisi' => $oldpersonel->polisi,
                'status_pernikahan' => $oldpersonel->status_pernikahan, 
                'status_personel_id' => $oldpersonel->status_personel_id, 
                'database' => $oldpersonel->database, 
    		]);
	        $personelData = DB::connection($DBName)->table('personel_data')
	        ->select('personel_id','nama_panggilan','handphone','golongan_darah','email','alamat_orang_tua','telepon_orang_tua','suku_bangsa','warna_kulit','jenis_rambut','warna_rambut','warna_mata','sidik_jari_1','sidik_jari_2','asabri_nomor','kta_nomor','ktp_nomor','npwp_nomor','kartu_keluarga_nomor','bpjs_nomor')
	        ->where('personel_id',$oldpersonel->personel_id)
	        ->first();

            PersonelData::create([
                'personel_id' => $personelData->personel_id,
                'nama_panggilan' => $personelData->nama_panggilan,
                'handphone' => $personelData->handphone,
                'golongan_darah' => $personelData->golongan_darah,
                'email' => $personelData->email,
                'alamat_orang_tua' => $personelData->alamat_orang_tua,
                'telepon_orang_tua' => $personelData->telepon_orang_tua,
                'suku_bangsa' => $personelData->suku_bangsa,
                'warna_kulit' => $personelData->warna_kulit,
                'jenis_rambut' => $personelData->jenis_rambut,
                'warna_rambut' => $personelData->warna_rambut,
                'warna_mata' => $personelData->warna_mata,
                'sidik_jari_1' => $personelData->sidik_jari_1,
                'sidik_jari_2' => $personelData->sidik_jari_2,
                'asabri_nomor' => $personelData->asabri_nomor,
                'kta_nomor' => $personelData->kta_nomor,
                'ktp_nomor' => $personelData->ktp_nomor,
                'npwp_nomor' => $personelData->npwp_nomor,
                'kartu_keluarga_nomor' => $personelData->kartu_keluarga_nomor,
                'bpjs_nomor' => $personelData->bpjs_nomor
            ]);

	        $personelFisik = DB::connection($DBName)->table('fisik_personel')
	        ->select('personel_id','tinggi','berat','ukuran_topi','ukuran_sepatu','ukuran_celana','ukuran_baju')
	        ->where('personel_id',$oldpersonel->personel_id)
	        ->first();
            Fisik::create([
                'personel_id' => $personelFisik->personel_id,
                'tinggi' => $personelFisik->tinggi,
                'berat' => $personelFisik->berat,
                'ukuran_topi' => $personelFisik->ukuran_topi,
                'ukuran_sepatu' => $personelFisik->ukuran_sepatu,
                'ukuran_celana' => $personelFisik->ukuran_celana,
                'ukuran_baju' => $personelFisik->ukuran_baju
            ]);
            // Keluarga
		        $keluarga = DB::connection($DBName)->table('keluarga_personel')
		        ->select('personel_id','hubungan_keluarga_id','nama_keluarga','jenis_kelamin','tanggal_lahir','status')
		        ->where('personel_id',$oldpersonel->personel_id)
		        ->get();
		        foreach($keluarga as $anggotaKeluarga){
		        	$skip=false;
			        $hubungan = DB::connection($DBName)->table('hubungan_keluarga')
			        ->select('hubungan')
			        ->where('hubungan_keluarga_id',$anggotaKeluarga->hubungan_keluarga_id)
			        ->first()->hubungan;
		        	switch($hubungan){
		        		case 'SAUDARA KANDUNG' : $hubunganKeluargaID = 1; break; 
		        		case 'SAUDARA' : $hubunganKeluargaID = 1; break; 
		        		case 'AYAH' : $hubunganKeluargaID = 2; break; 
		        		case 'AYAH KANDUNG' : $hubunganKeluargaID = 2; break; 
		        		case 'IBU' : $hubunganKeluargaID = 3; break; 
		        		case 'IBU KANDUNG' : $hubunganKeluargaID = 3; break; 
		        		case 'AYAH MERTUA' : $hubunganKeluargaID = 4; break; 
		        		case 'IBU MERTUA' : $hubunganKeluargaID = 5; break; 
		        		case 'AYAH ANGKAT' : $hubunganKeluargaID = 6; break; 
		        		case 'IBU ANGKAT' : $hubunganKeluargaID = 7; break; 
		        		case 'AYAH TIRI' : $hubunganKeluargaID = 8; break; 
		        		case 'IBU TIRI' : $hubunganKeluargaID = 9; break; 
		        		case 'ANAK TIRI' : $hubunganKeluargaID = 10; break; 
		        		case 'ANAK' : $hubunganKeluargaID = 11; break; 
		        		case 'ANAK KANDUNG' : $hubunganKeluargaID = 11; break; 
		        		case 'ANAK ANGKAT' : $hubunganKeluargaID = 12; break; 
		        		case 'SUAMI' : $hubunganKeluargaID = 13; break; 
		        		case 'ISTRI' : $hubunganKeluargaID = 14; break; 
		        		default : $skip = true; break; 
		        	}
		        	if($skip){
			            $keluargaPersonel =KeluargaPersonel::create([
			            	'personel_id' => $oldpersonel->personel_id,
			            	'hubungan_keluarga_id' => $anggotaKeluarga->hubungan_keluarga_id,
			            	'nama_keluarga' => strtoupper($anggotaKeluarga->nama_keluarga),
			            	'jenis_kelamin' => strtoupper($anggotaKeluarga->jenis_kelamin),
			            	'tanggal_lahir' => $anggotaKeluarga->tanggal_lahir,
			            	'status' => strtoupper($anggotaKeluarga->status),
			            ]);
		        	}
		        }
		    // End Keluarga
		    // Pendidikan Umum
		        $riwayatDikum = DB::connection($DBName)->table('pendidikan_personel')
		        ->select(
		        	'personel_id','dikum_id','tanggal_lulus','nama_institusi','surat_kelulusan_nomor'
		        )
		        ->where('personel_id',$oldpersonel->personel_id)
		        ->get();
		        foreach($riwayatDikum as $pendidikanUmum){
		            $keluargaPersonel = PendidikanPersonel::create([
		            	'personel_id' => $oldpersonel->personel_id,
		            	'dikum_id' => $pendidikanUmum->dikum_id,
		            	'tanggal_lulus' => $pendidikanUmum->tanggal_lulus,
		            	'nama_institusi' => strtoupper($pendidikanUmum->nama_institusi),
		            	'surat_kelulusan_nomor' => strtoupper($pendidikanUmum->surat_kelulusan_nomor),
		            ]);
		        }
		    // End Pendidikan Umum
	        // Riwayat Jabatan
		        $oldRiwayatJabatan = DB::connection($DBName)->table('riwayat_jabatan')
		        ->select('personel_id','keterangan','tmt_jabatan','surat_keputusan_nomor')
		        ->where('personel_id',$oldpersonel->personel_id)
		        ->get();
		        foreach($oldRiwayatJabatan as $riwayatJabatan){
			        $jabatan = RiwayatJabatan::create([
			        	'personel_id' => $oldpersonel->personel_id,
			        	'keterangan' => $riwayatJabatan->keterangan,
			        	'tmt_jabatan' => $riwayatJabatan->tmt_jabatan,
			        	'surat_keputusan_nomor' => $riwayatJabatan->surat_keputusan_nomor
			        ]);
		        }
	        // End Riwayat Jabatan
		    // Riwayat Pangkat
		        $oldRiwayatPangkat = DB::connection($DBName)->table('riwayat_pangkat')
		        ->select('personel_id','pangkat_id','tmt_pangkat','surat_keputusan_nomor')
		        ->where('personel_id',$oldpersonel->personel_id)
		        ->get();
		        foreach($oldRiwayatPangkat as $riwayatPangkat){
			        $jabatan = RiwayatPangkat::create([
			        	'personel_id' => $newpersonel->personel_id,
			        	'pangkat_id' => $riwayatPangkat->pangkat_id,
			        	'tmt_pangkat' => $riwayatPangkat->tmt_pangkat,
			        	'surat_keputusan_nomor' => $riwayatPangkat->surat_keputusan_nomor
			        ]);
		        }
		    // End Riwayat Pangkat

       		$tempPersonelID=$oldpersonel->personel_id;
	        $tempNrp=$oldpersonel->nrp;
	        $tempTanggalLahir=$oldpersonel->tanggal_lahir;
	        $tempNamaLengkap=$oldpersonel->nama_lengkap;
	        $tempCountJabatan=$countJabatan;
    	} //end foreach personel
    	echo "=================================================================================\n";
	    echo "MIGRASI SELURUH PERSONEL SELESAI. Jumlah Personel=$countPersonel\n";

    }

    private function show_status($done, $total, $size=30) {
	    static $start_time;

	    // if we go over our bound, just ignore it
	    if($done > $total) return;

	    if(empty($start_time)) $start_time=time();
	    $now = time();

	    $perc=(double)($done/$total);

	    $bar=floor($perc*$size);

	    $status_bar="\r[";
	    $status_bar.=str_repeat("=", $bar);
	    if($bar<$size){
	        $status_bar.=">";
	        $status_bar.=str_repeat(" ", $size-$bar);
	    } else {
	        $status_bar.="=";
	    }

	    $disp=number_format($perc*100, 0);

	    $status_bar.="] $disp%  $done/$total";

	    $rate = ($now-$start_time)/$done;
	    $left = $total - $done;
	    $eta = round($rate * $left, 2);

	    $elapsed = $now - $start_time;

	    $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

	    echo "$status_bar  ";

	    flush();

	    // when done, send a newline
	    if($done == $total) {
	        echo "\n";
	    }

	}
}
