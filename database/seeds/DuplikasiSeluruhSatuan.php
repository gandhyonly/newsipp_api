<?php

use Illuminate\Database\Seeder;

class DuplikasiSeluruhSatuan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(DuplikasiPoldaA::class);
        // $this->call(DuplikasiPoldaB::class);
        // $this->call(DuplikasiPoldaAKhusus::class);
        // $this->call(DuplikasiPolres::class);
        $this->call(DuplikasiPolresMetro::class);
        $this->call(DuplikasiPolresta::class);
        $this->call(DuplikasiPolrestabes::class);
        $this->call(DuplikasiPolsekMetro::class);
        $this->call(DuplikasiPolsekUrban::class);
        $this->call(DuplikasiPolsekRural::class);
        $this->call(DuplikasiPolsekPrarural::class);
        $this->call(DuplikasiPolsekSubsektor::class);
    }
}
