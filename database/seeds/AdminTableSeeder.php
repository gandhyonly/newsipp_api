<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Admin::create(['nrp'=>'superadmin', 'password' => bcrypt('superadmin'),'api_token'=>'superadmin']);
    }
}
