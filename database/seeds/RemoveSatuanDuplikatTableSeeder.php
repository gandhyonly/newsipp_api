<?php

use App\Member;
use App\Satuan;
use App\VerifikatorUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RemoveSatuanDuplikatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$i=0;
    	$tempNama='';
    	$tempID='';
    	$tempParentID='';
    	$dataSatuanDuplikat = Satuan::select('satuan_id','nama_satuan','parent_id')->where('nama_satuan','LIKE','POLSEK%')->orderBy('nama_satuan','ASC')->orderBy('parent_id','ASC')->orderBy('satuan_id','ASC')->get();
    	foreach($dataSatuanDuplikat as $satuan){
    		echo "Satuan $satuan->satuan_id ($satuan->nama_satuan) $satuan->parent_id -> ";
    		if($tempNama == $satuan->nama_satuan && $tempParentID == $satuan->parent_id){
    			Member::where('satuan_1',$satuan->satuan_id)->update(['satuan_1' => $tempID]);
    			VerifikatorUser::where('satuan_id',$satuan->satuan_id)->update(['satuan_id' => $tempID]);
    			echo "member dan verifikator terhapus -> ";
    			$satuan->delete();
    			$i++;
    			echo "satuan terhapus\n";
    		} else {
    			$tempID=$satuan->satuan_id;
    		}
    		$tempParentID=$satuan->parent_id;
    		$tempNama=$satuan->nama_satuan;
			echo "satuan tidak kembar\n";

    	}
    	echo "Jumlah Satuan Kembar: $i";
    }
}
