<?php

use Illuminate\Database\Seeder;

class DuplikasiKapal extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DuplikasiKapalA1::class);
        $this->call(DuplikasiKapalA2::class);
        $this->call(DuplikasiKapalA3::class);
        $this->call(DuplikasiKapalB1::class);
        $this->call(DuplikasiKapalB2::class);
        $this->call(DuplikasiKapalB3::class);
        $this->call(DuplikasiKapalC1::class);
        $this->call(DuplikasiKapalC2::class);
        $this->call(DuplikasiKapalC3::class);
    }
}
