<?php

use App\Dikbangspes;
use App\TipeDikbangspes;
use Illuminate\Database\Seeder;

class DikbangspesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load(public_path('migration/dikbangspes.xlsx'), function($reader){
            foreach($reader->get() as $row){
            	$tipe = TipeDikbangspes::where('tipe',$row->tipe_dikbangspes)->first();
            	if($tipe==null){
            		$tipe = TipeDikbangspes::create(['tipe' => $row->tipe_dikbangspes]);
            	}
            	Dikbangspes::create(['nama_dikbangspes' => $row->nama_dikbangspes,'tipe_dikbangspes_id' => $tipe->tipe_dikbangspes_id]);
            }
        });
    }
}
