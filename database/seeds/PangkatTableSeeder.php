<?php

use App\Golongan;
use App\Pangkat;
use Illuminate\Database\Seeder;

class PangkatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load(public_path('migration/pangkat.csv'), function($reader){
            foreach($reader->get() as $row){
                $pangkat = Pangkat::create([
                    'pangkat_id'          => $row->pangkat_id,
                    'nama_pangkat'          => $row->nama_pangkat,
                    'singkatan'             => $row->singkatan,
                    'singkatan_tentara'     => $row->singkatan_tentara,
                    'jenis'                 => $row->jenis,
                    'golongan_id'           => $row->golongan_id == "NULL" ? null :$row->golongan_id,
                ]);
            }
        });
    }
}
