<?php

use App\JenisBKO;
use Illuminate\Database\Seeder;

class JenisBKOTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	JenisBKO::create(['nama_jenis_bko' => 'Jenis BKO 1']);
    	JenisBKO::create(['nama_jenis_bko' => 'Jenis BKO 2']);
    	JenisBKO::create(['nama_jenis_bko' => 'Jenis BKO 3']);
    	JenisBKO::create(['nama_jenis_bko' => 'Jenis BKO 4']);
    	JenisBKO::create(['nama_jenis_bko' => 'Jenis BKO 5']);
    }
}
