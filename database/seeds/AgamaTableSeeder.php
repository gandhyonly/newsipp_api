<?php

use App\Agama;
use Illuminate\Database\Seeder;

class AgamaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(Agama::count()==0){
            Agama::create(['nama'=>'ISLAM']);
            Agama::create(['nama'=>'KRISTEN']);
            Agama::create(['nama'=>'KATHOLIK']);
            Agama::create(['nama'=>'HINDU']);
            Agama::create(['nama'=>'BUDDHA']);
            Agama::create(['nama'=>'KONG HU CHU']);
            Agama::create(['nama'=>'ALIRAN KEPERCAYAAN']);
        }
    }
}
