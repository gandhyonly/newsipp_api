<?php

use App\OldPersonel;
use App\Personel;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PersonelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tempNrp='0a';

        $query = DB::connection('local_ssdm')->table('t_personil')
        ->join('t_jabatan', 't_personil.ref_jabatan', '=', 't_jabatan.id')
        ->select(
            't_personil.c_nrp',
            't_personil.ref_agama',
            't_personil.c_nama',
            't_personil.c_kelamin',
            't_personil.c_tgllahir',
            't_personil.c_alamatKantor',
            't_personil.c_telpKantor',
            't_personil.c_telpRumah',
            't_personil.c_telpHP',
            't_personil.c_email',
            't_personil.c_tinggi',
            't_personil.c_berat',
            't_personil.c_kulit',
            't_personil.c_mata',
            't_personil.c_darah',
            't_personil.c_topi',
            't_personil.c_celana',
            't_personil.c_baju',
            't_personil.c_sepatu',
            't_personil.c_sidik1',
            't_personil.c_sidik2',
            't_personil.c_jenisRambut',
            't_personil.c_warnaRambut',
            't_personil.c_asabri',
            't_personil.c_bpjsKes',
            't_personil.c_npwp',
            't_personil.c_karkes',
            't_personil.c_kta',
            't_personil.c_tmtjabatan',
            't_personil.c_alamatOrtu',
            't_personil.c_telpOrtu',
            't_personil.c_status',
            't_personil.c_tmplahir',
            't_jabatan.c_jabatan as c_jabatan'
        )
        ->orderBy('c_nrp')
        ->limit(100);
    	foreach($query->get() as $oldpersonel){
            $skip=false;
            $error=false;
            $errorMessage='';

            // validasi error 
            // validasi nama dengan gelar
            if(!$this->validasiNamaLengkapMemilikiGelar($oldpersonel->c_nama)){
                $error=true;
                $errorMessage.='Nama Lengkap Memiliki Gelar.<br>';
            }
            // validasi digit NRP 8 dan 18 digit.
            if(!$this->validasiDigitNRP($oldpersonel->c_nrp)){
                $error=true;
                $errorMessage.="Format nrp tidak benar. jumlah angka nrp:".strlen($oldpersonel->c_nrp).'<br>';
            }


    		// validasi agama
    		if(!($oldpersonel->ref_agama >=1 && $oldpersonel->ref_agama<=5)){
    			$error=true;
    			$errorMessage.="data ref_agama salah. data ref_agama:".$oldpersonel->ref_agama.'<br>';
    		}

            if($oldpersonel->c_tgllahir != null){
                //validasi tanggal lahir
                $d = DateTime::createFromFormat('Y-m-d', $oldpersonel->c_tgllahir);
                if($d != null){
                    $valid = $d && $d->format('Y-m-d') == $oldpersonel->c_tgllahir;
                    if (!$valid) 
                    {
                        $error=true;
                        $errorMessage.="Format tanggal lahir salah. Tanggal Lahir :".$oldpersonel->c_tgllahir->format('d-m-Y');
                        $tanggal_lahir = null;
                    }
                    else{
                        $tanggal_lahir = $oldpersonel->c_tgllahir;
                    }
                }

                //validasi format nrp dan tanggal lahir (yymmxxxx)
                if(!$this->validasiFormatNRPdanTanggalLahir($oldpersonel->c_nrp,$oldpersonel->c_tgllahir)){
                    $error=true;
                    $errorMessage.="Format NRP dan tanggal lahir tidak sama. ";
                    $errorMessage.="Tanggal Lahir :".$oldpersonel->c_tgllahir.'<br>';
                }
            }

            //validasi tanggal tmt_jabatan
            if($oldpersonel->c_tmtjabatan != null){
                $d = DateTime::createFromFormat('Y-m-d', $oldpersonel->c_tmtjabatan);
                if($d != null){
                    $valid = $d && $d->format('Y-m-d') == $oldpersonel->c_tmtjabatan;
                    if (!$valid) 
                    {
                        $error=true;
                        $errorMessage.="Format tanggal tmt_jabatan salah. Tanggal Tmt Jabatan:".$oldpersonel->c_tmtjabatan->format('d-m-Y');
                        $tmt_jabatan = null;
                    }
                    else{
                        $tmt_jabatan = $oldpersonel->c_tmtjabatan;
                    }
                }
            }
            if(strlen($oldpersonel->c_nrp) == 18){
                $polisi=false;
            } elseif(strlen($oldpersonel->c_nrp) == 8) {
                $polisi=true;
            } else {
                $skip=true;
            }
            $statusPersonel = 1;
            if($oldpersonel->c_status==1){
                $status=1;
            } else if($oldpersonel->c_status==2){
                $status=4;
            } else if($oldpersonel->c_status==3){
                $skip=true;
            } else if($oldpersonel->c_status==4){
                $status=2;
            } else if($oldpersonel->c_status==5){
                $status=8; // MENINGGAL
            } else if($oldpersonel->c_status==6){
                $status=3;
            }

            if($skip){
                continue;
            }

    		$newpersonel = Personel::create([
                'nama_lengkap' => $oldpersonel->c_nama,
                'tempat_lahir' => $oldpersonel->c_tgllahir,
                'tanggal_lahir' => $oldpersonel->c_tmplahir,
                'jenis_kelamin' => $oldpersonel->c_kelamin,
                'agama_id' => $oldpersonel->ref_agama,
                'nrp' => $oldpersonel->c_nrp,
                'polisi' => $polisi,
                'status_personel_id' => $status, 
    			'agama_id' => $oldpersonel->ref_agama,
                'jenis_kelamin_id' => $oldpersonel->c_kelamin,

    			'alamat_kantor' => $oldpersonel->c_alamatKantor,
    			'telepon_kantor' => $oldpersonel->c_telpKantor,
    			'telepon_rumah' => $oldpersonel->c_telpRumah,
    			'handphone' => $oldpersonel->c_telpHP,
    			'email' => $oldpersonel->c_email,

    			'gol_darah' => $oldpersonel->c_darah,

    			'jenis_rambut' => $oldpersonel->c_jenisRambut,
    			'warna_rambut' => $oldpersonel->c_warnaRambut,

    		]);

            PersoneLData::create([
                'personel_id' => $newpersonel->personel_id,
                'nama_panggilan' => $oldpersonel->c_nama,
                'handphone' => $oldpersonel->c_telpHP,
                'golongan_darah' => $oldpersonel->c_darah,
                'email' => $oldpersonel->c_email,
                'alamat_orang_tua' => $oldpersonel->c_alamatOrtu,
                'telepon_orang_tua' => $oldpersonel->c_telpOrtu,
                'suku_bangsa' => $oldpersonel->c_sukubangsa,
                'warna_kulit' => $oldpersonel->c_kulit,
                'jenis_rambut' => $oldpersonel->c_jenisRambut,
                'warna_rambut' => $oldpersonel->c_warnaRambut,
                'warna_mata' => $oldpersonel->c_mata,
                'sidik_jari_1' => $oldpersonel->c_sidik1,
                'sidik_jari_2' => $oldpersonel->c_sidik2,
                'asabri_nomor' => $oldpersonel->c_asabri,
                'kta_nomor' => $oldpersonel->c_kta,
                'ktp_nomor' => $oldpersonel->c_ktp,
                'npwp_nomor' => $oldpersonel->c_npwp,
                'kartu_keluarga_nomor' => $oldpersonel->c_noKK,
                'bpjs_nomor' => $oldpersonel->c_bpjsKes
            ]);

            PersonelFisik::create([
                'personel_id' => $newpersonel->personel_id,
                'tinggi' => $oldpersonel->c_tinggi,
                'berat' => $oldpersonel->c_berat,
                'ukuran_topi' => $oldpersonel->c_topi,
                'ukuran_sepatu' => $oldpersonel->c_sepatu,
                'ukuran_celana' => $oldpersonel->c_celana,
                'ukuran_baju' => $oldpersonel->c_baju
            ]);

    	}
    }

    private function validasiNamaLengkapMemilikiGelar($nama)
    {
        if(preg_match_all("/[.,]+/", $nama)){
            return false;
        }
        return true;
    }
    private function validasiDigitNRP($nrp)
    {
        if(strlen($nrp)==8 || strlen($nrp)==18){
            return true;
        }
        return false;
    }

    private function validasiFormatNRPdanTanggalLahir($nrp,$tanggalLahir)
    {
        if($tanggalLahir == null){
            $d = DateTime::createFromFormat('Y-m-d', $tanggalLahir);
            $nrpFormatDate = substr($nrp,0,4);
            if($d){
                $tempDate = substr($d->format('Ym'),2,4);
                if($tempDate == $nrpFormatDate){
                    return true;
                }
            }
        }
        return false;
    }
}
