<?php

use Illuminate\Database\Seeder;
use App\Personel;
use Carbon\Carbon;

class ImportPersonelUpdateTamtama extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load(public_path('migration/personel/personel-patma-tamtama-2017.csv'), function($reader){
            $i=0;
            foreach($reader->get() as $row){
            	$personel = Personel::where('nrp',$row->nrp)->first();
            	if($personel){
            		$personel->update([
			    		'nama_lengkap' => $row->nama_lengkap,
			    		'tanggal_lahir' => Carbon::createFromFormat('Y-m-d', $row->tanggal_lahir)->toDateString(),
			    		'tmt_pertama' => Carbon::createFromFormat('Y-m-d', $row->tmt_pertama)->toDateString(),
                        'tmt_jabatan_pertama' => Carbon::createFromFormat('Y-m-d', $row->tmt_pertama)->toDateString(),
			    		'satker_pertama' => $row->satker_pertama
			    	]);
            	} else {
            		$i++;
            		echo "$i = $row->nrp \n";
            	}
            }
        });
    }
}
