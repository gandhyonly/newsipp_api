<?php

use Illuminate\Database\Seeder;
use App\Jurusan;

class JurusanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Jurusan::create(['nama_jurusan' => 'Teknik Informatika']);
        Jurusan::create(['nama_jurusan' => 'Sistem Informasi']);
        Jurusan::create(['nama_jurusan' => 'Sistem Komputer']);
        Jurusan::create(['nama_jurusan' => 'Keamanan Jaringan']);
    }
}
