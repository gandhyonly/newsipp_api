<?php

use App\StatusPersonel;
use Illuminate\Database\Seeder;

class StatusPersonelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusPersonel::create(['status' => 'AKTIF', 'status_aktif' => true]);
        StatusPersonel::create(['status' => 'PENDIDIKAN', 'status_aktif' => true]);
        StatusPersonel::create(['status' => 'INTELSUS', 'status_aktif' => true]);
        StatusPersonel::create(['status' => 'PENSIUN', 'status_aktif' => false]);
        StatusPersonel::create(['status' => 'PENSIUN DINI', 'status_aktif' => false]);
        StatusPersonel::create(['status' => 'PDH', 'status_aktif' => false]);
        StatusPersonel::create(['status' => 'PTDH', 'status_aktif' => false]);
        StatusPersonel::create(['status' => 'MENINGGAL', 'status_aktif' => false]);
    }
}
