<?php

use App\Satuan;
use Illuminate\Database\Seeder;

class CreateSatuanPolsekBaru extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Excel::load(public_path('migration/create_satuan_baru.csv'), function($reader){
            foreach($reader->get() as $row){
            	if(Satuan::where('nama_satuan',$row->nama_parent)->count() == 1){
            		// $satuanParent = Satuan::where('nama_satuan',$row->nama_parent)->first();
            		// echo "$row->nama_satuan > $row->nama_parent\n";
	            	// $satuanBaru = Satuan::create(['nama_satuan' => $row->nama_satuan,'jenis_satuan_id' => 77, 'parent_id' => $satuanParent->satuan_id]);
	            	// $satuanBaru->posisi=$satuanBaru->satuan_id;
	            	// $satuanBaru->save();
            	} else if(Satuan::where('nama_satuan',$row->nama_parent)->count() > 1){
            		echo "===================================== $row->nama_satuan > $row->nama_parent\n";
            	} else {
            		echo "Aneh\n";
            	}
            }
        });
    }
}
