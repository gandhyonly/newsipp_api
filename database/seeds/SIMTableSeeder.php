<?php

use App\SIM;
use Illuminate\Database\Seeder;

class SIMTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	SIM::create(['sim_nama' => 'SIM A']);
    	SIM::create(['sim_nama' => 'SIM B']);
        SIM::create(['sim_nama' => 'SIM C']);
        SIM::create(['sim_nama' => 'SIM D']);
    	SIM::create(['sim_nama' => 'SIM INTERNATIONAL']);
    }
}
