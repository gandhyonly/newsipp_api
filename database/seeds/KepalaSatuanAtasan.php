<?php

use App\Satuan;
use App\SatuanJabatan;
use Illuminate\Database\Seeder;

class KepalaSatuanAtasan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arrSatuanDuplikasi = Satuan::where('jenis_satuan_id',58)->pluck('satuan_id')->toArray();
        $kelompokSatuanJabatan = SatuanJabatan::where('satuan_jabatan_id','!=',26)->whereNotIn('satuan_id',$arrSatuanDuplikasi)->whereIn('jabatan_id',[145,809])->get();
        foreach($kelompokSatuanJabatan as $satuanJabatan){
            echo "$satuanJabatan->satuan_jabatan_id ";
            $satuan = Satuan::find($satuanJabatan->satuan_id);
            echo "$satuan->nama_satuan";
            $satuanJabatanAtasan = SatuanJabatan::where('satuan_id',$satuan->parent_id)->orderBy('nivelering_id','ASC')->first();
            $jabatan=$satuanJabatan->jabatan->nama_jabatan;
            echo "$jabatan\n";
            $satuanJabatan->parent_satuan_jabatan_id=$satuanJabatanAtasan->satuan_jabatan_id;
            $satuanJabatan->save();
        }
    }

}
