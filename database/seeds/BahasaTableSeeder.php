<?php

use App\Bahasa;
use Illuminate\Database\Seeder;

class BahasaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Bahasa::create(['bahasa' => 'Inggris', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Indonesia', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Mandarin', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Jepang', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Korea', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Perancis', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Belanda', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Arab', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Jerman', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Spanyok', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Portugis', 'lokal' => false]);
    	Bahasa::create(['bahasa' => 'Melayu', 'lokal' => false]);


    	Bahasa::create(['bahasa' => 'Sunda', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Jawa', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Madura', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Batak', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Minangkabau', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Bugis', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Aceh', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Bali', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Banjar', 'lokal' => true]);
    	Bahasa::create(['bahasa' => 'Alor', 'lokal' => true]);
    }
}