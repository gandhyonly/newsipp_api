<?php

use App\Jabatan;
use Illuminate\Database\Seeder;

class HapusJabatanKosong extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach(Jabatan::get() as $jabatan){
    		if($jabatan->satuanJabatan()->count()==0){
    			$jabatan->delete();
    		}
    	}
    }
}
