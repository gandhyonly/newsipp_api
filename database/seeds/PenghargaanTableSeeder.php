<?php

use App\Penghargaan;
use Illuminate\Database\Seeder;

class PenghargaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Penghargaan::create(['nama_penghargaan' => 'KPLB']);
    	Penghargaan::create(['nama_penghargaan' => 'KPLBA']);
    	Penghargaan::create(['nama_penghargaan' => 'PROMOSI MENGIKUTI PENDIDIKAN']);
    	Penghargaan::create(['nama_penghargaan' => 'PROMOSI JABATAN']);
        Penghargaan::create(['nama_penghargaan' => 'TANDA PENGHARGAAN']);
        Penghargaan::create(['nama_penghargaan' => 'PIN PERAK']);
        Penghargaan::create(['nama_penghargaan' => 'PIN PERUNGGU']);
    	Penghargaan::create(['nama_penghargaan' => 'PIN EMAS']);
    }
}
