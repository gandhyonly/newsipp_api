<?php

use App\Psikologi;
use Illuminate\Database\Seeder;

class PsikologiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Psikologi::create(['jenis' => 'Psikologi 1']);
    	Psikologi::create(['jenis' => 'Psikologi 2']);
    	Psikologi::create(['jenis' => 'Psikologi 3']);
    	Psikologi::create(['jenis' => 'Psikologi 4']);
    }
}
