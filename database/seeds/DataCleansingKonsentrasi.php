<?php

use App\Konsentrasi;
use App\PendidikanPersonel;
use Illuminate\Database\Seeder;

class DataCleansingKonsentrasi extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$tempID='567846a7345623';
    	$tempJurusan='567846a7345623';
    	$tempNamaKonsentrasi='567846a7345623';
    	foreach(Konsentrasi::orderBy('jurusan_id','nama_konsentrasi','konsentrasi_id')->get() as $konsentrasi){
    		if($konsentrasi->nama_konsentrasi==$tempNamaKonsentrasi && $konsentrasi->jurusan_id == $tempJurusan){
    			PendidikanPersonel::where('konsentrasi_id',$konsentrasi->konsentrasi_id)->update(['konsentrasi_id'=> $tempID]);
    			$konsentrasi->delete();
    		} else {
		    	$tempID=$konsentrasi->konsentrasi_id;
		    	$tempJurusan=$konsentrasi->jurusan_id;
		    	$tempNamaKonsentrasi=$konsentrasi->nama_konsentrasi;
    		}
    	}
    }
}
