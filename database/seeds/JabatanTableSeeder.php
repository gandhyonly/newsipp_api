<?php

use App\Jabatan2;
use App\Jabatan;
use Illuminate\Database\Seeder;

class JabatanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Jabatan2::get() as $jabatan){
            if(Jabatan::find($jabatan->jabatan_id)==null){
                Jabatan::create([
                    'jabatan_id' => $jabatan->jabatan_id,
                    'nama_jabatan' => $jabatan->nama_jabatan,
                ]);
            }
        }
    }
}
