<?php

use Illuminate\Database\Seeder;

class DuplikasiSeluruhPolres extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DuplikasiPolres::class);
        $this->call(DuplikasiPolresMetro::class);
        $this->call(DuplikasiPolresta::class);
        $this->call(DuplikasiPolrestabes::class);
    }
}
