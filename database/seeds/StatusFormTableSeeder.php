<?php

use App\StatusForm;
use Illuminate\Database\Seeder;

class StatusFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	StatusForm::create(['status_form_id' => 1,'name' => 'Form Biodata']);
    	StatusForm::create(['status_form_id' => 2,'name' => 'Form Nomor Identitas']);
    	StatusForm::create(['status_form_id' => 3,'name' => 'Form Kondisi Fisik']);
    	StatusForm::create(['status_form_id' => 4,'name' => 'Form Keluarga']);
    	StatusForm::create(['status_form_id' => 5,'name' => 'Form Pendidikan Umum']);
    	StatusForm::create(['status_form_id' => 6,'name' => 'Form Pendidikan Polisi']);
    	StatusForm::create(['status_form_id' => 7,'name' => 'Form Pendidikan Kejuruan']);
    	StatusForm::create(['status_form_id' => 8,'name' => 'Form Tanda Kehormatan']);
    	StatusForm::create(['status_form_id' => 9,'name' => 'Form Kemampuan Bahasa Asing']);
    	StatusForm::create(['status_form_id' => 10,'name' => 'Form Kemampuan Bahasa Daerah']);
        StatusForm::create(['status_form_id' => 11,'name' => 'Form Olahraga']);
    	StatusForm::create(['status_form_id' => 12,'name' => 'Form Hobi']);
    	StatusForm::create(['status_form_id' => 13,'name' => 'Form Riwayat Jabatan']);
    	StatusForm::create(['status_form_id' => 14,'name' => 'Form Riwayat Kepangkatan']);
    	StatusForm::create(['status_form_id' => 15,'name' => 'Form Penugasan Luar Struktur']);
        StatusForm::create(['status_form_id' => 16,'name' => 'Form Ops Kepolisian (BKO)']);
        // StatusForm::create(['status_form_id' => 17,'name' => 'Form Psikologi']);
        // StatusForm::create(['status_form_id' => 18,'name' => 'Form Kesehatan']);
    	StatusForm::create(['status_form_id' => 19,'name' => 'Form Penghargaan']);
        StatusForm::create(['status_form_id' => 20,'name' => 'Form Brevet']);
        StatusForm::create(['status_form_id' => 21,'name' => 'Form Riwayat Alamat']);
    	StatusForm::create(['status_form_id' => 22,'name' => 'Form Penugasan Luar Negeri & Dalam Negeri']);
    }
}
