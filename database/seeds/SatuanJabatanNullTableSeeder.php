<?php

use App\SatuanJabatan2;
use App\SatuanJabatan;
use Illuminate\Database\Seeder;

class SatuanJabatanNullTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$notIn=[26,123801,160817,160864,160906,161680,160985,162348,163264,163544,163358,163425,163659,163702,163714,163725,163734,7136, 7162, 7180, 7210, 7211, 7212, 7252, 7253, 7254];
    	$noob=SatuanJabatan2::whereNull('parent_satuan_jabatan_id')->whereNotIn('satuan_jabatan_id',$notIn)->pluck('satuan_jabatan_id')->toArray();
    	$count=1;
    	foreach($noob as $fak){
	    	$satuan = SatuanJabatan2::find($fak);
	    	SatuanJabatan::create([
	    		'satuan_jabatan_id' => $satuan->satuan_jabatan_id,
	    		'keterangan' => $satuan->keterangan,
	    		'dsp' => $satuan->dsp,
	    		'parent_satuan_jabatan_id' => $satuan->parent_satuan_jabatan_id,
	    		'jabatan_id' => $satuan->jabatan_id,
	    		'satuan_id' => $satuan->satuan_id,
	    		'status_jabatan_id' => $satuan->status_jabatan_id,
	    		'nivelering_id' => $satuan->nivelering_id
	    	]);
	    	echo $count++;
    	}


    }
}
