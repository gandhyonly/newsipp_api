<?php

use App\JenisPekerjaan;
use Illuminate\Database\Seeder;

class JenisPekerjaanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	JenisPekerjaan::create(['jenis'=>'PNS']);
        JenisPekerjaan::create(['jenis'=>'SWASTA']);
    }
}
