<?php

use App\Jabatan;
use App\JenisSatuan;
use App\Nivelering;
use App\Pangkat;
use App\Satuan;
use App\SatuanJabatan;
use Illuminate\Database\Seeder;

class SatuanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JenisSatuan::create(['nama_jenis_satuan' => 'POLDA','kategori' =>'SATUAN KERJA']);
        JenisSatuan::create(['nama_jenis_satuan' => 'BAG','kategori' =>'SATUAN KERJA']);
        JenisSatuan::create(['nama_jenis_satuan' => 'POLRESTABES','kategori' =>'SATUAN KERJA']);
        JenisSatuan::create(['nama_jenis_satuan' => 'POLSEK','kategori' =>'SATUAN KERJA']);
        	$mabes = Satuan::create([
        		'nama_satuan'             => 'MABES',
        		'jenis_satuan_id'		   => JenisSatuan::where('nama_jenis_satuan','LIKE','%POLDA%')->first()->jenis_satuan_id
        	]);
                $KAPOLRI = Jabatan::create(['nama_jabatan' => 'KAPOLRI']);
                $nivelering = Nivelering::where('nama_nivelering','IA')->first();
                $dsp = 1;
                $pangkat = Pangkat::where('singkatan','IRJEN POL')->first();
                $KAPOLRISJ = SatuanJabatan::create([
                    'satuan_id' => $mabes->satuan_id, 
                    'jabatan_id' => $KAPOLRI->jabatan_id, 
                    'nivelering_id' => $nivelering->nivelering_id, 
                    'dsp' => $dsp,
                    'status_jabatan_id' => 1,
                    'keterangan' => $KAPOLRI->nama_jabatan.' '.$mabes->fullName()
                ]);
            $satuan=$ssdm = Satuan::create([
                'nama_satuan' => 'ITWASUM',
                'parent_id' => $mabes->satuan_id,
                'jenis_satuan_id'       => JenisSatuan::where('nama_jenis_satuan','LIKE','%BAG%')->first()->jenis_satuan_id
            ]);
                $rorenmin = Satuan::create([
                    'nama_satuan' => 'RO RENMIN',
                    'parent_id' => $ssdm->satuan_id,
                    'jenis_satuan_id'       => JenisSatuan::where('nama_jenis_satuan','LIKE','%BAG%')->first()->jenis_satuan_id
                ]);
                $subbagprogar = Satuan::create([
                    'nama_satuan' => 'SUBBAG PROGAR',
                    'parent_id' => $rorenmin->satuan_id,
                    'jenis_satuan_id'       => JenisSatuan::where('nama_jenis_satuan','LIKE','%BAG%')->first()->jenis_satuan_id
                ]);
        	$satuan=$ssdm = Satuan::create([
        		'nama_satuan' => 'SSDM',
        		'parent_id' => $mabes->satuan_id,
        		'jenis_satuan_id'		=> JenisSatuan::where('nama_jenis_satuan','LIKE','%BAG%')->first()->jenis_satuan_id
        	]);
                $jabatan = Jabatan::create(['nama_jabatan' => 'ASSDM']);
                $nivelering = Nivelering::where('nama_nivelering','IA')->first();
                $dsp = 1;
                $pangkat = Pangkat::where('singkatan','IRJEN POL')->first();
                $satuanJabatan = SatuanJabatan::create([
                    'parent_satuan_jabatan_id' => $KAPOLRISJ->satuan_jabatan_id, 
                    'satuan_id' => $satuan->satuan_id, 
                    'jabatan_id' => $jabatan->jabatan_id, 
                    'nivelering_id' => $nivelering->nivelering_id, 
                    'dsp' => $dsp,
                    'status_jabatan_id' => 1,
                    'keterangan' => $jabatan->nama_jabatan.' '.$satuan->fullName()
                ]);
            $bagInfopers = Satuan::create([
                'nama_satuan' => 'BAG INFOPERS',
                'parent_id' => $ssdm->satuan_id,
                'jenis_satuan_id'       => JenisSatuan::where('nama_jenis_satuan','LIKE','%BAG%')->first()->jenis_satuan_id
            ]);
            $subbag = Satuan::create([
                'nama_satuan' => 'SUBBAG RENMIN',
                'parent_id' => $bagInfopers->satuan_id,
                'jenis_satuan_id'       => JenisSatuan::where('nama_jenis_satuan','LIKE','%BAG%')->first()->jenis_satuan_id
            ]);
                $urmin = Satuan::create([
                    'nama_satuan' => 'URMIN',
                    'parent_id' => $subbag->satuan_id,
                    'jenis_satuan_id'       => JenisSatuan::where('nama_jenis_satuan','LIKE','%BAG%')->first()->jenis_satuan_id
                ]);
        	$satuan = Satuan::create([
        		'nama_satuan' => 'POLDA JATENG',
                'parent_id'                 => $mabes->satuan_id,
        		'jenis_satuan_id'		=> JenisSatuan::where('nama_jenis_satuan','POLDA')->first()->jenis_satuan_id
        	]);
                $jabatan = Jabatan::create(['nama_jabatan' => 'KAPOLDA JATENG']);
                $nivelering = Nivelering::where('nama_nivelering','IA')->first();
                $dsp = 1;
                $pangkat = Pangkat::where('singkatan','IRJEN POL')->first();
                $KAPOLDASJ = SatuanJabatan::create([
                    'parent_satuan_jabatan_id' => $KAPOLRISJ->satuan_jabatan_id, 
                    'satuan_id' => $satuan->satuan_id, 
                    'jabatan_id' => $jabatan->jabatan_id, 
                    'nivelering_id' => $nivelering->nivelering_id, 
                    'dsp' => $dsp,
                    'status_jabatan_id' => 1,
                    'keterangan' => $jabatan->nama_jabatan.' '.$satuan->fullName()
                ]);
        	$satuan = Satuan::create([
        		'nama_satuan' => 'POLRESTABES SEMARANG',
        		'parent_id' => $satuan->satuan_id,
        		'jenis_satuan_id'		=> JenisSatuan::where('nama_jenis_satuan','POLRESTABES')->first()->jenis_satuan_id
        	]);
                $jabatan = Jabatan::create(['nama_jabatan' => 'KAPOLRES SEMARANG']);
                $nivelering = Nivelering::where('nama_nivelering','IA')->first();
                $dsp = 1;
                $pangkat = Pangkat::where('singkatan','IRJEN POL')->first();
                $KAPOLRESSJ = SatuanJabatan::create([
                    'parent_satuan_jabatan_id' => $KAPOLDASJ->satuan_jabatan_id, 
                    'satuan_id' => $satuan->satuan_id, 
                    'jabatan_id' => $jabatan->jabatan_id, 
                    'nivelering_id' => $nivelering->nivelering_id, 
                    'dsp' => $dsp,
                    'status_jabatan_id' => 1,
                    'keterangan' => $jabatan->nama_jabatan.' '.$satuan->fullName()
                ]);
            $satuan = Satuan::create([
                'nama_satuan' => 'POLSEK BANYUMAS BARAT',
                'parent_id' => $satuan->satuan_id,
                'jenis_satuan_id'       => JenisSatuan::where('nama_jenis_satuan','POLSEK')->first()->jenis_satuan_id
            ]);
                $jabatan = Jabatan::create(['nama_jabatan' => 'KAPOLSEK BANYUMAS']);
                $nivelering = Nivelering::where('nama_nivelering','IA')->first();
                $dsp = 1;
                $pangkat = Pangkat::where('singkatan','IRJEN POL')->first();
                $KAPOLSEK = SatuanJabatan::create([
                    'parent_satuan_jabatan_id' => $KAPOLRESSJ->satuan_jabatan_id, 
                    'satuan_id' => $satuan->satuan_id, 
                    'jabatan_id' => $jabatan->jabatan_id, 
                    'nivelering_id' => $nivelering->nivelering_id, 
                    'dsp' => $dsp,
                    'status_jabatan_id' => 1,
                    'keterangan' => $jabatan->nama_jabatan.' '.$satuan->fullName()
                ]);
    }
}
