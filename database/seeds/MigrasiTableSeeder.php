<?php

use Illuminate\Database\Seeder;

use App\Alamat;
use App\Dikum;
use App\Fisik;
use App\HubunganKeluarga;
use App\KeluargaPersonel;
use App\Pangkat;
use App\PendidikanPersonel;
use App\Personel;
use App\PersonelData;
use App\RiwayatJabatan;
use App\RiwayatPangkat;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MigrasiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$count=0;
    	$countPersonel=0;
    	$countDB=0;
  		// DB::statement("SET foreign_key_checks=0");
		// Personel::truncate();
		// PendidikanPersonel::truncate();
		// DB::statement("SET foreign_key_checks=1");
        $tempNrp='0a';
        // $arrayDB = [ 'M_baharkam','M_baintelkam','M_bareskrim','M_densus','M_divhubinter','M_divhumas','M_divkum','M_divpropam','M_divti','M_itwasum','M_korbrimob','M_korlantas','M_lemdikpol','M_litbang','M_pusdokkes','M_pusjarah','M_puskeu','M_sahli','M_setum','M_sops','M_spripim','M_srena','M_ssarpras','M_ssdm','M_yanma','P_ACEH','P_BABEL','P_BALI','P_BANTEN','P_BENGKULU','P_DIY','P_GORONTALO','P_JABAR','P_JAMBI','P_JATENG','P_JATIM','P_KALBAR','P_KALSEL','P_KALTIM','P_KALTENG','P_KALTIM','P_KEPRI','P_LAMPUNG','P_MALUKU','P_MALUT','P_METRO','P_NTB','P_NTT','P_PAPUA','P_PAPUABARAT','P_RIAU','P_SULSEL','P_SULTENG','P_SULTRA','P_SULUT','P_SUMBAR','P_SUMSEL','P_SUMUT'];
        // $arrayDB = [ 'P_JATIM','P_KALBAR','P_KALSEL','P_KALTIM','P_KALTENG','P_KALTIM','P_KEPRI','P_LAMPUNG','P_MALUKU','P_MALUT','P_METRO','P_NTB','P_NTT','P_PAPUA','P_PAPUABARAT','P_RIAU','P_SULSEL','P_SULTENG','P_SULTRA','P_SULUT','P_SUMBAR','P_SUMSEL','P_SUMUT'];
        // $arrayDB = ['M_baharkam','M_baintelkam','M_bareskrim','M_densus','M_divhubinter','M_divhumas','M_divkum','M_divpropam','M_divti','M_itwasum','P_BANTEN','P_BENGKULU','P_DIY','P_GORONTALO','P_JABAR','P_JAMBI','P_JATENG','P_JATIM','P_KALBAR','P_KALSEL','P_KALTIM','P_KALTENG','P_KALTIM','P_KEPRI','P_LAMPUNG','P_MALUKU','P_MALUT','P_METRO','P_NTB','P_NTT','P_PAPUA','P_PAPUABARAT','P_RIAU','P_SULSEL','P_SULTENG','P_SULTRA','P_SULUT','P_SUMBAR','P_SUMSEL','P_SUMUT'];
        // $arrayDBError= ['P_ACEH','P_BABEL','P_BALI'];
        $arrayDB = [ 'M_baharkam','M_baintelkam','M_bareskrim','M_densus','M_divhubinter','M_divhumas','M_divkum','M_divpropam','M_divti','M_itwasum'];
        // $arrayDB = [ 'local_ssdm'];
        $totalDB = count($arrayDB);
        foreach($arrayDB as $DBName){
    		$countPersonelPerDB=0;
        	$errorAgama=false;
        	$countDB++;
        	echo "\n====================================================================\n";
        	echo "Migrasi Database : $DBName";
        	echo "\n====================================================================\n";
	        $query = DB::connection($DBName)->table('t_personil')
	        ->join('t_jabatan', 't_personil.ref_jabatan', '=', 't_jabatan.id')
	        ->select(
	            't_personil.id',
	            't_personil.c_nrp',
	            't_personil.ref_agama',
	            't_personil.c_nama',
	            't_personil.c_kelamin',
	            't_personil.c_tempatMenikah',
	            't_personil.c_tgllahir',
	            't_personil.c_telpRumah',
	            't_personil.c_telpHP',
	            't_personil.c_email',
	            't_personil.c_tinggi',
	            't_personil.c_berat',
	            't_personil.c_kulit',
	            't_personil.c_mata',
	            't_personil.c_darah',
	            't_personil.c_topi',
	            't_personil.c_celana',
	            't_personil.c_baju',
	            't_personil.c_sepatu',
	            't_personil.c_sidik1',
	            't_personil.c_sidik2',
	            't_personil.c_jenisRambut',
	            't_personil.c_warnaRambut',
	            't_personil.c_asabri',
	            't_personil.c_bpjsKes',
	            't_personil.c_npwp',
	            't_personil.c_karkes',
	            't_personil.c_kta',
	            't_personil.c_tmtjabatan',
	            't_personil.c_alamatOrtu',
	            't_personil.c_telpOrtu',
	            't_personil.c_status',
	            't_personil.c_sukubangsa',
	            't_personil.c_nik',
	            't_personil.c_noKK',
	            't_personil.c_tmplahir',
	            't_jabatan.c_jabatan as c_jabatan'
	        )
	        ->orderBy('c_nrp')->get();
	        $totalPersonel = $query->count();
	    	foreach($query as $key => $oldpersonel){
        		$countPersonelPerDB++;
        		$countPersonel++;
	    		echo '.';
	    		$this->show_status($countPersonelPerDB, $totalPersonel, $size=30);
	            $skip=false;
	            $error=false;
	            $errorMessage='';

	            // validasi error 
	            // validasi nama dengan gelar
	            if(!$this->validasiNamaLengkapMemilikiGelar($oldpersonel->c_nama)){
	                $error=true;
	                $errorMessage.='Nama Lengkap Memiliki Gelar.<br>';
	            }
	            // validasi digit NRP 8 dan 18 digit.
	            if(!$this->validasiDigitNRP($oldpersonel->c_nrp)){
	                $error=true;
	                $errorMessage.="Format nrp tidak benar. jumlah angka nrp:".strlen($oldpersonel->c_nrp).'<br>';
	            }

	            $agamaID=null;
	    		// validasi agama
	    		if(!($oldpersonel->ref_agama >=1 && $oldpersonel->ref_agama<=5)){
	    			$errorAgama=true;
	            	$agamaID=null;
	    			$error=true;
	    			$errorMessage.="data ref_agama salah. data ref_agama:".$oldpersonel->ref_agama.'<br>';
	    		} else {
	    			$agamaID = $oldpersonel->ref_agama;
	    		}

	            if($oldpersonel->c_tgllahir != null){
	                //validasi tanggal lahir
	                if($this->validateDate($oldpersonel->c_tgllahir)){
                        $error=true;
                        $errorMessage.="Format tanggal lahir salah. Tanggal Lahir :".$oldpersonel->c_tgllahir;
                        $tanggal_lahir = null;
                    }
                    else{
                        $tanggal_lahir = $oldpersonel->c_tgllahir;
                    }

	                //validasi format nrp dan tanggal lahir (yymmxxxx)
	                if(!$this->validasiFormatNRPdanTanggalLahir($oldpersonel->c_nrp,$oldpersonel->c_tgllahir)){
	                    $error=true;
	                    $errorMessage.="Format NRP dan tanggal lahir tidak sama. ";
	                    $errorMessage.="Tanggal Lahir :".$oldpersonel->c_tgllahir.'<br>';
	                }
	            }

	            //validasi tanggal tmt_jabatan
	            if($oldpersonel->c_tmtjabatan != null){
	                if($this->validateDate($oldpersonel->c_tmtjabatan)){
                        $error=true;
                        $errorMessage.="Format tanggal tmt_jabatan salah. Tanggal Tmt Jabatan:".$oldpersonel->c_tmtjabatan;
                        $tmt_jabatan = null;
                    }
                    else{
                        $tmt_jabatan = $oldpersonel->c_tmtjabatan;
                    }
	            }
	            if(strlen($oldpersonel->c_nrp) == 18){
	                $polisi=false;
	            } elseif(strlen($oldpersonel->c_nrp) == 8) {
	                $polisi=true;
	            } else {
	                $skip=true;
	            }
	            $statusPersonel = 1;
	            if($oldpersonel->c_status==1){
	                $status=1;
	            } else if($oldpersonel->c_status==2){
	                $status=4;
	            } else if($oldpersonel->c_status==3){
	                $skip=true;
	            } else if($oldpersonel->c_status==4){
	                $status=2;
	            } else if($oldpersonel->c_status==5){
	                $status=8; // MENINGGAL
	            } else if($oldpersonel->c_status==6){
	                $status=3;
	            }

	            if($skip){
	                continue;
	            }

	    		$newpersonel = Personel::create([
	                'nama_lengkap' => $this->validateLength($oldpersonel->c_nama),
	                'tempat_lahir' => $this->validateLength($oldpersonel->c_tmplahir),
	                'tanggal_lahir' => $tanggal_lahir,
	                'jenis_kelamin' => $oldpersonel->c_kelamin,
	                'tmt_pertama' => $tmt_jabatan,
	                'tmt_jabatan_pertama' => $tmt_jabatan,
	                'agama_id' => $agamaID,
	                'nrp' => $this->validateLength($oldpersonel->c_nrp),
	                'polisi' => $polisi,
	                'status_pernikahan' => $oldpersonel->c_tempatMenikah==null?'MENIKAH':'BELUM NIKAH', 
	                'status_personel_id' => $status, 
	                'database' => $DBName, 
	    		]);

	            PersonelData::create([
	                'personel_id' => $newpersonel->personel_id,
	                'nama_panggilan' => $this->validateLength($oldpersonel->c_nama),
	                'handphone' => $this->validateLength($oldpersonel->c_telpHP),
	                'golongan_darah' => $this->validateLength($oldpersonel->c_darah),
	                'email' => $this->validateLength($oldpersonel->c_email),
	                'alamat_orang_tua' => $this->validateLength($oldpersonel->c_alamatOrtu),
	                'telepon_orang_tua' => $this->validateLength($oldpersonel->c_telpOrtu),
	                'suku_bangsa' => $this->validateLength($oldpersonel->c_sukubangsa),
	                'warna_kulit' => $this->validateLength($oldpersonel->c_kulit),
	                'jenis_rambut' => $this->validateLength($oldpersonel->c_jenisRambut),
	                'warna_rambut' => $this->validateLength($oldpersonel->c_warnaRambut),
	                'warna_mata' => $this->validateLength($oldpersonel->c_mata),
	                'sidik_jari_1' => $this->validateLength($oldpersonel->c_sidik1),
	                'sidik_jari_2' => $this->validateLength($oldpersonel->c_sidik2),
	                'asabri_nomor' => $this->validateLength($oldpersonel->c_asabri),
	                'kta_nomor' => $this->validateLength($oldpersonel->c_kta),
	                'ktp_nomor' => $this->validateLength($oldpersonel->c_nik),
	                'npwp_nomor' => $this->validateLength($oldpersonel->c_npwp),
	                'kartu_keluarga_nomor' => $this->validateLength($oldpersonel->c_noKK),
	                'bpjs_nomor' => $this->validateLength($oldpersonel->c_bpjsKes)
	            ]);

	            Fisik::create([
	                'personel_id' => $newpersonel->personel_id,
	                'tinggi' => is_numeric($oldpersonel->c_tinggi)?$oldpersonel->c_tinggi:null,
	                'berat' => is_numeric($oldpersonel->c_berat)?$oldpersonel->c_berat:null,
	                'ukuran_topi' => is_numeric($oldpersonel->c_topi)?$oldpersonel->c_topi:null,
	                'ukuran_sepatu' => is_numeric($oldpersonel->c_sepatu)?$oldpersonel->c_sepatu:null,
	                'ukuran_celana' => is_numeric($oldpersonel->c_celana)?$oldpersonel->c_celana:null,
	                'ukuran_baju' => is_numeric($oldpersonel->c_baju)?$oldpersonel->c_baju:null
	            ]);
	            // Keluarga
			        $keluarga = DB::connection($DBName)->table('t_Rfamily')
			        ->select(
			            't_Rfamily.c_type',
			            't_Rfamily.c_name',
			            't_Rfamily.c_kelamin',
			            't_Rfamily.c_tgllahir',
			            't_Rfamily.c_status'
			        )
			        ->where('ref_personil',$oldpersonel->id)
			        ->get();
			        foreach($keluarga as $anggotaKeluarga){

			            //validasi tanggal keluargaTanggalLahir
			            if($anggotaKeluarga->c_tgllahir != null){
	                		if($this->validateDate($anggotaKeluarga->c_tgllahir)){
		                        $error=true;
		                        $errorMessage.="Format tanggal lahir keluarga salah. ex.".$anggotaKeluarga->c_tgllahir;
		                        $keluargaTanggalLahir = null;
		                    }
		                    else{
		                        $keluargaTanggalLahir = $anggotaKeluarga->c_tgllahir;
		                    }
			            }
			        	$hubunganKeluarga = HubunganKeluarga::where('hubungan','LIKE',$anggotaKeluarga->c_type)->first();
			        	if($hubunganKeluarga==null){
			        		$hubunganKeluarga = HubunganKeluarga::create(['hubungan' => strtoupper($anggotaKeluarga->c_type)]);
			        	}
			            $keluargaPersonel =KeluargaPersonel::create([
			            	'personel_id' => $newpersonel->personel_id,
			            	'hubungan_keluarga_id' => $hubunganKeluarga->hubungan_keluarga_id,
			            	'nama_keluarga' => $this->validateLength(strtoupper($anggotaKeluarga->c_name)),
			            	'jenis_kelamin' => $this->validateLength(strtoupper($anggotaKeluarga->c_kelamin)),
			            	'tanggal_lahir' => $keluargaTanggalLahir,
			            	'status' => strtoupper($anggotaKeluarga->c_status),
			            ]);
			        	if(strtolower($anggotaKeluarga->c_type)==strtolower('ibu')){
			        		$newpersonel->nama_ibu = $keluargaPersonel->nama_keluarga;
			        		$newpersonel->save();
			        	}
			        }
			    // End Keluarga
			    // Pendidikan Umum
			        $oldRiwayatDikum = DB::connection($DBName)->table('t_Rdikum')
			        ->select(
			            't_Rdikum.c_dikum',
			            't_Rdikum.c_nama',
			            't_Rdikum.c_tahun',
			            't_Rdikum.c_ijasah'
			        )
			        ->where('ref_personil',$oldpersonel->id)
			        ->get();
			        foreach($oldRiwayatDikum as $riwayatDikum){
			        	$dikumID=null;
			        	$dikum = Dikum::where('tingkat','LIKE',"%$riwayatDikum->c_dikum%")->first();
			        	if($dikum==null){
			        		$dikumID=null;
			        		continue;
			        	} else {
			        		$dikumID=$dikum->dikum_id;
			        	}
			            //validasi tanggal tahunLulus
			            $tahunLulus=null;
			            if(is_numeric($riwayatDikum->c_tahun)){
			            	if($riwayatDikum->c_tahun > 1000 && $riwayatDikum->c_tahun<2100){
					            if($riwayatDikum->c_tahun != null){
					            	// $count++;
					            	// echo $riwayatDikum->c_tahun.'<br>';
	                				if($this->validateDate($riwayatDikum->c_tahun.'-01-01')){
				                        $error=true;
				                        $errorMessage.="Format tahun lulus salah. ex.".$riwayatDikum->c_tahun;
				                        $tahunLulus = null;
				                    }
				                    else{
				                        $tahunLulus = $riwayatDikum->c_tahun.'-01-01';
				                    }
					            }
					            $keluargaPersonel = PendidikanPersonel::create([
					            	'personel_id' => $newpersonel->personel_id,
					            	'dikum_id' => $dikumID,
					            	'tanggal_lulus' => $tahunLulus,
					            	'nama_institusi' => $this->validateLength(strtoupper($riwayatDikum->c_nama)),
					            	'surat_kelulusan_nomor' => $this->validateLength(strtoupper($riwayatDikum->c_ijasah)),
					            ]);
			            	}
			            }
			        }
			    // End Pendidikan Umum
		        // Riwayat Jabatan
			        $oldRiwayatJabatan = DB::connection($DBName)->table('t_Rjabatan')
			        ->select(
			            't_Rjabatan.c_jabatan',
			            't_Rjabatan.c_tahun',
			            't_Rjabatan.c_noSkep'
			        )
			        ->where('ref_personil',$oldpersonel->id)
			        ->get();
			        foreach($oldRiwayatJabatan as $riwayatJabatan){
			        	// $count++;
			        	// if($count==2){
				        // 	dd($riwayatJabatan);
			        	// }

			        	$tmtJabatan=null;
			            //validasi tanggal tmtJabatan
			            if($riwayatJabatan->c_tahun != null){
            				if($this->validateDate($riwayatJabatan->c_tahun)){
		                        $error=true;
		                        $errorMessage.="Format tmt_jabatan salah. ex.".$riwayatJabatan->c_tahun;
		                        $tmtJabatan = null;
		                    }
		                    else{
		                        $tmtJabatan = $riwayatJabatan->c_tahun;
		                    }
			            }
				        $jabatan = RiwayatJabatan::create([
				        	'personel_id' => $newpersonel->personel_id,
				        	'keterangan' => $riwayatJabatan->c_jabatan,
				        	'tmt_jabatan' => $tmtJabatan,
				        	'surat_keputusan_nomor' => $this->validateLength($riwayatJabatan->c_noSkep)
				        ]);
				        // dd($jabatan);
			        }
		        // End Riwayat Jabatan
			    // Riwayat Pangkat
			        $oldRiwayatPangkat = DB::connection($DBName)->table('t_Rpangkat')
	        		->join('t_pangkat', 't_Rpangkat.ref_pangkat', '=', 't_pangkat.id')
			        ->select(
			            't_Rpangkat.ref_pangkat',
			            't_pangkat.c_pangkat',
			            't_Rpangkat.c_tmtpangkat',
			            't_Rpangkat.c_noSkep'
			        )
			        ->where('t_Rpangkat.ref_personil',$oldpersonel->id)
			        ->orderBy('t_Rpangkat.c_tmtpangkat')
			        ->get();
			        foreach($oldRiwayatPangkat as $key=> $riwayatPangkat){
			        	$pangkatID=null;
			        	$pangkat = Pangkat::where('nama_pangkat',$riwayatPangkat->c_pangkat)->first();
			        	if($pangkat!=null){
				        	$pangkatID = $pangkat->pangkat_id;
			        	}
			        	if($pangkatID==null){
			        		// echo $riwayatPangkat->c_pangkat.'<br>';
			        	}

			        	// $count++;
			        	// if($count==2){
				        // 	dd($riwayatPangkat);
			        	// }

			        	$tmtPangkat=null;
			            //validasi tanggal tmtPangkat
			            if($riwayatPangkat->c_tmtpangkat != null){
            				if($this->validateDate($riwayatPangkat->c_tmtpangkat)){
		                        $error=true;
		                        $errorMessage.="Format tmt_jabatan salah. ex.".$riwayatPangkat->c_tmtpangkat;
		                        $tmtPangkat = null;
		                    }
		                    else{
		                        $tmtPangkat = $riwayatPangkat->c_tmtpangkat;
		                    }
			            }
				        $jabatan = RiwayatPangkat::create([
				        	'personel_id' => $newpersonel->personel_id,
				        	'pangkat_id' => $pangkatID,
				        	'tmt_pangkat' => $tmtPangkat,
				        	'surat_keputusan_nomor' => $this->validateLength($riwayatPangkat->c_noSkep)
				        ]);
			        	if($key==0){
			        		$newpersonel->tmt_pertama=$tmtPangkat;
			        		$newpersonel->save();
			        	}
				        // dd($jabatan);
			        }
			    // End Riwayat Pangkat
		        // Olahraga
			        
		        // End Olahraga
	    	} //end foreach personel
	    	echo "MIGRASI Database: $DBName SELESAI. Jumlah DB=$countDB dari $totalDB. Jumlah Personel=$countPersonelPerDB. Total Personel yang sudah di migrasi: $countPersonel\n";
	    } //end foreach db
    	echo "=================================================================================\n";
	    echo "MIGRASI SELURUH Database SELESAI. Jumlah DB=$countDB. Jumlah Personel=$countPersonel\n";

    }

    private function validasiNamaLengkapMemilikiGelar($nama)
    {
        if(preg_match_all("/[.,]+/", $nama)){
            return false;
        }
        return true;
    }
    private function validasiDigitNRP($nrp)
    {
        if(strlen($nrp)==8 || strlen($nrp)==18){
            return true;
        }
        return false;
    }

    private function validasiFormatNRPdanTanggalLahir($nrp,$tanggalLahir)
    {
        if($tanggalLahir == null){
            $d = Carbon::createFromFormat('Y-m-d', $tanggalLahir);
            $nrpFormatDate = substr($nrp,0,4);
            if($d){
                $tempDate = substr($d->format('Ym'),2,4);
                if($tempDate == $nrpFormatDate){
                    return true;
                }
            }
        }
        return false;
    }

    private function validateDate($date, $format = 'Y-m-d')
	{
		$valid=false;
	    $d = DateTime::createFromFormat($format, $date);
	    if($d!=null){
		    if($d->format('Y')>1500 && $d->format('Y')<=2500){
		    	$valid=true;
		    }
	    }
	    return !($d && $d->format($format) == $date && $valid);
	}
	private function validateLength($string)
	{
		if(strlen($string)>100){
			return null;
		}
		else {
			return $string;
		}
	}
    private function show_status($done, $total, $size=30) {
	    static $start_time;

	    // if we go over our bound, just ignore it
	    if($done > $total) return;

	    if(empty($start_time)) $start_time=time();
	    $now = time();

	    $perc=(double)($done/$total);

	    $bar=floor($perc*$size);

	    $status_bar="\r[";
	    $status_bar.=str_repeat("=", $bar);
	    if($bar<$size){
	        $status_bar.=">";
	        $status_bar.=str_repeat(" ", $size-$bar);
	    } else {
	        $status_bar.="=";
	    }

	    $disp=number_format($perc*100, 0);

	    $status_bar.="] $disp%  $done/$total";

	    $rate = ($now-$start_time)/$done;
	    $left = $total - $done;
	    $eta = round($rate * $left, 2);

	    $elapsed = $now - $start_time;

	    $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

	    echo "$status_bar  ";

	    flush();

	    // when done, send a newline
	    if($done == $total) {
	        echo "\n";
	    }

	}
}
