<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Helpdesk::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'nrp' => $faker->numerify('########'),
        'satker_pertama' => $faker->randomElement(['MABES','POLDA ACEH','POLDA JATENG']),
        'nama_lengkap' => $faker->name(),
        'nama_ibu' => $faker->firstNameFemale(),
        'tanggal_lahir' => $faker->date($format = 'Y-m-d', $max = '-10 years'),
        'email' => $faker->safeEmail(),
        'handphone' => $faker->phoneNumber(),
        'tmt_jabatan_pertama' => $faker->date($format = 'Y-m-d', $max = '-10 years'),
        'status' => $status = $faker->randomElement(['NOTFOUND','DUPLICATE','VERIFIED']),
        'duplicate_with' => $status == 'DUPLICATE' ? 1 : null ,
        'verified_by' => $status == 'VERIFIED' ? 1 : null 
    ];
});

