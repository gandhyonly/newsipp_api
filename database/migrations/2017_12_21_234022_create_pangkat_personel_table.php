<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePangkatPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pangkat_personel', function (Blueprint $table) {
            $table->increments('pangkat_personel_id');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->integer('pangkat_id')->unsigned();
            $table->foreign('pangkat_id')->references('pangkat_id')->on('pangkat')->onDelete('cascade');

            $table->boolean('status_terakhir')->default(false);
            $table->date('tmt_pangkat');
            $table->string('surat_keputusan_nomor')->nullable();
            $table->string('surat_keputusan_file')->nullable();
            $table->timestamp('tanggal_perubahan');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pangkat_personel');
    }
}
