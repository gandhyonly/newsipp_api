<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenghargaanPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penghargaan_personel', function (Blueprint $table) {
            $table->increments('penghargaan_personel_id');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->integer('penghargaan_id')->unsigned();
            $table->foreign('penghargaan_id')->references('penghargaan_id')->on('penghargaan')->onDelete('cascade');
            $table->integer('tingkat_penghargaan_id')->unsigned();
            $table->foreign('tingkat_penghargaan_id')->references('tingkat_penghargaan_id')->on('tingkat_penghargaan')->onDelete('cascade');

            $table->date('tanggal_penghargaan');
            $table->string('surat_nomor', 100);
            $table->string('surat_file', 100)->nullable();
            $table->string('penghargaan_file', 100)->nullable();
            $table->timestamp('tanggal_perubahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penghargaan_personel');
    }
}
