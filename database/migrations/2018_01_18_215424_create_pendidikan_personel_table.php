<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendidikanPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendidikan_personel', function (Blueprint $table) {
            $table->increments('pendidikan_personel_id');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->integer('dikum_id')->unsigned()->nullable();
            $table->foreign('dikum_id')->references('dikum_id')->on('dikum')->onDelete('cascade');

            $table->integer('dikpol_id')->unsigned()->nullable();
            $table->foreign('dikpol_id')->references('dikpol_id')->on('dikpol')->onDelete('cascade');

            $table->integer('dikbangspes_id')->unsigned()->nullable();
            $table->foreign('dikbangspes_id')->references('dikbangspes_id')->on('dikbangspes')->onDelete('cascade');
 
            $table->string('nama_institusi')->nullable();
            $table->date('tanggal_mulai')->nullable();
            $table->date('tanggal_lulus')->nullable();
            $table->double('nilai_akhir', 8, 2)->nullable();
            $table->string('nilai_file')->nullable();
            $table->integer('ranking')->unsigned()->nullable();

            $table->integer('gelar_id')->unsigned()->nullable();
            $table->foreign('gelar_id')->references('gelar_id')->on('gelar')->onDelete('set null');
            $table->integer('konsentrasi_id')->unsigned()->nullable();
            $table->foreign('konsentrasi_id')->references('konsentrasi_id')->on('konsentrasi')->onDelete('set null');

            $table->integer('total_siswa')->unsigned()->nullable();
            $table->boolean('status_terakhir')->default(false);
            $table->boolean('dinas')->nullable();
            $table->string('akreditasi', 2)->nullable();
            $table->string('surat_kelulusan_nomor')->nullable();
            $table->string('surat_kelulusan_file')->nullable();
            $table->integer('angkatan')->unsigned()->nullable();

            $table->timestamp('tanggal_perubahan')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendidikan_personel');
    }
}
