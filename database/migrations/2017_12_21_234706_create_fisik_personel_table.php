<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFisikPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fisik_personel', function (Blueprint $table) {
            $table->increments('fisik_personel_id');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->date('tanggal')->nullable();
            $table->integer('tinggi')->nullable();
            $table->integer('berat')->nullable();
            $table->double('ukuran_topi')->nullable();
            $table->double('ukuran_sepatu')->nullable();
            $table->double('ukuran_celana')->nullable();
            $table->double('ukuran_baju')->nullable();

            $table->timestamp('tanggal_perubahan')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fisik_personel');
    }
}
