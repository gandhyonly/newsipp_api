<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTandaKehormatanPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanda_kehormatan_personel', function (Blueprint $table) {
            $table->increments('tanda_kehormatan_personel_id');
            $table->integer('tanda_kehormatan_id')->unsigned()->nullable();
            $table->foreign('tanda_kehormatan_id')->references('tanda_kehormatan_id')->on('tanda_kehormatan')->onDelete('cascade');

            $table->integer('personel_id')->unsigned()->nullable();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');
            
            $table->string('surat_nomor', 100)->nullable();
            $table->string('surat_file', 100)->nullable();
            $table->date('tanggal_surat')->nullable();
            $table->timestamp('tanggal_perubahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanda_kehormatan_personel');
    }
}
