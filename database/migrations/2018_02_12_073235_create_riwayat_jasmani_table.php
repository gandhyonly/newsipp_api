<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiwayatJasmaniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_jasmani', function (Blueprint $table) {
            $table->increments('riwayat_jasmani_id');
            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');
            $table->integer('lari')->unsigned();
            $table->integer('lari_nilai')->unsigned();
            $table->integer('pull_up')->unsigned();
            $table->integer('pull_up_nilai')->unsigned();
            $table->integer('push_up')->unsigned();
            $table->integer('push_up_nilai')->unsigned();
            $table->integer('sit_up')->unsigned();
            $table->integer('sit_up_nilai')->unsigned();
            $table->double('shuttle_run', 15, 8)->nullable();
            $table->integer('shuttle_run_nilai')->unsigned();
            $table->integer('semester')->unsigned();
            $table->integer('golongan')->unsigned();
            $table->integer('tinggi')->unsigned();
            $table->integer('berat')->unsigned();
            $table->integer('imt')->unsigned();
            $table->date('tanggal_tes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_jasmani');
    }
}
