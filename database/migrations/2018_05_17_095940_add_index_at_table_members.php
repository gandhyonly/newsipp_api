<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexAtTableMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->index('tingkat');
            $table->index('satuan_1');
            $table->index('satuan_2');
            $table->index('satuan_3');
            $table->index('satuan_4');
            $table->index('personel_id');
        });
        Schema::table('helpdesks', function (Blueprint $table) {
            $table->index('status');
            $table->index('pangkat_id');
            $table->index('verified_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            //
        });
    }
}
