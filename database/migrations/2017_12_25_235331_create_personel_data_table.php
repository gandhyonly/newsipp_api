<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonelDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personel_data', function (Blueprint $table) {
            $table->increments('personel_data_id');
            $table->string('nama_panggilan')->nullable();
            $table->string('handphone')->nullable();
            $table->string('golongan_darah')->nullable();
            $table->string('email')->nullable();
            $table->integer('anak_ke')->nullable();
            $table->integer('jumlah_saudara')->nullable();

            $table->string('alamat_orang_tua')->nullable(); 
            $table->string('telepon_orang_tua')->nullable(); 

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->string('suku_bangsa')->nullable();
            $table->string('warna_kulit')->nullable();
            $table->string('jenis_rambut')->nullable();
            $table->string('warna_rambut')->nullable();
            $table->string('warna_mata')->nullable();

            $table->string('akte_kelahiran_file')->nullable(); // sementara nullable
            $table->string('sidik_jari_1')->nullable();
            $table->string('sidik_jari_2')->nullable();
            $table->string('asabri_nomor')->nullable();
            $table->string('asabri_file')->nullable();
            $table->string('tanda_kewenangan_nomor')->nullable();
            $table->string('tanda_kewenangan_file')->nullable();
            $table->string('keputusan_penyidik_nomor')->nullable();
            $table->string('keputusan_penyidik_file')->nullable();
            $table->string('kta_nomor')->nullable();
            $table->string('kta_file')->nullable();
            $table->string('ktp_nomor')->nullable();
            $table->string('ktp_file')->nullable();
            $table->string('npwp_nomor')->nullable();
            $table->string('npwp_file')->nullable();
            $table->string('paspor_nomor')->nullable();
            $table->string('paspor_file')->nullable();
            $table->string('kartu_keluarga_nomor')->nullable();
            $table->string('kartu_keluarga_file')->nullable();
            $table->string('bpjs_nomor')->nullable();
            $table->string('bpjs_file')->nullable();
            $table->string('skep_mk_file')->nullable();
            $table->string('skep_mk_nomor')->nullable();
            $table->string('lkhpn_file')->nullable();
            $table->date('tmt_masa_dinas_surut')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personel_data');
    }
}
