<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexPersonelIdAtAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('satuan', function (Blueprint $table) {
            $table->index('parent_id');
        });
        Schema::table('riwayat_jabatan', function (Blueprint $table) {
            $table->index('personel_id');
        });
        Schema::table('riwayat_pangkat', function (Blueprint $table) {
            $table->index('personel_id');
        });
        Schema::table('penghargaan_personel', function (Blueprint $table) {
            $table->index('personel_id');
        });
        Schema::table('tanda_kehormatan_personel', function (Blueprint $table) {
            $table->index('personel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
