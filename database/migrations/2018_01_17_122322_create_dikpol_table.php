<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDikpolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dikpol', function (Blueprint $table) {
            $table->increments('dikpol_id');
            $table->integer('jenis_dikpol_id')->unsigned();
            $table->foreign('jenis_dikpol_id')->references('jenis_dikpol_id')->on('jenis_dikpol')->onDelete('cascade');
            $table->string('tingkat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dikpol');
    }
}
