<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSatuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satuan', function (Blueprint $table) {
            $table->increments('satuan_id');
            $table->string('nama_satuan');
            $table->string('tipe')->default('STRUKTUR');

            $table->string('alamat')->nullable();
            $table->string('telepon')->nullable();

            $table->unsignedInteger('jenis_satuan_id');
            $table->foreign('jenis_satuan_id')->references('jenis_satuan_id')->on('jenis_satuan')->onDelete('cascade');

            $table->unsignedInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('satuan_id')->on('satuan')->onDelete('cascade');

            $table->boolean('is_verified')->default(false);
            $table->string('verified_by')->nullable();

            $table->integer('posisi')->default(5);
            
            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satuan');
    }
}
