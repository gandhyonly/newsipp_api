<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerifikatorUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verifikator_user', function (Blueprint $table) {
            $table->increments('verifikator_user_id');
            $table->string('nrp')->unique();
            $table->string('nama');
            $table->string('password');
            $table->integer('tingkat');
            $table->string('api_token',60)->unique();
            $table->boolean('is_superadmin')->default(false);
            $table->boolean('aktif')->default(true);

            $table->integer('satuan_id')->unsigned()->nullable();
            $table->foreign('satuan_id')->references('satuan_id')->on('satuan')->onDelete('cascade');
            $table->integer('dibuat_oleh')->unsigned()->nullable();
            $table->foreign('dibuat_oleh')->references('verifikator_user_id')->on('verifikator_user')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('verifikator_user');
    }
}
