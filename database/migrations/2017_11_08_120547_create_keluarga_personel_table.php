<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeluargaPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keluarga_personel', function (Blueprint $table) {
            $table->increments('keluarga_personel_id');

            $table->string('nama_keluarga')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('alamat')->nullable();
            $table->string('telepon')->nullable();
            $table->string('status')->nullable();
            $table->string('nrp_keluarga')->nullable();

            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();

            $table->unsignedInteger('personel_id');
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            //if anak
            $table->unsignedInteger('parent_keluarga_personel_id')->nullable();
            $table->foreign('parent_keluarga_personel_id')->references('keluarga_personel_id')->on('keluarga_personel');

            $table->unsignedInteger('hubungan_keluarga_id');
            $table->foreign('hubungan_keluarga_id')->references('hubungan_keluarga_id')->on('hubungan_keluarga');

            $table->unsignedInteger('personel_keluarga_id')->nullable();
            $table->foreign('personel_keluarga_id')->references('personel_id')->on('personel');

            $table->unsignedInteger('golongan_id')->nullable();
            $table->foreign('golongan_id')->references('golongan_id')->on('golongan');

            $table->date('tanggal_nikah')->nullable();

            $table->string('kpis_nomor', 100)->nullable();
            $table->string('kpis_file', 100)->nullable();

            $table->string('buku_nikah_nomor', 100)->nullable();
            $table->string('buku_nikah_file', 100)->nullable();
            $table->string('foto_file')->nullable();


            $table->timestamp('tanggal_perubahan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keluarga_personel');
    }
}
