<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBkoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bko', function (Blueprint $table) {
            $table->increments('bko_id');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->integer('satuan_asal_id')->unsigned();
            $table->foreign('satuan_asal_id')->references('satuan_id')->on('satuan')->onDelete('cascade');

            $table->integer('satuan_penugasan_id')->unsigned();
            $table->foreign('satuan_penugasan_id')->references('satuan_id')->on('satuan')->onDelete('cascade');

            $table->integer('jenis_bko_id')->unsigned()->nullable();
            $table->foreign('jenis_bko_id')->references('jenis_bko_id')->on('jenis_bko')->onDelete('cascade');

            $table->string('jabatan_bko', 100);
            $table->date('tmt_bko');
            $table->string('surat_keputusan_nomor', 100);
            $table->string('surat_keputusan_file', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bko');
    }
}
