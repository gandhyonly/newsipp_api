<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDikumIdAtTableJururusan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jurusan', function (Blueprint $table) {
            $table->integer('dikum_id')->unsigned()->nullable();
            $table->foreign('dikum_id')->references('dikum_id')->on('dikum')->onDelete('set null');
        });
        Schema::table('riwayat_jabatan', function (Blueprint $table) {
            $table->boolean('jabatan_sementara')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
