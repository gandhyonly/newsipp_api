<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenugasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penugasan', function (Blueprint $table) {
            $table->increments('penugasan_id');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->integer('jenis_penugasan_id')->unsigned();
            $table->foreign('jenis_penugasan_id')->references('jenis_penugasan_id')->on('jenis_penugasan')->onDelete('cascade');
            $table->integer('jabatan_penugasan_id')->unsigned();
            $table->foreign('jabatan_penugasan_id')->references('jabatan_penugasan_id')->on('jabatan_penugasan')->onDelete('cascade');

            $table->string('sprint_nomor')->nullable();
            $table->string('sprint_file')->nullable();

            $table->string('surat_keputusan_nomor')->nullable();
            $table->string('surat_keputusan_file')->nullable();

            $table->date('tmt_penugasan');
            $table->date('tmt_selesai')->nullable();
            $table->timestamp('tanggal_perubahan');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penugasan');
    }
}
