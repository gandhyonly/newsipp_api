<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberStatusFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_status_form', function (Blueprint $table) {
            $table->integer('member_id')->unsigned();
            $table->integer('status_form_id')->unsigned();
            $table->foreign('member_id')->references('member_id')->on('members')->onDelete('cascade');
            $table->foreign('status_form_id')->references('status_form_id')->on('status_form')->onDelete('cascade');

            $table->integer('verifikator_1')->unsigned()->nullable();
            $table->foreign('verifikator_1')->references('verifikator_user_id')->on('verifikator_user')->onDelete('cascade');
            $table->integer('verifikator_2')->unsigned()->nullable();
            $table->foreign('verifikator_2')->references('verifikator_user_id')->on('verifikator_user')->onDelete('cascade');
            $table->integer('verifikator_3')->unsigned()->nullable();
            $table->foreign('verifikator_3')->references('verifikator_user_id')->on('verifikator_user')->onDelete('cascade');
            $table->integer('verifikator_4')->unsigned()->nullable();
            $table->foreign('verifikator_4')->references('verifikator_user_id')->on('verifikator_user')->onDelete('cascade');
            $table->mediumText('keterangan')->nullable();
            $table->timestamp('tanggal_perubahan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_status_form');
    }
}
