<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('satuan', 'deleted_by')) {
            Schema::table('satuan', function (Blueprint $table) {
                $table->unsignedInteger('deleted_by')->nullable();
                $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
            });
        }
        if (!Schema::hasColumn('satuan_jabatan', 'deleted_by')) {
            Schema::table('satuan_jabatan', function (Blueprint $table) {
                $table->unsignedInteger('deleted_by')->nullable();
                $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
            });
        }
        if (!Schema::hasColumn('satuan_jabatan', 'verified_by')) {
            Schema::table('satuan_jabatan', function (Blueprint $table) {
                $table->unsignedInteger('verified_by')->nullable();
                $table->foreign('verified_by')->references('id')->on('users')->onDelete('set null');
            });
        }
        if (!Schema::hasColumn('jabatan', 'deleted_by')) {
            Schema::table('jabatan', function (Blueprint $table) {
                $table->unsignedInteger('deleted_by')->nullable();
                $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
