<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStatusPersonilToStatusPersonelAtKeluargaPersonel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keluarga_personel', function (Blueprint $table) {
            $table->dropColumn('status_personil')->nullable();
            $table->boolean('status_personel')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keluarga_personel', function (Blueprint $table) {
            //
        });
    }
}
