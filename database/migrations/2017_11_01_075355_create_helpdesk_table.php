<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpdeskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesks', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nrp')->nullable();
            $table->string('nama_lengkap')->nullable();
            $table->unsignedInteger('pangkat_id')->nullable();
            $table->foreign('pangkat_id')->references('pangkat_id')->on('pangkat')->onDelete('cascade');
            $table->string('satker_pertama')->nullable();
            $table->string('nama_ibu')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('petikan_pertama')->unique()->nullable();
            $table->string('riwayat_hidup')->unique()->nullable();
            $table->unsignedInteger('satker_sekarang_id')->nullable();
            $table->foreign('satker_sekarang_id')->references('satuan_id')->on('satuan')->onDelete('set null');

            $table->string('email')->nullable();
            $table->string('handphone')->nullable();
            
            $table->date('tmt_jabatan_pertama')->nullable();

            $table->unsignedInteger('verified_by')->nullable();
            $table->foreign('verified_by')->references('helpdesk_user_id')->on('helpdesk_users')->onDelete('set null');

            $table->unsignedInteger('duplicate_with')->nullable();
            $table->foreign('duplicate_with')->references('personel_id')->on('personel')->onDelete('set null');


            $table->enum('status',['NOTFOUND','DUPLICATE','VERIFIED']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesks');
    }
}
