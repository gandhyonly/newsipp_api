<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeKotaIdToTempatLahirId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personel', function (Blueprint $table) {
            $table->integer('tempat_lahir_id')->unsigned()->nullable();
            $table->foreign('tempat_lahir_id')->references('kota_id')->on('kota')->onDelete('set null')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
