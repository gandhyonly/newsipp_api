<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJabatanPenugasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jabatan_penugasan', function (Blueprint $table) {
            $table->increments('jabatan_penugasan_id');
            $table->string('nama_jabatan_penugasan');
            $table->string('eselon')->nullable();
            $table->integer('misi_id')->unsigned()->nullable();
            $table->foreign('misi_id')->references('misi_id')->on('misi')->onDelete('cascade');
            $table->integer('lokasi_penugasan_id')->unsigned()->nullable();
            $table->foreign('lokasi_penugasan_id')->references('lokasi_penugasan_id')->on('lokasi_penugasan')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jabatan_penugasan');
    }
}
