<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSatuanVerifikatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('satuan_verifikator', function (Blueprint $table) {
            $table->string('nama_satuan');
            $table->integer('satuan_id')->unsigned()->nullable();
            $table->integer('parent_id')->unsigned()->nullable();
            $table->integer('total_personel')->unsigned()->default(0);
            $table->integer('jumlah_personel_terdaftar')->unsigned()->default(0);
            $table->integer('jumlah_personel_terverifikasi')->unsigned()->default(0);
            $table->integer('jumlah_personel_belum_mengirim')->unsigned()->default(0);
            $table->integer('jumlah_personel_belum_terverifikasi')->unsigned()->default(0);
            $table->string('keterangan')->nullable();
            $table->integer('tingkat')->unsigned()->nullable();
            $table->boolean('is_satker')->default(false);

            $table->foreign('parent_id')->references('id')->on('satuan_verifikator')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
