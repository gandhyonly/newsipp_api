<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiwayatPsikologiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_psikologi', function (Blueprint $table) {
            $table->increments('riwayat_psikologi_id');
            $table->integer('psikologi_id')->unsigned();
            $table->foreign('psikologi_id')->references('psikologi_id')->on('psikologi')->onDelete('cascade');
            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->integer('nilai')->unsigned();
            $table->string('nilai_file', 100)->nullable();
            $table->date('tanggal_tes');
            $table->timestamp('tanggal_perubahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_psikologi');
    }
}
