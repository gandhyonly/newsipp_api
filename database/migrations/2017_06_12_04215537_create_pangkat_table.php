<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePangkatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('pangkat', function (Blueprint $table) {
                $table->increments('pangkat_id');
                $table->string('nama_pangkat');
                $table->string('singkatan');
                $table->string('singkatan_tentara');
                $table->string('jenis')->nullable();

                $table->unsignedInteger('golongan_id')->nullable();
                $table->foreign('golongan_id')->references('golongan_id')->on('golongan')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pangkat');
    }
}
