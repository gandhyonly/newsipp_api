<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sim_personel', function (Blueprint $table) {
            $table->increments('sim_personel_id');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->integer('sim_id')->unsigned();
            $table->foreign('sim_id')->references('sim_id')->on('sim')->onDelete('cascade');

            $table->string('sim_nomor')->nullable();

            $table->string('file', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sim_personel');
    }
}
