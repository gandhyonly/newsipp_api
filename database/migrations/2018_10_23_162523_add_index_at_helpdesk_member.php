<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexAtHelpdeskMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('helpdesks', function (Blueprint $table) {
            $table->index('nrp');
            $table->index('nama_lengkap');
            $table->index('satker_pertama');
            $table->index('nama_ibu');
            $table->index('tanggal_lahir');
            $table->index('satker_sekarang_id');
            $table->index('email');
            $table->index('handphone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('helpdesks', function (Blueprint $table) {
            //
        });
    }
}
