<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpdeskUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helpdesk_users', function (Blueprint $table) {
            $table->increments('helpdesk_user_id');
            $table->string('nrp')->unique();
            $table->string('nama');
            $table->string('password');
            $table->string('api_token',60)->unique();
            $table->boolean('is_superadmin')->default(false);
            $table->boolean('aktif')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helpdesk_users');
    }
}
