<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->increments('member_id');
            $table->string('nrp')->unique();

            $table->string('password')->nullable();
            $table->string('satker_pertama')->nullable();
            $table->string('nama_ibu')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('petikan_pertama')->nullable();
            $table->string('riwayat_hidup')->nullable();
            $table->string('email')->nullable();
            $table->string('handphone')->nullable();
            $table->date('tmt_jabatan_pertama')->nullable();
            $table->string('satker_sekarang')->nullable();
            $table->unsignedInteger('satuan_1')->nullable();
            $table->foreign('satuan_1')->references('satuan_id')->on('satuan')->onDelete('cascade');
            $table->unsignedInteger('satuan_2')->nullable();
            $table->foreign('satuan_2')->references('satuan_id')->on('satuan')->onDelete('cascade');
            $table->unsignedInteger('satuan_3')->nullable();
            $table->foreign('satuan_3')->references('satuan_id')->on('satuan')->onDelete('cascade');
            $table->unsignedInteger('satuan_4')->nullable();
            $table->foreign('satuan_4')->references('satuan_id')->on('satuan')->onDelete('cascade');

            $table->string('api_token',60)->nullable();

            $table->unsignedInteger('personel_id')->nullable();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('set null');

            $table->boolean('updated')->default(false);
            // $table->enum('status',['INVALID','UNVERIFIED','DOUBLE','VALID','READ','WAITING','VERIFIED'])->default('INVALID');
            $table->integer('tingkat')->unsigned()->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
