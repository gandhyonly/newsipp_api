<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLokasiPenugasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lokasi_penugasan', function (Blueprint $table) {
            $table->increments('lokasi_penugasan_id');

            $table->integer('negara_id')->unsigned();
            $table->foreign('negara_id')->references('negara_id')->on('negara')->onDelete('cascade');

            $table->string('nama_lembaga');
            $table->string('kota');
            $table->boolean('verified')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lokasi_penugasan');
    }
}
