<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiwayatPangkatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_pangkat', function (Blueprint $table) {
            $table->increments('riwayat_pangkat_id');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->integer('pangkat_id')->unsigned()->nullable();
            $table->foreign('pangkat_id')->references('pangkat_id')->on('pangkat')->onDelete('cascade');

            $table->boolean('status_terakhir')->default(false);
            $table->date('tmt_pangkat')->nullable();
            $table->string('surat_keputusan_nomor')->nullable();
            $table->string('surat_keputusan_file')->nullable();
            $table->timestamp('tanggal_perubahan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pangkat');
    }
}
