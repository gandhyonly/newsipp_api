<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBahasaPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahasa_personel', function (Blueprint $table) {
            $table->increments('bahasa_personel_id');

            $table->integer('bahasa_id')->unsigned();
            $table->foreign('bahasa_id')->references('bahasa_id')->on('bahasa')->onDelete('cascade');

            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');

            $table->boolean('aktif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bahasa_personel');
    }
}
