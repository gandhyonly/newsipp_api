<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePangkatJabatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('pangkat_jabatan', function (Blueprint $table) {
                $table->unsignedInteger('pangkat_id');
                $table->foreign('pangkat_id')->references('pangkat_id')->on('pangkat')->onDelete('cascade');

                $table->unsignedInteger('satuan_jabatan_id');
                $table->foreign('satuan_jabatan_id')->references('satuan_jabatan_id')->on('satuan_jabatan')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pangkat_jabatan');
    }
}
