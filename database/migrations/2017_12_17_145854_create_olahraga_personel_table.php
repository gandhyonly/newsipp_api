<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOlahragaPersonelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('olahraga_personel', function (Blueprint $table) {
            $table->increments('olahraga_personel_id');
            $table->integer('olahraga_id')->unsigned();
            $table->foreign('olahraga_id')->references('olahraga_id')->on('olahraga')->onDelete('cascade');
            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('olahraga_personel');
    }
}
