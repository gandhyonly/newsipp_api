<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumn3AtTablePersonelData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personel_data', function (Blueprint $table) {
            $table->string('masa_dinas_surut_file')->nullable();
            $table->string('peninjauan_masa_kerja_nomor')->nullable();
            $table->string('peninjauan_masa_kerja_file')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personel_data', function (Blueprint $table) {
            //
        });
    }
}
