<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisSatuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // if (!Schema::hasTable('jenis_satuan')) {
            Schema::create('jenis_satuan', function (Blueprint $table) {
                $table->increments('jenis_satuan_id');
                $table->string('nama_jenis_satuan')->unique();
                $table->string('kategori');
            });
        // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_satuan');
    }
}
