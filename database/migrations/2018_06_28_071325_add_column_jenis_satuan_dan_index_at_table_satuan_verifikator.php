<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnJenisSatuanDanIndexAtTableSatuanVerifikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('satuan_verifikator', function (Blueprint $table) {
            $table->dropColumn(['tipe_satuan']);
            $table->boolean('aktif');
            $table->string('jenis_satuan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('satuan_verifikator', function (Blueprint $table) {
            //
        });
    }
}
