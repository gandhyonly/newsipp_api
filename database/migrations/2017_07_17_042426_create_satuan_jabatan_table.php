<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSatuanJabatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satuan_jabatan', function (Blueprint $table) {
            $table->increments('satuan_jabatan_id');

            $table->string('keterangan')->nullable();
            $table->integer('dsp');

            $table->unsignedInteger('parent_satuan_jabatan_id')->nullable();
            $table->foreign('parent_satuan_jabatan_id')->references('satuan_jabatan_id')->on('satuan_jabatan')->onDelete('cascade');

            $table->unsignedInteger('jabatan_id')->nullable();
            $table->foreign('jabatan_id')->references('jabatan_id')->on('jabatan')->onDelete('cascade');

            $table->unsignedInteger('satuan_id')->nullable();
            $table->foreign('satuan_id')->references('satuan_id')->on('satuan')->onDelete('cascade');

            $table->unsignedInteger('status_jabatan_id')->nullable();
            $table->foreign('status_jabatan_id')->references('status_jabatan_id')->on('status_jabatan')->onDelete('cascade');

            $table->unsignedInteger('nivelering_id')->nullable();
            $table->foreign('nivelering_id')->references('nivelering_id')->on('nivelering')->onDelete('cascade');

            $table->unsignedInteger('verified_by')->nullable();
            $table->foreign('verified_by')->references('id')->on('users')->onDelete('cascade');


            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('set null');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satuan_jabatan');
    }
}
