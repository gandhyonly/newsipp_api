<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNiveleringTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nivelering', function (Blueprint $table) {
            $table->increments('nivelering_id');
            $table->string('nama_nivelering');
            $table->integer('eselon_id')->unsigned();
            $table->foreign('eselon_id')->references('eselon_id')->on('eselon')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nivelering');
    }
}
