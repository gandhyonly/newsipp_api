<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('misi', function (Blueprint $table) {
            $table->increments('misi_id');
            $table->string('nama_misi', 100);
            $table->string('kota', 100);
            $table->boolean('verified')->default(false);
            $table->integer('negara_id')->unsigned();
            $table->foreign('negara_id')->references('negara_id')->on('negara')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('misi');
    }
}
