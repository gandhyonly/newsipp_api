<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePekerjaanKeluargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pekerjaan_keluarga', function (Blueprint $table) {
            $table->increments('pekerjaan_keluarga_id');
            $table->string('nama_institusi')->nullable();
            $table->string('telepon')->nullable();
            $table->string('alamat')->nullable();

            $table->boolean('status_terakhir')->default(false);

            $table->unsignedInteger('jenis_pekerjaan_id')->nullable();
            $table->foreign('jenis_pekerjaan_id')->references('jenis_pekerjaan_id')->on('jenis_pekerjaan')->onDelete('set null');
            $table->unsignedInteger('keluarga_personel_id');
            $table->foreign('keluarga_personel_id')->references('keluarga_personel_id')->on('keluarga_personel')->onDelete('cascade');
            $table->timestamp('tanggal_perubahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pekerjaan_keluarga');
    }
}
