<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiwayatJabatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_jabatan', function (Blueprint $table) {
            $table->increments('riwayat_jabatan_id');
            $table->integer('personel_id')->unsigned();
            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');
            $table->integer('satuan_jabatan_id')->unsigned()->nullable();
            $table->foreign('satuan_jabatan_id')->references('satuan_jabatan_id')->on('satuan_jabatan')->onDelete('cascade');
            $table->boolean('status_aktif')->default(false);
            $table->date('tmt_jabatan')->nullable();
            $table->date('tmt_selesai')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('surat_keputusan_nomor')->nullable();
            $table->string('surat_keputusan_file')->nullable();
            $table->timestamp('tanggal_perubahan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_jabatan');
    }
}
