<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGelarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('gelar', function (Blueprint $table) {
            $table->increments('gelar_id');

            $table->string('gelar');
            $table->boolean('gelar_posisi_sebelum')->default(false);

            $table->unsignedInteger('jurusan_id');
            $table->foreign('jurusan_id')->references('jurusan_id')->on('jurusan')->onDelete('cascade');

            $table->unsignedInteger('gelar_sebelum')->nullable();
            $table->foreign('gelar_sebelum')->references('gelar_id')->on('gelar')->onDelete('cascade');

            $table->boolean('verified')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gelar');
    }
}
