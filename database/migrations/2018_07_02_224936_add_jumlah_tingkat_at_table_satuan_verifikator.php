<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJumlahTingkatAtTableSatuanVerifikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('satuan_verifikator', function (Blueprint $table) {
            $table->integer('jumlah_personel_tingkat_1')->unsigned()->default(0);
            $table->integer('jumlah_personel_tingkat_2')->unsigned()->default(0);
            $table->integer('jumlah_personel_tingkat_3')->unsigned()->default(0);
            $table->integer('jumlah_personel_tingkat_4')->unsigned()->default(0);
            $table->integer('jumlah_personel_tingkat_5')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('satuan_verifikator', function (Blueprint $table) {
            //
        });
    }
}
