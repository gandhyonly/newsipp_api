<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonelTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personel', function (Blueprint $table) {
            $table->increments('personel_id');
            // Biodata
            $table->string('nama_lengkap')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('status_pernikahan')->nullable();
            $table->integer('agama_id')->unsigned()->nullable();
            $table->foreign('agama_id')->references('agama_id')->on('agama');
            // tidak ada pada form
            $table->string('satker_pertama')->nullable(); //sementara nullable
            $table->string('nama_ibu')->nullable(); //sementara nullable
            $table->date('tmt_jabatan_pertama')->nullable(); //sementara nullable
            $table->date('tmt_pertama')->nullable(); //sementara nullable
            $table->string('nrp')->nullable();
            $table->boolean('polisi')->nullable();
            $table->mediumText('error_message')->nullable();

            $table->string('foto_file')->nullable();
            $table->string('database')->nullable();

            $table->unsignedInteger('status_personel_id')->default(1);//sementara nullable
            $table->foreign('status_personel_id')->references('status_personel_id')->on('status_personel');
            $table->timestamp('tanggal_pembuatan')->nullable();
            $table->timestamp('tanggal_perubahan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personel');
    }
}
