<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesanMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesan_member', function (Blueprint $table) {
            $table->increments('pesan_member_id');
            $table->integer('member_id')->unsigned();
            $table->foreign('member_id')->references('member_id')->on('members')->onDelete('cascade');
            $table->mediumText('judul')->nullable();
            $table->mediumText('pesan')->nullable();
            $table->integer('verifikator_user_id')->unsigned()->nullable();
            $table->foreign('verifikator_user_id')->references('verifikator_user_id')->on('verifikator_user')->onDelete('cascade');
            $table->integer('tingkat')->unsigned();
            $table->boolean('bermasalah')->default(false);
            $table->timestamp('tanggal_dibuat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesan_member');
    }
}
