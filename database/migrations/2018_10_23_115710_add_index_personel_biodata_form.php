<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexPersonelBiodataForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personel', function (Blueprint $table) {
            $table->index('personel_id');
            $table->index('status_personel_id');
        });
        Schema::table('personel_data', function (Blueprint $table) {
            $table->index('personel_id');
        });
        Schema::table('fisik_personel', function (Blueprint $table) {
            $table->index('personel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personel', function (Blueprint $table) {
            //
        });
    }
}
