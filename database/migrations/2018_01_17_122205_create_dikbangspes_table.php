<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDikbangspesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dikbangspes', function (Blueprint $table) {
            $table->increments('dikbangspes_id');
            $table->integer('tipe_dikbangspes_id')->unsigned();
            $table->foreign('tipe_dikbangspes_id')->references('tipe_dikbangspes_id')->on('tipe_dikbangspes')->onDelete('cascade');
            $table->string('nama_dikbangspes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dikbangspes');
    }
}
