<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiwayatBrevetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_brevet', function (Blueprint $table) {
            $table->increments('riwayat_brevet_id');
            $table->integer('personel_id')->unsigned();
            $table->integer('brevet_id')->unsigned();

            $table->foreign('personel_id')->references('personel_id')->on('personel')->onDelete('cascade');
            $table->foreign('brevet_id')->references('brevet_id')->on('brevet')->onDelete('cascade');
            $table->date('tmt_brevet');
            $table->string('surat_keputusan_nomor', 100);
            $table->string('surat_keputusan_file', 100)->nullable();
            $table->timestamp('tanggal_perubahan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_brevet');
    }
}
