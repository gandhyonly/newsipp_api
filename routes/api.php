<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'v1'],function(){
	Route::group(['namespace' => 'Helpdesk'], function() {
		Route::post('helpdesk-user/login','HelpdeskUserController@login');

		Route::group(['middleware' => 'auth:api-helpdesk'], function() {
	        Route::get('helpdesk/countData','HelpdeskController@countAll');
			Route::get('helpdesk/get-data','HelpdeskController@getData');
			Route::get('helpdesk/export-data-excel','HelpdeskController@exportToExcel');
			Route::get('helpdesk/export-data-excel-by-date','HelpdeskController@exportToExcelByDate');
			Route::get('helpdesk/detail','HelpdeskController@detail');
			Route::post('helpdesk/verified/{helpdesk}','HelpdeskController@verified');
			Route::post('helpdesk/tambah-personel','HelpdeskController@tambahPersonel');
			Route::post('helpdesk/hapus/{id}','HelpdeskController@hapus');
		});
	});
	Route::group(['namespace' => 'Verifikator'], function() {
		Route::post('verifikator/login','VerifikatorController@login');

		Route::group(['middleware' => 'auth:api-verifikator'], function() {
			Route::post('verifikator/ganti-password','VerifikatorController@gantiPassword');
			Route::get('verifikator/count-personel','PersonelController@countPersonel');
			Route::get('verifikator/daftar-personel','PersonelController@daftarPersonel');
			Route::get('verifikator/daftar-personel/{status}','PersonelController@daftarPersonelFilter');
			Route::get('verifikator/detail-personel/{member}','PersonelController@detailPersonel');
			Route::post('verifikator/tolak','VerifikatorController@tolak');
			Route::post('verifikator/verifikasi','VerifikatorController@verifikasi');
			Route::get('verifikator/daftar-verifikator','VerifikatorController@daftarVerifikator');
			Route::post('verifikator/tambah-verifikator','VerifikatorController@tambahVerifikator');
			Route::delete('verifikator/hapus-verifikator/{verifikatorUser}','VerifikatorController@hapusVerifikator');
			Route::patch('verifikator/aktifkan-verifikator/{verifikatorUser}','VerifikatorController@aktifkanVerifikator');
			Route::patch('verifikator/reset-password-verifikator/{verifikatorUser}','VerifikatorController@resetPasswordVerifikator');
		});
	});
	Route::group(['namespace' => 'Admin'], function() {
		Route::post('admin/login','AdminController@login');

		Route::group(['middleware' => 'auth:api-admin'], function() {
			Route::post('admin/tambah-helpdesk','AdminController@tambahHelpdesk');
			Route::post('admin/tambah-verifikator','AdminController@tambahVerifikator');
			Route::get('admin/daftar-helpdesk','AdminController@daftarHelpdesk');
			Route::get('admin/daftar-verifikator','AdminController@daftarVerifikator');
			Route::delete('admin/hapus-verifikator/{verifikatorUser}','AdminController@hapusVerifikator');
			Route::patch('admin/aktifkan-verifikator/{verifikatorUser}','AdminController@aktifkanVerifikator');
			Route::patch('admin/reset-password-verifikator/{verifikatorUser}','AdminController@resetPasswordVerifikator');
			Route::delete('admin/hapus-helpdesk/{helpdeskUser}','AdminController@hapusHelpdeskUser');
			Route::patch('admin/aktifkan-helpdesk/{helpdeskUser}','AdminController@aktifkanHelpdeskUser');
			Route::patch('admin/reset-password-helpdesk/{helpdeskUser}','AdminController@resetPasswordHelpdeskUser');
		});
	});
	Route::group(['namespace' => 'API'], function() {
		Route::get('satuan/listChild/{id}','SatuanController@listChild');
		Route::get('satuan/listJabatan/{id}','SatuanController@listJabatan');
		Route::get('satuan/daftar-jabatan/{satuan}','SatuanController@daftarJabatan');
		Route::get('satuan/daftar-satuan-aktif/{satuan}','SatuanController@daftarSatuanAktif');
		Route::get('satuan/daftar-satuan/{satuan}','SatuanController@daftarSatuan');
		Route::post('member/helpdesk','MemberController@helpdesk');
		Route::post('member/checkNRP','MemberController@checkNRP');
		Route::post('member/registrasi','MemberController@registrasi');
		Route::post('member/lupa-password','MemberController@lupaPassword');
		Route::post('member/registrasi-data-duplikat','MemberController@registrasiDataDuplikat');
		Route::post('member/registrasi-data-tidak-ditemukan','MemberController@registrasiDataTidakDitemukan');
		Route::post('/login','MemberController@login');
		Route::post('testing-file-upload','FileController@fileUploadTesting');
		Route::post('testing-post','FileController@testingPost');
		Route::get('get-file-testing','FileController@getFileTesting');
		Route::get('get-file','FileController@getFile');
		Route::post('get-form-file','FileController@getFileForm');
		Route::group(['middleware' => 'auth:api'], function() {
			Route::post('member/reset-satuan-verifikator','MemberController@resetSatuanVerifikator');
			Route::post('member/ganti-password','MemberController@gantiPassword');
			Route::get('personel/pesan','PersonelController@daftarPesanMember');
			// Route::post('personel/getData','PersonelController@getData');
			Route::post('personel/data-biodata','PersonelController@dataBiodata');
			Route::post('personel/data-riwayat-jabatan','PersonelController@dataRiwayatJabatan');
			Route::post('personel/data-riwayat-alamat','PersonelController@dataRiwayatAlamat');
			Route::post('personel/data-kondisi-fisik','PersonelController@dataKondisiFisik');
			Route::post('personel/data-nomor-identitas','PersonelController@dataNomorIdentitas');
			Route::post('personel/data-keluarga','PersonelController@dataKeluarga');
			Route::post('personel/data-pendidikan-umum','PersonelController@dataPendidikanUmum');
			Route::post('personel/data-pendidikan-polisi','PersonelController@dataPendidikanPolisi');
			Route::post('personel/data-dikbangspes','PersonelController@dataDikbangspes');
			Route::post('personel/data-riwayat-pangkat','PersonelController@dataRiwayatPangkat');
			Route::post('personel/data-tanda-kehormatan','PersonelController@dataTandaKehormatan');
			Route::post('personel/data-penghargaan','PersonelController@dataPenghargaan');
			Route::post('personel/data-bahasa-asing','PersonelController@dataBahasaAsing');
			Route::post('personel/data-bahasa-daerah','PersonelController@dataBahasaDaerah');
			Route::post('personel/data-olahraga','PersonelController@dataOlahraga');
			Route::post('personel/data-hobi','PersonelController@dataHobi');
			Route::post('personel/data-penugasan-luar-struktur','PersonelController@dataPenugasanLuarStruktur');
			Route::post('personel/data-misi','PersonelController@dataMisi');
			Route::post('personel/data-brevet','PersonelController@dataBrevet');
			Route::post('personel/data-bko','PersonelController@dataBKO');
			Route::get('personel/status-form','MemberController@getStatusForm');
			Route::get('get-file-personel','FileController@getFilePersonel');
			Route::patch('member/ajukan-verifikator','MemberController@ajukanVerifikator');
			// Biodata
			Route::post('personel/formBiodata','PersonelController@formBiodata');
			Route::post('personel/formKondisiFisik','PersonelController@formKondisiFisik');
			Route::post('personel/formNomorIdentitas','PersonelController@formNomorIdentitas');
			Route::post('sim/tambah-sim','SIMController@tambahSIM');
			Route::patch('sim/edit-sim/{SIMPersonel}','SIMController@editSIM');
			Route::delete('sim/hapus-sim/{SIMPersonel}','SIMController@hapusSIM');
			// Pendidikan Umum
			Route::post('pendidikan/tambah-pendidikan-umum','PendidikanUmumController@tambahPendidikanUmum');
			Route::patch('pendidikan/edit-pendidikan-umum/{pendidikanPersonel}','PendidikanUmumController@editPendidikanUmum');
			Route::delete('pendidikan/hapus-pendidikan-umum/{pendidikanPersonel}','PendidikanUmumController@hapusPendidikanUmum');
			// Dikpol
			Route::post('pendidikan/tambah-dikpol','PendidikanPolisiController@tambahPendidikanPolisi');
			Route::patch('pendidikan/edit-dikpol/{pendidikanPersonel}','PendidikanPolisiController@editPendidikanPolisi');
			Route::delete('pendidikan/hapus-dikpol/{pendidikanPersonel}','PendidikanPolisiController@hapusPendidikanPolisi');
			// Dikbangspes
			Route::post('pendidikan/tambah-dikbangspes','DikbangspesController@tambahDikbangspes');
			Route::patch('pendidikan/edit-dikbangspes/{pendidikanPersonel}','DikbangspesController@editDikbangspes');
			Route::delete('pendidikan/hapus-dikbangspes/{pendidikanPersonel}','DikbangspesController@hapusDikbangspes');
			// end
			Route::post('personel/formTandaJasa','PersonelController@formTandaJasa');
			// Keluarga
			Route::post('keluarga/tambah-keluarga','KeluargaController@tambahKeluarga');
			Route::patch('keluarga/edit-keluarga/{keluargaPersonel}','KeluargaController@editKeluarga');
			Route::delete('keluarga/hapus-keluarga/{keluargaPersonel}','KeluargaController@hapusKeluarga');
			// Bahasa
			Route::post('bahasa/tambah-bahasa-daerah','BahasaController@tambahBahasaDaerah');
			Route::patch('bahasa/edit-bahasa-daerah/{bahasaPersonel}','BahasaController@editBahasaDaerah');
			Route::post('bahasa/tambah-bahasa-international','BahasaController@tambahBahasaInternational');
	        Route::patch('bahasa/edit-bahasa-international/{bahasaPersonel}','BahasaController@editBahasaAsing');
			Route::delete('bahasa/hapus-bahasa/{bahasaPersonel}','BahasaController@hapusBahasa');
			// Tanda Kehormatan
			Route::post('tanda-kehormatan/tambah-tanda-kehormatan', 'TandaKehormatanController@tambahTandaKehormatan');
	        Route::patch('tanda-kehormatan/edit-tanda-kehormatan/{tandaKehormatanPersonel}', 'TandaKehormatanController@editTandaKehormatan');
			Route::delete('tanda-kehormatan/hapus-tanda-kehormatan/{tandaKehormatanPersonel}', 'TandaKehormatanController@hapusTandaKehormatan');
			// Hobi
			Route::post('hobi/tambah-hobi', 'HobiController@tambahHobi');
	        Route::patch('hobi/edit-hobi/{hobiPersonel}','HobiController@editHobi');
			Route::delete('hobi/hapus-hobi/{hobiPersonel}', 'HobiController@hapusHobi');
			// Olahraga
			Route::post('olahraga/tambah-olahraga', 'OlahragaController@tambahOlahraga');
			Route::delete('olahraga/hapus-olahraga/{olahragaPersonel}', 'OlahragaController@hapusOlahraga');
	        Route::patch('olahraga/edit-olahraga/{olahragaPersonel}','OlahragaController@editOlaharaga');
			// alamat
			Route::post('alamat/tambah-alamat', 'AlamatController@tambahAlamat');
			Route::patch('alamat/edit-alamat/{alamat}', 'AlamatController@editAlamat');
			Route::delete('alamat/hapus-alamat/{alamat}', 'AlamatController@hapusAlamat');
			// Kesehatan
			Route::post('kesehatan/tambah-kesehatan', 'KesehatanController@tambahKesehatan');
	        Route::patch('kesehatan/edit-kesehatan/{kesehatanPersonel}','KesehatanController@editKesehatan');
			Route::delete('kesehatan/hapus-kesehatan/{kesehatanPersonel}', 'KesehatanController@hapusKesehatan');
			// Jasmani
			// Route::post('jasmani/tambah-jasmani', 'JasmaniController@tambahJasmani');
			// Psikologi
			Route::post('psikologi/tambah-psikologi', 'PsikologiController@tambahRiwayatPsikologi');
	        Route::put('psikologi/edit-put-psikologi/{riwayatPsikologi}','PsikologiController@editPutRiwayatPsikologi');
	        Route::patch('psikologi/edit-psikologi/{riwayatPsikologi}','PsikologiController@editRiwayatPsikologi');
			Route::delete('psikologi/hapus-psikologi/{riwayatPsikologi}', 'PsikologiController@hapusRiwayatPsikologi');
			// Penghargaan
			Route::post('penghargaan/tambah-penghargaan', 'PenghargaanController@tambahPenghargaan');
			Route::patch('penghargaan/edit-penghargaan/{penghargaanPersonel}', 'PenghargaanController@editPenghargaan');
			Route::delete('penghargaan/hapus-penghargaan/{penghargaanPersonel}', 'PenghargaanController@hapusPenghargaan');
			// Brevet
			Route::post('brevet/tambah-brevet', 'BrevetController@tambahRiwayatBrevet');
			Route::delete('brevet/hapus-brevet/{riwayatBrevet}', 'BrevetController@hapusRiwayatBrevet');
	        Route::patch('brevet/edit-brevet/{riwayatBrevet}','BrevetController@editRiwayatBrevet');
			// BKO
			Route::post('bko/tambah-bko', 'BKOController@tambahBKO');
			Route::patch('bko/edit-bko/{bko}', 'BKOController@editBKO');
			Route::delete('bko/hapus-bko/{bko}', 'BKOController@hapusBKO');
			// Penugasan Luar dan Dalam Negeri
			Route::post('penugasan-negara/tambah-penugasan-negara', 'PenugasanNegaraController@tambahPenugasanNegara');
			Route::patch('penugasan-negara/edit-penugasan-negara/{penugasan}', 'PenugasanNegaraController@editPenugasanNegara');
			Route::delete('penugasan-negara/hapus-penugasan-negara/{penugasan}', 'PenugasanNegaraController@hapusPenugasanNegara');
			// Penugasan Luar Struktur
			Route::post('penugasan-luar-struktur/tambah-penugasan-luar-struktur', 'PenugasanLuarStrukturController@tambahPenugasanLuarStruktur');
			Route::patch('penugasan-luar-struktur/edit-penugasan-luar-struktur/{penugasan}', 'PenugasanLuarStrukturController@editPenugasanLuarStruktur');
			Route::delete('penugasan-luar-struktur/hapus-penugasan-luar-struktur/{penugasan}', 'PenugasanLuarStrukturController@hapusPenugasanLuarStruktur');
			// Pangkat
			Route::post('pangkat/tambah-pangkat', 'PangkatController@tambahRiwayatPangkat');
			Route::patch('pangkat/edit-pangkat/{riwayatPangkat}', 'PangkatController@editRiwayatPangkat');
			Route::delete('pangkat/hapus-pangkat/{riwayatPangkat}', 'PangkatController@hapusRiwayatPangkat');
			// Jabatan
			Route::post('jabatan/tambah-jabatan', 'JabatanController@tambahRiwayatJabatan');
			Route::patch('jabatan/edit-jabatan/{riwayatJabatan}', 'JabatanController@editRiwayatJabatan');
			Route::delete('jabatan/hapus-jabatan/{riwayatJabatan}', 'JabatanController@hapusRiwayatJabatan');


			// Alamat
			Route::get('alamat/daftar-alamat', 'AlamatController@daftarAlamat');
			Route::get('alamat/alamat-terakhir', 'AlamatController@alamatTerakhir');
		});

		Route::get('jabatan/urutan-satuan/{riwayatJabatan}', 'JabatanController@urutanSatuan');
		Route::get('eselon/daftar-eselon','EselonController@daftarEselon');
		// Pendidikan
		Route::get('pendidikan/daftar-gelar','PendidikanUmumController@daftarGelar');
		Route::get('pendidikan/daftar-gelar-dikpol/{jurusanID}','PendidikanPolisiController@daftarGelarDikpol');
		Route::get('pendidikan/daftar-konsentrasi/{jurusan}','PendidikanUmumController@daftarKonsentrasi');
		Route::get('pendidikan/daftar-dikum','PendidikanUmumController@daftarDikum');
		Route::get('pendidikan/daftar-jenis-dikpol','PendidikanPolisiController@daftarJenisDikpol');
		Route::get('pendidikan/daftar-dikpol/{jenisDikpol}','PendidikanPolisiController@daftarDikpol');
		Route::get('pendidikan/daftar-tipe-dikbangspes','DikbangspesController@daftarTipeDikbangspes');
		Route::get('pendidikan/daftar-dikbangspes/{tipeDikbangspes}','DikbangspesController@daftarDikbangspes');
		// end Pendidikan
		Route::get('tanda-kehormatan/daftar-tanda-kehormatan', 'TandaKehormatanController@daftarTandaKehormatan');
		Route::get('hobi/daftar-hobi', 'HobiController@daftarHobi');
		Route::get('pangkat/daftar-pangkat', 'PangkatController@daftarPangkat');
		Route::get('brevet/daftar-brevet', 'BrevetController@daftarBrevet');
		Route::get('bko/daftar-jenis-bko', 'BKOController@daftarJenisBKO');
		// Route::get('penugasan/daftar-misi', 'PenugasanController@daftarMisi');
		Route::get('sim/daftar-sim', 'SIMController@daftarSIM');
		Route::get('penugasan-negara/daftar-misi/{negara}', 'PenugasanNegaraController@daftarMisi');
		Route::get('penugasan-negara/daftar-jabatan-penugasan/{misi}', 'PenugasanNegaraController@daftarJabatanPenugasan');
		Route::get('penugasan-luar-struktur/daftar-lokasi-penugasan/{negara}', 'PenugasanLuarStrukturController@daftarLokasiPenugasan');
		Route::get('penugasan-luar-struktur/daftar-jabatan-penugasan/{lokasiPenugasan}', 'PenugasanLuarStrukturController@daftarJabatanPenugasan');
		Route::get('negara/daftar-negara', 'NegaraController@daftarNegara');
		Route::get('agama/daftar-agama', 'AgamaController@daftarAgama');
		Route::get('psikologi/daftar-psikologi', 'PsikologiController@daftarPsikologi');
		Route::get('olahraga/daftar-olahraga', 'OlahragaController@daftarOlahraga');
		Route::get('penghargaan/daftar-penghargaan','PenghargaanController@daftarPenghargaan');
		Route::get('penghargaan/daftar-tingkat-penghargaan','PenghargaanController@daftarTingkatPenghargaan');
		Route::get('kesehatan/daftar-kesehatan','KesehatanController@daftarKesehatan');
		// Route::get('jasmani/daftar-jasmani','JasmaniController@daftarJasmani');
		Route::get('kota/daftar-kota','KotaController@daftarKota');
		Route::get('personel/daftar-status-personel','PersonelController@daftarStatusPersonel');
			
		Route::get('bahasa/daftar-bahasa-daerah','BahasaController@daftarBahasaDaerah');
		Route::get('bahasa/daftar-bahasa-international','BahasaController@daftarBahasaInternational');
		Route::get('jurusan/daftar-jurusan','JurusanController@daftarJurusan');
		Route::get('jurusan/daftar-jurusan-dikpol','JurusanController@daftarJurusanDikpol');
		Route::get('jurusan/daftar-jurusan-by-dikum/{dikumID}','JurusanController@daftarJurusanByDikum');
		Route::get('golongan/daftar-golongan','GolonganController@daftarGolongan');
		Route::get('keluarga/daftar-hubungan-keluarga','KeluargaController@daftarHubunganKeluarga');
		Route::get('keluarga/sejarah-pekerjaan-keluarga','KeluargaController@sejarahPekerjaanKeluarga');

		Route::get('enum/hubungan-keluarga','KeluargaController@daftarHubunganKeluarga');
		Route::get('jenis-pekerjaan/daftar-jenis-pekerjaan','KeluargaController@daftarJenisPekerjaan');
		Route::get('satuan/daftar-satuan-bawah','SatuanController@listChild');
		// Route::get('satuan/daftar-satuan','SatuanController@listChild');
		// Route::get('satuan/daftar-satuan-pertama','SatuanController@daftarSatuanPertama');
		Route::post('/upload-file','FileController@uploadFile');

		// Enum Other
		Route::get('enum/daftar-jenis-kelamin', 'OtherController@daftarJenisKelamin');
		Route::get('enum/daftar-golongan-darah', 'OtherController@daftarGolonganDarah');
		Route::get('enum/daftar-status-pernikahan', 'OtherController@daftarStatusPernikahan');
		Route::get('enum/daftar-warna-kulit', 'OtherController@daftarWarnaKulit');
		Route::get('enum/daftar-suku-bangsa', 'OtherController@daftarSukuBangsa');
		Route::get('enum/daftar-jenis-rambut', 'OtherController@daftarJenisRambut');
		Route::get('enum/daftar-warna-rambut', 'OtherController@daftarWarnaRambut');
		Route::get('enum/daftar-warna-mata', 'OtherController@daftarWarnaMata');
		Route::get('enum/daftar-akreditasi', 'OtherController@daftarAkreditasi');
		Route::get('enum/daftar-status-keluarga', 'OtherController@daftarStatusKeluarga');
	});
	Route::post('/sort', '\Rutorika\Sortable\SortableController@sort')->name('sort');
});
Route::group(['prefix' => 'mobile_v1'],function(){
	Route::group(['namespace' => 'API'], function() {
		Route::get('satuan/listChild/{id}','SatuanController@listChild');
		Route::get('satuan/listJabatan/{id}','SatuanController@listJabatan');
		Route::get('satuan/daftar-jabatan/{satuan}','SatuanController@daftarJabatan');
		Route::get('satuan/daftar-satuan-aktif/{satuan}','SatuanController@daftarSatuanAktif');
		Route::get('satuan/daftar-satuan/{satuan}','SatuanController@daftarSatuan');
		Route::post('member/helpdesk','MemberController@helpdesk');
		Route::post('member/checkNRP','MemberController@checkNRP');
		Route::post('member/registrasi','MemberController@registrasi');
		Route::post('member/lupa-password','MemberController@lupaPassword');
		Route::post('member/registrasi-data-duplikat','MemberController@registrasiDataDuplikat');
		Route::post('member/registrasi-data-tidak-ditemukan','MemberController@registrasiDataTidakDitemukan');
		Route::post('/login','MemberController@login');
		Route::post('testing-file-upload','FileController@fileUploadTesting');
		Route::post('testing-post','FileController@testingPost');
		Route::get('get-file-testing','FileController@getFileTesting');
		Route::get('get-file','FileController@getFile');
		Route::post('get-form-file','FileController@getFileForm');
		Route::group(['middleware' => 'auth:api'], function() {
			Route::post('member/reset-satuan-verifikator','MemberController@resetSatuanVerifikator');
			Route::post('member/ganti-password','MemberController@gantiPassword');
			Route::get('personel/pesan','PersonelController@daftarPesanMember');
			// Route::post('personel/getData','PersonelController@getData');
			Route::post('personel/data-biodata','PersonelController@dataBiodata');
			Route::post('personel/data-riwayat-jabatan','PersonelController@dataRiwayatJabatan');
			Route::post('personel/data-riwayat-alamat','PersonelController@dataRiwayatAlamat');
			Route::post('personel/data-kondisi-fisik','PersonelController@dataKondisiFisik');
			Route::post('personel/data-nomor-identitas','PersonelController@dataNomorIdentitas');
			Route::post('personel/data-keluarga','PersonelController@dataKeluarga');
			Route::post('personel/data-pendidikan-umum','PersonelController@dataPendidikanUmum');
			Route::post('personel/data-pendidikan-polisi','PersonelController@dataPendidikanPolisi');
			Route::post('personel/data-dikbangspes','PersonelController@dataDikbangspes');
			Route::post('personel/data-riwayat-pangkat','PersonelController@dataRiwayatPangkat');
			Route::post('personel/data-tanda-kehormatan','PersonelController@dataTandaKehormatan');
			Route::post('personel/data-penghargaan','PersonelController@dataPenghargaan');
			Route::post('personel/data-bahasa-asing','PersonelController@dataBahasaAsing');
			Route::post('personel/data-bahasa-daerah','PersonelController@dataBahasaDaerah');
			Route::post('personel/data-olahraga','PersonelController@dataOlahraga');
			Route::post('personel/data-hobi','PersonelController@dataHobi');
			Route::post('personel/data-penugasan-luar-struktur','PersonelController@dataPenugasanLuarStruktur');
			Route::post('personel/data-misi','PersonelController@dataMisi');
			Route::post('personel/data-brevet','PersonelController@dataBrevet');
			Route::post('personel/data-bko','PersonelController@dataBKO');
			Route::get('personel/status-form','MemberController@getStatusForm');
			Route::get('get-file-personel','FileController@getFilePersonel');
			Route::patch('member/ajukan-verifikator','MemberController@ajukanVerifikator');
			// Biodata
			Route::post('personel/formBiodata','PersonelController@formBiodata');
			Route::post('personel/formKondisiFisik','PersonelController@formKondisiFisik');
			Route::post('personel/formNomorIdentitas','PersonelController@formNomorIdentitas');
			Route::post('sim/tambah-sim','SIMController@tambahSIM');
			Route::patch('sim/edit-sim/{SIMPersonel}','SIMController@editSIM');
			Route::delete('sim/hapus-sim/{SIMPersonel}','SIMController@hapusSIM');
			// Pendidikan Umum
			Route::post('pendidikan/tambah-pendidikan-umum','PendidikanUmumController@tambahPendidikanUmum');
			Route::patch('pendidikan/edit-pendidikan-umum/{pendidikanPersonel}','PendidikanUmumController@editPendidikanUmum');
			Route::delete('pendidikan/hapus-pendidikan-umum/{pendidikanPersonel}','PendidikanUmumController@hapusPendidikanUmum');
			// Dikpol
			Route::post('pendidikan/tambah-dikpol','PendidikanPolisiController@tambahPendidikanPolisi');
			Route::patch('pendidikan/edit-dikpol/{pendidikanPersonel}','PendidikanPolisiController@editPendidikanPolisi');
			Route::delete('pendidikan/hapus-dikpol/{pendidikanPersonel}','PendidikanPolisiController@hapusPendidikanPolisi');
			// Dikbangspes
			Route::post('pendidikan/tambah-dikbangspes','DikbangspesController@tambahDikbangspes');
			Route::patch('pendidikan/edit-dikbangspes/{pendidikanPersonel}','DikbangspesController@editDikbangspes');
			Route::delete('pendidikan/hapus-dikbangspes/{pendidikanPersonel}','DikbangspesController@hapusDikbangspes');
			// end
			Route::post('personel/formTandaJasa','PersonelController@formTandaJasa');
			// Keluarga
			Route::post('keluarga/tambah-keluarga','KeluargaController@tambahKeluarga');
			Route::patch('keluarga/edit-keluarga/{keluargaPersonel}','KeluargaController@editKeluarga');
			Route::delete('keluarga/hapus-keluarga/{keluargaPersonel}','KeluargaController@hapusKeluarga');
			// Bahasa
			Route::post('bahasa/tambah-bahasa-daerah','BahasaController@tambahBahasaDaerah');
			Route::patch('bahasa/edit-bahasa-daerah/{bahasaPersonel}','BahasaController@editBahasaDaerah');
			Route::post('bahasa/tambah-bahasa-international','BahasaController@tambahBahasaInternational');
	        Route::patch('bahasa/edit-bahasa-international/{bahasaPersonel}','BahasaController@editBahasaAsing');
			Route::delete('bahasa/hapus-bahasa/{bahasaPersonel}','BahasaController@hapusBahasa');
			// Tanda Kehormatan
			Route::post('tanda-kehormatan/tambah-tanda-kehormatan', 'TandaKehormatanController@tambahTandaKehormatan');
	        Route::patch('tanda-kehormatan/edit-tanda-kehormatan/{tandaKehormatanPersonel}', 'TandaKehormatanController@editTandaKehormatan');
			Route::delete('tanda-kehormatan/hapus-tanda-kehormatan/{tandaKehormatanPersonel}', 'TandaKehormatanController@hapusTandaKehormatan');
			// Hobi
			Route::post('hobi/tambah-hobi', 'HobiController@tambahHobi');
	        Route::patch('hobi/edit-hobi/{hobiPersonel}','HobiController@editHobi');
			Route::delete('hobi/hapus-hobi/{hobiPersonel}', 'HobiController@hapusHobi');
			// Olahraga
			Route::post('olahraga/tambah-olahraga', 'OlahragaController@tambahOlahraga');
			Route::delete('olahraga/hapus-olahraga/{olahragaPersonel}', 'OlahragaController@hapusOlahraga');
	        Route::patch('olahraga/edit-olahraga/{olahragaPersonel}','OlahragaController@editOlaharaga');
			// alamat
			Route::post('alamat/tambah-alamat', 'AlamatController@tambahAlamat');
			Route::patch('alamat/edit-alamat/{alamat}', 'AlamatController@editAlamat');
			Route::delete('alamat/hapus-alamat/{alamat}', 'AlamatController@hapusAlamat');
			// Kesehatan
			Route::post('kesehatan/tambah-kesehatan', 'KesehatanController@tambahKesehatan');
	        Route::patch('kesehatan/edit-kesehatan/{kesehatanPersonel}','KesehatanController@editKesehatan');
			Route::delete('kesehatan/hapus-kesehatan/{kesehatanPersonel}', 'KesehatanController@hapusKesehatan');
			// Jasmani
			// Route::post('jasmani/tambah-jasmani', 'JasmaniController@tambahJasmani');
			// Psikologi
			Route::post('psikologi/tambah-psikologi', 'PsikologiController@tambahRiwayatPsikologi');
	        Route::put('psikologi/edit-put-psikologi/{riwayatPsikologi}','PsikologiController@editPutRiwayatPsikologi');
	        Route::patch('psikologi/edit-psikologi/{riwayatPsikologi}','PsikologiController@editRiwayatPsikologi');
			Route::delete('psikologi/hapus-psikologi/{riwayatPsikologi}', 'PsikologiController@hapusRiwayatPsikologi');
			// Penghargaan
			Route::post('penghargaan/tambah-penghargaan', 'PenghargaanController@tambahPenghargaan');
			Route::patch('penghargaan/edit-penghargaan/{penghargaanPersonel}', 'PenghargaanController@editPenghargaan');
			Route::delete('penghargaan/hapus-penghargaan/{penghargaanPersonel}', 'PenghargaanController@hapusPenghargaan');
			// Brevet
			Route::post('brevet/tambah-brevet', 'BrevetController@tambahRiwayatBrevet');
			Route::delete('brevet/hapus-brevet/{riwayatBrevet}', 'BrevetController@hapusRiwayatBrevet');
	        Route::patch('brevet/edit-brevet/{riwayatBrevet}','BrevetController@editRiwayatBrevet');
			// BKO
			Route::post('bko/tambah-bko', 'BKOController@tambahBKO');
			Route::patch('bko/edit-bko/{bko}', 'BKOController@editBKO');
			Route::delete('bko/hapus-bko/{bko}', 'BKOController@hapusBKO');
			// Penugasan Luar dan Dalam Negeri
			Route::post('penugasan-negara/tambah-penugasan-negara', 'PenugasanNegaraController@tambahPenugasanNegara');
			Route::patch('penugasan-negara/edit-penugasan-negara/{penugasan}', 'PenugasanNegaraController@editPenugasanNegara');
			Route::delete('penugasan-negara/hapus-penugasan-negara/{penugasan}', 'PenugasanNegaraController@hapusPenugasanNegara');
			// Penugasan Luar Struktur
			Route::post('penugasan-luar-struktur/tambah-penugasan-luar-struktur', 'PenugasanLuarStrukturController@tambahPenugasanLuarStruktur');
			Route::patch('penugasan-luar-struktur/edit-penugasan-luar-struktur/{penugasan}', 'PenugasanLuarStrukturController@editPenugasanLuarStruktur');
			Route::delete('penugasan-luar-struktur/hapus-penugasan-luar-struktur/{penugasan}', 'PenugasanLuarStrukturController@hapusPenugasanLuarStruktur');
			// Pangkat
			Route::post('pangkat/tambah-pangkat', 'PangkatController@tambahRiwayatPangkat');
			Route::patch('pangkat/edit-pangkat/{riwayatPangkat}', 'PangkatController@editRiwayatPangkat');
			Route::delete('pangkat/hapus-pangkat/{riwayatPangkat}', 'PangkatController@hapusRiwayatPangkat');
			// Jabatan
			Route::post('jabatan/tambah-jabatan', 'JabatanController@tambahRiwayatJabatan');
			Route::patch('jabatan/edit-jabatan/{riwayatJabatan}', 'JabatanController@editRiwayatJabatan');
			Route::delete('jabatan/hapus-jabatan/{riwayatJabatan}', 'JabatanController@hapusRiwayatJabatan');


			// Alamat
			Route::get('alamat/daftar-alamat', 'AlamatController@daftarAlamat');
			Route::get('alamat/alamat-terakhir', 'AlamatController@alamatTerakhir');
		});

		Route::get('jabatan/urutan-satuan/{riwayatJabatan}', 'JabatanController@urutanSatuan');
		Route::get('eselon/daftar-eselon','EselonController@daftarEselon');
		// Pendidikan
		Route::get('pendidikan/daftar-gelar','PendidikanUmumController@daftarGelar');
		Route::get('pendidikan/daftar-gelar-dikpol/{jurusanID}','PendidikanPolisiController@daftarGelarDikpol');
		Route::get('pendidikan/daftar-konsentrasi/{jurusan}','PendidikanUmumController@daftarKonsentrasi');
		Route::get('pendidikan/daftar-dikum','PendidikanUmumController@daftarDikum');
		Route::get('pendidikan/daftar-jenis-dikpol','PendidikanPolisiController@daftarJenisDikpol');
		Route::get('pendidikan/daftar-dikpol/{jenisDikpol}','PendidikanPolisiController@daftarDikpol');
		Route::get('pendidikan/daftar-tipe-dikbangspes','DikbangspesController@daftarTipeDikbangspes');
		Route::get('pendidikan/daftar-dikbangspes/{tipeDikbangspes}','DikbangspesController@daftarDikbangspes');
		// end Pendidikan
		Route::get('tanda-kehormatan/daftar-tanda-kehormatan', 'TandaKehormatanController@daftarTandaKehormatan');
		Route::get('hobi/daftar-hobi', 'HobiController@daftarHobi');
		Route::get('pangkat/daftar-pangkat', 'PangkatController@daftarPangkat');
		Route::get('brevet/daftar-brevet', 'BrevetController@daftarBrevet');
		Route::get('bko/daftar-jenis-bko', 'BKOController@daftarJenisBKO');
		// Route::get('penugasan/daftar-misi', 'PenugasanController@daftarMisi');
		Route::get('sim/daftar-sim', 'SIMController@daftarSIM');
		Route::get('penugasan-negara/daftar-misi/{negara}', 'PenugasanNegaraController@daftarMisi');
		Route::get('penugasan-negara/daftar-jabatan-penugasan/{misi}', 'PenugasanNegaraController@daftarJabatanPenugasan');
		Route::get('penugasan-luar-struktur/daftar-lokasi-penugasan/{negara}', 'PenugasanLuarStrukturController@daftarLokasiPenugasan');
		Route::get('penugasan-luar-struktur/daftar-jabatan-penugasan/{lokasiPenugasan}', 'PenugasanLuarStrukturController@daftarJabatanPenugasan');
		Route::get('negara/daftar-negara', 'NegaraController@daftarNegara');
		Route::get('agama/daftar-agama', 'AgamaController@daftarAgama');
		Route::get('psikologi/daftar-psikologi', 'PsikologiController@daftarPsikologi');
		Route::get('olahraga/daftar-olahraga', 'OlahragaController@daftarOlahraga');
		Route::get('penghargaan/daftar-penghargaan','PenghargaanController@daftarPenghargaan');
		Route::get('penghargaan/daftar-tingkat-penghargaan','PenghargaanController@daftarTingkatPenghargaan');
		Route::get('kesehatan/daftar-kesehatan','KesehatanController@daftarKesehatan');
		// Route::get('jasmani/daftar-jasmani','JasmaniController@daftarJasmani');
		Route::get('kota/daftar-kota','KotaController@daftarKota');
		Route::get('personel/daftar-status-personel','PersonelController@daftarStatusPersonel');
			
		Route::get('bahasa/daftar-bahasa-daerah','BahasaController@daftarBahasaDaerah');
		Route::get('bahasa/daftar-bahasa-international','BahasaController@daftarBahasaInternational');
		Route::get('jurusan/daftar-jurusan','JurusanController@daftarJurusan');
		Route::get('jurusan/daftar-jurusan-dikpol','JurusanController@daftarJurusanDikpol');
		Route::get('jurusan/daftar-jurusan-by-dikum/{dikumID}','JurusanController@daftarJurusanByDikum');
		Route::get('golongan/daftar-golongan','GolonganController@daftarGolongan');
		Route::get('keluarga/daftar-hubungan-keluarga','KeluargaController@daftarHubunganKeluarga');
		Route::get('keluarga/sejarah-pekerjaan-keluarga','KeluargaController@sejarahPekerjaanKeluarga');

		Route::get('enum/hubungan-keluarga','KeluargaController@daftarHubunganKeluarga');
		Route::get('jenis-pekerjaan/daftar-jenis-pekerjaan','KeluargaController@daftarJenisPekerjaan');
		Route::get('satuan/daftar-satuan-bawah','SatuanController@listChild');
		// Route::get('satuan/daftar-satuan','SatuanController@listChild');
		// Route::get('satuan/daftar-satuan-pertama','SatuanController@daftarSatuanPertama');
		Route::post('/upload-file','FileController@uploadFile');

		// Enum Other
		Route::get('enum/daftar-jenis-kelamin', 'OtherController@daftarJenisKelamin');
		Route::get('enum/daftar-golongan-darah', 'OtherController@daftarGolonganDarah');
		Route::get('enum/daftar-status-pernikahan', 'OtherController@daftarStatusPernikahan');
		Route::get('enum/daftar-warna-kulit', 'OtherController@daftarWarnaKulit');
		Route::get('enum/daftar-suku-bangsa', 'OtherController@daftarSukuBangsa');
		Route::get('enum/daftar-jenis-rambut', 'OtherController@daftarJenisRambut');
		Route::get('enum/daftar-warna-rambut', 'OtherController@daftarWarnaRambut');
		Route::get('enum/daftar-warna-mata', 'OtherController@daftarWarnaMata');
		Route::get('enum/daftar-akreditasi', 'OtherController@daftarAkreditasi');
		Route::get('enum/daftar-status-keluarga', 'OtherController@daftarStatusKeluarga');
	});
	Route::post('/sort', '\Rutorika\Sortable\SortableController@sort')->name('sort');
});