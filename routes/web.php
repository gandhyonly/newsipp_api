<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/migrasi/migrasi1', 'Migration\Migrasi1Controller@index')->name('migration.migrasi1');


// Route::get('/migrasi/testingmigrasi', 'Migration\Migrasi1Controller@testing')->name('migration.testing');
Route::group(['middleware' => 'auth'], function() {
	Route::get('/', 'SatuanController@redirectToIndex')->name('redirectTo.index');
	Route::get('/dashboard','DashboardController@index')->name('dashboard.index');
	Route::get('/duplikasi-satuan', 'SatuanController@getDuplikasi')->name('satuan.duplication.get');
	Route::post('/duplikasi-satuan', 'SatuanController@postDuplikasi')->name('satuan.duplication.post');
	Route::get('/personel', 'PersonelController@index')->name('personel.index');
	Route::get('/migration', 'Migration\MigrationController@index')->name('migration.index');
	Route::post('/migration/reset-satuan-jabatan', 'MigrationController@resetSatuanJabatan')->name('migration.reset.satuan-jabatan');
	Route::post('/migration/reset-personel', 'MigrationController@resetPersonel')->name('migration.reset.personel');
	Route::get('/migration/insert-jabatan', 'MigrationController@insertJabatan')->name('migration.insert.jabatan');
	Route::post('/migration/seed', 'MigrationController@seed')->name('migration.seed');
	Route::get('/personel/{personel}/edit', 'PersonelController@edit')->name('personel.edit');
	Route::post('/personel/{personel}/update', 'PersonelController@update')->name('personel.update');
	Route::get('/personel/filter', 'PersonelController@filter')->name('personel.filter');
	Route::get('/personel/duplicate', 'PersonelController@duplicate')->name('personel.duplicate');
	Route::patch('/personel/verification/{id}', 'PersonelController@verification')->name('personel.verification');
	Route::get('/personel/create', 'PersonelController@create')->name('personel.create');


	Route::post('/satuan/filter', 'SatuanController@filter')->name('satuan.filter');
	Route::resource('/satuan','SatuanController');
	Route::get('/satuan/{satuan}/create-satuan', 'SatuanController@createSatuan')->name('satuan.create-satuan');
	// Route::get('/get-data-wiget/{satuan}', 'SatuanController@APIdataWidget')->name('satuan.api-data-widget');
	Route::post('/satuan/{satuan}/store-satuan', 'SatuanController@storeSatuan')->name('satuan.store-satuan');
	Route::get('/satuan/{satuan}/create-jabatan', 'SatuanController@createJabatan')->name('satuan.create-jabatan');
	Route::post('/satuan/{satuan}/store-jabatan', 'SatuanController@storeJabatan')->name('satuan.store-jabatan');
	Route::get('/satuan/verification/{satuan}', 'SatuanController@verification')->name('satuan.verification');
	Route::get('/satuan/{satuan}/edit-jabatan/{satuanJabatan}', 'SatuanJabatanController@edit')->name('satuan-jabatan.edit');
	Route::patch('/satuan/{satuan}/update-jabatan/{satuanJabatan}', 'SatuanJabatanController@update')->name('satuan-jabatan.update');
	Route::delete('/satuan/{satuan}/delete-jabatan/{satuanJabatan}', 'SatuanJabatanController@destroy')->name('satuan-jabatan.destroy');
	Route::post('/satuan/{satuan}/verification/{satuanJabatan}', 'SatuanJabatanController@verification')->name('satuan-jabatan.verification');
	Route::post('/satuan/{satuan}/data-error/{satuanJabatan}', 'SatuanJabatanController@dataError')->name('satuan-jabatan.data-error');


	Route::get('/jabatan-filter', 'JabatanController@filter')->name('jabatan.filter');


	Route::resource('/jenis-satuan','JenisSatuanController');
	Route::resource('/jabatan','JabatanController');
	Route::resource('/pangkat','PangkatController');
	Route::resource('/nivelering','NiveleringController');


});

Route::get('/testing-request','API\FileController@getFileTesting');
Route::get('/report-harian','ReportController@reportHarian')->name('report.harian');
Route::get('/', 'UserController@getLogin');
// Duplication
	Route::get('/duplication/duplikasi','Duplication\DuplicationController@halamanDuplikasi')->name('duplication.halamanDuplikasi');
	Route::get('/duplication/sort','Duplication\DuplicationController@sort')->name('duplication.sort');
	Route::get('/duplication/satuan-masalah','Duplication\DuplicationController@cariSatuanBermasalah')->name('duplication.cariSatuanBermasalah');
	// set Kepala Atasan
	// set Nivelering
	Route::get('/duplication/nivelering-norm','Duplication\NiveleringController@niveleringNormalisasi')->name('duplication.niveleringNormalisasi');
	Route::get('/duplication/hapusSatuanKerja','Duplication\DuplicationController@hapusSatuanKerja')->name('duplication.hapusSatuanKerja');
	Route::get('/duplication/hapusSeluruhJabatan','Duplication\DuplicationController@hapusSeluruhJabatan')->name('duplication.hapusSeluruhJabatan');
	Route::get('/masalah/polsubsektor','SatuanController@masalahPolsubsektor')->name('duplication.masalahPolsubsektor');
	Route::get('/masalah/tambah-keterangan-satuan','SatuanController@tambahKeteranganSatuan')->name('duplication.tambahKeteranganSatuan');
	Route::get('/masalah/satuan-double','SatuanController@masalahSatuanDouble')->name('duplication.masalahSatuanDouble');
Route::get('/nivelering-norm','Duplication\DuplicationController@niveleringNormalisasi')->name('duplication.niveleringNormalisasi');
Route::get('/create-eselon','Duplication\DuplicationController@createEselon')->name('duplication.createEselon');
Route::get('/login', 'UserController@getLogin')->name('login');
Route::post('/login', 'UserController@postLogin')->name('user.login.post');
Route::get('/logout', 'UserController@logout')->name('user.logout');



Route::group(['prefix'=>'laporan','namespace'=> 'Laporan','as'=>'laporan.'],function(){
	Route::get('/', 'DashboardController@redirectToLogin');
	Route::get('/login', 'LoginController@getLogin')->name('login.get');
	Route::post('/login', 'LoginController@postLogin')->name('login.post');
	Route::group(['middleware' => 'auth:verifikator'],function(){
		Route::get('/', 'DashboardController@dashboard')->name('dashboard');
	});
});