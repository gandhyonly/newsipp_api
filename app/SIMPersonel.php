<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class SIMPersonel extends Model
{
	protected $primaryKey = 'sim_personel_id';
	protected $table = 'sim_personel';
	protected $fillable = ['personel_id','sim_id','sim_nomor','file'];
	public $timestamps = false;

	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
	public function sim()
	{
		return $this->belongsTo(SIM::class,'sim_id');
	}
	public function hapusFile($file)
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->$file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->$file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}
}
