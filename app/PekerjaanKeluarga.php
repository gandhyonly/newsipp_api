<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PekerjaanKeluarga extends Model
{
	protected $primaryKey = 'pekerjaan_keluarga_id';
	protected $table = 'pekerjaan_keluarga';
	protected $guarded = [];

	public $timestamps=true;

	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';

	public function jenisPekerjaan()
	{
		return $this->belongsTo(JenisPekerjaan::class,'jenis_pekerjaan_id');
	}

	public function scopeActive($query)
	{
		return $query->where('status_terakhir',1);
	}
}
