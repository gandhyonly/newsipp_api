<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Helpdesk extends Model
{
	protected $table='helpdesks';
	protected $dates=['tanggal_lahir','tmt_jabatan_pertama','tmt_jabatan_terakhir'];
	protected $guarded=[]; 

	public function satuanSekarang()
	{
		return $this->belongsTo(Satuan::class,'satker_sekarang_id');
	}
	public function satuan_sekarang()
	{
		return $this->belongsTo(Satuan::class,'satker_sekarang_id');
	}
	public function verifikator()
	{
		return $this->belongsTo(HelpdeskUser::class,'verified_by');
	}

	static public function emailFrom($satuanID)
	{
		switch($satuanID){
			case 3: 	$email = 'sipphelpdesk.metrojaya@gmail.com'; break;
			case 4: 	$email = 'sipphelpdesk.jabar@gmail.com'; break;
			case 5: 	$email = 'sipphelpdesk.jateng@gmail.com'; break;
			case 6: 	$email = 'sipphelpdesk.jatim@gmail.com'; break;
			case 7: 	$email = 'sipphelpdesk.aceh@gmail.com'; break;
			case 8: 	$email = 'sipphelpdesk.sumut@gmail.com'; break;
			case 9: 	$email = 'sipphelpdesk.sumsel@gmail.com'; break;
			case 10: 	$email = 'sipphelpdesk.kaltim@gmail.com'; break;
			case 11: 	$email = 'sipphelpdesk.sulsel@gmail.com'; break;
			case 12: 	$email = 'sipphelpdesk.bali@gmail.com'; break;
			case 13: 	$email = 'sipphelpdesk.papua@gmail.com'; break;
			case 14: 	$email = 'sipphelpdesk.sumbar@gmail.com'; break;
			case 15: 	$email = 'sipphelpdesk.riau@gmail.com'; break;
			case 16: 	$email = 'sipphelpdesk.jambi@gmail.com'; break;
			case 17: 	$email = 'sipphelpdesk.lampung@gmail.com'; break;
			case 18: 	$email = 'sipphelpdesk.kalbar@gmail.com'; break;
			case 19: 	$email = 'sipphelpdesk.kalsel@gmail.com'; break;
			case 20: 	$email = 'sipphelpdesk.diy@gmail.com'; break;
			case 21: 	$email = 'sipphelpdesk.sulut@gmail.com'; break;
			case 22: 	$email = 'sipphelpdesk.ntb@gmail.com'; break;
			case 23: 	$email = 'sipphelpdesk.ntt@gmail.com'; break;
			case 24: 	$email = 'sipphelpdesk.bengkulu@gmail.com'; break;
			case 25: 	$email = 'sipphelpdesk.kalteng@gmail.com'; break;
			case 26: 	$email = 'sipphelpdesk.sulteng@gmail.com'; break;
			case 27: 	$email = 'sipphelpdesk.sultra@gmail.com'; break;
			case 28: 	$email = 'sipphelpdesk.maluku@gmail.com'; break;
			case 29: 	$email = 'sipphelpdesk.banten@gmail.com'; break;
			case 30: 	$email = 'sipphelpdesk.kepri@gmail.com'; break;
			case 31: 	$email = 'sipphelpdesk.babel@gmail.com'; break;
			case 32: 	$email = 'sipphelpdesk.gorontalo@gmail.com'; break;
			case 33: 	$email = 'sipphelpdesk.malut@gmail.com'; break;
			case 12407: $email = 'sipphelpdesk.sulbar@gmail.com'; break;
			case 12408: $email = 'sipphelpdesk.papuabarat@gmail.com'; break;
			case 21895: $email = 'sipphelpdesk.kaltara@gmail.com'; break;
			case 1168 : $email = 'sipphelpdesk.itwasum@gmail.com'; break;
			case 1169 : $email = 'sipphelpdesk.sops@gmail.com'; break;
			case 1170 : $email = 'sipphelpdesk.srena@gmail.com'; break;
			case 1171 : $email = 'sipphelpdesk.ssdm@gmail.com'; break;
			case 1172 : $email = 'sipphelpdesk.ssarpras@gmail.com'; break;
			case 1173 : $email = 'sipphelpdesk.divpropam@gmail.com'; break;
			case 1174 : $email = 'sipphelpdesk.divhumas@gmail.com'; break;
			case 1175 : $email = 'sipphelpdesk.divkum@gmail.com'; break;
			case 1176 : $email = 'sipphelpdesk.divhubinter@gmail.com'; break;
			case 1177 : $email = 'sipphelpdesk.divtik@gmail.com'; break;
			case 1178 : $email = 'sipphelpdesk.sahli@gmail.com'; break;
			case 1179 : $email = 'sipphelpdesk.spripim@gmail.com'; break;
			case 1180 : $email = 'sipphelpdesk.setum@gmail.com'; break;
			case 1181 : $email = 'sipphelpdesk.yanma@gmail.com'; break;
			case 1182 : $email = 'sipphelpdesk.baintelkam@gmail.com'; break;
			case 1183 : $email = 'sipphelpdesk.baharkam@gmail.com'; break;
			case 1190 : $email = 'sipphelpdesk.bareskrim@gmail.com'; break;
			case 1194 : $email = 'sipphelpdesk.korlantas@gmail.com'; break;
			case 1195 : $email = 'sipphelpdesk.korbrimob@gmail.com'; break;
			case 1196 : $email = 'sipphelpdesk.densus88@gmail.com'; break;
			case 1196 : $email = 'sipphelpdesk.lemdiklat@gmail.com'; break;
			case 1209 : $email = 'sipphelpdesk.pusjarah@gmail.com'; break;
			case 1211 : $email = 'sipphelpdesk.puslitbang@gmail.com'; break;
			case 1212 : $email = 'sipphelpdesk.puskeu@gmail.com'; break;
			case 1213 : $email = 'sipphelpdesk.pusdokkes@gmail.com'; break;
			case 3588 : $email = 'sipphelpdesk.slog@gmail.com'; break;
			default : $email = 'helpdesk.notification@polri.go.id'; break;
		}
		return $email;
	}

	public function pangkat()
	{
		return $this->belongsTo(Pangkat::class,'pangkat_id');
	}
	public function hapus()
	{
		if($this->petikan_pertama != null){
			$fileLocation = 'file/'.$this->petikan_pertama;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
		if($this->riwayat_hidup != null){
			$fileLocation = 'file/'.$this->riwayat_hidup;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
		if($this->delete()){
			return true;
		} else {
			return false;
		}
	}
}
