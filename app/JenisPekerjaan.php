<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPekerjaan extends Model
{
	protected $table = 'jenis_pekerjaan';
	protected $primaryKey = 'jenis_pekerjaan_id';
	protected $fillable = ['jenis'];
	public $timestamps = false;
}
