<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HubunganKeluarga extends Model
{
	protected $table = 'hubungan_keluarga';
	protected $primaryKey = 'hubungan_keluarga_id';
	protected $fillable = ['hubungan'];
	public $timestamps = false;
}
