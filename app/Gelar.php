<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gelar extends Model
{
	protected $table = 'gelar';
	protected $primaryKey = 'gelar_id';
	protected $fillable = ['gelar','gelar_sebelum','jurusan_id','gelar_posisi_sebelum'];
	public $timestamps = false;

	public function jurusan()
	{
		return $this->belongsTo(Jurusan::class,'jurusan_id');
	}
}
