<?php

namespace App;

use App\JenisSatuan;
use Illuminate\Database\Eloquent\Model;

class Satuan extends Model
{

    use \Rutorika\Sortable\SortableTrait;

    protected static $sortableField = 'posisi';
	protected $primaryKey = 'satuan_id';
	protected $table = 'satuan';
	protected $guarded = [];
	public $timestamps = false;
	// use \Rutorika\Sortable\SortableTrait;
	// protected static $sortableField = 'posisi';

	public function scopeRoot($query)
	{
		return $query->where('parent_id',null);
	}

	public function scopeVerified($query)
	{
		return $query->whereNotNull('verified_by');
	}

	public function scopeActive($query)
	{
		return $query->where('deleted_by',null);
	}

	public function scopeCategory($query,$category)
	{
		$arrayJenisSatuan = JenisSatuan::where('kategori','=',$category)->pluck('jenis_satuan_id')->toArray();
		return $query->whereIn('jenis_satuan_id',$arrayJenisSatuan);
	}

	public function jumlahTipe($tipe)
	{
		return $this->where('tipe',$tipe)->count();
	}

	public function childs()
	{
		return $this->hasMany(Satuan::class,'parent_id','satuan_id')->orderBy('satuan_id');
	}
	public function childsSatker()
	{
		return $this->hasMany(Satuan::class,'parent_id','satuan_id')->category('SATUAN KERJA');
	}

	public function parent()
	{
		return $this->belongsTo(Satuan::class,'parent_id','satuan_id');
	}

	public function jabatan()
	{
		return $this->hasMany(SatuanJabatan::class,'satuan_id')->orderBy('satuan_jabatan_id');
	}
	public function fullPrint()
	{
		$satuan = $this->parent()->first();
		$temp = $this->nama_satuan;
		while($satuan != null){
			$temp = $temp.' > '.$satuan->nama_satuan;
			$satuan=$satuan->parent()->first();
		}
		return $temp;
	}
	public function fullPrintBackward()
	{
		$satuan = $this->parent()->first();
		$temp = $this->nama_satuan;
		while($satuan != null){
			$temp = $satuan->nama_satuan.' > '.$temp;
			$satuan=$satuan->parent()->first();
		}
		return $temp;
	}
	public function fullName()
	{
		$satuan = $this->parent()->first();
		$temp = $this->nama_satuan;
		while($satuan != null){
			$temp = $temp.' '.$satuan->nama_satuan;
			$satuan=$satuan->parent()->first();
		}
		return $temp;
	}
	public function fullNameUntukJabatan()
	{
		$satuan = $this->parent()->first();
		$temp = $this->nama_satuan;
		while($satuan != null){
			$temp = $temp.' '.$satuan->nama_satuan;
			$satuan=$satuan->parent()->first();
		}
		$str = explode(' ', $temp);
		$str = array_slice($str, 1);
		$str = implode(' ', $str);
		return $str;
	}

	public function printPath($id)
	{
		$satuan = Satuan::find($id);
		if($satuan->parent()->count() != 0){
			$this->printPath($satuan->parent_id);
		}
		if($satuan->parent!=null)
		echo ' > ';
		echo $satuan->nama_satuan;
	}

	public function printNamaSatuan()
	{
		return $this->printPath($this->satuan_id);
	}

	public static function printPathStatic($id)
	{
		$satuan = Satuan::find($id);
		if($satuan->parent()->count() != 0){
			$this->printPath($satuan->parent_id);
		}
		if($satuan->parent!=null)
		echo ' > ';
		echo $satuan->nama_satuan;
	}

	public static function widgetSatuan($dataSatuan)
	{
		$total['totalVerifiedSatuan']=0;
		$total['totalSatuan']=0;
		$total['totalJabatan']=0;
		$total['totalVerifiedJabatan']=0;
		$totalVerified=0;
		$totalSatuan=0;
		$totalJabatan=0;
		$totalVerifiedJabatan=0;
		if($dataSatuan!=null)
		{
			$childSatuan = $dataSatuan->childs()->active()->get();
			$childJabatan = $dataSatuan->jabatan()->active()->get();
			$totalSatuan = $childSatuan->count();
			$totalVerifiedSatuan = $childSatuan->where('verified_by',!null)->count();
			$totalJabatan = $childJabatan->count();
			$totalVerifiedJabatan = $childJabatan->where('verified_by',!null)->count();
			// $totalVerified = $dataSatuan->childs()->verified()->count();
			// $totalSatuan = $dataSatuan->childs()->count();
			foreach($childSatuan as $satuan){
				$temp = Satuan::widgetSatuan($satuan);
				$totalVerifiedSatuan += $temp['totalVerifiedSatuan'];
				$totalSatuan += $temp['totalSatuan'];
				$totalJabatan += $temp['totalJabatan'];
				$totalVerifiedJabatan += $temp['totalVerifiedJabatan'];
			}	
		}
		return $total = [
			'totalVerifiedSatuan' => $totalVerifiedSatuan,
			'totalSatuan' => $totalSatuan,
			'totalJabatan' => $totalJabatan,
			'totalVerifiedJabatan' => $totalVerifiedJabatan
		];
	}


	public function printStatus()
	{
		if($this->is_verified)
			return "<span class='label bg-green'>Terverifikasi</span>";
		else
			return "<span class='label bg-red'>Belum diverifikasi</span>";
	}

	public function jenisSatuan()
	{
		return $this->belongsTo(JenisSatuan::class,'jenis_satuan_id');
	}
	public static function createSatuan($namaSatuan, $jenisSatuan, $parentSatuan)
	{
		return self::create([
			'nama_satuan' 		=> $namaSatuan,
			'jenis_satuan_id' 	=> JenisSatuan::where('nama_jenis_satuan',$jenisSatuan)->first()->jenis_satuan_id,
			'parent_id' 		=> $parentSatuan->satuan_id
		]);
	}

	public function setSatuanJabatanKeterangan()
	{
		foreach($this->jabatan()->get() as $satuanJabatan){
			$satuanJabatan->setKeterangan();
		}
	}
	static public function fullArraySatuan($id)
	{
		$arrayJabatan = [];
		$satuan = Satuan::find($id);
		array_push($arrayJabatan,$satuan->satuan_id);
		if($satuan==null)
			return null;
		while($satuan->parent){
			$satuan=$satuan->parent;
			array_push($arrayJabatan,$satuan->satuan_id);
		}
		return array_reverse($arrayJabatan);
	}
	static public function arraySatuanBaru($id)
	{
		$arrayJabatan = [];
		$satuan = Satuan::find($id);
		array_push($arrayJabatan,$satuan);
		if($satuan==null)
			return null;
		while($satuan->parent){
			$satuan=$satuan->parent;
			array_push($arrayJabatan,$satuan);
		}
		return array_reverse($arrayJabatan);
	}
	static public function satuanTerkecil($id)
	{
		$satuan = Satuan::find($id);
		while($satuan->childs->count() > 0){
			$satuan=$satuan->childs->first();
		}
		return $satuan->satuan_id;
	}
}
