<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPenugasan extends Model
{
	protected $table = 'jenis_penugasan';
	protected $primaryKey = 'jenis_penugasan_id';
	protected $fillable = ['nama_jenis_penugasan'];
	public $timestamps = false;
}
