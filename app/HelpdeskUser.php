<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class HelpdeskUser extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['nrp','','tanggal_lahir','tmt_jabatan_pertama','satker_pertama','api_token'];
    protected $primaryKey = 'helpdesk_user_id';
    protected $guarded = [];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','api_token','is_superadmin'
    ];

    public function scopeActive($query)
    {
        return $query->where('aktif',1);
    }
}
