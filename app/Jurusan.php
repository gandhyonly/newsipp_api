<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
	protected $table = 'jurusan';
	protected $primaryKey = 'jurusan_id';
	protected $fillable = ['nama_jurusan','fakultas_id'];
	public $timestamps = false;

	public function gelar()
	{
		return $this->hasMany(Gelar::class,'jurusan_id')->orderBy('gelar');
	}
	public function konsentrasi()
	{
		return $this->hasMany(Konsentrasi::class,'jurusan_id')->orderBy('nama_konsentrasi');
	}
}
