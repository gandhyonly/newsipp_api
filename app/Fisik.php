<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fisik extends Model
{

	protected $primaryKey = 'fisik_personel_id';
	protected $table = 'fisik_personel';
	protected $guarded = [];
	protected $dates = ['tanggal','tanggal_perubahan'];
	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';

	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}

}
