<?php

namespace App;

use App\Personel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RiwayatPsikologi extends Model
{
	protected $table = 'riwayat_psikologi';
	protected $primaryKey = 'riwayat_psikologi_id';
	protected $fillable = ['personel_id','psikologi_id','nilai','nilai_file','tanggal_tes'];
	public $dates = ['tanggal_tes'];
	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';

	public function psikologi()
	{
		return $this->belongsTo(Psikologi::class,'psikologi_id');
	}
	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
	public function hapusFile()
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->surat_keputusan_file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->surat_keputusan_file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}
}
