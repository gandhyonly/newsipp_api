<?php

namespace App;

use App\Personel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Penugasan extends Model
{
	protected $table = 'penugasan';
	protected $primaryKey = 'penugasan_id';
	protected $fillable = ['jenis_penugasan_id','personel_id','lokasi_penugasan_id','jabatan_penugasan_id','tmt_penugasan','tmt_selesai','misi_id','sprint_file','sprint_nomor','surat_keputusan_nomor','surat_keputusan_file'];
	public $dates = ['tmt_selesai','tmt_penugasan'];

	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';


	public function misi()
	{
		return $this->belongsTo(Misi::class,'misi_id');
	}
	public function jenisPenugasan()
	{
		return $this->belongsTo(JenisPenugasan::class,'jenis_penugasan_id');
	}

	public function personel()
	{
		return $this->hasMany(Personel::class,'personel_id');
	}

	public function lokasiPenugasan()
	{
		return $this->belongsTo(LokasiPenugasan::class,'lokasi_penugasan_id');
	}

	public function jabatanPenugasan()
	{
		return $this->belongsTo(JabatanPenugasan::class,'jabatan_penugasan_id');
	}

	public function suratKeputusanPenugasan()
	{
		return $this->belongsTo(SuratKeputusanPenugasan::class,'surat_keputusan_penugasan_id');
	}
	
	public function hapusFile($file)
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->$file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->$file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}

}
