<?php

namespace App;

use App\Personel;
use App\SatuanJabatan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RiwayatJabatan extends Model
{
	protected $table = 'riwayat_jabatan';
	protected $primaryKey = 'riwayat_jabatan_id';
	protected $fillable = ['satuan_jabatan_id','personel_id','tmt_jabatan','tmt_selesai','keterangan','status_aktif','surat_keputusan_nomor','surat_keputusan_file','jabatan_sementara'];
	protected $dates = ['tmt_jabatan','tmt_selesai'];
	public $timestamps=null;
	const CREATED_AT = null;
	const UPDATED_AT = 'tanggal_perubahan';

	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
	public function keterangan()
	{
		if($this->satuan_jabatan_id==null){
			return $this->keterangan;
		} else {
			return $this->satuanJabatan()->fullKeterangan();
		}
	}

	public function satuanJabatan()
	{
		return $this->belongsTo(SatuanJabatan::class,'satuan_jabatan_id');
	}
	public function hapusFile()
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->surat_keputusan_file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->surat_keputusan_file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}
	static public function addKeterangan()
	{
		foreach(self::whereNotNull('satuan_jabatan_id')->get() as $riwayatJabatan){
			echo $riwayatJabatan->satuan_jabatan_id."\n";
			$riwayatJabatan->keterangan=SatuanJabatan::find($riwayatJabatan->satuan_jabatan_id)->fullKeterangan();
			$riwayatJabatan->save();
		} 
	}
	public function buatStatusAktif()
	{
		if(self::orderBy('tmt_jabatan','DESC')->where('personel_id',$this->personel_id)->first()->riwayat_jabatan_id == $this->riwayat_jabatan_id){
			self::where('personel_id',$this->personel_id)->update(['status_aktif' => false]);
			$this->status_aktif=true;
			$this->save();
			return true;
		}
		return false;
	}
}
