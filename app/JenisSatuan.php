<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisSatuan extends Model
{
	protected $primaryKey = 'jenis_satuan_id';
	protected $table = 'jenis_satuan';
	protected $guarded = [];
	public $timestamps = false;

	public function satuan()
	{
		return $this->hasMany(Satuan::class,'jenis_satuan_id');
	}
}
