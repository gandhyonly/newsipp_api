<?php

namespace App\Http\Controllers;

use App\Nivelering;
use Illuminate\Http\Request;

class NiveleringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataNivelering = Nivelering::paginate(60);
        $niveleringCount = Nivelering::count();
        return view('nivelering.index')
            ->with('niveleringCount',$niveleringCount)
            ->with('dataNivelering',$dataNivelering);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nivelering $nivelering)
    {
        if($nivelering->delete()){
            return redirect()->route('nivelering.index')->with('success','Hapus nivelering berhasil.');
        } else{
            return redirect()->route('nivelering.index')->with('error','Hapus nivelering gagal.');
        }
    }
}
