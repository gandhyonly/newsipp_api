<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\Nivelering;
use App\Pangkat;
use App\Satuan;
use App\SatuanJabatan;
use App\StatusJabatan;
use Illuminate\Http\Request;

class SatuanJabatanController extends Controller
{
	public function create()
	{
		return view('satuan-jabatan.create');
	}

	public function edit(Satuan $satuan, SatuanJabatan $satuanJabatan)
	{
		$selectDataJabatan = Jabatan::select('jabatan_id','nama_jabatan')->orderBy('nama_jabatan')->get();
		if($satuan->parent()->count()){
			$selectDataAtasanJabatan = SatuanJabatan::where('satuan_id',$satuan->satuan_id)->orWhere('satuan_id',$satuan->parent->satuan_id)->active()->get();
		} else {
			$selectDataAtasanJabatan = SatuanJabatan::active()->where('satuan_id',$satuan->satuan_id)->get();
		}

		$selectDataNivelering = Nivelering::get();
		$selectDataPangkat = Pangkat::get();
		$selectDataStatusJabatan = StatusJabatan::get();
		$pangkatID = 'null';
		if($satuanJabatan->pangkat()->where('jenis','POLISI')->count() > 0){
			$pangkatID = $satuanJabatan->pangkat()->where('jenis','POLISI')->first()->pangkat_id;
		}
		if($pangkatID <=30 && $pangkatID>=19){
			$pangkatID='BA';
		} elseif($pangkatID == 31 || $pangkatID==32){
			$pangkatID='IP';
		} else{
			switch($pangkatID){
				case 33: 
					$pangkatID='AKP';
					break;
				case 34: 
					$pangkatID='KP';
					break;
				case 35: 
					$pangkatID='AKBP';
					break;
				case 36: 
					$pangkatID='KBP';
					break;
				case 37: 
					$pangkatID='BRIGJEN';
					break;
				case 38: 
					$pangkatID='IRJEN';
					break;
				case 39: 
					$pangkatID='KOMJEN';
					break;
				case 40: 
					$pangkatID='JENDERAL';
					break;
				default : 
					$pangkatPolisi = 'null';
					break;
			}
		}
		

		return view('satuan-jabatan.edit',compact([
			'satuanJabatan','selectDataJabatan','selectDataNivelering','pangkatID',
			'selectDataPangkat','selectDataAtasanJabatan','selectDataStatusJabatan','satuan'
		]));
	}
	
	public function update(Request $request,Satuan $satuan, SatuanJabatan $satuanJabatan)
	{
		$input = $request->only(['dsp','jabatan_id','satuan_id','nivelering_id','status_jabatan_id']);

		$pangkat = $request->pangkat; 
		$pangkatPolisi = true;
		$pangkatID=[];
		switch($pangkat){
			case 'BA': 
				$pangkatID = [19,20,21,22,23,24,25,26,27,28,29,30];
				break;
			case 'IP':
				$pangkatID = [31,32];
				break;
			case 'AKP':
				$pangkatID = [33];
				break;
			case 'KP':
				$pangkatID = [34];
				break;
			case 'AKBP':
				$pangkatID = [35];
				break;
			case 'KBP':
				$pangkatID = [36]; 
				break;
			case 'BRIGJEN':
				$pangkatID = [37]; 
				break;
			case 'IRJEN':
				$pangkatID = [38]; 
				break;
			case 'KOMJEN':
				$pangkatID = [39]; 
				break;
			case 'JENDERAL':
				$pangkatID = [40]; 
				break;
			default : 
				$pangkatPolisi = false;
				break;
		}

		$input['parent_satuan_jabatan_id']=$request->input('parent_satuan_jabatan_id') == 'null' ? null : $request->input('parent_satuan_jabatan_id');
		$input['keterangan'] = Jabatan::find($input['jabatan_id'])->nama_jabatan.' '.Satuan::find($input['satuan_id'])->fullName();
		$input['nivelering_id']=$request->input('nivelering_id') == 'null' ? null : $request->input('nivelering_id');

		if($pangkatPolisi){
			$satuanJabatan->pangkat()->sync($pangkatID);
		}
		if($request->has('pangkat_id')){
			foreach($request->pangkat_id as $pangkat_id){
				array_push($pangkatID,intval($pangkat_id));
			}
			$satuanJabatan->pangkat()->sync($pangkatID);
		}
		if($satuanJabatan->update($input)){
			return redirect()->route('satuan.show',$satuan->satuan_id)->with('success','Update jabatan berhasil dilakukan.');
		}
		else {
			return redirect()->route('satuan.show',$satuan->satuan_id)->with('error','Update jabatan gagal dilakukan.');
		}
	}

	public function verification(Satuan $satuan, SatuanJabatan $satuanJabatan)
	{
		if($satuanJabatan->verified_by==null){
			$satuanJabatan->verified_by=auth('user')->user()->id;
		} else{
			$satuanJabatan->verified_by=null;
		}
		if($satuanJabatan->save())
			return redirect()->back()->with('success','Verifikasi berhasil.');
		else
			return redirect()->back()->with('error','Verifikasi gagal.');
	}

	public function dataError(Request $request,Satuan $satuan, SatuanJabatan $satuanJabatan)
	{
		$satuanJabatan->error_message=$request->error_message;
		$satuanJabatan->save();

		return response()->json([
			'data' => $satuanJabatan
		],200);
	}
	public function destroy(Satuan $satuan, SatuanJabatan $satuanJabatan)
	{
		$satuanJabatan->deleted_by=auth('user')->user()->id;
		if($satuanJabatan->save()){
			return redirect()->route('satuan.show',$satuan->satuan_id)->with('success','Berhasil menghapus jabatan.');
		}
		else{
			return redirect()->route('satuan.show',$satuan->satuan_id)->with('success','Gagal menghapus jabatan.');
		}
	}
}
