<?php

namespace App\Http\Controllers;

use App\JenisSatuan;
use Illuminate\Http\Request;

class JenisSatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jenis-satuan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jenisSatuan = JenisSatuan::create($request->except('_token'));

        if($jenisSatuan){
            return redirect()->route('satuan.index')->with('success','Buat jenis satuan berhasil dilakukan.');
        }
        else {
            return redirect()->route('satuan.index')->with('error','Buat jenis satuan gagal dilakukan.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisSatuan $jenisSatuan)
    {
        $dataSatuan = $jenisSatuan->satuan()->paginate(10);
        return view('jenis-satuan.edit',compact('jenisSatuan','dataSatuan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JenisSatuan $jenisSatuan)
    {
        if($jenisSatuan->update($request->except('_token'))){
            return redirect()->route('satuan.index')->with('success','Buat jenis satuan berhasil dilakukan.');
        }
        else {
            return redirect()->route('satuan.index')->with('error','Buat jenis satuan gagal dilakukan.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisSatuan $jenisSatuan)
    {
        if($jenisSatuan->delete()){
            return redirect()->route('satuan.index')->with('success','Hapus jenis satuan berhasil dilakukan.');
        }
        else {
            return redirect()->route('satuan.index')->with('error','Hapus jenis satuan gagal dilakukan.');
        }
        
    }
}
