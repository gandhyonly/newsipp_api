<?php

namespace App\Http\Controllers;

use App\Agama;
use App\JenisKelamin;
use App\Personel;
use App\StatusPersonel;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PersonelController extends Controller
{
	public function index()
	{
		$jumlahDataDuplikat = Personel::active()->where('is_duplicate',true)->count();
		$jumlahDataError = Personel::active()->where('is_error',true)->count();
		$jumlahDataBersih = Personel::active()->where('is_duplicate',false)->where('is_error',false)->count();
		$jumlahPersonel = Personel::active()->count();
		$dataPersonel = Personel::active()->paginate(10);
		return view('personel.index')
				->with('dataPersonel',$dataPersonel)
				->with('jumlahPersonel',$jumlahPersonel)
				->with('jumlahDataError',$jumlahDataError)
				->with('jumlahDataBersih',$jumlahDataBersih)
				->with('jumlahDataDuplikat',$jumlahDataDuplikat);
	}
	public function create()
	{
		return view('personel.create');
	}
	public function duplicate()
	{
		$jumlahDataError = Personel::active()->where('is_error',true)->count();
		$jumlahDataDuplikat = Personel::active()->where('is_duplicate',true)->count();
		$jumlahDataBersih = Personel::active()->where('is_duplicate',false)->where('is_error',false)->count();
		$jumlahPersonel = Personel::active()->count();
		$dataPersonel = Personel::active()->paginate(10);
		return view('personel.index')
				->with('dataPersonel',$dataPersonel)
				->with('jumlahPersonel',$jumlahPersonel)
				->with('jumlahDataError',$jumlahDataError)
				->with('jumlahDataBersih',$jumlahDataBersih)
				->with('jumlahDataDuplikat',$jumlahDataDuplikat);
	}

	public function filter(Request $request)
	{
		$filterDisplay=$request->input('display');
		$filterSortBy=$request->input('sort_by');
		$filterErrorMessage=$request->input('error_message');
		$filterSearchColumn=$request->input('search_column');
		$filterSearchData=$request->input('search_data');
		$dataPersonel = Personel::active();

		$dataPersonel->where($filterSearchColumn,'LIKE','%'.$filterSearchData.'%');

		if($filterDisplay == 'error_only') {
			$dataPersonel = $dataPersonel->where('is_error',true)->orderBy($filterSortBy);
		} elseif ($filterDisplay == 'duplicate_only') {
			$dataPersonel = $dataPersonel->where('is_duplicate',true)->orderBy($filterSortBy);
		} elseif ($filterDisplay == 'both') {
			$dataPersonel = $dataPersonel->where('is_duplicate',true)->where('is_error',true)->orderBy($filterSortBy);
		} elseif ($filterDisplay == 'clean') {
			$dataPersonel = $dataPersonel->where('is_duplicate',false)->where('is_error',false)->orderBy($filterSortBy);
		} else {
			$dataPersonel = $dataPersonel->orderBy($filterSortBy);
		}

		$dataPersonel = $dataPersonel->where('error_message','LIKE','%'.$filterErrorMessage.'%');
		$dataPersonel = $dataPersonel->paginate(10);
		$jumlahDataBersih = Personel::active()->where('is_duplicate',false)->where('is_error',false)->count();
		$jumlahDataError = Personel::active()->where('is_error',true)->count();
		$jumlahDataDuplikat = Personel::active()->where('is_duplicate',true)->count();
		$jumlahPersonel = Personel::active()->count();
		return view('personel.filter')
				->with('dataPersonel',$dataPersonel)
				->with('jumlahPersonel',$jumlahPersonel)
				->with('jumlahDataError',$jumlahDataError)
				->with('jumlahDataBersih',$jumlahDataBersih)
				->with('jumlahDataDuplikat',$jumlahDataDuplikat)
				->with('filterDisplay',$filterDisplay)
				->with('filterSortBy',$filterSortBy)
				->with('filterErrorMessage',$filterErrorMessage)
				->with('filterSearchColumn',$filterSearchColumn)
				->with('filterSearchData',$filterSearchData);
	}

	public function edit(Personel $personel)
	{
		$dataAgama = Agama::select('agama_id','nama')->get();
		$dataJenisKelamin = JenisKelamin::select('jenis_kelamin_id','jenis')->get();
		$dataStatusPersonel = StatusPersonel::select('status_personel_id','status')->get();
		return view('personel.edit')
			->with('dataAgama',$dataAgama)
			->with('dataJenisKelamin',$dataJenisKelamin)
			->with('dataStatusPersonel',$dataStatusPersonel)
			->with('personel',$personel);
	}
	public function update(Request $request, Personel $personel)
	{
		$personel->update($request->except('_token'));
		return redirect()->route('personel.index')->with('success','Update Personel Berhasil.');
	}

	public function verification(Personel $personel)
	{
		$personel = Personel::find($id);
		Personel::where('nrp',$personel->nrp)->update(['is_removed'=>true]);
		$personel->is_removed=false;
		$personel->is_duplicate=false;
		$personel->save();
		return redirect()->route('personel.index')->with('success','Data berhasil diverifikasi');
	}

    public function createTable()
    {
        Schema::create('personel', function (Blueprint $table) {
            $table->increments('personel_id');
            $table->string('nrp');
            $table->string('nama_lengkap');
            $table->string('nama_panggilan')->nullable(); // sementara nullable
            $table->date('tanggal_lahir')->nullable();
            $table->date('tmt_pertama')->nullable(); //sementara nullable

            $table->mediumText('alamat_kantor')->nullable();
            $table->mediumText('telepon_rumah')->nullable();//sementara medium text ada data yang terlalu panjang
            $table->string('telepon_kantor')->nullable();
            $table->string('bpjs')->nullable();
            $table->string('handphone')->nullable();
            $table->string('email')->nullable();
            $table->unsignedInteger('tinggi')->nullable();
            $table->unsignedInteger('berat')->nullable();
            $table->string('warna_kulit')->nullable();
            $table->string('warna_mata')->nullable();
            $table->string('gol_darah')->nullable();
            $table->string('sidik_jari_1')->nullable();
            $table->string('sidik_jari_2')->nullable();
            $table->unsignedInteger('ukuran_topi')->nullable();
            $table->unsignedInteger('ukuran_sepatu')->nullable();
            $table->unsignedInteger('ukuran_celana')->nullable();
            $table->unsignedInteger('ukuran_baju')->nullable();
            $table->string('no_seri_kta')->nullable();
            $table->string('jenis_rambut')->nullable();
            $table->string('warna_rambut')->nullable();
            $table->string('asabri')->nullable();
            $table->string('npwp')->nullable();
            $table->string('karkes')->nullable();
            $table->string('kpi')->nullable(); //tidak nemu data apa
            $table->mediumText('alamat_orang_tua')->nullable();
            $table->string('telepon_orang_tua')->nullable(); 
            //temporary
            $table->string('jabatan')->nullable();
            $table->date('tmt_jabatan')->nullable();
            $table->boolean('is_error')->default(true);
            $table->string('error_message');
            $table->boolean('is_duplicate')->default(false);
            $table->boolean('is_removed')->default(false);
            //end temporary

            $table->unsignedInteger('agama_id')->nullable();//sementara nullable
            $table->foreign('agama_id')->references('agama_id')->on('agama');

            $table->unsignedInteger('jenis_kelamin_id')->nullable();//sementara nullable
            $table->foreign('jenis_kelamin_id')->references('jenis_kelamin_id')->on('jenis_kelamin');

            $table->unsignedInteger('status_personel_id')->nullable();//sementara nullable
            $table->foreign('status_personel_id')->references('status_personel_id')->on('status_personel');

            // $table->unsignedInteger('status_personel_id')->nullable();//sementara nullable
            // $table->foreign('status_personel_id')->references('status_personel_id')->on('status_personel');
        });
    }
    public function dropTable()
    {
        Schema::dropIfExists('personel');
    }
}
