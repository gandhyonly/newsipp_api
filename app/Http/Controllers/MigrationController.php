<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\JenisSatuan;
use App\Nivelering;
use App\Personel;
use App\Satuan;
use App\SatuanJabatan;
use Artisan;
use DB;
use DateTime;
use Illuminate\Http\Request;

class MigrationController extends Controller
{
	public function index()
	{
		$jumlahSatuan = Satuan::count();
		$jumlahJabatan = Jabatan::count();
		$jumlahPersonel = Personel::active()->count();
		return view('migration.index')
				->with('jumlahPersonel',$jumlahPersonel)
				->with('jumlahSatuan',$jumlahSatuan)
				->with('jumlahJabatan',$jumlahJabatan);
	}

    public function jabatanMigration()
    {
        // POLDA
        $polda = Satuan::where('POLDA ACEH')->first();
            // JABATAN
                // KAPOLDA
                // WAKAPOLDA
            $jabatan = Jabatan::where('nama_jabatan', 'KAPOLDA')->first();
            $nivelering = Jabatan::where('nama_nivelering', 'II\B')->first();

            $kapolda = SatuanJabatan::create([
                'satuan_id' => $polda->satuan_id, 
                'jabatan_id' => $jabatan->jabatan_id,
                'status_jabatan_id' => 1, 
                'dsp' => 1,
                'nivelering' => $nivelering->nivelering_id
            ]); 
            // pangkat
            $kapolda->pangkat()->attach([38]);

            // ITWASDA
            $satuan = Satuan::where('ITWASDA')->first();
                // JABATAN
                // IRWASDA

                $jabatan = Jabatan::where('nama_jabatan', 'IRWASDA')->first();
                $nivelering = Jabatan::where('nama_nivelering', 'II/B')->first();
                $jabatan = SatuanJabatan::create([
                    'satuan_id' => $satuan->satuan_id, 
                    'jabatan_id' => $jabatan->jabatan_id,
                    'status_jabatan_id' => 1, 
                    'dsp' => 1,
                    'nivelering' => $nivelering->nivelering_id
                ]);
                $jabatan->pangkat()->attach([36]);
                // BAMIN

                $jabatan = Jabatan::where('nama_jabatan', 'BAMIN')->first();
                $jabatan = SatuanJabatan::create([
                    'satuan_id' => $satuan->satuan_id, 
                    'jabatan_id' => $jabatan->jabatan_id,
                    'status_jabatan_id' => 1, 
                    'dsp' => 1
                ]);
                $jabatan->pangkat()->attach([36]);
                    // SUBBAGRENMIN
                        // KASUBBAGRENMIN
                        // BAMIN/BANUM
       
        // Satuan POLDA
        $satuan = $polda = Satuan::find(7);
            // Jabatan
            SatuanJabatan::create(['satuan_id' => $polda->satuan_id, 'jabatan_id' => $jabatan->jabatan_id,'status_jabatan_id' => 1,'nivelering' => '', 'dsp' => 1]);
        $jabatan = Jabatan::where('nama_jabatan', 'WAKAPOLDA')->first();
        // $nivelering = Nivelering::where();
            SatuanJabatan::create(['satuan_id' => $polda->satuan_id, 'jabatan_id' => $jabatan->jabatan_id,'status_jabatan_id' => 1, 'dsp' => 1]);


        $jabatan = Jabatan::where('nama_jabatan', 'IRWASDA')->first();
        $nivelering = Nivelering::where('nama_nivelering','II/B')->first();
            $irwasda = SatuanJabatan::create(['satuan_id' => $polda->satuan_id, 'jabatan_id' => $jabatan->jabatan_id,'status_jabatan_id' => 1, 'nivelering_id' => $nivelering->nivelering_id, 'parent_satuan_jabatan_id' => $kapolda->satuan_jabatan_id, 'dsp' => 1]);
            // Pangkat
            $irwasda->pangkat()->attach(36); //KBP

            // Satuan ITWASDA
            $bag = $satuan->childs()->where('nama_satuan','ITWASDA')->first();
            $jabatan = Jabatan::where('nama_jabatan','KASUBBAGRENMIN')->first();
            $nivelering = Nivelering::where('nama_nivelering','II/B')->first();
            $satuanJabatan = SatuanJabatan::create([
                'satuan_id' => $bag->satuan_id,
                'jabatan_id' => $jabatan->jabatan_id,
                'status_jabatan_id' => 1, 
                'nivelering_id' => $nivelering->nivelering_id, 
                'parent_satuan_jabatan_id' => $irwasda->satuan_jabatan_id, 
                'dsp' => 1
            ]);
            // Pangkat
            $satuanJabatan->pangkat()->attach(34); //KP
    }

	public function seed(Request $request)
	{
        $database = $request->input('database');
		$table = $request->input('table');
        if($table=='PERSONEL')
		  $this->insertPersonel($database);

		return redirect()->route('migration.index')->with('success','migration success.');
	}

    public function insertJabatanPolsek()
    {
        //Kapolsek rural, Satuan Kerja UNIT SIHUMAS

            // Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
            $SIHUMAS = $POLSEKRURAL->childs()->where('nama_satuan','SIHUMAS')->first();

            // JABATAN PADA SIHUMAS
            $KASIHUMAS = SatuanJabatan::createJabatan($SIHUMAS,$KAPOLSEK,'KASIHUMAS',['IP'], 'IV/B',1);
            $BAMIN = SatuanJabatan::createJabatan($SIHUMAS,$KASIHUMAS,'BAMIN',['BA'],'-',2);
            //Kapolsek rural, Satuan Kerja SIUM

            // Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
            $SIUM = $POLSEKRURAL->childs()->where('nama_satuan','SIUM')->first();

            // JABATAN PADA SIUM
            $KASIUM = SatuanJabatan::createJabatan($SIUM,$KAPOLSEK,'KASIUM',['IP'],'IV/B',1);
            $BANUM = SatuanJabatan::createJabatan($SIUM,$KASIUM,'BANUM',['PNS II','PNS I'],'-',2);

            // SATUAN PADA SIUM, URRENMIN
            $URRENMIN = $SIUM->childs()->where('nama_satuan','URRENMIN')->first();

                // JABATAN PADA URRENMIN
                $BAMIN = SatuanJabatan::createJabatan($URRENMIN,$KASIUM,'BAMIN',['BA'],'-',1);

            // SATUAN PADA SIUM, URTAUD
            $URTAUD = $SIUM->childs()->where('nama_satuan','URTAUD')->first();

                // JABATAN PADA URRENMIN
                $BAMIN = SatuanJabatan::createJabatan($URTAUD,$KASIUM,'BAMIN',['BA'],'-',1);
                
            // SATUAN PADA SIUM, URTAHTI
            $URTAHTI = $SIUM->childs()->where('nama_satuan','URTAHTI')->first();

                // JABATAN PADA URTAHTI
                $BAMIN = SatuanJabatan::createJabatan($URTAHTI,$KASIUM,'BAMIN',['BA'],'-',1);

    }


    public function insertJabatan($db = 'newsipp_ssdm')
    {
        // $query = DB::connection($db)->table('t_personil')
        //         ->where('t_personil.c_status','1')
        //         ->count();

        // echo $query;

        $query = DB::connection($db)->table('t_personil')
                ->join('t_jabatan', 't_personil.ref_jabatan', '=', 't_jabatan.id')
                ->select('c_jabatan')
                ->where('t_personil.c_status','1')
                ->distinct('c_jabatan')
                ->orderBy('c_jabatan')
                ->get();
        // $query = [
        //     "KABAG SUBBAG RE BAG INFOPERS SSDM POLRI (BARU)",
        //     "BA POLSEK TASIKMADU POLRES KARANGANYA POLDA JATENG",
        // ];
        foreach($query as $key => $qJabatan){
            if($key==99)
                echo $qJabatan->c_jabatan;
            if($key%500==0){
                echo $key.'<br>';
            }
            $jabatan = $qJabatan->c_jabatan;
            // if(substr($jabatan,-6)!='(BARU)')
            //     continue;
            // $jabatan = $qJabatan;
            // echo 'Jabatan : '.$jabatan.'<br>';
            $arrJabatan = explode(" ",$jabatan);
            $tempArrJabatan = $arrJabatan;
            $countNewJabatan=0;
            $newArrJabatan=[];
            $arrTrigger = ['POLRI','POLDA','POLRES','POLSEK','RO','DIT','PUS','BAG','SUBBAG ','SUBDIT'];
            $isSatker = false;
            for($i=count($arrJabatan)-1;$i>0 ;$i--){
                if($isSatker==true){
                    $tempJabatan=$tempArrJabatan[$i];
                    $newArrJabatan[$countNewJabatan++]=$tempJabatan;
                    $jabatan = substr($jabatan,0,-(strlen($tempJabatan)+1));
                    $tempArrJabatan= explode(" ",$jabatan);
                    // echo 'new Satuan : '.$tempJabatan.'<br>';
                    // echo 'Sisa String : '.$jabatan.'<br>';
                    $isSatker=false;
                } else {
                    $tempJabatan='';
                    foreach($arrTrigger as $trigger){
                        if($key==99)
                            echo 'i :'.$i.'<br>';
                        if(substr($tempArrJabatan[$i],0,strlen($trigger)) == $trigger){
                            if($trigger == 'POLRI'){
                                $isSatker = true;
                            }
                            for($j=$i;$j<count($tempArrJabatan);$j++){
                                if($j==count($tempArrJabatan)-1){
                                    $tempJabatan.=$tempArrJabatan[$j];
                                } else{
                                    $tempJabatan.=$tempArrJabatan[$j].' ';
                                }
                            }
                            $newArrJabatan[$countNewJabatan++]=$tempJabatan;
                            $jabatan = substr($jabatan,0,-(strlen($tempJabatan)+1));
                            $tempArrJabatan= explode(" ",$jabatan);
                            // echo 'new Satuan : '.$tempJabatan.'<br>';
                            // echo 'Sisa String : '.$jabatan.'<br>';
                            break;
                        }
                    }                
                }
            }
            $newArrJabatan[$countNewJabatan]=$jabatan;
            // echo '- Jabatan : '.$jabatan.'<br>';
            $beforeSatuan = null;
            foreach($newArrJabatan as $key => $newJabatan){
                if(count($newArrJabatan)-1 == $key){
                    if(Jabatan::where('nama_jabatan',$newJabatan)->count()==0){
                        Jabatan::create([
                            'nama_jabatan' => $newJabatan,
                            'satuan_id' => $beforeSatuan == null ? null : $beforeSatuan->satuan_id,
                            'status_jabatan_id' => 1,
                        ]);
                    }
                } else{
                    if(Satuan::where('nama_satuan',$newJabatan)->count()==0){
                        $arrTrigger =['POLRI','POLDA','POLRES','POLSEK','BAG','SUBBAG '];
                        foreach($arrTrigger as $trigger){
                            if(substr($tempArrJabatan[$i],0,strlen($trigger)) == 'POLRI' || 
                                substr($tempArrJabatan[$i],0,strlen($trigger)) == 'POLDA' ||
                                substr($tempArrJabatan[$i],0,strlen($trigger)) == 'POLSEK' ||
                                substr($tempArrJabatan[$i],0,strlen($trigger)) == 'POLRES'
                                ){
                                $tipe = 'SATWIL';
                                break;
                            }
                            else{
                                $tipe = 'SATKER';
                            }
                        }
                        $beforeSatuan = Satuan::create([
                            'nama_satuan' => $newJabatan,
                            'tipe'  => $tipe,
                            'parent_id' => $beforeSatuan == null ? null : $beforeSatuan->satuan_id
                        ]);
                    } else {
                        $beforeSatuan = Satuan::where('nama_satuan',$newJabatan)->first();
                    }
                }
            }
        }
        return true;
    }

    public function resetPersonel(Request $request)
    {
        DB::statement("SET foreign_key_checks=0");
        Personel::truncate();
        DB::statement("SET foreign_key_checks=1");
        return redirect()->route('migration.index')->with('success','Reset data personel berhasil.');
    }
    public function resetSatuanJabatan(Request $request)
    {
        DB::statement("SET foreign_key_checks=0");
        Satuan::truncate();
        Jabatan::truncate();
        DB::statement("SET foreign_key_checks=1");
        return redirect()->route('migration.index')->with('success','Reset data satuan & jabatan berhasil.');
    }

	public function insertPersonel($db = 'newsipp_ssdm')
	{
        $tempNrp='0a';

        $query = DB::connection($db)->table('t_personil')
        ->join('t_jabatan', 't_personil.ref_jabatan', '=', 't_jabatan.id')
        ->select(
            't_personil.c_nrp',
            't_personil.ref_agama',
            't_personil.ref_pangkat',
            't_personil.c_nama',
            't_personil.c_kelamin',
            't_personil.c_tgllahir',
            't_personil.c_alamatKantor',
            't_personil.c_telpKantor',
            't_personil.c_telpRumah',
            't_personil.c_telpHP',
            't_personil.c_email',
            't_personil.c_tinggi',
            't_personil.c_berat',
            't_personil.c_kulit',
            't_personil.c_mata',
            't_personil.c_darah',
            't_personil.c_topi',
            't_personil.c_celana',
            't_personil.c_baju',
            't_personil.c_sepatu',
            't_personil.c_sidik1',
            't_personil.c_sidik2',
            't_personil.c_jenisRambut',
            't_personil.c_warnaRambut',
            't_personil.c_asabri',
            't_personil.c_bpjsKes',
            't_personil.c_npwp',
            't_personil.c_karkes',
            't_personil.c_kta',
            't_personil.c_tmtjabatan',
            't_personil.c_alamatOrtu',
            't_personil.c_telpOrtu',
            't_jabatan.c_jabatan as c_jabatan'
        )
        ->where('c_status','1')
        ->orderBy('c_nrp');
    	foreach($query->get() as $oldpersonel){
            $error=false;
            $errorMessage='';

            // validasi error 
            // validasi nama dengan gelar
            if(!$this->validasiNamaLengkapMemilikiGelar($oldpersonel->c_nama)){
                $error=true;
                $errorMessage.='Nama Lengkap Memiliki Gelar.<br>';
            }
            // validasi digit NRP 8 dan 10 digit.
            if(!$this->validasiDigitNRP($oldpersonel->c_nrp)){
                $error=true;
                $errorMessage.="Format nrp tidak benar. jumlah angka nrp:".strlen($oldpersonel->c_nrp).'<br>';
            }
            if(!$this->validasiNRPdanPangkat($oldpersonel->c_nrp,$oldpersonel->ref_pangkat)){
                $error=true;
                $errorMessage.='Digit NRP tidak sesuai pangkat.<br> Pangkat yang dimiliki : '.($oldpersonel->ref_pangkat <=49 ? 'PNS' : 'POLISI').' <br>';
            }


    		// validasi agama
    		if(!($oldpersonel->ref_agama >=1 && $oldpersonel->ref_agama<=5)){
    			$error=true;
    			$errorMessage.="data ref_agama salah. data ref_agama:".$oldpersonel->ref_agama.'<br>';
    		}
    		// validasi kelamin
    		if($oldpersonel->c_kelamin == 'Pria'){
    			$kelamin=1;
    		} else if($oldpersonel->c_kelamin == 'Wanita'){
    			$kelamin=2;
    		} else {
    			$error=true;
    			$errorMessage.="data c_kelamin diluar Pria dan Wanita. data c_kelamin:".$oldpersonel->c_kelamin.'<br>';
    		}


            if($oldpersonel->c_tgllahir != null){
                //validasi tanggal lahir
                $d = DateTime::createFromFormat('Y-m-d', $oldpersonel->c_tgllahir);
                if($d != null){
                    $valid = $d && $d->format('Y-m-d') == $oldpersonel->c_tgllahir;
                    if (!$valid) 
                    {
                        $tanggal_lahir = null;
                    }
                    else{
                        $tanggal_lahir = $oldpersonel->c_tgllahir;
                    }
                }
            }

            //validasi tanggal tmt_jabatan
            if($oldpersonel->c_tmtjabatan != null){
                $d = DateTime::createFromFormat('Y-m-d', $oldpersonel->c_tmtjabatan);
                if($d != null){
                    $valid = $d && $d->format('Y-m-d') == $oldpersonel->c_tmtjabatan;
                    if (!$valid) 
                    {
                        $error=true;
                        $errorMessage.="Format tanggal tmt_jabatan salah. Tanggal Tmt Jabatan:".$oldpersonel->c_tmtjabatan;
                        $tmt_jabatan = null;
                    }
                    else{
                        $tmt_jabatan = $oldpersonel->c_tmtjabatan;
                    }
                }
            }

    		$personel = Personel::create([
    			'nama_lengkap' => $oldpersonel->c_nama,
    			'nrp' => $oldpersonel->c_nrp,
    			'agama_id' => $oldpersonel->ref_agama,
                'jenis_kelamin_id' => $kelamin,
    			'status_personel_id' => 1,
                'error_message' => $errorMessage,
                'is_error' => $error,
    			'is_duplicate' => false,
    			'tanggal_lahir' => $tanggal_lahir,

    			'alamat_kantor' => $oldpersonel->c_alamatKantor,
    			'telepon_kantor' => $oldpersonel->c_telpKantor,
    			'telepon_rumah' => $oldpersonel->c_telpRumah,
    			'handphone' => $oldpersonel->c_telpHP,
    			'email' => $oldpersonel->c_email,

    			'tinggi' => $oldpersonel->c_tinggi,
    			'berat' => $oldpersonel->c_berat,

    			'warna_kulit' => $oldpersonel->c_kulit,
    			'warna_mata' => $oldpersonel->c_mata,

    			'gol_darah' => $oldpersonel->c_darah,

    			'ukuran_topi' => is_int($oldpersonel->c_topi) ? $oldpersonel->c_topi : null,
    			'ukuran_celana' => is_int($oldpersonel->c_celana) ? $oldpersonel->c_celana : null,
    			'ukuran_baju' => is_int($oldpersonel->c_baju) ? $oldpersonel->c_baju : null,
    			'ukuran_sepatu' => is_int($oldpersonel->c_sepatu) ? $oldpersonel->c_sepatu : null,

    			'sidik_jari_1' => $oldpersonel->c_sidik1,
    			'sidik_jari_2' => $oldpersonel->c_sidik2,

    			'jenis_rambut' => $oldpersonel->c_jenisRambut,
    			'warna_rambut' => $oldpersonel->c_warnaRambut,

    			'asabri' => $oldpersonel->c_asabri,
    			'bpjs' => $oldpersonel->c_bpjsKes,
    			'npwp' => $oldpersonel->c_npwp,
    			'karkes' => $oldpersonel->c_karkes,
    			'no_seri_kta' => $oldpersonel->c_kta,

                'tmt_jabatan' => $tmt_jabatan,
                'jabatan' => $oldpersonel->c_jabatan,

    			'alamat_orang_tua' => $oldpersonel->c_alamatOrtu,
    			'telepon_orang_tua' => $oldpersonel->c_telpOrtu,
    		]);
            if($personel->personel_id%500==0){
                echo $personel->personel_id.'<br>';
            }

            // validasi duplikat nrp 
            if($personel->nrp == $tempNrp){
                $dataDuplicatePersonel = Personel::where('nrp',$tempNrp)->get();
                foreach($dataDuplicatePersonel as $duplicatePersonel){
                    $duplicatePersonel->is_duplicate = true;
                    $duplicatePersonel->error_message = 'NRP duplikat.'.$duplicatePersonel->error_message;     
                    $duplicatePersonel->save();  
                }
            }
            $tempNrp=$personel->nrp;
    	}
        $tempNama='asdausidh.';

        $query = DB::connection($db)->table('t_personil')
        ->join('t_jabatan', 't_personil.ref_jabatan', '=', 't_jabatan.id')
        ->select(
            't_personil.c_nrp',
            't_personil.ref_agama',
            't_personil.c_nama',
            't_personil.c_kelamin',
            't_personil.c_tgllahir',
            't_personil.c_alamatKantor',
            't_personil.c_telpKantor',
            't_personil.c_telpRumah',
            't_personil.c_telpHP',
            't_personil.c_email',
            't_personil.c_tinggi',
            't_personil.c_berat',
            't_personil.c_kulit',
            't_personil.c_mata',
            't_personil.c_darah',
            't_personil.c_topi',
            't_personil.c_celana',
            't_personil.c_baju',
            't_personil.c_sepatu',
            't_personil.c_sidik1',
            't_personil.c_sidik2',
            't_personil.c_jenisRambut',
            't_personil.c_warnaRambut',
            't_personil.c_asabri',
            't_personil.c_bpjsKes',
            't_personil.c_npwp',
            't_personil.c_karkes',
            't_personil.c_kta',
            't_personil.c_tmtjabatan',
            't_personil.c_alamatOrtu',
            't_personil.c_telpOrtu',
            't_jabatan.c_jabatan as c_jabatan'
        )
        ->where('c_status','1')
        ->orderBy('c_nama','ASC');
        foreach($query->get() as $oldpersonel){
            // validasi duplikat nrp 
            if($oldpersonel->c_nama == $tempNama){
                $dataDuplicatePersonel = Personel::where('nama_lengkap',$tempNama)->get();
                foreach($dataDuplicatePersonel as $duplicatePersonel){
                    $duplicatePersonel->is_duplicate = true;
                    if(!substr($duplicatePersonel->error_message,0,22)=='Nama Lengkap Duplikat.'){
                        $duplicatePersonel->error_message = 'Nama Lengkap Duplikat.<br>'.$duplicatePersonel->error_message;
                    }
                    $duplicatePersonel->save();
                }
            }
            $tempNama=$oldpersonel->c_nama;
        }

        echo 'Migrasi Sukses';
        return true;
	}

	//personel function
    private function validasiNamaLengkapMemilikiGelar($nama)
    {
        if(preg_match_all("/[.,]+/", $nama)){
            return false;
        }
        return true;
    }

    private function validasiDigitNRP($nrp)
    {
        if(strlen($nrp)==18 || strlen($nrp)==8){
            return true;
        }
        return false;
    }

    private function validasiFormatNRPdanTanggalLahir($nrp,$tanggalLahir)
    {
        $d = DateTime::createFromFormat('Y-m-d', $tanggalLahir);
        if(strlen($nrp) == 18){
            $nrpFormatDate = substr($nrp,0,6);
            $tempDate = substr($d->format('Ymd'),0,6);
            if($tempDate == $nrpFormatDate){
                return true;
            }
        } else{
            $nrpFormatDate = substr($nrp,0,4);
            $tempDate = substr($d->format('Ym'),0,4);
            if($tempDate == $nrpFormatDate){
                return true;
            }
        }
        return false;
    }

    private function validasiNRPdanPangkat($nrp,$pangkat_id)
    {
        if((strlen($nrp)==18 && $pangkat_id <= 49) || (strlen($nrp)==8 && $pangkat_id >49 )){
            return true;
        }
        return false;
    }
}
