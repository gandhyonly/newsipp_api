<?php

namespace App\Http\Controllers\Migration;

use App\Alamat;
use App\Dikum;
use App\Fisik;
use App\Http\Controllers\Controller;
use App\HubunganKeluarga;
use App\KeluargaPersonel;
use App\Pangkat;
use App\PendidikanPersonel;
use App\Personel;
use App\PersonelData;
use App\RiwayatJabatan;
use App\RiwayatPangkat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;

class Migrasi1Controller extends Controller
{
    public function index()
    {
    	$count=0;
    	DB::statement("SET foreign_key_checks=0");
		Personel::truncate();
		PendidikanPersonel::truncate();
		DB::statement("SET foreign_key_checks=1");
        $tempNrp='0a';

        $query = DB::connection('local_ssdm')->table('t_personil')
        ->join('t_jabatan', 't_personil.ref_jabatan', '=', 't_jabatan.id')
        ->select(
            't_personil.id',
            't_personil.c_nrp',
            't_personil.ref_agama',
            't_personil.c_nama',
            't_personil.c_kelamin',
            't_personil.c_tempatMenikah',
            't_personil.c_tgllahir',
            't_personil.c_telpRumah',
            't_personil.c_telpHP',
            't_personil.c_email',
            't_personil.c_tinggi',
            't_personil.c_berat',
            't_personil.c_kulit',
            't_personil.c_mata',
            't_personil.c_darah',
            't_personil.c_topi',
            't_personil.c_celana',
            't_personil.c_baju',
            't_personil.c_sepatu',
            't_personil.c_sidik1',
            't_personil.c_sidik2',
            't_personil.c_jenisRambut',
            't_personil.c_warnaRambut',
            't_personil.c_asabri',
            't_personil.c_bpjsKes',
            't_personil.c_npwp',
            't_personil.c_karkes',
            't_personil.c_kta',
            't_personil.c_tmtjabatan',
            't_personil.c_alamatOrtu',
            't_personil.c_telpOrtu',
            't_personil.c_status',
            't_personil.c_sukubangsa',
            't_personil.c_nik',
            't_personil.c_noKK',
            't_personil.c_tmplahir',
            't_jabatan.c_jabatan as c_jabatan'
        )
        ->orderBy('c_nrp');
    	foreach($query->get() as $oldpersonel){
            $skip=false;
            $error=false;
            $errorMessage='';

            // validasi error 
            // validasi nama dengan gelar
            if(!$this->validasiNamaLengkapMemilikiGelar($oldpersonel->c_nama)){
                $error=true;
                $errorMessage.='Nama Lengkap Memiliki Gelar.<br>';
            }
            // validasi digit NRP 8 dan 18 digit.
            if(!$this->validasiDigitNRP($oldpersonel->c_nrp)){
                $error=true;
                $errorMessage.="Format nrp tidak benar. jumlah angka nrp:".strlen($oldpersonel->c_nrp).'<br>';
            }


    		// validasi agama
    		if(!($oldpersonel->ref_agama >=1 && $oldpersonel->ref_agama<=5)){
    			$error=true;
    			$errorMessage.="data ref_agama salah. data ref_agama:".$oldpersonel->ref_agama.'<br>';
    		}

            if($oldpersonel->c_tgllahir != null){
                //validasi tanggal lahir
                $d = Carbon::createFromFormat('Y-m-d', $oldpersonel->c_tgllahir);
                if($d != null){
                    $valid = $d && $d->format('Y-m-d') == $oldpersonel->c_tgllahir;
                    if (!$valid) 
                    {
                        $error=true;
                        $errorMessage.="Format tanggal lahir salah. Tanggal Lahir :".$oldpersonel->c_tgllahir;
                        $tanggal_lahir = null;
                    }
                    else{
                        $tanggal_lahir = $oldpersonel->c_tgllahir;
                    }
                }

                //validasi format nrp dan tanggal lahir (yymmxxxx)
                if(!$this->validasiFormatNRPdanTanggalLahir($oldpersonel->c_nrp,$oldpersonel->c_tgllahir)){
                    $error=true;
                    $errorMessage.="Format NRP dan tanggal lahir tidak sama. ";
                    $errorMessage.="Tanggal Lahir :".$oldpersonel->c_tgllahir.'<br>';
                }
            }

            //validasi tanggal tmt_jabatan
            if($oldpersonel->c_tmtjabatan != null){
                $d = Carbon::createFromFormat('Y-m-d', $oldpersonel->c_tmtjabatan);
                if($d != null){
                    $valid = $d && $d->format('Y-m-d') == $oldpersonel->c_tmtjabatan;
                    if (!$valid) 
                    {
                        $error=true;
                        $errorMessage.="Format tanggal tmt_jabatan salah. Tanggal Tmt Jabatan:".$oldpersonel->c_tmtjabatan;
                        $tmt_jabatan = null;
                    }
                    else{
                        $tmt_jabatan = $oldpersonel->c_tmtjabatan;
                    }
                }
            }
            if(strlen($oldpersonel->c_nrp) == 18){
                $polisi=false;
            } elseif(strlen($oldpersonel->c_nrp) == 8) {
                $polisi=true;
            } else {
                $skip=true;
            }
            $statusPersonel = 1;
            if($oldpersonel->c_status==1){
                $status=1;
            } else if($oldpersonel->c_status==2){
                $status=4;
            } else if($oldpersonel->c_status==3){
                $skip=true;
            } else if($oldpersonel->c_status==4){
                $status=2;
            } else if($oldpersonel->c_status==5){
                $status=8; // MENINGGAL
            } else if($oldpersonel->c_status==6){
                $status=3;
            }

            if($skip){
                continue;
            }

    		$newpersonel = Personel::create([
                'nama_lengkap' => $oldpersonel->c_nama,
                'tempat_lahir' => $oldpersonel->c_tmplahir,
                'tanggal_lahir' => $tanggal_lahir,
                'jenis_kelamin' => $oldpersonel->c_kelamin,
                'tmt_pertama' => $tmt_jabatan,
                'tmt_jabatan_pertama' => $tmt_jabatan,
                'agama_id' => $oldpersonel->ref_agama,
                'nrp' => $oldpersonel->c_nrp,
                'polisi' => $polisi,
                'status_pernikahan' => $oldpersonel->c_tempatMenikah==null?'MENIKAH':'BELUM NIKAH', 
                'status_personel_id' => $status, 
    			'agama_id' => $oldpersonel->ref_agama,
    			'email' => $oldpersonel->c_email,
    			'gol_darah' => $oldpersonel->c_darah,
    			'jenis_rambut' => $oldpersonel->c_jenisRambut,
    			'warna_rambut' => $oldpersonel->c_warnaRambut,
    		]);

            PersonelData::create([
                'personel_id' => $newpersonel->personel_id,
                'nama_panggilan' => $oldpersonel->c_nama,
                'handphone' => $oldpersonel->c_telpHP,
                'golongan_darah' => $oldpersonel->c_darah,
                'email' => $oldpersonel->c_email,
                'alamat_orang_tua' => $oldpersonel->c_alamatOrtu,
                'telepon_orang_tua' => $oldpersonel->c_telpOrtu,
                'suku_bangsa' => $oldpersonel->c_sukubangsa,
                'warna_kulit' => $oldpersonel->c_kulit,
                'jenis_rambut' => $oldpersonel->c_jenisRambut,
                'warna_rambut' => $oldpersonel->c_warnaRambut,
                'warna_mata' => $oldpersonel->c_mata,
                'sidik_jari_1' => $oldpersonel->c_sidik1,
                'sidik_jari_2' => $oldpersonel->c_sidik2,
                'asabri_nomor' => $oldpersonel->c_asabri,
                'kta_nomor' => $oldpersonel->c_kta,
                'ktp_nomor' => $oldpersonel->c_nik,
                'npwp_nomor' => $oldpersonel->c_npwp,
                'kartu_keluarga_nomor' => $oldpersonel->c_noKK,
                'bpjs_nomor' => $oldpersonel->c_bpjsKes
            ]);

            Fisik::create([
                'personel_id' => $newpersonel->personel_id,
                'tinggi' => $oldpersonel->c_tinggi,
                'berat' => $oldpersonel->c_berat,
                'ukuran_topi' => is_numeric($oldpersonel->c_topi)?$oldpersonel->c_topi:null,
                'ukuran_sepatu' => is_numeric($oldpersonel->c_sepatu)?$oldpersonel->c_sepatu:null,
                'ukuran_celana' => is_numeric($oldpersonel->c_celana)?$oldpersonel->c_celana:null,
                'ukuran_baju' => is_numeric($oldpersonel->c_baju)?$oldpersonel->c_baju:null
            ]);
            // Keluarga
		        $keluarga = DB::connection('local_ssdm')->table('t_rfamily')
		        ->select(
		            't_rfamily.c_type',
		            't_rfamily.c_name',
		            't_rfamily.c_kelamin',
		            't_rfamily.c_tgllahir',
		            't_rfamily.c_status'
		        )
		        ->where('ref_personil',$oldpersonel->id)
		        ->get();
		        foreach($keluarga as $anggotaKeluarga){

		            //validasi tanggal keluargaTanggalLahir
		            if($anggotaKeluarga->c_tgllahir != null){
		                $d = Carbon::createFromFormat('Y-m-d', $anggotaKeluarga->c_tgllahir);
		                if($d != null){
		                    $valid = $d && $d->format('Y-m-d') == $anggotaKeluarga->c_tgllahir;
		                    if (!$valid) 
		                    {
		                        $error=true;
		                        $errorMessage.="Format tanggal lahir keluarga salah. ex.".$anggotaKeluarga->c_tgllahir;
		                        $keluargaTanggalLahir = null;
		                    }
		                    else{
		                        $keluargaTanggalLahir = $anggotaKeluarga->c_tgllahir;
		                    }
		                }
		            }
		        	$hubunganKeluarga = HubunganKeluarga::where('hubungan','LIKE',$anggotaKeluarga->c_type)->first();
		        	if($hubunganKeluarga==null){
		        		$hubunganKeluarga = HubunganKeluarga::create(['hubungan' => strtoupper($anggotaKeluarga->c_type)]);
		        	}
		            $keluargaPersonel =KeluargaPersonel::create([
		            	'personel_id' => $newpersonel->personel_id,
		            	'hubungan_keluarga_id' => $hubunganKeluarga->hubungan_keluarga_id,
		            	'nama_keluarga' => strtoupper($anggotaKeluarga->c_name),
		            	'jenis_kelamin' => strtoupper($anggotaKeluarga->c_kelamin),
		            	'tanggal_lahir' => $keluargaTanggalLahir,
		            	'status' => strtoupper($anggotaKeluarga->c_status),
		            ]);
		        	if(strtolower($anggotaKeluarga->c_type)==strtolower('ibu')){
		        		$newpersonel->nama_ibu = $keluargaPersonel->nama_keluarga;
		        		$newpersonel->save();
		        	}
		        }
		    // End Keluarga
		    // Pendidikan Umum
		        $oldRiwayatDikum = DB::connection('local_ssdm')->table('t_rdikum')
		        ->select(
		            't_rdikum.c_dikum',
		            't_rdikum.c_nama',
		            't_rdikum.c_tahun',
		            't_rdikum.c_ijasah'
		        )
		        ->where('ref_personil',$oldpersonel->id)
		        ->get();
		        foreach($oldRiwayatDikum as $riwayatDikum){
		        	$dikumID=null;
		        	$dikum = Dikum::where('tingkat','LIKE',"%$riwayatDikum->c_dikum%")->first();
		        	if($dikum==null){
		        		$dikumID=null;
		        		continue;
		        	} else {
		        		$dikumID=$dikum->dikum_id;
		        	}
		            //validasi tanggal tahunLulus
		            $tahunLulus=null;
		            if(is_numeric($riwayatDikum->c_tahun)){
			            if($riwayatDikum->c_tahun != null){
			            	if($riwayatDikum->c_tahun<3000){
			            	// $count++;
			            	// echo $riwayatDikum->c_tahun.'<br>';
				                $d = Carbon::createFromFormat('Y-m-d', $riwayatDikum->c_tahun.'-01-01');
				                if($d != null){
				                    $valid = $d && ($d->format('Y-m-d') == $riwayatDikum->c_tahun.'-01-01');
				                    if (!$valid) 
				                    {
				                        $error=true;
				                        $errorMessage.="Format tahun lulus salah. ex.".$riwayatDikum->c_tahun;
				                        $tahunLulus = null;
				                    }
				                    else{
				                        $tahunLulus = $d->format('Y-m-d');
				                    }
				                }
				            }
			            }
			            $keluargaPersonel = PendidikanPersonel::create([
			            	'personel_id' => $newpersonel->personel_id,
			            	'dikum_id' => $dikumID,
			            	'tanggal_lulus' => $tahunLulus,
			            	'nama_institusi' => strtoupper($riwayatDikum->c_nama),
			            	'surat_kelulusan_nomor' => strtoupper($riwayatDikum->c_ijasah),
			            ]);
		            }
		        }
		    // End Pendidikan Umum
	        // Riwayat Jabatan
		        $oldRiwayatJabatan = DB::connection('local_ssdm')->table('t_rjabatan')
		        ->select(
		            't_rjabatan.c_jabatan',
		            't_rjabatan.c_tahun',
		            't_rjabatan.c_noSkep'
		        )
		        ->where('ref_personil',$oldpersonel->id)
		        ->get();
		        foreach($oldRiwayatJabatan as $riwayatJabatan){
		        	// $count++;
		        	// if($count==2){
			        // 	dd($riwayatJabatan);
		        	// }

		        	$tmtJabatan=null;
		            //validasi tanggal tmtJabatan
		            if($riwayatJabatan->c_tahun != null){
		                $d = Carbon::createFromFormat('Y-m-d', $riwayatJabatan->c_tahun);
		                if($d != null){
		                    $valid = $d && $d->format('Y-m-d') == $riwayatJabatan->c_tahun;
		                    if (!$valid) 
		                    {
		                        $error=true;
		                        $errorMessage.="Format tmt_jabatan salah. ex.".$riwayatJabatan->c_tahun;
		                        $tmtJabatan = null;
		                    }
		                    else{
		                        $tmtJabatan = $riwayatJabatan->c_tahun;
		                    }
		                }
		            }
			        $jabatan = RiwayatJabatan::create([
			        	'personel_id' => $newpersonel->personel_id,
			        	'keterangan' => $riwayatJabatan->c_jabatan,
			        	'tmt_jabatan' => $tmtJabatan,
			        	'surat_keputusan_nomor' => $riwayatJabatan->c_noSkep
			        ]);
			        // dd($jabatan);
		        }
	        // End Riwayat Jabatan
		    // Riwayat Pangkat
		        $oldRiwayatPangkat = DB::connection('local_ssdm')->table('t_rpangkat')
        		->join('t_pangkat', 't_rpangkat.ref_pangkat', '=', 't_pangkat.id')
		        ->select(
		            't_rpangkat.ref_pangkat',
		            't_pangkat.c_pangkat',
		            't_rpangkat.c_tmtpangkat',
		            't_rpangkat.c_noSkep'
		        )
		        ->where('t_rpangkat.ref_personil',$oldpersonel->id)
		        ->get();
		        foreach($oldRiwayatPangkat as $riwayatPangkat){
		        	$pangkatID=null;
		        	$pangkat = Pangkat::where('nama_pangkat',$riwayatPangkat->c_pangkat)->first();
		        	if($pangkat!=null){
			        	$pangkatID = $pangkat->pangkat_id;
		        	}
		        	if($pangkatID==null){
		        		echo $riwayatPangkat->c_pangkat.'<br>';
		        	}

		        	// $count++;
		        	// if($count==2){
			        // 	dd($riwayatPangkat);
		        	// }

		        	$tmtPangkat=null;
		            //validasi tanggal tmtPangkat
		            if($riwayatPangkat->c_tmtpangkat != null){
		                $d = Carbon::createFromFormat('Y-m-d', $riwayatPangkat->c_tmtpangkat);
		                if($d != null){
		                    $valid = $d && $d->format('Y-m-d') == $riwayatPangkat->c_tmtpangkat;
		                    if (!$valid) 
		                    {
		                        $error=true;
		                        $errorMessage.="Format tmt_jabatan salah. ex.".$riwayatPangkat->c_tmtpangkat;
		                        $tmtPangkat = null;
		                    }
		                    else{
		                        $tmtPangkat = $riwayatPangkat->c_tmtpangkat;
		                    }
		                }
		            }
			        $jabatan = RiwayatPangkat::create([
			        	'personel_id' => $newpersonel->personel_id,
			        	'pangkat_id' => $pangkatID,
			        	'tmt_pangkat' => $tmtPangkat,
			        	'surat_keputusan_nomor' => $riwayatPangkat->c_noSkep
			        ]);
			        // dd($jabatan);
		        }
		    // End Riwayat Pangkat
	        // Olahraga
		        
	        // End Olahraga
    	}
    }

    public function testing()
    {
    	$tmtJabatan='0000-01-01';
        $tmt_jabatan = null;
        //validasi tanggal tmt_jabatan
        if($tmtJabatan != null){
            if ($this->validateDate($tmtJabatan)) 
            {
                $error=true;
                $tmt_jabatan = null;
            }
            else{
                $tmt_jabatan = $tmtJabatan;
            }
        }
        dd($tmt_jabatan);
    }
        
    private function validateDate($date, $format = 'Y-m-d')
	{
		$valid=false;
	    $d = DateTime::createFromFormat($format, $date);
	    if($d!=null){
		    if($d->format('Y')>1500 && $d->format('Y')<=2500){
		    	$valid=true;
		    }
	    }
	    return !($d && $d->format($format) == $date && $valid);
	}

    private function validasiNamaLengkapMemilikiGelar($nama)
    {
        if(preg_match_all("/[.,]+/", $nama)){
            return false;
        }
        return true;
    }
    private function validasiDigitNRP($nrp)
    {
        if(strlen($nrp)==8 || strlen($nrp)==18){
            return true;
        }
        return false;
    }

    private function validasiFormatNRPdanTanggalLahir($nrp,$tanggalLahir)
    {
        if($tanggalLahir == null){
            $d = Carbon::createFromFormat('Y-m-d', $tanggalLahir);
            $nrpFormatDate = substr($nrp,0,4);
            if($d){
                $tempDate = substr($d->format('Ym'),2,4);
                if($tempDate == $nrpFormatDate){
                    return true;
                }
            }
        }
        return false;
    }
}
