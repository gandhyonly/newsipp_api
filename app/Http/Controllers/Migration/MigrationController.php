<?php

namespace App\Http\Controllers\Migration;

use App\Http\Controllers\Controller;
use App\MigrationStatus;
use Illuminate\Http\Request;

class MigrationController extends Controller
{
	public function index()
	{
		$migrationStatuses = MigrationStatus::get();
		return view('migration.index', compact(
			'migrationStatuses'
		));
	}

	public function migrationPersonel()
	{
        $tempNrp='0a';

        $query = DB::connection($db)->table('t_personil')
        ->join('t_jabatan', 't_personil.ref_jabatan', '=', 't_jabatan.id')
        ->select(
            't_personil.c_nrp',
            't_personil.ref_agama',
            't_personil.ref_pangkat',
            't_personil.c_nama',
            't_personil.c_kelamin',
            't_personil.c_tgllahir',
            't_personil.c_alamatKantor',
            't_personil.c_telpKantor',
            't_personil.c_telpRumah',
            't_personil.c_telpHP',
            't_personil.c_email',
            't_personil.c_tinggi',
            't_personil.c_berat',
            't_personil.c_kulit',
            't_personil.c_mata',
            't_personil.c_darah',
            't_personil.c_topi',
            't_personil.c_celana',
            't_personil.c_baju',
            't_personil.c_sepatu',
            't_personil.c_sidik1',
            't_personil.c_sidik2',
            't_personil.c_jenisRambut',
            't_personil.c_warnaRambut',
            't_personil.c_asabri',
            't_personil.c_bpjsKes',
            't_personil.c_npwp',
            't_personil.c_karkes',
            't_personil.c_kta',
            't_personil.c_tmtjabatan',
            't_personil.c_alamatOrtu',
            't_personil.c_telpOrtu',
            't_jabatan.c_jabatan as c_jabatan'
        )
        ->where('c_status','1')
        ->orderBy('c_nrp');
        $personel = new Personel;
    	foreach($query->get() as $oldpersonel){
    		if(str_len($oldpersonel->c_nrp) == 8){
	    		$personel->nrp = $oldpersonel->nrp;
    		}else{
    		}
    	}
	}
}
