<?php

namespace App\Http\Controllers\Migration\Jabatan;

use App\Http\Controllers\Controller;
use App\Jabatan;
use App\JenisSatuan;
use App\SatuanJabatan;
use Illuminate\Http\Request;

class KepalaSatuanWilayahController extends Controller
{
	public function index()
	{
		$KAPOLRI = SatuanJabatan::find(26);
		if(Jabatan::where('nama_jabatan','KAPOLDA')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLDA']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLDA')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLDA']);
		}
		$this->kepalaSatuanPOLDATipeA($KAPOLRI);
		$this->kepalaSatuanPOLDATipeAKhusus($KAPOLRI);
		$this->kepalaSatuanPOLDATipeB($KAPOLRI);

		$this->kepalaSatuanPOLRESMETRO();
		$this->kepalaSatuanPOLRESTA();
		$this->kepalaSatuanPOLRESTABES();
		$this->kepalaSatuanPOLRES();

		$this->kepalaSatuanPolsekUrban();
		$this->kepalaSatuanPolsekRural();
		$this->kepalaSatuanPolsekPraRural();
		$this->kepalaSatuanPolsekMetro();
		$this->kepalaSatuanPolSubSektor();
	}
	public function kepalaSatuanPOLDATipeA($KAPOLRI)
	{
		$POLDAtipeA = JenisSatuan::where('nama_jenis_satuan', 'POLDA A')->first()->satuan()->get();
		foreach($POLDAtipeA as $POLDA){
			$KAPOLDA = SatuanJabatan::createJabatan($POLDA,$KAPOLRI,'KAPOLDA',['IRJEN'],'I/B',1);
			$KAPOLDA->is_leader=true;
			$KAPOLDA->save();
			$WAKAPOLDA = SatuanJabatan::createJabatan($POLDA,$KAPOLDA,'WAKAPOLDA',['BRIRGJEN'],'II/A',1);
		}
	}
	public function kepalaSatuanPOLDATipeAKhusus($KAPOLRI)
	{
		$POLDATipeAKhusus = JenisSatuan::where('nama_jenis_satuan', 'POLDA A KHUSUS')->first()->satuan()->get();
		foreach($POLDATipeAKhusus as $POLDA){
			$KAPOLDA = SatuanJabatan::createJabatan($POLDA,$KAPOLRI,'KAPOLDA',['IRJEN'],'I/B',1);
			$KAPOLDA->is_leader=true;
			$KAPOLDA->save();
			$WAKAPOLDA = SatuanJabatan::createJabatan($POLDA,$KAPOLDA,'WAKAPOLDA',['BRIRGJEN'],'II/A',1);
		}
	}
	public function kepalaSatuanPOLDATipeB($KAPOLRI)
	{
		$POLDAtipeB = JenisSatuan::where('nama_jenis_satuan', 'POLDA B')->first()->satuan()->get();
		foreach($POLDAtipeB as $POLDA){
			$KAPOLDA = SatuanJabatan::createJabatan($POLDA,$KAPOLRI,'KAPOLDA',['BRIGJEN'],'I/B',1);
			$KAPOLDA->is_leader=true;
			$KAPOLDA->save();
			$WAKAPOLDA = SatuanJabatan::createJabatan($POLDA,$KAPOLDA,'WAKAPOLDA',['KBP'],'II/A',1);
		}
	}
	public function kepalaSatuanPOLRESMETRO()
	{
		$POLRESMETRO = JenisSatuan::where('nama_jenis_satuan', 'POLRESMETRO')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLRESMETRO')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLRESMETRO']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLRESMETRO')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLRESMETRO']);
		}
		foreach($POLRESMETRO as $POLRES){
			$kepalaSatuan = $POLRES->parent()->first()->jabatan()->leader()->first();
			$KAPOLRESMETRO = SatuanJabatan::createJabatan($POLRES,$kepalaSatuan,'KAPOLRESMETRO',['KBP'],'II/B2',1);
			$KAPOLRESMETRO->is_leader=true;
			$KAPOLRESMETRO->save();
			$WAKAPOLRESMETRO = SatuanJabatan::createJabatan($POLRES,$KAPOLRESMETRO,'WAKAPOLRESMETRO',['AKBP'],'III/A1',1);
		}
	}
	public function kepalaSatuanPOLRESTA()
	{
		$POLRESTA = JenisSatuan::where('nama_jenis_satuan','POLRESTA')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLRESTA')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLRESTA']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLRESTA')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLRESTA']);
		}
		foreach($POLRESTA as $POLRES){
			$kepalaSatuan = $POLRES->parent()->first()->jabatan()->leader()->first();
			$KAPOLRESTA = SatuanJabatan::createJabatan($POLRES,$kepalaSatuan,'KAPOLRESTA',['KBP'],'II/B3',1);
			$KAPOLRESTA->is_leader=true;
			$KAPOLRESTA->save();
			$WAKAPOLRESTA = SatuanJabatan::createJabatan($POLRES,$KAPOLRESTA,'WAKAPOLRESTA',['AKBP'],'III/A1',1);
		}
	}
	public function kepalaSatuanPOLRES()
	{
		$dataPOLRES = JenisSatuan::where('nama_jenis_satuan','POLRES')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLRES')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLRES']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLRES')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLRES']);
		}
		foreach($dataPOLRES as $POLRES){
			$kepalaSatuan = $POLRES->parent()->first()->jabatan()->leader()->first();
			$KAPOLRES = SatuanJabatan::createJabatan($POLRES,$kepalaSatuan,'KAPOLRES',['AKBP'],'III/A2',1);
			$KAPOLRES->is_leader=true;
			$KAPOLRES->save();
			$WAKAPOLRES = SatuanJabatan::createJabatan($POLRES,$KAPOLRES,'WAKAPOLRES',['KP'],'III/B1',1);
		}
	}
	public function kepalaSatuanPOLRESTABES()
	{
		$POLRESTABES = JenisSatuan::where('nama_jenis_satuan','POLRESTABES')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLRESTABES')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLRESTABES']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLRESTABES')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLRESTABES']);
		}
		foreach($POLRESTABES as $POLRES){
			$kepalaSatuan = $POLRES->parent()->first()->jabatan()->leader()->first();
			if($POLRES->nama_satuan == 'POLRESTABES SURABAYA' || $POLRES->nama_satuan == 'POLRESTABES BANDUNG'){
				$KAPOLRESTABES = SatuanJabatan::createJabatan($POLRES,$kepalaSatuan,'KAPOLRESTABES',['KBP'],'II/B1',1);
				$KAPOLRESTABES->is_leader=true;
				$KAPOLRESTABES->save();
			} else {
				$KAPOLRESTABES = SatuanJabatan::createJabatan($POLRES,$kepalaSatuan,'KAPOLRESTABES',['KBP'],'II/B2',1);
				$KAPOLRESTABES->is_leader=true;
				$KAPOLRESTABES->save();
			}
			$WAKAPOLRESTABES = SatuanJabatan::createJabatan($POLRES,$KAPOLRESTABES,'WAKAPOLRESTABES',['AKBP'],'III/A1',1);
		}
	}
	public function kepalaSatuanPolsekPraRural( )
	{
		$POLSEKPRARURAL = JenisSatuan::where('nama_jenis_satuan','POLSEK PRARURAL')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLSEK')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLSEK']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLSEK')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLSEK']);
		}
		foreach($POLSEKPRARURAL as $POLSEK){
			$kepalaSatuan = $POLSEK->parent()->first()->jabatan()->leader()->first();
			$KAPOLSEK = SatuanJabatan::createJabatan($POLSEK,$kepalaSatuan,'KAPOLSEK',['IP'],'IV/B',1);
		}
		// Kapolsek IP IV B
	}
	public function kepalaSatuanPolsekUrban()
	{
		$POLSEKPRARURAL = JenisSatuan::where('nama_jenis_satuan','POLSEK URBAN')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLSEK')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLSEK']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLSEK')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLSEK']);
		}
		// Kapolsek KP IIIB2
		// Wakapolsek AKP IV A
		foreach($POLSEKPRARURAL as $POLSEK){
			$kepalaSatuan = $POLSEK->parent()->first()->jabatan()->leader()->first();
			$KAPOLSEK = SatuanJabatan::createJabatan($POLSEK,$kepalaSatuan,'KAPOLSEK',['IP'],'III/B2',1);
			$WAKAPOLSEK = SatuanJabatan::createJabatan($POLSEK,$KAPOLSEK,'WAKAPOLSEK',['AKP'],'IV/A',1);
		}
	}
	public function kepalaSatuanPolsekMetro()
	{
		$POLSEKPRARURAL = JenisSatuan::where('nama_jenis_satuan','POLSEK METRO')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLSEK')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLSEK METRO']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLSEK')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLSEK METRO']);
		}
		
		// KAPOLSEK METRO KP III/B2
		// WAKAPOLSEK METRO AKP IV/A
		foreach($POLSEKPRARURAL as $POLSEK){
			$kepalaSatuan = $POLSEK->parent()->first()->jabatan()->leader()->first();
			$KAPOLSEK = SatuanJabatan::createJabatan($POLSEK,$kepalaSatuan,'KAPOLSEK METRO',['IP'],'III/B2',1);
			$WAKAPOLSEK = SatuanJabatan::createJabatan($POLSEK,$KAPOLSEK,'WAKAPOLSEK METRO',['AKP'],'IV/A',1);
		}
	}

	public function kepalaSatuanPolsekRural()
	{
		$POLSEKPRARURAL = JenisSatuan::where('nama_jenis_satuan','POLSEK RURAL')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLSEK')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLSEK']);
		}
		if(Jabatan::where('nama_jabatan','WAKAPOLSEK')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'WAKAPOLSEK']);
		}
		
		// Kapolsek AKP IV/A
		// Wakapolsek IP IV/B
		foreach($POLSEKPRARURAL as $POLSEK){
			$kepalaSatuan = $POLSEK->parent()->first()->jabatan()->leader()->first();
			$KAPOLSEK = SatuanJabatan::createJabatan($POLSEK,$KAPOLDA,'KAPOLSEK',['AKP'],'IV/A',1);
			$WAKAPOLSEK = SatuanJabatan::createJabatan($POLSEK,$KAPOLSEK,'WAKAPOLSEK',['IP'],'IV/B',1);
		}
	}

	public function kepalaSatuanPolSubSektor()
	{
		$POLSEKSUBSEKTOR = JenisSatuan::where('nama_jenis_satuan','POLSEK SUBSEKTOR')->first()->satuan()->get();

		if(Jabatan::where('nama_jabatan','KAPOLSUBSEKTOR')->count() > 0){
			Jabatan::create(['nama_jabatan' => 'KAPOLSUBSEKTOR']);
		}
		
		// Kapolsek AKP IV/A
		// Wakapolsek IP IV/B
		foreach($POLSEKSUBSEKTOR as $POLSEK){
			$kepalaSatuan = $POLSEK->parent()->first()->jabatan()->leader()->first();
			$KAPOLSUBSEKTOR = SatuanJabatan::createJabatan($POLSEK,$KAPOLDA,'KAPOLSUBSEKTOR',['IP'],'IV/B',1);
		}
	}
}
