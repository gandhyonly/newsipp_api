<?php

namespace App\Http\Controllers\Migration\Satuan;

use App\Http\Controllers\Controller;
use App\JenisSatuan;
use App\MigrationStatus;
use App\Satuan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JenisPolresController extends Controller
{

    public function index()
    {
        echo 'insertPOLRESTA';
        $countSkipPolres = $this->insertPolres();
        echo 'insertPOLRESTABES';
        $countSkipPolrestabes = $this->insertPOLRESTABES();
        echo 'insertPOLRESMETRO';
        $countSkipPolresMetro = $this->insertPOLRESMETRO();
        echo 'insertPOLRESTA';
        $countSkipPolresta = $this->insertPOLRESTA();
        $temp = $countSkipPolres.' '.$countSkipPolresta.','.$countSkipPolrestabes.','.$countSkipPolresMetro;
        $migrationStatus = MigrationStatus::find(3);
        $migrationStatus->migration_date=Carbon::now();
        $migrationStatus->save();
        return redirect('migration')->with('success',"Berhasil melakukan migrasi satuan polres.".$temp);
    }
    public function insertPolres()
    {
    	$i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLRES')->first();
        $daftarPolres = $jenisSatuan->satuan()->get();
        foreach($daftarPolres as $POLRES){
            
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLRES->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
            	$i++;
                continue;
            }
            $satuan = Satuan::createSatuan('SIWAS','SI',$POLRES);
            $satuan = Satuan::createSatuan('SIPROPAM','SI',$POLRES);
            $satuan = Satuan::createSatuan('SIKEU','SI',$POLRES);
            $satuan = Satuan::createSatuan('SIUM','SI',$POLRES);

            $bag =  Satuan::createSatuan('BAGOPS','BAG',$POLRES);
                    Satuan::createSatuan('SUBBAG BINOPS','SUBBAG ',$bag);
                    Satuan::createSatuan('SUBBAG DALOPS','SUBBAG ',$bag);
                    Satuan::createSatuan('SUBBAG HUMAS','SUBBAG ',$bag);
            $bag =  Satuan::createSatuan('BAGREN','BAG',$POLRES);
                    Satuan::createSatuan('SUBBAG PROGAR','SUBBAG ',$bag);
                    Satuan::createSatuan('SUBBAG DALGAR','SUBBAG ',$bag);
            $bag =  Satuan::createSatuan('BAGSUMDA','BAG',$POLRES);
                    Satuan::createSatuan('SUBBAG PERS','SUBBAG ',$bag);
                    Satuan::createSatuan('SUBBAG SARPRAS','SUBBAG ',$bag);
                    Satuan::createSatuan('SUBBAG KUM','SUBBAG ',$bag);

            $satuan = Satuan::createSatuan('SENTRA PELAYANAN KEPOLISIAN TERPADU','SENTRA PELAYANAN',$POLRES);
            $satuan = Satuan::createSatuan('SATINTELKAM','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SATRESKRIM','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SATRESNARKOBA','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SATBINMAS','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SATSABHARA','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SATLANTAS','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SATPAMOBVIT','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SATPOLAIR','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SATTAHTI','SAT',$POLRES);
            $satuan = Satuan::createSatuan('SITIPOL','SI',$POLRES);
        }
        return $i;
    }

    public function insertPOLRESTA()
    {
    	$i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLRESTA')->first();
        $daftarPOLRESTA = $jenisSatuan->satuan()->get();
        foreach($daftarPOLRESTA as $POLRESTA){
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLRESTA->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
            	$i++;
                continue;
            }
            $satuan = Satuan::createSatuan('SIWAS','SI',$POLRESTA);
            $satuan = Satuan::createSatuan('SIPROPAM','SI',$POLRESTA);
            $satuan = Satuan::createSatuan('SIKEU','SI',$POLRESTA);
            $satuan = Satuan::createSatuan('SIUM','SI',$POLRESTA);

            $bag = Satuan::createSatuan('BAGOPS','BAG',$POLRESTA);
                Satuan::createSatuan('SUBBAG BINOPS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG DALOPS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG HUMAS','SUBBAG ',$bag);
            $bag = Satuan::createSatuan('BAGREN','BAG',$POLRESTA);
                Satuan::createSatuan('SUBBAG PROGAR','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG DALGAR','SUBBAG ',$bag);
            $bag = Satuan::createSatuan('BAGSUMDA','BAG',$POLRESTA);
                Satuan::createSatuan('SUBBAG PERS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG SARPRAS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG KUM','SUBBAG ',$bag);

            $satuan = Satuan::createSatuan('SENTRA PELAYANAN KEPOLISIAN TERPADU','SENTRA PELAYANAN',$POLRESTA);
            $satuan = Satuan::createSatuan('SATINTELKAM','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SATRESKRIM','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SATRESNARKOBA','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SATBINMAS','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SATSABHARA','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SATLANTAS','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SATPAMOBVIT','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SATPOLAIR','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SATTAHTI','SAT',$POLRESTA);
            $satuan = Satuan::createSatuan('SITIPOL','SI',$POLRESTA);
        }
        return $i;
    }

    public function insertPOLRESTABES()
    {
    	$i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLRESTABES')->first();
        $daftarPOLRESTABES = $jenisSatuan->satuan()->get();
        foreach($daftarPOLRESTABES as $POLRESTABES){
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLRESTABES->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
            	$i++;
                continue;
            }
            $satuan = Satuan::createSatuan('SIWAS','SI',$POLRESTABES);
            $satuan = Satuan::createSatuan('SIPROPAM','SI',$POLRESTABES);
            $satuan = Satuan::createSatuan('SIKEU','SI',$POLRESTABES);
            $satuan = Satuan::createSatuan('SIUM','SI',$POLRESTABES);

            $bag = Satuan::createSatuan('BAGOPS','BAG',$POLRESTABES);
                Satuan::createSatuan('SUBBAG BINOPS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG DALOPS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG HUMAS','SUBBAG ',$bag);
            $bag = Satuan::createSatuan('BAGREN','BAG',$POLRESTABES);
                Satuan::createSatuan('SUBBAG PROGAR','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG DALGAR','SUBBAG ',$bag);
            $bag = Satuan::createSatuan('BAGSUMDA','BAG',$POLRESTABES);
                Satuan::createSatuan('SUBBAG PERS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG SARPRAS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG KUM','SUBBAG ',$bag);

            $satuan = Satuan::createSatuan('SENTRA PELAYANAN KEPOLISIAN TERPADU','SENTRA PELAYANAN',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATINTELKAM','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATRESKRIM','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATRESNARKOBA','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATBINMAS','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATSABHARA','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATLANTAS','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATPAMOBVIT','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATPOLAIR','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SATTAHTI','SAT',$POLRESTABES);
            $satuan = Satuan::createSatuan('SITIPOL','SI',$POLRESTABES);
        }
        return $i;
    }

    public function insertPOLRESMETRO()
    {
    	$i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLRESMETRO')->first();
        $daftarPOLRESMETRO = $jenisSatuan->satuan()->get();
        foreach($daftarPOLRESMETRO as $POLRESMETRO){
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLRESMETRO->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
            	$i++;
                continue;
            }
            $satuan = Satuan::createSatuan('SIWAS','SI',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SIPROPAM','SI',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SIKEU','SI',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SIUM','SI',$POLRESMETRO);

            $bag = Satuan::createSatuan('BAGOPS','BAG',$POLRESMETRO);
                Satuan::createSatuan('SUBBAG BINOPS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG DALOPS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG HUMAS','SUBBAG ',$bag);
            $bag = Satuan::createSatuan('BAGREN','BAG',$POLRESMETRO);
                Satuan::createSatuan('SUBBAG PROGAR','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG DALGAR','SUBBAG ',$bag);
            $bag = Satuan::createSatuan('BAGSUMDA','BAG',$POLRESMETRO);
                Satuan::createSatuan('SUBBAG PERS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG SARPRAS','SUBBAG ',$bag);
                Satuan::createSatuan('SUBBAG KUM','SUBBAG ',$bag);

            $satuan = Satuan::createSatuan('SENTRA PELAYANAN KEPOLISIAN TERPADU','SENTRA PELAYANAN',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATINTELKAM','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATRESKRIM','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATRESNARKOBA','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATBINMAS','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATSABHARA','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATLANTAS','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATPAMOBVIT','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATPOLAIR','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SATTAHTI','SAT',$POLRESMETRO);
            $satuan = Satuan::createSatuan('SITIPOL','SI',$POLRESMETRO);
        }
        return $i;
    }
}
