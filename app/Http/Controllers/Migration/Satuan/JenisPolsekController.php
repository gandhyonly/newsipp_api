<?php

namespace App\Http\Controllers\Migration\Satuan;

use App\Http\Controllers\Controller;
use App\JenisSatuan;
use App\MigrationStatus;
use App\Satuan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class JenisPolsekController extends Controller
{
    public function index()
    {
        echo 'insertPOLSEKURBAN';
        $countSkipPolsekUrban = $this->insertPOLSEKURBAN();
        echo 'insertPOLSEKRURAL';
        $countSkipPolsekRural = $this->insertPOLSEKRURAL();
        echo 'insertPOLSEKPRARURAL';
        $countSkipPolsekPrarural = $this->insertPOLSEKPRARURAL();
        echo 'insertPOLSEKMETRO';
        $countSkipPolsekMetro = $this->insertPOLSEKMETRO();
        echo 'insertPOLSUBSEKTOR';
        $countSkipPolsekSubSektor = $this->insertPOLSUBSEKTOR();

        $temp = $countSkipPolsekUrban.','.$countSkipPolsekRural.','.$countSkipPolsekPrarural.','.$countSkipPolsekMetro.','.$countSkipPolsekSubSektor;
        $migrationStatus = MigrationStatus::find(2);
        $migrationStatus->migration_date=Carbon::now();
        $migrationStatus->save();
        
        return redirect('migration')->with('success',"Berhasil melakukan migrasi satuan polsek ".$temp);
    }
    public function insertPOLSUBSEKTOR()
    {
        $i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLSEK SUBSEKTOR')->first();
        $daftarPOLSEKSUBSEKTOR = $jenisSatuan->satuan()->get();
        foreach($daftarPOLSEKSUBSEKTOR as $POLSEKSUBSEKTOR){
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLSEKSUBSEKTOR->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
                $i++;
                continue;
            }

            $satuan = Satuan::createSatuan('URMIN','UR',$POLSEKSUBSEKTOR);
            
            $satuan = Satuan::createSatuan('UNIT PATROLI','UNIT',$POLSEKSUBSEKTOR);
            
            $satuan = Satuan::createSatuan('UNIT YANMAS','UNIT',$POLSEKSUBSEKTOR);
        }
        return $i;
    }

    public function insertPOLSEKPRARURAL()
    {
        $i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLSEK PRARURAL')->first();
        $daftarPOLSEKPRARURAL = $jenisSatuan->satuan()->get();
        foreach($daftarPOLSEKPRARURAL as $POLSEKPRARURAL){
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLSEKPRARURAL->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
                $i++;
                continue;
            }
            $satuan = Satuan::createSatuan('UNIT PROVOS','UNIT',$POLSEKPRARURAL);
            $bag = Satuan::createSatuan('SIUM','SI',$POLSEKPRARURAL);
                Satuan::createSatuan('URRENMIN','UR',$bag);
                Satuan::createSatuan('URTAUD','UR',$bag);
                Satuan::createSatuan('URTAHTI','UR',$bag);
            $satuan = Satuan::createSatuan('SENTRA PELAYANAN KEPOLISIAN TERPADU','SENTRA PELAYANAN',$POLSEKPRARURAL);
            $satuan = Satuan::createSatuan('UNIT INTELKAM','UNIT',$POLSEKPRARURAL);
            $satuan = Satuan::createSatuan('UNIT RESKRIM','UNIT',$POLSEKPRARURAL);
            $satuan = Satuan::createSatuan('UNIT BINMAS','UNIT',$POLSEKPRARURAL);
            $satuan = Satuan::createSatuan('UNIT SABHARA','UNIT',$POLSEKPRARURAL);
            $satuan = Satuan::createSatuan('UNIT LANTAS','UNIT',$POLSEKPRARURAL);
            $satuan = Satuan::createSatuan('UNIT POLAIR','UNIT',$POLSEKPRARURAL);
        }
        return $i;
    }
    public function insertPOLSEKRURAL()
    {
        $i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLSEK RURAL')->first();
        $daftarPOLSEKRURAL = $jenisSatuan->satuan()->get();
        foreach($daftarPOLSEKRURAL as $POLSEKRURAL){
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLSEKRURAL->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
                $i++;
                continue;
            }
            $satuan = Satuan::createSatuan('UNIT PROVOS','UNIT',$POLSEKRURAL);
            $bag = Satuan::createSatuan('SIUM','SI',$POLSEKRURAL);
                Satuan::createSatuan('URRENMIN','UR',$bag);
                Satuan::createSatuan('URTAUD','UR',$bag);
                Satuan::createSatuan('URTAHTI','UR',$bag);
            $satuan = Satuan::createSatuan('SIHUMAS','SI',$POLSEKRURAL);
            $satuan = Satuan::createSatuan('SENTRA PELAYANAN KEPOLISIAN TERPADU','SENTRA PELAYANAN',$POLSEKRURAL);
            $satuan = Satuan::createSatuan('UNIT INTELKAM','UNIT',$POLSEKRURAL);
            $satuan = Satuan::createSatuan('UNIT RESKRIM','UNIT',$POLSEKRURAL);
            $satuan = Satuan::createSatuan('UNIT BINMAS','UNIT',$POLSEKRURAL);
            $satuan = Satuan::createSatuan('UNIT SABHARA','UNIT',$POLSEKRURAL);
            $satuan = Satuan::createSatuan('UNIT LANTAS','UNIT',$POLSEKRURAL);
            $satuan = Satuan::createSatuan('UNIT POLAIR','UNIT',$POLSEKRURAL);
        }
        return $i;
    }
    public function insertPOLSEKMETRO()
    {
        $i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLSEK METRO')->first();
        $daftarPOLSEKMETRO = $jenisSatuan->satuan()->get();
        foreach($daftarPOLSEKMETRO as $POLSEKMETRO){
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLSEKMETRO->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
                $i++;
                continue;
            }
            $satuan = Satuan::createSatuan('UNIT PROVOS','UNIT',$POLSEKMETRO);
            $bag = Satuan::createSatuan('SIUM','SI',$POLSEKMETRO);
                Satuan::createSatuan('URRENMIN','UR',$bag);
                Satuan::createSatuan('URTAUD','UR',$bag);
                Satuan::createSatuan('URTAHTI','UR',$bag);
            $satuan = Satuan::createSatuan('SIKUM','SI',$POLSEKMETRO);
            $satuan = Satuan::createSatuan('SIHUMAS','SI',$POLSEKMETRO);
            $satuan = Satuan::createSatuan('SENTRA PELAYANAN KEPOLISIAN TERPADU','SENTRA PELAYANAN',$POLSEKMETRO);
            $satuan = Satuan::createSatuan('UNIT INTELKAM','UNIT',$POLSEKMETRO);
            $satuan = Satuan::createSatuan('UNIT RESKRIM','UNIT',$POLSEKMETRO);
            $satuan = Satuan::createSatuan('UNIT BINMAS','UNIT',$POLSEKMETRO);
            $satuan = Satuan::createSatuan('UNIT SABHARA','UNIT',$POLSEKMETRO);
            $satuan = Satuan::createSatuan('UNIT LANTAS','UNIT',$POLSEKMETRO);
            $satuan = Satuan::createSatuan('UNIT POLAIR','UNIT',$POLSEKMETRO);
        }
        return $i;
    }
    public function insertPOLSEKURBAN()
    {
        $i=0;
        $jenisSatuan = JenisSatuan::where('nama_jenis_satuan','POLSEK URBAN')->first();
        $daftarPOLSEKURBAN = $jenisSatuan->satuan()->get();
        foreach($daftarPOLSEKURBAN as $POLSEKURBAN){
            $jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray();
            if($POLSEKURBAN->childs()->whereIn('jenis_satuan_id',$jenisSatker)->count()>0){
                $i++;
                continue;
            }
            $satuan = Satuan::createSatuan('UNIT PROVOS','UNIT',$POLSEKURBAN);
            $bag = Satuan::createSatuan('SIUM','SI',$POLSEKURBAN);
                Satuan::createSatuan('URRENMIN','UR',$bag);
                Satuan::createSatuan('URTAUD','UR',$bag);
                Satuan::createSatuan('URTAHTI','UR',$bag);
            $satuan = Satuan::createSatuan('SIKUM','SI',$POLSEKURBAN);
            $satuan = Satuan::createSatuan('SIHUMAS','SI',$POLSEKURBAN);
            $satuan = Satuan::createSatuan('SENTRA PELAYANAN KEPOLISIAN TERPADU','SENTRA PELAYANAN',$POLSEKURBAN);
            $satuan = Satuan::createSatuan('UNIT INTELKAM','UNIT',$POLSEKURBAN);
            $satuan = Satuan::createSatuan('UNIT RESKRIM','UNIT',$POLSEKURBAN);
            $satuan = Satuan::createSatuan('UNIT BINMAS','UNIT',$POLSEKURBAN);
            $satuan = Satuan::createSatuan('UNIT SABHARA','UNIT',$POLSEKURBAN);
            $satuan = Satuan::createSatuan('UNIT LANTAS','UNIT',$POLSEKURBAN);
            $satuan = Satuan::createSatuan('UNIT POLAIR','UNIT',$POLSEKURBAN);
        }
        return $i;
    }
}
