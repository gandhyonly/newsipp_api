<?php

namespace App\Http\Controllers\Migration\Satuan;

use App\Http\Controllers\Controller;
use App\JenisSatuan;
use App\MigrationStatus;
use App\Satuan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ResetPolresPolsekController extends Controller
{
	public function index()
	{
		echo 'RESET POLSEK';
		$this->POLSEK();
		echo 'RESET POLRES';
		$this->POLRES();

        $migrationStatus = MigrationStatus::find(2);
        $migrationStatus->migration_date=null;
        $migrationStatus->save();
        $migrationStatus = MigrationStatus::find(3);
        $migrationStatus->migration_date=null;
        $migrationStatus->save();

        return redirect('migration')->with('success',"Berhasil melakukan reset satuan polres dan polsek.");
	}
	public function POLSEK()
	{
		$jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray()
		;
		$jenisPolsek = JenisSatuan::where('nama_jenis_satuan','LIKE','POLSEK%')->pluck('jenis_satuan_id')->toArray();

		$satuanPolsek = Satuan::whereIn('jenis_satuan_id',$jenisPolsek)->get();

		foreach($satuanPolsek as $polsek){
			$polsek->childs()->whereIn('jenis_satuan_id',$jenisSatker)->delete();
		}
	}
	public function POLRES()
	{
		$jenisSatker = JenisSatuan::where('kategori','SATUAN KERJA')->pluck('jenis_satuan_id')->toArray()
		;
		$jenisPolres = JenisSatuan::where('nama_jenis_satuan','LIKE','POLRES%')->pluck('jenis_satuan_id')->toArray();

		$satuanPolres = Satuan::whereIn('jenis_satuan_id',$jenisPolres)->get();

		foreach($satuanPolres as $polres){
			$polres->childs()->whereIn('jenis_satuan_id',$jenisSatker)->delete();
		}
	}
}
