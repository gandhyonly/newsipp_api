<?php

namespace App\Http\Controllers\Migration;

use App\Http\Controllers\Controller;
use App\PendidikanPersonel;
use App\Personel;
use App\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MigrasiSatuanJabatanController extends Controller
{
	public function migrasiSatuan()
	{
        $tempNrp='0a';

        $query = DB::connection('newsipp_mabes')->table('satuan');
        foreach($query->get() as $satuan){
        	Satuan::create([
        		'satuan_id' => $satuan->satuan_id,
        		'nama_satuan' => $satuan->nama_satuan,
        		'tipe' => $satuan->tipe,
        		'alamat' => $satuan->alamat,
        		'telepon' => $satuan->telepon,
        		'jenis_satuan_id' => $satuan->jenis_satuan_id,
        		'posisi' => $satuan->satuan_id
        	]);
        }
	}
}
