<?php

namespace App\Http\Controllers\Migration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CorrectionController extends Controller
{
	public function index()
	{
		if(Satuan::where('nama_satuan','LIKE','UR %')->count()>0){
			foreach(Satuan::where('nama_satuan','LIKE','UR %')->get() as $UR){
				$name = str_replace(' ','',$UR->nama_satuan);
				$UR->nama_satuan=$name;
				$UR->save();
			}
		}
		if(Satuan::where('nama_satuan','LIKE','BAG %')->count()>0){
			foreach(Satuan::where('nama_satuan','LIKE','BAG %')->get() as $BAG){
				$name = str_replace(' ','',$BAG->nama_satuan);
				$BAG->nama_satuan=$name;
				$BAG->save();
			}
		}
		if(Satuan::where('nama_satuan','LIKE','SUBBAG %')->count()>0){
			foreach(Satuan::where('nama_satuan','LIKE','SUBBAG %')->get() as $SUBBAG){
				$name = str_replace(' ','',$SUBBAG->nama_satuan);
				$SUBBAG->nama_satuan=$name;
				$SUBBAG->save();
			}
		}
	}
}
