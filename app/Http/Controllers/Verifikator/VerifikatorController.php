<?php

namespace App\Http\Controllers\Verifikator;

use App\Http\Controllers\Controller;
use App\Member;
use App\PesanMember;
use App\Satuan;
use App\StatusForm;
use App\VerifikatorUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Hash;
use DB;

class VerifikatorController extends Controller
{
	public function login(Request $request)
	{
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'password.required'    => 'Kolom kata kunci tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' => 'required',
			'password' => 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    if(auth('verifikator')->attempt(['nrp'=>strtolower($request->nrp),'password' => $request->password,'aktif' => true])){
			$verifikator = VerifikatorUser::find(auth('verifikator')->user()->verifikator_user_id);
			$verifikator->satuan;
			return response()->json([
				'token' => auth('verifikator')->user()->api_token,
				'nrp' => $verifikator->nrp,
				'verifikator' => $verifikator
			], 200);
	    } else{
			return response()->json([
				'message' => 'NRP atau Password anda salah.',
				'status_code' => 400
			],400);
	    }
	}

	public function tambahVerifikator(Request $request)
	{
		$verifikator = auth()->user();
		if($verifikator->tingkat==1){
			return response()->json([
				'message' => 'Unauthenticated.',
				'status_code' => 402
			],402);
		} else {

		    $error_message = [
				'satuan_id.required' 	=> 'Kolom satuan_id tidak boleh dikosongkan.',
				'nama.required' 				=> 'Kolom nama tidak boleh dikosongkan.',
				'nrp.required' 				=> 'Kolom nrp tidak boleh dikosongkan.',
				'nrp.unique' 				=> 'NRP telah terdaftar sebagai verifikator.',
				'password.required' 				=> 'Kolom password tidak boleh dikosongkan.',
				'password.confirmed' 				=> 'Kolom password dan konfirmasi password tidak sama.',
			];

			$validation = Validator::make($request->all(),[
				'nrp' 		=> 'required|unique:verifikator_user',
				'nama' 		=> 'required',
				'satuan_id' 		=> 'required',
				'password' 		=> 'required|confirmed',
		    ],$error_message);


		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				], 400);
		    }

		    $satuan = Satuan::find($request->satuan_id);
		    if($satuan== null){
				return response()->json([
					'errors' => 'Satuan Tidak Ditemukan',
					'status_code' => 400
				], 400);
		    }

		    $verifikatorBaru = VerifikatorUser::create([
		    	'nama' => $request->nama,
		    	'nrp' => strtolower($request->nrp),
		    	'satuan_id' => $satuan->satuan_id,
		    	'tingkat' => $verifikator->tingkat-1,
		    	'password' => bcrypt($request->password),
		    	'api_token' => str_random(60),
		    	'dibuat_oleh' => $verifikator->verifikator_user_id
		    ]);

			return response()->json([
				'message' => "Verifikator dengan nama $verifikatorBaru->nama (NRP $verifikatorBaru->nrp) tingkat $verifikatorBaru->tingkat berhasil terdaftar",
				'status_code' => 200
			], 200);

		}
		
	}

	public function verifikasi(Request $request)
	{
		$verifikator = auth()->user();
	    $error_message = [
			'status_form_id.required' 		=> 'Kolom status form tidak boleh dikosongkan.',
			'nrp.required' 		=> 'Kolom nrp tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' 		=> 'required',
			'status_form_id' 		=> 'required',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    $member = Member::where('nrp',$request->nrp)->first();
	    if($member == null){
			return response()->json([
				'errors' => 'Nrp tidak ditemukan',
				'status_code' => 400
			], 400);
	    }
	    $statusForm = StatusForm::find($request->status_form_id);
	    if($statusForm == null){
			return response()->json([
				'errors' => 'Status Form tidak ditemukan',
				'status_code' => 400
			], 400);
	    }
	    DB::beginTransaction();
	    $tingkatVerifikator=$verifikator->tingkat;
		$member->statusForm()->syncWithoutDetaching([$statusForm->status_form_id => ["verifikator_$tingkatVerifikator" => $verifikator->verifikator_user_id,"keterangan" => null]]);
		if($member->statusForm()->wherePivot("verifikator_$tingkatVerifikator","!=",null)->count()==StatusForm::count()){
			$member->tingkat=$verifikator->tingkat+1;
			$member->save();
		}
		PesanMember::create([
			'verifikator_user_id' => $verifikator->verifikator_user_id,
			'member_id' => $member->member_id,
			'judul' => $statusForm->name,
			'pesan' => 'Data '.$statusForm->name.' telah diverifikasi.',
			'tingkat' => $verifikator->tingkat,
			'bermasalah' => false,
			'tanggal_dibuat' => Carbon::now(),
		]);
		DB::commit();

		return response()->json([
			'message' => 'Berhasil melakukan verifikasi dengan pesan: '.$request->keterangan,
			'status_code' => 200
		],200);
	}

	public function tolak(Request $request)
	{
		$verifikator = auth()->user();
	    $error_message = [
			'status_form_id.required' 		=> 'Kolom status form tidak boleh dikosongkan.',
			'nrp.required' 		=> 'Kolom nrp tidak boleh dikosongkan.',
			'keterangan.required' 		=> 'Kolom keterangan tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'keterangan' 		=> 'required',
			'nrp' 		=> 'required',
			'status_form_id' 		=> 'required',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    $member = Member::where('nrp',$request->nrp)->first();
	    if($member == null){
			return response()->json([
				'errors' => 'Nrp tidak ditemukan',
				'status_code' => 400
			], 400);
	    }
	    $statusForm = StatusForm::find($request->status_form_id);
	    if($statusForm == null){
			return response()->json([
				'errors' => 'Status Form tidak ditemukan',
				'status_code' => 400
			], 400);
	    }
	    DB::beginTransaction();
	    $member->updated=false;
		$member->statusForm()->syncWithoutDetaching([$statusForm->status_form_id => ['verifikator_1' => null, 'verifikator_2' => null, 'verifikator_3' => null, 'verifikator_4' => null,'keterangan' => $request->keterangan, 'tanggal_perubahan' => Carbon::now()]]);
		$member->tingkat=1;
		$member->save();

		PesanMember::create([
			'verifikator_user_id' => $verifikator->verifikator_user_id,
			'member_id' => $member->member_id,
			'judul' => $statusForm->name,
			'pesan' => 'Terdapat beberapa masalah pada data '.$statusForm->name.'. Harap melakukan perbaikan.<br>Masalah:<br>'.$request->keterangan,
			'tingkat' => $verifikator->tingkat,
			'bermasalah' => true,
			'tanggal_dibuat' => Carbon::now(),
		]);
		DB::commit();
		return response()->json([
			'message' => 'Berhasil melakukan penolakan pada form '.$statusForm->name.' dengan pesan: '.$request->keterangan,
			'status_code' => 200
		],200);
	}
	public function statusVerifikator(VerifikatorUser $verifikatorUser)
	{
		$verifikatorUser->aktif=!$verifikatorUser->aktif;
		$verifikatorUser->save();
		return response()->json([
			'message' => 'Status Verifikator berhasil diubah',
			'status_code' => 200
		],200);
	}

	public function daftarVerifikator()
	{
		return datatables(VerifikatorUser::with('satuan')->createdByMe()->latest()->get())->toJson();
		
	}
	public function hapusVerifikator(VerifikatorUser $verifikatorUser)
	{
		$verifikatorUser->aktif=false;
		if($verifikatorUser->save()){
			return response()->json([
				'message' => 'Verifikator berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus verifikator.',
				'status_code' => 400
			],400);
		}
	}
	public function aktifkanVerifikator(VerifikatorUser $verifikatorUser)
	{
		$verifikatorUser->aktif=true;
		if($verifikatorUser->save()){
			return response()->json([
				'message' => 'Verifikator berhasil diaktifkan.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal mengaktifkan verifikator.',
				'status_code' => 400
			],400);
		}
	}
	public function resetPasswordVerifikator(VerifikatorUser $verifikatorUser)
	{
		$verifikatorUser->password=bcrypt('verifikatorsipp');
		if($verifikatorUser->save()){
			return response()->json([
				'message' => 'Verifikator berhasil direset menjadi password: verifikatorsipp',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal mereset password verifikator.',
				'status_code' => 400
			],400);
		}
	}
	public function gantiPassword(Request $request)
	{
		$verifikatorID = auth('api-verifikator')->user()->verifikator_user_id;
		$verifikator = VerifikatorUser::find($verifikatorID);
		if($verifikator==null){
			return response()->json([
				'errors' => 'Verifikator Tidak Ditemukan.',
				'status_code' => 400
			], 400);
		}
	    $error_message = [
		    'old_password.required'    => 'Kolom kata kunci lama tidak boleh dikosongkan.',
		    'password.required'    => 'Kolom kata kunci tidak boleh dikosongkan.',
		    'password_confirmation.required'    => 'Kolom konfirmasi kata kunci  tidak boleh dikosongkan.',
		    'password_confirmation.same'    => 'Konfirmasi password lama anda tidak sama.',
		];

		$validation = Validator::make($request->all(),[
	        'old_password' => 'required',
	        'password' => 'required',
	        'password_confirmation' => 'required|same:password',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    if(Hash::check($request->old_password, $verifikator->password)){
			$verifikator->password=bcrypt($request->password);
			$verifikator->save();
	        return response()->json([
	            'data' => [ 'message' => 'Ganti password berhasil.' ]
	        ],200);		
	    }else {
	        return response()->json([
	            'data' => [ 'message' => 'Password anda salah.' ]
	        ],400);		
	    }
	}
}
