<?php

namespace App\Http\Controllers\Verifikator;

use App\BKO;
use App\Bahasa;
use App\BahasaPersonel;
use App\HobiPersonel;
use App\Http\Controllers\Controller;
use App\KeluargaPersonel;
use App\KesehatanPersonel;
use App\Member;
use App\OlahragaPersonel;
use App\PendidikanPersonel;
use App\PenghargaanPersonel;
use App\Penugasan;
use App\RiwayatBrevet;
use App\RiwayatJabatan;
use App\RiwayatPangkat;
use App\RiwayatPsikologi;
use App\SIMPersonel;
use App\Satuan;
use App\TandaKehormatanPersonel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonelController extends Controller
{
	public function countPersonel()
    {
		$verifikator = auth()->user();
		$tingkat= $verifikator->tingkat;
		DB::disableQueryLog();
		$countPersonel=Member::with('personel')->where('updated',1)->where("satuan_$tingkat",$verifikator->satuan_id)->where('tingkat','>=',$tingkat)->count();
		if($tingkat==1){
			$countPersonelPerSatker = Member::where("satuan_1",$verifikator->satuan_id)->latest()->count();
			$countPersonelVerified = Member::where('updated',1)->where("satuan_1",$verifikator->satuan_id)->where('tingkat','>',$tingkat)->latest()->count();
		} else if($tingkat==2){
			$countPersonelPerSatker = Member::where("satuan_2",$verifikator->satuan_id)->latest()->count();
			$countPersonelVerified = Member::where('updated',1)->where("satuan_2",$verifikator->satuan_id)->where('tingkat','>',$tingkat)->latest()->count();
		} else if($tingkat==3){
			$countPersonelPerSatker = Member::where("satuan_3",$verifikator->satuan_id)->latest()->count();
			$countPersonelVerified = Member::where('updated',1)->where("satuan_3",$verifikator->satuan_id)->where('tingkat','>',$tingkat)->latest()->count();
		} else if($tingkat==4){
			$countPersonelPerSatker = Member::where("satuan_4",$verifikator->satuan_id)->latest()->count();
			$countPersonelVerified = Member::where('updated',1)->where("satuan_4",$verifikator->satuan_id)->where('tingkat','>',$tingkat)->latest()->count();
		}
		$countPersonelNotVerified = $countPersonel-$countPersonelVerified;
        return response()->json([
            'data' => [ 
            	'jumlahPersonel' => $countPersonel,
            	'jumlahPersonelVerified' => $countPersonelVerified,
            	'jumlahPersonelPerSatker' => $countPersonelPerSatker,
            	'jumlahPersonelNotVerified' => $countPersonelNotVerified,
            	'data-input' => [
	            					'satuan_id' =>$verifikator->satuan_id,
	            					'tingkat' =>$tingkat
            					]
            ],
        ],200);
    }
    

	public function daftarPersonel()
	{
		DB::disableQueryLog();
		$verifikator = auth()->user();
		$tingkat=$verifikator->tingkat;
		if($tingkat>=1 && $tingkat <=4) {
			$member = DB::table('members')->join('personel','personel.personel_id','=','members.personel_id')->select('personel.nama_lengkap as personel.nama_lengkap','tingkat','members.nrp','satker_sekarang','members.updated_at','member_id','members.personel_id')->where('updated',1)->where("satuan_$tingkat",$verifikator->satuan_id)->where('tingkat','>=',$tingkat);
		}

		return datatables()->of($member)->toJson();
	}
	public function daftarPersonelFilter($status)
	{
		$verifikator = auth()->user();
		$tingkat=$verifikator->tingkat;
		DB::disableQueryLog();
		if($tingkat>=1 && $tingkat <=4) {
			if($status==1){
				$member = DB::table('members')->join('personel','personel.personel_id','=','members.personel_id')->select('personel.nama_lengkap as personel.nama_lengkap','tingkat','members.nrp','satker_sekarang','members.updated_at','member_id','members.personel_id')->where('updated',1)->where("satuan_$tingkat",$verifikator->satuan_id)->where('tingkat','>=',$tingkat);
			}
			else if($status==2){
				$member = DB::table('members')->join('personel','personel.personel_id','=','members.personel_id')->select('personel.nama_lengkap as personel.nama_lengkap','tingkat','members.nrp','satker_sekarang','members.updated_at','member_id','members.personel_id')->where("satuan_$tingkat",$verifikator->satuan_id);
			}
			else if($status==3){
				$member = DB::table('members')->join('personel','personel.personel_id','=','members.personel_id')->select('personel.nama_lengkap as personel.nama_lengkap','tingkat','members.nrp','satker_sekarang','members.updated_at','member_id','members.personel_id')->where('updated',1)->where("satuan_$tingkat",$verifikator->satuan_id)->where('tingkat','>',$tingkat);
			}
			else if($status==4){
				$member = DB::table('members')->join('personel','personel.personel_id','=','members.personel_id')->select('personel.nama_lengkap as personel.nama_lengkap','tingkat','members.nrp','satker_sekarang','members.updated_at','member_id','members.personel_id')->where('updated',1)->where("satuan_$tingkat",$verifikator->satuan_id)->where('tingkat','=',$tingkat);
			}
		}
		return datatables()->of($member)->toJson();
	}
	public function detailPersonel(Member $member)
	{
		DB::disableQueryLog();
		$verifikator = auth()->user();
		$personelID = $member->personel_id;
		$member->personel->tempatLahir;
		$member->personel->statusForm = $member->statusForm;
		$member->personel->data;
		$member->personel->statusPersonel;
		$member->personel->fisik;
		$member->personel->keluarga=KeluargaPersonel::with('pekerjaanKeluarga.jenisPekerjaan')->with('hubunganKeluarga')->with('golongan')->where('personel_id',$personelID)->get();
		$member->personel->hobi=HobiPersonel::with('hobi')->where('personel_id',$personelID)->get();
		$member->personel->olahraga=OlahragaPersonel::with('olahraga')->where('personel_id',$personelID)->get();;

		$bahasaDaerahIDs = Bahasa::daerah()->pluck('bahasa_id');
		$bahasaInternationalIDs = Bahasa::international()->pluck('bahasa_id');
		$member->personel->bahasa_daerah = BahasaPersonel::with('bahasa')->whereIn('bahasa_id',$bahasaDaerahIDs)->where('personel_id',$personelID)->get();
		$member->personel->bahasa_international = BahasaPersonel::with('bahasa')->whereIn('bahasa_id',$bahasaInternationalIDs)->where('personel_id',$personelID)->get();
		$member->personel->sim=SIMPersonel::with('sim')->where('personel_id',$personelID)->get();
		$member->personel->penghargaan=PenghargaanPersonel::with('tingkatPenghargaan')->with('penghargaan')->where('personel_id',$personelID)->get();
		$member->personel->tanda_kehormatan=TandaKehormatanPersonel::with('tandaKehormatan')->where('personel_id',$personelID)->get();
		// $member->personel->kesehatan=KesehatanPersonel::with('kesehatan')->where('personel_id',$personelID)->get();
		$member->personel->riwayat_pangkat=RiwayatPangkat::with('pangkat')->where('personel_id',$personelID)->orderBy('tmt_pangkat')->get();
		$member->personel->alamat;
		$member->personel->riwayat_brevet=RiwayatBrevet::with('brevet')->where('personel_id',$personelID)->get();
		$member->personel->bko=BKO::with('jenisBko')->with('satuanAsal')->with('satuanPenugasan')->where('personel_id',$personelID)->get();
		$member->personel->pendidikan_umum=PendidikanPersonel::with('dikum')->with('konsentrasi.jurusan')->with('gelar.jurusan')->whereNotNull('dikum_id')->where('personel_id',$personelID)->orderBy('dikum_id')->get();
		$member->personel->pendidikan_polisi=PendidikanPersonel::with('dikpol.jenisDikpol')->with('gelar.jurusan')->whereNotNull('dikpol_id')->where('personel_id',$personelID)->get();
		$member->personel->dikbangspes=PendidikanPersonel::with('dikbangspes.tipeDikbangspes')->whereNotNull('dikbangspes_id')->where('personel_id',$personelID)->get();
		$member->personel->penugasan_negara=Penugasan::with('jabatanPenugasan.misi.negara')->where('personel_id',$personelID)->where('jenis_penugasan_id',1)->get();
		$member->personel->penugasan_luar_struktur=Penugasan::with('jabatanPenugasan.lokasiPenugasan.negara')->where('personel_id',$personelID)->where('jenis_penugasan_id',2)->get();
		$member->personel->riwayat_jabatan=RiwayatJabatan::with('satuanJabatan.nivelering')->where('personel_id',$personelID)->orderBy('tmt_jabatan')->get();

		$count=$member->statusForm()->wherePivot("verifikator_$verifikator->tingkat","!=",null)->count();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => Satuan::find($member->satuan_1)->nama_satuan.' > '.Satuan::find($member->satuan_2)->nama_satuan.' > '.Satuan::find($member->satuan_3)->nama_satuan,
			'member' => $member,
			'count' => $count
		], 200);
	}
}
