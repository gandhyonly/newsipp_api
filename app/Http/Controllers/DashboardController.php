<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\JenisSatuan;
use App\Personel;
use App\Satuan;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
	public function index()
	{
		$personelCount = Personel::all()->count();
		$jabatanCount = Jabatan::all()->count();
		$jenisSatuanCount = JenisSatuan::all()->count();
		$satuanCount = Satuan::all()->count();
		return view('dashboard', compact('personelCount','satuanCount','jenisSatuanCount','jabatanCount'));
	}
}
