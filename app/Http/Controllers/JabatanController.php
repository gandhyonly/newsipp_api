<?php

namespace App\Http\Controllers;

use App\Jabatan;
use Auth;
use Illuminate\Http\Request;

class JabatanController extends Controller
{
	public function index()
	{
		$jumlahJabatanUnique = Jabatan::active()->distinct()->count();
		$jumlahJabatan = Jabatan::active()->count();
		$dataJabatan = Jabatan::active()->orderBy('nama_jabatan')->paginate(15);
		return view('jabatan.index')
				->with('jumlahJabatanUnique',$jumlahJabatanUnique)
				->with('jumlahJabatan',$jumlahJabatan)
				->with('dataJabatan',$dataJabatan);
	}
	public function filter(Request $request)
	{
		$jumlahJabatanUnique = Jabatan::active()->distinct()->count();
		$jumlahJabatan = Jabatan::active()->count();
		$dataJabatan = Jabatan::active()->where('nama_jabatan','LIKE','%'.$request->nama_jabatan.'%')->paginate(15)->appends('nama_jabatan',$request->nama_jabatan);
		return view('jabatan.filter')
				->with('jumlahJabatanUnique',$jumlahJabatanUnique)
				->with('jumlahJabatan',$jumlahJabatan)
				->with('request',$request)
				->with('dataJabatan',$dataJabatan);
	}
	public function create()
	{
		return view('jabatan.create');
	}
	public function store(Request $request)
	{
		$jabatan = Jabatan::create($request->except(['_token']));
		if($jabatan){
			return redirect()->route('jabatan.create')->with('success','Berhasil membuat jabatan baru.');
		} else {
			return redirect()->route('jabatan.create')->with('error','Gagal membuat jabatan baru.');
		}
	}
	public function destroy(Jabatan $jabatan)
	{
		$jabatan->deleted_by=auth('user')->user()->id;
		if($jabatan->save())
			return redirect()->back()->with('success','Hapus jabatan berhasil dilakukan.');
		else 
			return redirect()->back()->with('error','Hapus jabatan gagal dilakukan.');
	}
}
