<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
	public function getLogin()
	{
		return view('login');
	}
	public function postLogin(Request $request)
	{
		if(Auth::attempt($request->except('_token'))){
			return redirect()->route('satuan.index');
		}
		return redirect()->route('login')->with('error','Username atau Password anda salah.');
	}
	public function logout()
	{
		Auth::logout();

		return redirect()->route('login')->with('success','Logout Berhasil.');
	}
}
