<?php

namespace App\Http\Controllers\Helpdesk;

use App\Fisik;
use App\Helpdesk;
use App\Http\Controllers\Controller;
use App\Personel;
use App\PersonelData;
use App\Satuan;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Mail;

class HelpdeskController extends Controller
{
    public function countAll()
    {
        $countAllHelpdesk = Helpdesk::count();
        $countHelpdeskRight = Helpdesk::where('status', 'VERIFIED')->count();
        $countHelpdeskNotFound = Helpdesk::where('status', 'NOTFOUND')->count();
        $countHelpdeskDouble = Helpdesk::where('status', 'DUPLICATE')->count();

        return response()->json([
            'all' => $countAllHelpdesk,
            'right' => $countHelpdeskRight,
            'duplicate' => $countHelpdeskDouble,
            'notfound' => $countHelpdeskNotFound
        ], 200);
    }
    public function getData(Request $request)
    {
        if (strtolower($request->status=="all") || $request->status==null) {
            $helpdesk = Helpdesk::with('pangkat')->with('verifikator')->with('satuan_sekarang');
        } elseif (strtolower($request->status)=="problem") {
            $helpdesk = Helpdesk::with('pangkat')->with('verifikator')->with('satuan_sekarang')->where('status', "NOTFOUND")->orWhere('status', 'DUPLICATE');
        } else {
            $helpdesk = Helpdesk::with('pangkat')->with('verifikator')->with('satuan_sekarang')->where('status', 'LIKE', strtoupper($request->status));
        }
        return datatables($helpdesk)->toJson();
    }

    public function transformCollection($datas)
    {
        return array_map([$this, 'transform'], $datas->toArray());
    }
    public function transform($data)
    {
        $array=[];
        $array['nama_satuan']=$data['satuan_sekarang'];
        return [
            'id' => $data['id'],
            'nrp' => $data['nrp'],
            'nrp_baru' => $data['nrp_baru'],
            'nama_lengkap' => $data['nama_lengkap'],
            'pangkat' => $data['pangkat'],
            'satker_pertama' => $data['satker_pertama'],
            'tmt_jabatan_pertama' => $data['tmt_jabatan_pertama'],
            'tmt_jabatan_terakhir' => $data['tmt_jabatan_terakhir'],
            'satuan_sekarang' => $array,
            'status' => $data['status'],
            'handphone' => $data['handphone'],
            'email' => $data['email'],
            'created_at' => $data['created_at'],
        ];
    }

    public function exportToExcel(Request $request)
    {
        if (strtolower($request->status)=="all" || $request->status==null) {
            $helpdesk = Helpdesk::with('pangkat')->latest()->get();
        } elseif (strtolower($request->status)=="problem") {
            $helpdesk = Helpdesk::with('pangkat')->where('status', "NOTFOUND")->orWhere('status', 'DUPLICATE')->latest()->get();
        } else {
            $helpdesk = Helpdesk::with('pangkat')->where('status', 'LIKE', strtoupper($request->status))->latest()->get();
        }
        $test = Excel::create('daftar-data-helpdesk.xls', function ($excel) use ($helpdesk) {
            $excel->sheet('daftar-data-helpdesk', function ($sheet) use ($helpdesk) {
                $sheet->setColumnFormat(array(
                   "A:Z" => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                ));
                $sheet->loadView('excel.helpdesk-excel', compact('helpdesk'));
            });
        })->download('xls', ['Access-Control-Allow-Origin'=>'*']);
    }

    public function exportToExcelByDate(Request $request)
    {
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        if (strtolower($request->status)=="all" || $request->status==null) {
            $helpdesk = Helpdesk::with('pangkat')->latest()->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->get();
        } elseif (strtolower($request->status)=="problem") {
            $helpdesk = Helpdesk::with('pangkat')->where('status', "NOTFOUND")->orWhere('status', 'DUPLICATE')->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->latest()->get();
        } else {
            $helpdesk = Helpdesk::with('pangkat')->where('status', 'LIKE', strtoupper($request->status))->where('created_at','>=',$start_date)->where('created_at','<=',$end_date)->latest()->get();
        }
        $test = Excel::create('daftar-data-helpdesk.xls', function ($excel) use ($helpdesk) {
            $excel->sheet('daftar-data-helpdesk', function ($sheet) use ($helpdesk) {
                $sheet->setColumnFormat(array(
                   "A:Z" => \PHPExcel_Style_NumberFormat::FORMAT_TEXT,
                ));
                $sheet->loadView('excel.helpdesk-excel', compact('helpdesk'));
            });
        })->download('xls', ['Access-Control-Allow-Origin'=>'*']);
    }
    public function detail(Request $request)
    {
        $helpdesk = Helpdesk::find($request->id);
        
        if ($helpdesk==null) {
            return response()->json([
                'error' => 'NOT FOUND'
            ], 400);
        }
        $helpdesk->satuanSekarang;
        $helpdesk->pangkat;
        return response()->json([
            'data' => $helpdesk
        ], 200);
    }

    public function verified($id)
    {
        $helpdesk=Helpdesk::find($id);
        if ($helpdesk==null) {
            return response()->json([
                'error' => 'NOT FOUND'
            ], 400);
        }
        $helpdesk->status = 'VERIFIED';
        $helpdesk->save();
        // if ($helpdesk->email!=null) {
        //     $email = $helpdesk->email;
        //     Mail::send('email.notifikasi-nrp-tidak-bermasalah', ['helpdesk' => $helpdesk], function ($message) use ($email) {
        //         $message->subject('Konfirmasi Helpdesk - HELPDESK SDM POLRI');
        //         $message->from('helpdesk.notification@polri.go.id', 'HELPDESK SDM POLRI');

        //         $message->to($email);
        //     });
        // }
        return response()->json([
            'data' => $helpdesk
        ], 200);
    }

    public function hapus($id)
    {
        $helpdesk=Helpdesk::find($id);
        if ($helpdesk==null) {
            return response()->json([
                'error' => 'NOT FOUND'
            ], 400);
        }
        if($helpdesk->hapus()){
            return response()->json([
                'data' => 'Data helpdesk berhasil dihapus.'
            ], 200);
        } else {
            return response()->json([
                'error' => 'Data helpdesk gagal dihapus.'
            ], 400);
        }
    }

    public function tambahPersonel(Request $request)
    {
        $error_message = [
            'nrp.required' => 'Kolom nrp tidak boleh dikosongkan.',
            'tmt_pertama.required' => 'Kolom tmt pertama tidak boleh dikosongkan.',
        ];

        $validation = Validator::make($request->all(), [
            'nrp' => 'required',
            'tmt_pertama' => 'required|date',
        ], $error_message);

        if ($validation->fails()) {
            return response()->json([
                'errors' => $validation->errors(),
                'status_code' => 400
            ], 400);
        }
        DB::disableQueryLog();
        DB::beginTransaction();
        $helpdesk = Helpdesk::find($request->helpdesk_id);
        $helpdesk=Helpdesk::find($request->helpdesk_id);
        if ($helpdesk==null) {
            return response()->json([
                'error' => 'NOT FOUND'
            ], 400);
        }
        $helpdesk->nrp_baru = $request->nrp;
        $helpdesk->tmt_jabatan_pertama = $request->tmt_pertama ? $request->tmt_pertama : $helpdesk->tmt_jabatan_pertama;
        $helpdesk->update(['status' => 'VERIFIED']);
        $helpdesk->save();
        $personel = Personel::createPersonel([
            'nrp' => $helpdesk->nrp_baru,
            'nama_lengkap' => $helpdesk->nama_lengkap,
            'nama_ibu' => $helpdesk->nama_ibu,
            'satker_pertama' => $helpdesk->satker_pertama,
            'agama_id' => 1,
            'jenis_kelamin' => "PRIA",
            'tmt_jabatan_pertama' => $request->tmt_pertama,
            'tmt_pertama' => $request->tmt_pertama,
            'tanggal_lahir' => $helpdesk->tanggal_lahir,
        ]);
        if($personel){
            DB::commit();
        } else {
            return response()->json([
                'error' => 'personel gagal dibuat.'
            ], 400);
        }
        return response()->json([
            'data' => $personel
        ], 200);
    }
}
