<?php

namespace App\Http\Controllers\Helpdesk;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class HelpdeskUserController extends Controller
{
	public function login(Request $request)
	{
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'password.required'    => 'Kolom kata kunci tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' => 'required',
			'password' => 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    if(auth('helpdesk')->attempt(['nrp'=>$request->nrp,'password' => $request->password])){
			return response()->json([
				'token' => auth('helpdesk')->user()->api_token,
				'nrp' => $request->nrp,
			], 200);
	    } else{
			return response()->json([
				'message' => 'NR atau, Password anda salah.',
				'status_code' => 400
			],400);
	    }
	}
}
