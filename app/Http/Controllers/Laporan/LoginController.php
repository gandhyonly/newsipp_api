<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class LoginController extends Controller
{
	public function getLogin()
	{
		return view('laporan.views.login');
	}
	public function postLogin(Request $request)
	{
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'password.required'    => 'Kolom kata kunci tidak boleh dikosongkan.'
		];

		$validation = Validator::make($request->all(),[
			'nrp' => 'required',
			'password' => 'required'
	    ],$error_message);

	    if($validation->fails()){
	    	
	    }
	    if(auth('verifikator')->attempt(['nrp'=>strtolower($request->nrp),'password' => $request->password,'aktif' => true])){
			return redirect()->route(['laporan.dashboard']);
	    } else{
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	}
}
