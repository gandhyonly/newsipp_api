<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function redirectToLogin()
	{
		return redirect()->route('laporan.login');
	}
	public function dashboard()
	{
		return view('laporan.dashboard');
	}
}
