<?php

namespace App\Http\Controllers\API;

use App\Dikpol;
use App\Http\Controllers\Controller;
use App\JenisDikpol;
use App\Jurusan;
use App\PendidikanPersonel;
use App\Personel;
use App\TipeDikpol;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Cache;
use DB;
use Log;

class PendidikanPolisiController extends Controller
{
	public function daftarJenisDikpol()
	{
		$daftarJenisDikpol = Cache::remember('daftar_jenis_dikpol',60,function(){
			return JenisDikpol::orderBy('nama_jenis_pendidikan')->get();
		});
		return response()->json([
			'data' => $daftarJenisDikpol->toArray()
		],200);
	}
	public function daftarGelarDikpol($jurusanID)
	{
		$jurusan = Jurusan::find($jurusanID);
	    if($jurusan==null){
			return response()->json([
				'errors' => 'Jurusan Tidak Ditemukan.',
				'status_code' => 400
			], 400);
	    }
		$jurusan->gelar;
		return response()->json([
			'data' => $jurusan
		],200);
	}
	public function daftarDikpol(JenisDikpol $jenisDikpol)
	{
		return response()->json([
			'data' => $jenisDikpol->dikpol
		],200);
	}
	
	public function tambahPendidikanPolisi(Request $request)
	{
		$personelID = auth()->user()->personel_id;
		$personel=Personel::find($personelID);
	    $error_message = [
			'tanggal_mulai.required' 	=> 'Kolom tanggal mulai tidak boleh dikosongkan.',
			'tanggal_mulai.date' 	=> 'Format tanggal mulai salah.',
			'tanggal_lulus.date' 	=> 'Format tanggal lulus salah.',
			'dikpol_id.required' 	=> 'Kolom dikpol tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_lulus' 	=> 'nullable|date',
			'dikpol_id' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$dikpol = Dikpol::find($request->dikpol_id);
		if($dikpol == null){
			return response()->json([
				'errors' => 'Pendidikan Polisi tidak ditemukan.'
			],400);
		}
		DB::beginTransaction();

		$pendidikanPersonel = PendidikanPersonel::create([
			'dikpol_id' => $dikpol->dikpol_id,
			'personel_id' => $personelID,
			'gelar_id' => $request->gelar_id,
			'tanggal_mulai' => $request->tanggal_mulai,
			'tanggal_lulus' => $request->tanggal_lulus,
			'nilai_akhir' => $request->nilai_akhir,
			'angkatan' => $request->angkatan,
			'ranking' => $request->ranking,
			'total_siswa' => $request->total_siswa,
			'status_terakhir' => 1,
			'surat_kelulusan_nomor' => strtoupper($request->surat_kelulusan_nomor),
		]);

		$random = str_random(10);
		$dikpolTingkat= str_slug($dikpol->tingkat);
		$dikpolID= $dikpol->dikpol_id;
		$personelNRP = $personel->nrp;
	    if($request->hasFile('nilai_file')){
	    	$temp = "$personelNRP-dikpol_personel-nilai_file-$dikpolID-$dikpolTingkat-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
			$pendidikanPersonel->nilai_file = $temp.'.'.$request->file('nilai_file')->extension();
	    }
	    if($request->hasFile('surat_kelulusan_file')){
	    	$temp = "$personelNRP-dikpol_personel-surat_kelulusan_file-$dikpolID-$dikpolTingkat-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_kelulusan_file'), $temp.'.'.$request->file('surat_kelulusan_file')->extension());
			$pendidikanPersonel->surat_kelulusan_file = $temp.'.'.$request->file('surat_kelulusan_file')->extension();
	    }
	    $pendidikanPersonel->save();
	    DB::commit();
	    $pendidikanPersonel->dikpol;

		return response()->json([
			'data' => $pendidikanPersonel
		],200);
	}

	public function editPendidikanPolisi(Request $request,PendidikanPersonel $pendidikanPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel=Personel::find($personelID);
	    $error_message = [
			'tanggal_mulai.required' 	=> 'Kolom tanggal mulai tidak boleh dikosongkan.',
			'tanggal_mulai.date' 	=> 'Format tanggal mulai salah.',
			'tanggal_lulus.date' 	=> 'Format tanggal lulus salah.',
			'dikpol_id.required' 	=> 'Kolom dikpol tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_lulus' 	=> 'nullable|date',
			'dikpol_id' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$dikpol = Dikpol::find($request->dikpol_id);
		if($dikpol == null){
			return response()->json([
				'errors' => 'Pendidikan Polisi tidak ditemukan.'
			],400);
		}


		DB::beginTransaction();
		$pendidikanPersonel->update([
			'dikpol_id' => $dikpol->dikpol_id,
			'personel_id' => $personelID,
			'gelar_id' => $request->gelar_id,
			'tanggal_mulai' => $request->tanggal_mulai,
			'tanggal_lulus' => $request->tanggal_lulus,
			'nilai_akhir' => $request->nilai_akhir,
			'angkatan' => $request->angkatan,
			'ranking' => $request->ranking,
			'total_siswa' => $request->total_siswa,
			'status_terakhir' => 1,
			'surat_kelulusan_nomor' => strtoupper($request->surat_kelulusan_nomor),
		]);

		$random = str_random(5);
		$dikpolTingkat= str_slug($dikpol->tingkat);
		$dikpolID= $dikpol->dikpol_id;
		$personelNRP = $personel->nrp;
	    if($request->hasFile('nilai_file')){
			$pendidikanPersonel->hapusFile('nilai_file');
	    	$temp = "$personelNRP-dikpol_personel-nilai_file-$dikpolID-$dikpolTingkat-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
			$pendidikanPersonel->nilai_file = $temp.'.'.$request->file('nilai_file')->extension();
	    }
	    if($request->hasFile('surat_kelulusan_file')){
			$pendidikanPersonel->hapusFile('surat_kelulusan_file');
	    	$temp = "$personelNRP-dikpol_personel-surat_kelulusan_file-$dikpolID-$dikpolTingkat-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_kelulusan_file'), $temp.'.'.$request->file('surat_kelulusan_file')->extension());
			$pendidikanPersonel->surat_kelulusan_file = $temp.'.'.$request->file('surat_kelulusan_file')->extension();
	    }
	    $pendidikanPersonel->save();
	    DB::commit();

	    $pendidikanPersonel->dikpol;

		return response()->json([
			'data' => $pendidikanPersonel
		],200);
	}
	public function hapusPendidikanPolisi(PendidikanPersonel $pendidikanPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Riwayat Pendidikan Polisi '. $pendidikanPersonel->pendidikan_personel_id.' pada URL :'.url()->full());
		$pendidikanPersonel->hapusFile('nilai_file');
		$pendidikanPersonel->hapusFile('surat_kelulusan_file');
		$pendidikanPersonel->delete();
		return response()->json([
			'data' => 'Data berhasil dihapus'
		],200);
	}
}
