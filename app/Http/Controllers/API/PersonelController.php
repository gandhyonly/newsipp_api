<?php

namespace App\Http\Controllers\API;

use App\Agama;
use App\BKO;
use App\Bahasa;
use App\BahasaPersonel;
use App\HobiPersonel;
use App\Http\Controllers\Controller;
use App\JenisPekerjaan;
use App\KeluargaPersonel;
use App\KesehatanPersonel;
use App\Kota;
use App\Member;
use App\OlahragaPersonel;
use App\PendidikanPersonel;
use App\PenghargaanPersonel;
use App\Penugasan;
use App\Personel;
use App\PesanMember;
use App\RiwayatBrevet;
use App\RiwayatJabatan;
use App\RiwayatPangkat;
use App\RiwayatPsikologi;
use App\SIM;
use App\SIMPersonel;
use App\Satuan;
use App\StatusPersonel;
use App\TandaKehormatanPersonel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;
use Validator;
use Cache;
use Log;

class PersonelController extends Controller
{
	function __construct()
	{
		DB::disableQueryLog();
	}
	public function formBiodata(Request $request)
	{
		$today=Carbon::now()->format('Ymd');
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
	    $error_message = [
		    'nama_lengkap.required'    => 'Kolom nama lengkap tidak boleh dikosongkan.',
		    'tempat_lahir_id.required'    => 'Kolom tempat lahir tidak boleh dikosongkan.',
		    'tanggal_lahir.required'    => 'Kolom tanggal lahir tidak boleh dikosongkan.',
		    'tanggal_lahir.date'    => 'Format tanggal lahir salah.',
		    'jenis_kelamin.required'    => 'Kolom jenis kelamin tidak boleh dikosongkan.',
		    'golongan_darah.required'    => 'Kolom golongan darah tidak boleh dikosongkan.',
		    'status_pernikahan.required'    => 'Kolom status perkawinan tidak boleh dikosongkan.',
		    'agama_id.required'    => 'Kolom agama tidak boleh dikosongkan.',
		    'anak_ke.required'    => 'Kolom anak ke tidak boleh dikosongkan.',
		    'jumlah_saudara.required'    => 'Kolom Jumlah Saudara tidak boleh dikosongkan.',
		    'status_personel_id.required'    => 'Kolom status personel tidak boleh dikosongkan.',
			'foto_file.mimes' => "Pastikan file berupa jpeg",
			'foto_file.max' => "Pastikan file berupa jpeg dengan ukuran maksimum $maxFileUpload KB",
		];
		$validation = Validator::make($request->all(),[
	        'nama_lengkap' => 'required',
	        'tempat_lahir_id' => 'required',
	        'tanggal_lahir' => 'required|date',
	        'jenis_kelamin' => 'required',
	        'golongan_darah' => 'required',
	        'status_pernikahan' => 'required',
	        'agama_id' => 'required',
	        'anak_ke' => 'required',
	        'jumlah_saudara' => 'required',
	        'status_personel_id' => 'required',
	        'foto_file' => "nullable|mimes:jpg,jpeg|max:$maxFileUpload",
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			],400);
	    }
		$member = auth()->user();
		$member->statusForm()->syncWithoutDetaching(1,['keterangan' => null]);
		
		$member->save();

	    $personel=Personel::find($member->personel_id);

	    if(Kota::find($request->tempat_lahir_id)==null){
			return response()->json([
				'errors' => 'Tempat Lahir tidak ditemukan.',
				'status_code' => 400
			],400);
	    }
	    $tempatLahir = Kota::find($request->tempat_lahir_id)->nama_kota;
	    if(strtolower($tempatLahir)=='australia'){
	    	Log::info("NRP : $personel->nrp , Tempat Lahir : Australia");
	    }
	    $personel->update([
			'nama_lengkap' => strtoupper($request->nama_lengkap),
			'tempat_lahir_id' => $request->tempat_lahir_id,
			'tempat_lahir' => $tempatLahir,
			'tanggal_lahir' => $request->tanggal_lahir,
			'jenis_kelamin' => strtoupper($request->jenis_kelamin),
			'status_pernikahan' => strtoupper($request->status_pernikahan),
			'tmt_pertama' => $request->tmt_pertama,
			'agama_id' => $request->agama_id,
			'status_personel_id' => $request->status_personel_id
		]);
	    $personel->data()->update([
	    	'nama_panggilan' => strtoupper($request->nama_panggilan),
	    	'golongan_darah'=> strtoupper($request->golongan_darah),
	    	'telepon_orang_tua'=> $request->telepon_orang_tua,
	    	'alamat_orang_tua'=> strtoupper($request->alamat_orang_tua),
	    	'handphone'=> $request->handphone,
	    	'email'=> strtoupper($request->email),
	    	'anak_ke'=> $request->anak_ke,
	    	'jumlah_saudara'=> $request->jumlah_saudara
	    ]);
	    $personel->save();
	    $personelNRP = $personel->nrp;
	    $random=str_random(20);
		if(strtolower($request->foto_file)=='null'){
	    	$personel->foto_file=null;
	    	$persone->save();
	    } else if($request->hasFile('foto_file')){
			$temp="$personelNRP-foto-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('foto_file'), $temp.'.'.$request->file('foto_file')->extension());
			$foto_file = $temp.'.'.$request->file('foto_file')->extension();
		    $personel->foto_file = $foto_file;
		    $personel->save();
		}
		if(strtolower($request->lkhpn_file)=='null'){
	    	$personel->data()->update(['lkhpn_file' => null]);
	    } else if($request->hasFile('lkhpn_file')){
		    $error_message = [
				'lkhpn_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'lkhpn_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'lkhpn_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$temp="$personelNRP-lkhpn-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('lkhpn_file'), $temp.'.'.$request->file('lkhpn_file')->extension());
			$lkhpn_file = $temp.'.'.$request->file('lkhpn_file')->extension();
		    $personel->data()->update([
		    	'lkhpn_file' => $lkhpn_file,
		    ]);
		}
	    $personel->data;
	    $personel->member->statusForm;
		return response()->json([
			'status' => 'UNVERIFIED',
			'data' => $personel
		], 200);
	}

	public function formKondisiFisik(Request $request)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
		    'suku_bangsa.required'    => 'Kolom suku bangsa tidak boleh dikosongkan.',
		    'warna_kulit.required'    => 'Kolom warna kulit tidak boleh dikosongkan.',
		    'jenis_rambut.required'    => 'Kolom jenis rambut tidak boleh dikosongkan.',
		    'warna_rambut.required'    => 'Kolom warna rambut tidak boleh dikosongkan.',
		    'tinggi.required'    => 'Kolom tinggi tidak boleh dikosongkan.',
		    'tinggi.numeric'    => 'Kolom tinggi harus berupa angka.',
		    'berat.required'    => 'Kolom berat boleh dikosongkan.',
		    'berat.numeric'    => 'Kolom berat harus berupa angka.',
		    'ukuran_baju.required'    => 'Kolom ukuran baju tidak boleh dikosongkan.',
		    'ukuran_baju.numeric'    => 'Kolom ukuran celana harus berupa angka.',
		    'ukuran_celana.required'    => 'Kolom ukuran celana tidak boleh dikosongkan.',
		    'ukuran_celana.numeric'    => 'Kolom ukuran celana harus berupa angka.',
		    'ukuran_topi.required'    => 'Kolom ukuran topi tidak boleh dikosongkan.',
		    'ukuran_topi.numeric'    => 'Kolom ukuran topi harus berupa angka.',
		    'ukuran_sepatu.required'    => 'Kolom ukuran sepatu tidak boleh dikosongkan.',
		    'ukuran_sepatu.numeric'    => 'Kolom ukuran sepatu harus berupa angka.',
		];
		$validation = Validator::make($request->all(),[
	        'suku_bangsa' => 'required',
	        'warna_kulit' => 'required',
	        'jenis_rambut' => 'required',
	        'warna_rambut' => 'required',
	        'tinggi' => 'required|numeric',
	        'berat' => 'required|numeric',
	        'ukuran_baju' => 'required|numeric',
	        'ukuran_celana' => 'required|numeric',
	        'ukuran_topi' => 'required|numeric',
	        'ukuran_sepatu' => 'required|numeric',
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			]);
	    }
		$member = auth()->user();
		$member->statusForm()->syncWithoutDetaching(2,['verifikator_1' => null, 'verifikator_2' => null, 'verifikator_3' => null, 'verifikator_4' => null, 'tanggal_perubahan' => Carbon::now(), 'keterangan' => null]);
		
		$member->save();
	    $personel=Personel::find($member->personel_id);
	    $personel->fisik()->update([
	    	'tinggi' => $request->tinggi,
	    	'berat' => $request->berat,
	    	'ukuran_baju' => $request->ukuran_baju,
	    	'ukuran_celana' => $request->ukuran_celana,
	    	'ukuran_topi' => $request->ukuran_topi,
	    	'ukuran_sepatu' => $request->ukuran_sepatu,
	    ]);
	    $personel->data()->update([
	    	'suku_bangsa' => strtoupper($request->suku_bangsa),
	    	'warna_kulit' => strtoupper($request->warna_kulit),
	    	'warna_mata' => strtoupper($request->warna_mata),
	    	'jenis_rambut' => strtoupper($request->jenis_rambut),
	    	'warna_rambut' => strtoupper($request->warna_rambut),
	    ]);
	    $personel->fisik;
	    $personel->data;
		return response()->json([
			'status' => 'UNVERIFIED',
			'data' => $personel
		], 200);
	}

	public function formNomorIdentitas(Request $request)
	{
		$today=Carbon::now()->format('Ymd');
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			'asabri_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'asabri_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'akte_kelahiran_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'akte_kelahiran_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'tanda_kewenangan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'tanda_kewenangan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'kta_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'kta_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'npwp_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'npwp_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'bpjs_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'bpjs_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'kartu_keluarga_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'kartu_keluarga_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'ktp_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'ktp_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];
		$validation = Validator::make($request->all(),[
	        'asabri_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'akte_kelahiran_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'tanda_kewenangan_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'kta_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'npwp_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'bpjs_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'kartu_keluarga_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'ktp_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			],400);
	    }
		$personelID = auth()->user()->personel_id;
		$personelNRP = Personel::find($personelID)->nrp;
		$member = auth()->user();
		$member->statusForm()->syncWithoutDetaching(3,['verifikator_1' => null, 'verifikator_2' => null, 'verifikator_3' => null, 'verifikator_4' => null, 'tanggal_perubahan' => Carbon::now(), 'keterangan' => null]);
	    $personel=Personel::find($member->personel_id);
	    $personelNRP=$personel->nrp;
	    $arraySim=[];
		if($request->sim_id != null){
		    foreach($request->sim_id as $key => $simID){
		    	if($request->sim_id[$key]==''){
		    		continue;
		    	}
		    	$sim = SIM::find($request->sim_id[$key]);
				$tempPivot=[];
				if(array_key_exists($key, $request->sim_file??[])){
					$random=str_random(5);
					$namaSim=str_slug($sim->sim_nama);
					$temp="$personelNRP-data_personel-sim-$namaSim-$personelID-$today-$random";
					$temp = $temp.'.'.$request->file('sim_file')[$key]->extension();
					Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('sim_file')[$key], $temp);
					$tempPivot = array_add($tempPivot,'file', $temp);
				}
				if(array_key_exists($key, $request->sim_nomor??[])){
					$tempPivot = array_add($tempPivot,'sim_nomor', $request->input('sim_nomor')[$key]);
				}
				$arraySim = array_add($arraySim,$request->sim_id[$key], $tempPivot);
			    
		    }
		    $personel->sim()->sync($arraySim);
		}
		if(strtolower($request->asabri_file) == 'null'){
	    	$personel->data()->update(['asabri_file' => null]);
		} else if($request->hasFile('asabri_file')){
			$random=str_random(5);
			$temp="$personelNRP-data_personel-asabri_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('asabri_file'), $temp.'.'.$request->file('asabri_file')->extension());
			$asabri_file = $temp.'.'.$request->file('asabri_file')->extension();
		    $personel->data()->update([
		    	'asabri_file' => $asabri_file,
		    ]);
	    }
		if(strtolower($request->akte_kelahiran_file) == 'null'){
	    	$personel->data()->update(['akte_kelahiran_file' => null]);
		} else if($request->hasFile('akte_kelahiran_file')){
			$random=str_random(5);
			$temp="$personelNRP-data_personel-akte_kelahiran_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('akte_kelahiran_file'), $temp.'.'.$request->file('akte_kelahiran_file')->extension());
			$akte_kelahiran_file = $temp.'.'.$request->file('akte_kelahiran_file')->extension();
		    $personel->data()->update([
		    	'akte_kelahiran_file' => $akte_kelahiran_file,
		    ]);
	    }
	    if(strtolower($request->tanda_kewenangan_file)=='null'){
	    	$personel->data()->update(['tanda_kewenangan_file' => null]);
	    } else if($request->hasFile('tanda_kewenangan_file')){
			$random=str_random(5);
			$temp="$personelNRP-data_personel-tanda_kewenangan_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('tanda_kewenangan_file'), $temp.'.'.$request->file('tanda_kewenangan_file')->extension());
			$tanda_kewenangan_file = $temp.'.'.$request->file('tanda_kewenangan_file')->extension();
		    $personel->data()->update([
		    	'tanda_kewenangan_file' => $tanda_kewenangan_file,
		    ]);
		} 
		if(strtolower($request->keputusan_penyidik_file)=='null'){
	    	$personel->data()->update(['keputusan_penyidik_file' => null]);
	    } else if($request->hasFile('keputusan_penyidik_file')){

		    $error_message = [
				'keputusan_penyidik_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'keputusan_penyidik_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'keputusan_penyidik_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$random=str_random(5);
			$temp="$personelNRP-data_personel-keputusan_penyidik_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('keputusan_penyidik_file'), $temp.'.'.$request->file('keputusan_penyidik_file')->extension());
			$keputusan_penyidik_file = $temp.'.'.$request->file('keputusan_penyidik_file')->extension();
		    $personel->data()->update([
		    	'keputusan_penyidik_file' => $keputusan_penyidik_file,
		    ]);
		}  
		if(strtolower($request->kta_file)=='null'){
	    	$personel->data()->update(['kta_file' => null]);
	    } else if($request->hasFile('kta_file')){
			$random=str_random(5);
			$temp="$personelNRP-data_personel-kta_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('kta_file'), $temp.'.'.$request->file('kta_file')->extension());
			$kta_file = $temp.'.'.$request->file('kta_file')->extension();
		    $personel->data()->update([
		    	'kta_file' => $kta_file,
		    ]);
		} 
		if(strtolower($request->npwp_file)=='null'){
	    	$personel->data()->update(['npwp_file' => null]);
	    } else if($request->hasFile('npwp_file')){
			$random=str_random(5);
			$temp="$personelNRP-data_personel-npwp_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('npwp_file'), $temp.'.'.$request->file('npwp_file')->extension());
			$npwp_file = $temp.'.'.$request->file('npwp_file')->extension();
		    $personel->data()->update([
		    	'npwp_file' => $npwp_file,
		    ]);
		} 
		if(strtolower($request->paspor_file)=='null'){
	    	$personel->data()->update(['paspor_file' => null]);
	    } else if($request->hasFile('paspor_file')){

		    $error_message = [
				'paspor_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'paspor_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'paspor_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$random=str_random(5);
			$temp="$personelNRP-data_personel-paspor_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('paspor_file'), $temp.'.'.$request->file('paspor_file')->extension());
			$paspor_file = $temp.'.'.$request->file('paspor_file')->extension();
		    $personel->data()->update([
		    	'paspor_file' => $paspor_file,
		    ]);
		}  
		if(strtolower($request->bpjs_file)=='null'){
	    	$personel->data()->update(['bpjs_file' => null]);
	    } else if($request->hasFile('bpjs_file')){
			$random=str_random(5);
			$temp="$personelNRP-data_personel-bpjs_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('bpjs_file'), $temp.'.'.$request->file('bpjs_file')->extension());
			$bpjs_file = $temp.'.'.$request->file('bpjs_file')->extension();
		    $personel->data()->update([
		    	'bpjs_file' => $bpjs_file,
		    ]);
		}   
		if(strtolower($request->kartu_keluarga_file)=='null'){
	    	$personel->data()->update(['kartu_keluarga_file' => null]);
	    } else if($request->hasFile('kartu_keluarga_file')){
			$random=str_random(5);
			$temp="$personelNRP-data_personel-kartu_keluarga_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('kartu_keluarga_file'), $temp.'.'.$request->file('kartu_keluarga_file')->extension());
			$kartu_keluarga_file = $temp.'.'.$request->file('kartu_keluarga_file')->extension();
		    $personel->data()->update([
		    	'kartu_keluarga_file' => $kartu_keluarga_file,
		    ]);
		}
		if(strtolower($request->ktp_file)=='null'){
	    	$personel->data()->update(['ktp_file' => null]);
	    } else if($request->hasFile('ktp_file')){
			$random=str_random(5);
			$temp="$personelNRP-data_personel-ktp_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('ktp_file'), $temp.'.'.$request->file('ktp_file')->extension());
			$ktp_file = $temp.'.'.$request->file('ktp_file')->extension();
		    $personel->data()->update([
		    	'ktp_file' => $ktp_file,
		    ]);
		}  
		if(strtolower($request->skep_mk_file)=='null'){
	    	$personel->data()->update(['skep_mk_file' => null]);
	    } else if($request->hasFile('skep_mk_file')){
		    $error_message = [
				'skep_mk_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'skep_mk_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'skep_mk_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$random=str_random(5);
			$temp="$personelNRP-data_personel-skep_mk_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('skep_mk_file'), $temp.'.'.$request->file('skep_mk_file')->extension());
			$skep_mk_file = $temp.'.'.$request->file('skep_mk_file')->extension();
		    $personel->data()->update([
		    	'skep_mk_file' => $skep_mk_file,
		    ]);
		}  
		if(strtolower($request->masa_dinas_surut_file)=='null'){
	    	$personel->data()->update(['masa_dinas_surut_file' => null]);
	    } else if($request->hasFile('masa_dinas_surut_file')){
		    $error_message = [
				'masa_dinas_surut_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'masa_dinas_surut_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'masa_dinas_surut_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$random=str_random(5);
			$temp="$personelNRP-data_personel-masa_dinas_surut_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('masa_dinas_surut_file'), $temp.'.'.$request->file('masa_dinas_surut_file')->extension());
			$masa_dinas_surut_file = $temp.'.'.$request->file('masa_dinas_surut_file')->extension();
		    $personel->data()->update([
		    	'masa_dinas_surut_file' => $masa_dinas_surut_file,
		    ]);
		}  
		if(strtolower($request->peninjauan_masa_kerja_file)=='null'){
	    	$personel->data()->update(['peninjauan_masa_kerja_file' => null]);
	    } else if($request->hasFile('peninjauan_masa_kerja_file')){
		    $error_message = [
				'peninjauan_masa_kerja_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'peninjauan_masa_kerja_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'peninjauan_masa_kerja_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$random=str_random(5);
			$temp="$personelNRP-data_personel-peninjauan_masa_kerja_file-$personelID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('peninjauan_masa_kerja_file'), $temp.'.'.$request->file('peninjauan_masa_kerja_file')->extension());
			$peninjauan_masa_kerja_file = $temp.'.'.$request->file('peninjauan_masa_kerja_file')->extension();
		    $personel->data()->update([
		    	'peninjauan_masa_kerja_file' => $peninjauan_masa_kerja_file,
		    ]);
		}  
	    $personel->data()->update([
	    	'npwp_nomor' => strtoupper($request->npwp_nomor),
	    	'ktp_nomor' => strtoupper($request->ktp_nomor),
	    	'asabri_nomor' => strtoupper($request->asabri_nomor),
	    	'tanda_kewenangan_nomor' => strtoupper($request->tanda_kewenangan_nomor),
	    	'keputusan_penyidik_nomor' => strtoupper($request->keputusan_penyidik_nomor),
	    	'kta_nomor' => strtoupper($request->kta_nomor),
	    	'paspor_nomor' => strtoupper($request->paspor_nomor),
	    	'bpjs_nomor' => strtoupper($request->bpjs_nomor),
	    	'kartu_keluarga_nomor' => strtoupper($request->kartu_keluarga_nomor),
	    	'paspor_nomor' => strtoupper($request->paspor_nomor),
	    	'sidik_jari_1' => strtoupper($request->sidik_jari_1),
	    	'sidik_jari_2' => strtoupper($request->sidik_jari_2),
	    	'skep_mk_nomor' => strtoupper($request->skep_mk_nomor),
	    	'tmt_masa_dinas_surut' => $request->tmt_masa_dinas_surut,
	    	'peninjauan_masa_kerja_nomor' => $request->peninjauan_masa_kerja_nomor
	    ]);
		$personel->data;
		$personel->sim;
		return response()->json([
			'status' => 'UNVERIFIED',
			'data' => $personel
		], 200);
	}
	// 1
	public function dataBiodata(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->data;
		$member->personel->fisik;
		$member->personel->statusPersonel;

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 2
	public function dataRiwayatJabatan(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->riwayat_jabatan=RiwayatJabatan::where('personel_id',$personelID)->orderBy('tmt_jabatan')->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 3
	public function dataRiwayatAlamat(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->alamat;

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 4
	public function dataKondisiFisik(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->data;
		$member->personel->statusPersonel;
		$member->personel->fisik;

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 5
	public function dataNomorIdentitas(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->data;
		$member->personel->statusPersonel;
		$member->personel->sim=SIMPersonel::with('sim')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 6
	public function dataKeluarga(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->keluarga=KeluargaPersonel::with('pekerjaanKeluarga.jenisPekerjaan')->with('hubunganKeluarga')->with('golongan')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 7
	public function dataPendidikanUmum(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->pendidikan_umum=PendidikanPersonel::with('dikum')->with('konsentrasi.jurusan')->with('gelar.jurusan')->whereNotNull('dikum_id')->where('personel_id',$personelID)->orderBy('dikum_id')->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 8
	public function dataPendidikanPolisi(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->pendidikan_polisi=PendidikanPersonel::with('dikpol.jenisDikpol')->with('gelar.jurusan')->whereNotNull('dikpol_id')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 9
	public function dataDikbangspes(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->dikbangspes=PendidikanPersonel::with('dikbangspes.tipeDikbangspes')->whereNotNull('dikbangspes_id')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 10
	public function dataRiwayatPangkat(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->riwayat_pangkat=RiwayatPangkat::with('pangkat')->where('personel_id',$personelID)->orderBy('tmt_pangkat','DESC')->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 11
	public function dataTandaKehormatan(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->tanda_kehormatan=TandaKehormatanPersonel::with('tandaKehormatan')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 12
	public function dataPenghargaan(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->penghargaan=PenghargaanPersonel::with('tingkatPenghargaan')->with('penghargaan')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 13
	public function dataBahasaAsing(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$bahasaInternationalIDs = Bahasa::international()->pluck('bahasa_id');
		$member->personel->bahasa_international = BahasaPersonel::with('bahasa')->whereIn('bahasa_id',$bahasaInternationalIDs)->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 14
	public function dataBahasaDaerah(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$bahasaDaerahIDs = Bahasa::daerah()->pluck('bahasa_id');
		$member->personel->bahasa_daerah = BahasaPersonel::with('bahasa')->whereIn('bahasa_id',$bahasaDaerahIDs)->where('personel_id',$personelID)->get();


		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 15
	public function dataOlahraga(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->olahraga=OlahragaPersonel::with('olahraga')->where('personel_id',$personelID)->get();;

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 16
	public function dataHobi(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->hobi=HobiPersonel::with('hobi')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 17
	public function dataPenugasanLuarStruktur(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->penugasan_luar_struktur=Penugasan::with('jabatanPenugasan.lokasiPenugasan.negara')->where('personel_id',$personelID)->where('jenis_penugasan_id',2)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 18
	public function dataMisi(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->penugasan_negara=Penugasan::with('jabatanPenugasan.misi.negara')->where('personel_id',$personelID)->where('jenis_penugasan_id',1)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 19
	public function dataBrevet(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->riwayat_brevet=RiwayatBrevet::with('brevet')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}
	// 20
	public function dataBKO(Request $request)
	{
		$member = auth()->user();
		$personelID = $member->personel_id;
		$satkerSekarang='-';
		$member->personel->bko=BKO::with('jenisBko')->with('satuanAsal')->with('satuanPenugasan')->where('personel_id',$personelID)->get();

		return response()->json([
			'data' => $member->personel,
			'satker_sekarang' => $satkerSekarang
		], 200);
	}


	public function daftarStatusPersonel()
	{
		$daftarStatusPersonel = Cache::remember('daftar_status_personel',1440,function(){
			return StatusPersonel::get();
		});
		return response()->json([
			'data' => $daftarStatusPersonel
		], 200);
	}
	public function daftarPesanMember()
	{
		$member = auth()->user();

		return response()->json([
			'data' => PesanMember::where('member_id',$member->member_id)->get()
		], 200);
	}
}
