<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Member;
use App\Personel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileController extends Controller
{
	public function uploadFile(Request $request)
	{
		$file = Storage::putFileAs('personel/'.$request->nrp,$request->file('file'), str_random(10).'.'.$request->file('file')->extension());
		return response()->json([
			'data' => $file
		], 200);
	}
	public function getFile(Request $request)
	{
		$flag=Storage::exists('dont-remove.txt');
		if(!$flag){
			return response()->json(['error_title' => 'Server Storage Sedang Bermasalah.','error_message' => 'Harap Hubungi Administrator.'],400);
		}
		$file = Storage::exists('file/'.$request->file);
		if($file){
			$file = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix('file/'.$request->file); 

			return response()->file($file);
		}
		else {
			return response()->json(['error_title' => 'File Tidak Ditemukan.','error_message' => "Silahkan upload ulang file bernama : $request->file"],400);
		}
	}
	public function getFilePersonel(Request $request)
	{
		$personelID = auth()->user()->personel_id;
		$personel=Personel::find($personelID);
		$personelNRP = $personel->nrp;
		$flag=Storage::exists('dont-remove.txt');
		if(!$flag){
			return response()->json(['error_title' => 'Server Storage Sedang Bermasalah.','error_message' => 'Harap Hubungi Administrator.'],400);
		}
		$file = Storage::exists('personel/'.$personelNRP.'/'.$request->file);
		if($file){
		$file = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix('personel/'.$personelNRP.'/'.$request->file); 
			return response()->download($file, $request->file);
		} else {
			return response()->json(['error_title' => 'File Tidak Ditemukan.','error_message' => "Silahkan upload ulang file bernama : $request->file"],400);
		}
	}

	public function fileUploadTesting(Request $request)
	{
		$temp=str_random(10);
		if($request->file('petikan_pertama')!=null){
			Storage::putFileAs('testing/',$request->file('petikan_pertama'), $temp.'.'.$request->file('petikan_pertama')->extension());
			$data['petikan_pertama'] = $temp.'.'.$request->file('petikan_pertama')->extension();
		} else{
			return response()->json([
				'error_message' => 'File petikan_pertama null',
				'status_code' => 500 
			],400);
		}
		$temp=str_random(10);
	    if($request->hasFile('riwayat_hidup')){
			Storage::putFileAs('testing/',$request->file('riwayat_hidup'), $temp.'.'.$request->file('riwayat_hidup')->extension());
			$data['riwayat_hidup'] = $temp.'.'.$request->file('riwayat_hidup')->extension();
		} else{
			return response()->json([
				'error_message' => 'File riwayat_hidup null',
				'status_code' => 500 
			],400);
		}
		return response()->json([
			'data' => $data
		],200);
	}

	public function getFileTesting(Request $request)
	{
		$file = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix('testing/'.$request->file); 
		return response()->download($file, $request->file);
	}
	public function getFileForm(Request $request)
	{
	    $error_message = [
			'nrp.required' 	=> 'Kolom nrp tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' 		=> 'required',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$flag=Storage::exists('dont-remove.txt');
		if(!$flag){
			return response()->json(['error_title' => 'Server Storage Sedang Bermasalah.','error_message' => 'Harap Hubungi Administrator.'],400);
		}
		$member = Member::where('nrp',$request->nrp)->first();
		if($member==null){
			return response()->json(['error' => 'File Not Found.'],400);
		}else{
			$personelID = $member->personel_id;
			$personel = Personel::find($personelID);

			$file = Storage::exists('personel/'.$personel->nrp.'/'.$request->file);
			if($file){
				$file = Storage::disk('local')->getDriver()->getAdapter()->applyPathPrefix('personel/'.$personel->nrp.'/'.$request->file); 
				return response()->download($file, $request->file);
			} else {
				return response()->json(['error_title' => 'File Tidak Ditemukan.','error_message' => "Silahkan upload ulang file bernama : $request->file"],400);
			}
		}
	}
}
