<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Other;
use App\OtherGroup;
use Illuminate\Http\Request;

class StatusPernikahanController extends Controller
{
	public function daftarStatusPernikahan()
	{
		$statusPernikahan = StatusPernikahan::pluck('value');
		return response()->json(['data' => $statusPernikahan],200);
	}
}
