<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Penghargaan;
use App\PenghargaanPersonel;
use App\Personel;
use App\TingkatPenghargaan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Log;

class PenghargaanController extends Controller
{
	public function daftarPenghargaan()
	{
		return response()->json([
			'data' => Penghargaan::orderBy('nama_penghargaan')->get()
		],200);
	}
	public function daftarTingkatPenghargaan()
	{
		return response()->json([
			'data' => TingkatPenghargaan::orderBy('tingkat_penghargaan')->get()
		],200);
	}

	public function tambahPenghargaan(Request $request)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			'surat_nomor.required' 		=> 'Kolom surat penghargaan tidak boleh dikosongkan.',
			'penghargaan_id.required' 	=> 'Kolom penghargaan tidak boleh dikosongkan.',
			'tanggal_penghargaan.required' 	=> 'Kolom tanggal pemberian penghargaan tidak boleh dikosongkan.',
			'tanggal_penghargaan.date' 	=> 'Format tanggal pemberian penghargaan harus berupa tanggal.',
			'tingkat_penghargaan_id.required' 	=> 'Kolom tingkat penghargaan tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'surat_nomor' 		=> 'required',
			'tanggal_penghargaan' 				=> 'required|date',
			'penghargaan_id' 		=> 'required',
			'tingkat_penghargaan_id' 		=> 'required',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		if($request->penghargaan_id == 'other'){
		    $error_message = [
				'penghargaan.required' 		=> 'Kolom nama penghargaan tidak boleh dikosongkan.',
			];

			$validation = Validator::make($request->all(),[
				'penghargaan' 		=> 'required',
		    ],$error_message);


		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				], 400);
		    }
			$penghargaan = Penghargaan::create(['nama_penghargaan' => strtoupper($request->penghargaan)]);
		} else {
			$penghargaan = Penghargaan::find($request->penghargaan_id);
			if($penghargaan == null){
				return response()->json([
					'errors' => 'Penghargaan tidak ditemukan.'
				],400);
			}
		}
		$tingkatPenghargaan = TingkatPenghargaan::find($request->tingkat_penghargaan_id);
		if($tingkatPenghargaan == null){
			return response()->json([
				'errors' => 'Tingkat Penghargaan tidak ditemukan.'
			],400);
		}
		$personel = Personel::find($personelID);
		$penghargaanPersonel = PenghargaanPersonel::create([
			'personel_id' => $personelID,
			'penghargaan_id' => $penghargaan->penghargaan_id,
			'tingkat_penghargaan_id' => $tingkatPenghargaan->tingkat_penghargaan_id,
			'surat_nomor' => strtoupper($request->surat_nomor),
			'pemberi_penghargaan' => strtoupper($request->pemberi_penghargaan),
			'tanggal_penghargaan' => $request->tanggal_penghargaan,
		]);
		$personelNRP = $personel->nrp;
		$suratFile=null;
		if($request->hasFile('surat_file')){
			$namaPenghargaan=str_slug($penghargaan->nama_penghargaan);
			$random=str_random(5);
	    	$temp = $personelNRP."-penghargaan_personel-surat_file-$namaPenghargaan-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_file'), $temp.'.'.$request->file('surat_file')->extension());
			$suratFile = $temp.'.'.$request->file('surat_file')->extension();
			$penghargaanPersonel->surat_file=$suratFile;
			$penghargaanPersonel->save();
		}
		$penghargaanFile=null;
		if($request->hasFile('penghargaan_file')){
			$namaPenghargaan=str_slug($penghargaan->nama_penghargaan);
			$random=str_random(5);
	    	$temp = $personelNRP."-penghargaan_personel-penghargaan_file-$namaPenghargaan-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('penghargaan_file'), $temp.'.'.$request->file('penghargaan_file')->extension());
			$penghargaanFile = $temp.'.'.$request->file('penghargaan_file')->extension();
			$penghargaanPersonel->penghargaan_file=$penghargaanFile;
			$penghargaanPersonel->save();
		}
		$penghargaanPersonel->personel;
		$penghargaanPersonel->penghargaan;
		$penghargaanPersonel->tingkatPenghargaan;
		return response()->json([
			'data' => $penghargaanPersonel
		],200);
	}

    public function editPenghargaan(Request $request, PenghargaanPersonel $penghargaanPersonel)
    {
        $personelID = auth()->user()->personel_id;
        $error_message = [
            'surat_nomor.required' 		=> 'Kolom surat penghargaan tidak boleh dikosongkan.',
            'penghargaan_id.required' 	=> 'Kolom penghargaan tidak boleh dikosongkan.',
            'tanggal_penghargaan.required' 	=> 'Kolom tanggal pemberian penghargaan tidak boleh dikosongkan.',
            'tanggal_penghargaan.date' 	=> 'Format tanggal pemberian penghargaan harus berupa tanggal.',
            'tingkat_penghargaan_id.required' 	=> 'Kolom tingkat penghargaan tidak boleh dikosongkan.',
        ];

        $validation = Validator::make($request->all(),[
            'surat_nomor' 		=> 'required',
            'tanggal_penghargaan' 				=> 'required|date',
            'penghargaan_id' 		=> 'required',
            'tingkat_penghargaan_id' 		=> 'required',
        ],$error_message);


        if($validation->fails()){
            return response()->json([
                'errors' => $validation->errors(),
                'status_code' => 400
            ], 400);
        }
        if($request->penghargaan_id == 'other'){
            $error_message = [
                'penghargaan.required' 		=> 'Kolom nama penghargaan tidak boleh dikosongkan.',
            ];

            $validation = Validator::make($request->all(),[
                'penghargaan' 		=> 'required',
            ],$error_message);


            if($validation->fails()){
                return response()->json([
                    'errors' => $validation->errors(),
                    'status_code' => 400
                ], 400);
            }
            $penghargaan = Penghargaan::create(['nama_penghargaan' => strtoupper($request->penghargaan)]);
        } else {
            $penghargaan = Penghargaan::find($request->penghargaan_id);
            if($penghargaan == null){
                return response()->json([
                    'errors' => 'Penghargaan tidak ditemukan.'
                ],400);
            }
        }
        $tingkatPenghargaan = TingkatPenghargaan::find($request->tingkat_penghargaan_id);
        if($tingkatPenghargaan == null){
            return response()->json([
                'errors' => 'Tingkat Penghargaan tidak ditemukan.'
            ],400);
        }
        $personel = Personel::find($personelID);
		$personelNRP = $personel->nrp;
        $penghargaanPersonel->update([
            'penghargaan_id' => $penghargaan->penghargaan_id,
            'tingkat_penghargaan_id' => $tingkatPenghargaan->tingkat_penghargaan_id,
            'surat_nomor' => strtoupper($request->surat_nomor),
            'pemberi_penghargaan' => strtoupper($request->pemberi_penghargaan),
            'tanggal_penghargaan' => $request->tanggal_penghargaan,
        ]);
        $suratFile=null;
        if($request->hasFile('surat_file')){
			$penghargaanPersonel->hapusFile('surat_file');
			$namaPenghargaan=str_slug($penghargaan->nama_penghargaan);
			$random=str_random(5);
	    	$temp = $personelNRP."-penghargaan_personel-surat_file-$namaPenghargaan-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_file'), $temp.'.'.$request->file('surat_file')->extension());
			$suratFile = $temp.'.'.$request->file('surat_file')->extension();
			$penghargaanPersonel->surat_file=$suratFile;
			$penghargaanPersonel->save();
        }
        $penghargaanFile=null;
        if($request->hasFile('penghargaan_file')){
			$penghargaanPersonel->hapusFile('penghargaan_file');
			$namaPenghargaan=str_slug($penghargaan->nama_penghargaan);
			$random=str_random(5);
	    	$temp = $personelNRP."-penghargaan_personel-penghargaan_file-$namaPenghargaan-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('penghargaan_file'), $temp.'.'.$request->file('penghargaan_file')->extension());
			$penghargaanFile = $temp.'.'.$request->file('penghargaan_file')->extension();
			$penghargaanPersonel->penghargaan_file=$penghargaanFile;
			$penghargaanPersonel->save();
        }
        $penghargaanPersonel->personel;
        $penghargaanPersonel->penghargaan;
        $penghargaanPersonel->tingkatPenghargaan;
        return response()->json([
            'data' => $penghargaanPersonel
        ],200);
    }

	public function hapusPenghargaan(PenghargaanPersonel $penghargaanPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Daftar Penghargaan '. $penghargaanPersonel->penghargaan_personel_id.' pada URL :'.url()->full());
		$penghargaanPersonel->hapusFile('surat_file');
		$penghargaanPersonel->hapusFile('penghargaan_file');
		if($penghargaanPersonel->delete()){
			return response()->json([
				'message' => 'Penghargaan berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus penghargaan.',
				'status_code' => 400
			],400);
		}
	}
}
