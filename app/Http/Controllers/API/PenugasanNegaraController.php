<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\JabatanPenugasan;
use App\Misi;
use App\Negara;
use App\Penugasan;
use App\Personel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Log;

class PenugasanNegaraController extends Controller
{
	public function daftarMisi(Negara $negara)
	{
		return response()->json([
			'data' => $negara->misi
		],200);
	}
	public function daftarJabatanPenugasan(Misi $misi)
	{
		return response()->json([
			'data' => $misi->jabatanPenugasan
		],200);
	}

	public function tambahPenugasanNegara(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		$personelNRP = $personel->nrp;
	    $error_message = [
			'misi_id.required' 				=> 'Kolom Misi tidak boleh dikosongkan.',
			'tmt_penugasan.required' 		=> 'Kolom tmt penugasan tidak boleh dikosongkan.',
			'tmt_penugasan.date' 	=> 'Format tmt penugasan harus berupa tanggal.',
			'tmt_selesai.date' 	=> 'Format tmt selesai harus berupa tanggal.',
			'negara_id.required' 		=> 'Kolom negara tidak boleh dikosongkan.',
			'sprint_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'sprint_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];

		$validation = Validator::make($request->all(),[
			'misi_id' 				=> 'required',
			'tmt_penugasan' 		=> 'required|date',
			'tmt_selesai' 		=> 'nullable|date',
			'negara_id' 		=> 'required',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	        'sprint_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

		if($request->misi_id == 'other'){
		    $error_message = [
				'nama_misi.required' 		=> 'Kolom nama misi tidak boleh dikosongkan.',
				'kota.required' 		=> 'Kolom nama kota tidak boleh dikosongkan.',
				'nama_jabatan_penugasan.required' 		=> 'Kolom jabatan penugasan tidak boleh dikosongkan.',
				'eselon.required' 		=> 'Kolom eselon tidak boleh dikosongkan.',
			];
			$validation = Validator::make($request->all(),[
				'nama_misi' 	=> 'required',
				'kota' 	=> 'required',
				'nama_jabatan_penugasan' 	=> 'required',
				'eselon' 	=> 'required',
		    ],$error_message);

		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				], 400);
		    }
			$misi = Misi::create([
				'nama_misi' => strtoupper($request->nama_misi),
				'negara_id' => $request->negara_id,
				'kota' => strtoupper($request->kota)
			]);
			$jabatanPenugasan = JabatanPenugasan::create([
				'nama_jabatan_penugasan' => strtoupper($request->nama_jabatan_penugasan),
				'eselon' => $request->eselon,
				'misi_id' => $misi->misi_id
			]);
		} else {
			$misi = Misi::find($request->misi_id);
			if($misi==null){
				return response()->json([
					'errors' => 'Misi tidak ditemukan.',
					'status_code' => 400
				], 400);
			}
			if($request->jabatan_penugasan_id == 'other'){
			    $error_message = [
					'nama_jabatan_penugasan.required' 		=> 'Kolom jabatan penugasan tidak boleh dikosongkan.',
					'eselon.required' 		=> 'Kolom eselon tidak boleh dikosongkan.',
				];
				$validation = Validator::make($request->all(),[
					'nama_jabatan_penugasan' 	=> 'required',
					'eselon' 	=> 'required',
			    ],$error_message);

			    if($validation->fails()){
					return response()->json([
						'errors' => $validation->errors(),
						'status_code' => 400
					], 400);
			    }
				$jabatanPenugasan = JabatanPenugasan::create([
					'nama_jabatan_penugasan' => strtoupper($request->nama_jabatan_penugasan),
					'eselon' => $request->eselon,
					'misi_id' => $misi->misi_id
				]);
			} else {
				$jabatanPenugasan = JabatanPenugasan::find($request->jabatan_penugasan_id);
				if($jabatanPenugasan==null || $jabatanPenugasan->misi_id==null){
					return response()->json([
						'errors' => 'Jabatan penugasan tidak ditemukan.',
						'status_code' => 400
					], 400);
				}
			}
		}

		$penugasan = Penugasan::create([
			'jenis_penugasan_id' => 1,
			'jabatan_penugasan_id' => $jabatanPenugasan->jabatan_penugasan_id,
			'personel_id' => $personel->personel_id,
			'sprint_nomor' => $request->sprint_nomor,
			'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
			'tmt_penugasan' => $request->tmt_penugasan,
			'tmt_selesai' => $request->tmt_selesai,
		]);
		$penugasanID=$penugasan->penugasan_id;
		$skep=null;
    	$random =str_random(5);
	    if($request->hasFile('sprint_file')){
	    	$temp = "$personelNRP-penugasan_luar_dalam_negeri_personel-surat_perintah_file-$penugasanID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('sprint_file'), $temp.'.'.$request->file('sprint_file')->extension());
			$penugasan->sprint_file = $temp.'.'.$request->file('sprint_file')->extension();
	    }
	    if($request->hasFile('surat_keputusan_file')){
	    	$temp = "$personelNRP-penugasan_luar_dalam_negeri_personel-surat_keputusan_file-$penugasanID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
			$penugasan->surat_keputusan_file = $temp.'.'.$request->file('surat_keputusan_file')->extension();
	    }
	    $penugasan->save();
	    $penugasan->jabatanPenugasan->misi->negara;
		return response()->json([
			'data' => $penugasan
		],200);
	}
	public function editPenugasanNegara(Request $request,Penugasan $penugasan)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		$personelNRP = $personel->nrp;
	    $error_message = [
			'misi_id.required' 				=> 'Kolom Misi tidak boleh dikosongkan.',
			'tmt_penugasan.required' 		=> 'Kolom tmt penugasan tidak boleh dikosongkan.',
			'tmt_penugasan.date' 	=> 'Format tmt penugasan harus berupa tanggal.',
			'tmt_selesai.date' 	=> 'Format tmt selesai harus berupa tanggal.',
			'negara_id.required' 		=> 'Kolom negara tidak boleh dikosongkan.',
			'sprint_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'sprint_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];

		$validation = Validator::make($request->all(),[
			'misi_id' 				=> 'required',
			'tmt_penugasan' 		=> 'required|date',
			'tmt_selesai' 		=> 'nullable|date',
			'negara_id' 		=> 'required',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	        'sprint_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

		if($request->misi_id == 'other'){
		    $error_message = [
				'nama_misi.required' 		=> 'Kolom nama misi tidak boleh dikosongkan.',
				'kota.required' 		=> 'Kolom nama kota tidak boleh dikosongkan.',
				'nama_jabatan_penugasan.required' 		=> 'Kolom jabatan penugasan tidak boleh dikosongkan.',
				'eselon.required' 		=> 'Kolom eselon tidak boleh dikosongkan.',
			];
			$validation = Validator::make($request->all(),[
				'nama_misi' 	=> 'required',
				'kota' 	=> 'required',
				'nama_jabatan_penugasan' 	=> 'required',
				'eselon' 	=> 'required',
		    ],$error_message);

		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				], 400);
		    }
			$misi = Misi::create([
				'nama_misi' => strtoupper($request->nama_misi),
				'negara_id' => $request->negara_id,
				'kota' => strtoupper($request->kota)
			]);
			$jabatanPenugasan = JabatanPenugasan::create([
				'nama_jabatan_penugasan' => strtoupper($request->nama_jabatan_penugasan),
				'eselon' => $request->eselon,
				'misi_id' => $misi->misi_id
			]);
		} else {
			$misi = Misi::find($request->misi_id);
			if($misi==null){
				return response()->json([
					'errors' => 'Misi tidak ditemukan.',
					'status_code' => 400
				], 400);
			}
			if($request->jabatan_penugasan_id == 'other'){
			    $error_message = [
					'nama_jabatan_penugasan.required' 		=> 'Kolom jabatan penugasan tidak boleh dikosongkan.',
					'eselon.required' 		=> 'Kolom eselon tidak boleh dikosongkan.',
				];
				$validation = Validator::make($request->all(),[
					'nama_jabatan_penugasan' 	=> 'required',
					'eselon' 	=> 'required',
			    ],$error_message);

			    if($validation->fails()){
					return response()->json([
						'errors' => $validation->errors(),
						'status_code' => 400
					], 400);
			    }
				$jabatanPenugasan = JabatanPenugasan::create([
					'nama_jabatan_penugasan' => strtoupper($request->nama_jabatan_penugasan),
					'eselon' => $request->eselon,
					'misi_id' => $misi->misi_id
				]);
			} else {
				$jabatanPenugasan = JabatanPenugasan::find($request->jabatan_penugasan_id);
				if($jabatanPenugasan==null || $jabatanPenugasan->misi_id==null){
					return response()->json([
						'errors' => 'Jabatan penugasan tidak ditemukan.',
						'status_code' => 400
					], 400);
				}
			}
		}

		$penugasan->update([
			'jenis_penugasan_id' => 1,
			'jabatan_penugasan_id' => $jabatanPenugasan->jabatan_penugasan_id,
			'personel_id' => $personel->personel_id,
			'sprint_nomor' => strtoupper($request->sprint_nomor),
			'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
			'tmt_penugasan' => $request->tmt_penugasan,
			'tmt_selesai' => $request->tmt_selesai,
		]);
		$skep=null;
		$penugasanID=$penugasan->penugasan_id;
    	$random =str_random(5);
	    if($request->hasFile('sprint_file')){
	    	$temp = "$personelNRP-penugasan_luar_dalam_negeri_personel-surat_perintah_file-$penugasanID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('sprint_file'), $temp.'.'.$request->file('sprint_file')->extension());
			$penugasan->sprint_file = $temp.'.'.$request->file('sprint_file')->extension();
	    }
	    if($request->hasFile('surat_keputusan_file')){
	    	$temp = "$personelNRP-penugasan_luar_dalam_negeri_personel-surat_keputusan_file-$penugasanID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
			$penugasan->surat_keputusan_file = $temp.'.'.$request->file('surat_keputusan_file')->extension();
	    }
	    $penugasan->save();
	    $penugasan->jabatanPenugasan->misi->negara;
		return response()->json([
			'data' => $penugasan
		],200);
	}
	public function hapusPenugasanNegara(Penugasan $penugasan)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Penugasan Negara '. $penugasan->penugasan_id.' pada URL :'.url()->full());
		$penugasan->hapusFile('sprint_file');
		$penugasan->hapusFile('surat_keputusan_file');
		if($penugasan->delete()){
			return response()->json([
				'message' => 'Data riwayat penugasan berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus data riwayat penugasan.',
				'status_code' => 400
			],400);
		}
	}
}
