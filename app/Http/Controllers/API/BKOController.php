<?php

namespace App\Http\Controllers\API;

use App\BKO;
use App\Http\Controllers\Controller;
use App\JenisBKO;
use App\Misi;
use App\Personel;
use App\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use Log;


class BKOController extends Controller
{
	public function daftarJenisBKO()
	{
		return response()->json([
			'data' => JenisBKO::get()
		],200);
	}
	public function tambahBKO(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			// 'jenis_bko_id.required' => 'Kolom Jenis BKO tidak boleh dikosongkan.',
			'satuan_asal_id.required' => 'Kolom satuan asal tidak boleh dikosongkan.',
			'satuan_penugasan_id.required' => 'Kolom satuan penugasan tidak boleh dikosongkan.',
			'tmt_bko.required' => 'Kolom tmt bko tidak boleh dikosongkan.',
			'surat_keputusan_nomor.required' => 'Kolom Nomor Surat Keputusan tidak boleh dikosongkan.',
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];

		$validation = Validator::make($request->all(),[
			// 'jenis_bko_id' => 'required',
			'tmt_bko' => 'required',
			'satuan_asal_id' => 'required',
			'satuan_penugasan_id' => 'required',
			'surat_keputusan_nomor' => 'required',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

		if($request->jenis_bko_id == 'other'){
			if($request->nama_jenis_bko == null){
				return response()->json([
					'errors' => 'Kolom jenis bko tidak boleh dikosongkan.'
				],400);
			}
			$jenisBKO = JenisBKO::create(['nama_jenis_bko' => strtoupper($request->nama_jenis_bko)]);
		} else {
			$jenisBKO = JenisBKO::find($request->jenis_bko_id);
		}
		$personel = Personel::find($personelID);
		$satuanAsal = Satuan::find($request->satuan_asal_id);
		if($satuanAsal==null){
			return response()->json([
				'errors' => ['satuan_asal_id' => 'Data satuan asal salah.'],
				'status_code' => 400
			], 400);
		}
		$satuanPenugasan = Satuan::find($request->satuan_penugasan_id);
		if($satuanPenugasan==null){
			return response()->json([
				'errors' => ['satuan_penugasan_id' => ['Data satuan penugasan salah.']],
				'status_code' => 400
			], 400);
		}
		$bko = BKO::create([
			'jenis_bko_id' => $jenisBKO==null?null:$jenisBKO->jenis_bko_id,
			'personel_id' => $personel->personel_id,
			'satuan_asal_id' => $request->satuan_asal_id,
			'satuan_penugasan_id' => $request->satuan_penugasan_id,
			'tmt_bko' => $request->tmt_bko,
			'jabatan_bko' => strtoupper($request->jabatan_bko),
			'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
		]);
		$personelNRP = $personel->nrp;
		$suratKeputusanFile=null;
		if($request->hasFile('surat_keputusan_file')){
			$namaJabatanBKO=str_slug($bko->jabatan_bko);
			$satuanAsalID=$bko->satuan_asal_id;
			$satuanTujuanID=$bko->satuan_penugasan_id;
			$random=str_random(5);
	    	$temp = "$personelNRP-bko_personel-surat_keputusan_file-$satuanAsalID-$satuanTujuanID-$namaJabatanBKO-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
			$suratKeputusanFile = $temp.'.'.$request->file('surat_keputusan_file')->extension();
			$bko->surat_keputusan_file=$suratKeputusanFile;
			$bko->save();
		}
		$bko->personel;
		$bko->jenisBko;
		$bko->satuanAsal;
		$bko->satuanPenugasan;
		return response()->json([
			'data' => $bko
		],200);
	}
	public function editBKO(Request $request, BKO $bko)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			// 'jenis_bko_id.required' => 'Kolom Jenis BKO tidak boleh dikosongkan.',
			'satuan_asal_id.required' => 'Kolom satuan asal tidak boleh dikosongkan.',
			'satuan_penugasan_id.required' => 'Kolom satuan penugasan tidak boleh dikosongkan.',
			'tmt_bko.required' => 'Kolom tmt bko tidak boleh dikosongkan.',
			'surat_keputusan_nomor.required' => 'Kolom Nomor Surat Keputusan tidak boleh dikosongkan.',
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];

		$validation = Validator::make($request->all(),[
			// 'jenis_bko_id' => 'required',
			'tmt_bko' => 'required',
			'satuan_asal_id' => 'required',
			'satuan_penugasan_id' => 'required',
			'surat_keputusan_nomor' => 'required',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

		if($request->jenis_bko_id == 'other'){
			if($request->nama_jenis_bko == null){
				return response()->json([
					'errors' => 'Kolom jenis bko tidak boleh dikosongkan.'
				],400);
			}
			$jenisBKO = JenisBKO::create(['nama_jenis_bko' => strtoupper($request->nama_jenis_bko)]);
		} else {
			$jenisBKO = JenisBKO::find($request->jenis_bko_id);
		}
		$satuanAsal = Satuan::find($request->satuan_asal_id);
		if($satuanAsal==null){
			return response()->json([
				'errors' => ['satuan_asal_id' => 'Data satuan asal salah.'],
				'status_code' => 400
			], 400);
		}
		$satuanPenugasan = Satuan::find($request->satuan_penugasan_id);
		if($satuanPenugasan==null){
			return response()->json([
				'errors' => ['satuan_penugasan_id' => ['Data satuan penugasan salah.']],
				'status_code' => 400
			], 400);
		}
		$personel = Personel::find($personelID);
		$personelNRP = $personel->nrp;
		$suratKeputusanFile=null;
		if($request->hasFile('surat_keputusan_file')){
			$bko->hapusFile();
			$namaJabatanBKO=str_slug($bko->jabatan_bko);
			$satuanAsalID=$bko->satuan_asal_id;
			$satuanTujuanID=$bko->satuan_penugasan_id;
			$random=str_random(5);
	    	$temp = "$personelNRP-bko_personel-surat_keputusan_file-$satuanAsalID-$satuanTujuanID-$namaJabatanBKO-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
			$suratKeputusanFile = $temp.'.'.$request->file('surat_keputusan_file')->extension();
			$bko->surat_keputusan_file=$suratKeputusanFile;
			$bko->save();
		}
		$bko->update([
			'jenis_bko_id' => $jenisBKO==null?null:$jenisBKO->jenis_bko_id,
			'satuan_asal_id' => $request->satuan_asal_id,
			'satuan_penugasan_id' => $request->satuan_penugasan_id,
			'tmt_bko' => $request->tmt_bko,
			'jabatan_bko' => strtoupper($request->jabatan_bko),
			'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
		]);
		$bko->personel;
		$bko->jenisBko;
		$bko->satuanAsal;
		$bko->satuanPenugasan;
		return response()->json([
			'data' => $bko
		],200);
	}
	public function hapusBKO(BKO $bko)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus BKO '. $bko->bko_id.' pada URL :'.url()->full());
		$bko->hapusFile();
		if($bko->delete()){
			return response()->json([
				'message' => 'Data riwayat bko berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus data riwayat bko.',
				'status_code' => 400
			],400);
		}
	}
}
