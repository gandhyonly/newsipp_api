<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\HubunganKeluarga;
use Illuminate\Http\Request;

class HubunganKeluargaController extends Controller
{
	public function daftarHubunganKeluarga()
	{
		$daftarHubunganKeluarga = HubunganKeluarga::get();

		return response()->json([
			'data' => $daftarHubunganKeluarga
		],200);
	}
}
