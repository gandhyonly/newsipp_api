<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\SIM;
use App\SIMPersonel;
use App\Personel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Log;

class SIMController extends Controller
{
	public function daftarSIM()
	{
		return response()->json([
			'data' => SIM::get()
		],200);
	}
	public function tambahSIM(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
	    $error_message = [
		    'sim_id.required'    => 'Kolom sim tidak boleh dikosongkan.',
		    'sim_nomor.required'    => 'Kolom nomor sim tidak boleh dikosongkan.',
	        'file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
		];

		$validation = Validator::make($request->all(),[
			'sim_id' => 'required',
	        'sim_nomor' => 'required',
	        'file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		$simID=$request->sim_id;
		$simPersonel = SIMPersonel::create([
			'personel_id' => $personelID,
			'sim_id' => $simID,
			'sim_nomor' => $request->sim_nomor,
		]);
		$personelNRP = $personel->nrp;
		if($request->hasFile('file')){
			$random=str_random(5);
	    	$temp = $personelNRP."-data_personel-sim_file-$simID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('file'), $temp.'.'.$request->file('file')->extension());
			$fileName = $temp.'.'.$request->file('file')->extension();
			$simPersonel->file=$fileName;
			$simPersonel->save();
		}
		$personel->sim;
		return response()->json([
			'data' => $personel,
		],200);
	}
	public function editSIM(Request $request,SIMPersonel $SIMPersonel)
	{

		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
	    $error_message = [
		    'sim_id.required'    => 'Kolom sim tidak boleh dikosongkan.',
		    'sim_nomor.required'    => 'Kolom nomor sim tidak boleh dikosongkan.',
	        'file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
		];

		$validation = Validator::make($request->all(),[
			'sim_id' => 'required',
	        'sim_nomor' => 'required',
	        'file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		$simID=$request->sim_id;
		$SIMPersonel->update([
			'personel_id' => $personelID,
			'sim_id' => $simID,
			'sim_nomor' => $request->sim_nomor,
		]);
		$personelNRP = $personel->nrp;
		if($request->hasFile('file')){
			$SIMPersonel->hapusFile('file');
			$random=str_random(5);
	    	$temp = $personelNRP."-data_personel-sim_file-$simID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('file'), $temp.'.'.$request->file('file')->extension());
			$fileName = $temp.'.'.$request->file('file')->extension();
			$SIMPersonel->file=$fileName;
			$SIMPersonel->save();
		}

		$personel->sim;
		return response()->json([
			'data' => $personel,
		],200);
	}
	public function hapusSIM(SIMPersonel $SIMPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Daftar SIM '. $SIMPersonel->sim_personel_id.' pada URL :'.url()->full());
		$SIMPersonel->hapusFile('file');
		if($SIMPersonel->delete()){
			return response()->json([
				'message' => 'SIM berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus sim.',
				'status_code' => 400
			],400);
		}
	}
}
