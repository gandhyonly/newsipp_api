<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Personel;
use App\RiwayatJabatan;
use App\SatuanJabatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use Log;

class JabatanController extends Controller
{
	public function urutanSatuan(RiwayatJabatan $riwayatJabatan)
	{
		return $riwayatJabatan->satuanJabatan->urutanSatuan();
	}
	public function tambahRiwayatJabatan(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
	    $error_message = [
		    'surat_keputusan_nomor.required'    => 'Kolom surat keputusan nomor tidak boleh dikosongkan.',
		    'tmt_jabatan.required'    => 'Kolom tanggal jabatan tidak boleh dikosongkan.',
	    'jabatan_sementara.required'    => 'Kolom PS tidak boleh dikosongkan.',
		    'tmt_jabatan.date'    => 'format tanggal jabatan harus berupa tanggal.',
		    'tmt_selesai.date'    => 'format tanggal selesai harus berupa tanggal.',
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];
		$validation = Validator::make($request->all(),[
	        'surat_keputusan_nomor' => 'required',
	        'tmt_jabatan' => 'required|date',
	        'tmt_selesai' => 'nullable|date',
	        'surat_keputusan_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'jabatan_sementara' => "required",
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			],400);
	    }
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		$satuanJabatan=null;
		if($request->satuan_jabatan_id!='other'){
			if($request->satuan_jabatan_id!=null){
				$satuanJabatan = SatuanJabatan::find($request->satuan_jabatan_id);
				if($satuanJabatan == null){
					return response()->json([
						'errors' => 'Satuan Jabatan Tidak Ditemukan'
					],400);
				}
			}
		}
		$fileName=null;
		$personelNRP = $personel->nrp;
		$random=str_random(10);

		if($request->jabatan_sementara=='true'){
			$requestKeterangan = 'PS. '.strtoupper($request->keterangan);
		} else {
			$requestKeterangan = strtoupper($request->keterangan);
		}
		$today=Carbon::now()->format('Ymd');
		$riwayatJabatan = RiwayatJabatan::create([
			'personel_id' => $personelID,
			'satuan_jabatan_id' => $satuanJabatan==null?null:$satuanJabatan->satuan_jabatan_id,
			'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
			'tmt_jabatan' => $request->tmt_jabatan,
			'status_aktif' => false,
			'keterangan' => $requestKeterangan,
			'jabatan_sementara' => $request->jabatan_sementara
		]);
		if($request->hasFile('surat_keputusan_file')){
			$satuanJabatanID = $satuanJabatan?$satuanJabatan->satuan_jabatan_id:'other';
	    	$temp = "$personelNRP-jabatan_personel-surat_keputusan_file-$satuanJabatanID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
			$fileName = $temp.'.'.$request->file('surat_keputusan_file')->extension();
			$riwayatJabatan->surat_keputusan_file = $fileName;
			$riwayatJabatan->save();
		}
		if($satuanJabatan){
			$riwayatJabatan->keterangan = $satuanJabatan->fullKeterangan();
			$riwayatJabatan->save();
		}
		$riwayatJabatan->buatStatusAktif();
		$personel->riwayatJabatan;
		return response()->json([
			'data' => $personel
		],200);
	}

	public function editRiwayatJabatan(Request $request,RiwayatJabatan $riwayatJabatan)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
	    $error_message = [
		    'surat_keputusan_nomor.required'    => 'Kolom surat keputusan nomor tidak boleh dikosongkan.',
		    'jabatan_sementara.required'    => 'Kolom PS tidak boleh dikosongkan.',
		    'tmt_jabatan.required'    => 'Kolom tanggal jabatan tidak boleh dikosongkan.',
		    'tmt_jabatan.date'    => 'format tanggal jabatan harus berupa tanggal.',
		    'tmt_selesai.date'    => 'format tanggal selesai harus berupa tanggal.',
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpg/jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			'surat_keputusan_file.max' => "Pastikan file berupa jpg/jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];
		$validation = Validator::make($request->all(),[
	        'surat_keputusan_nomor' => 'required',
	        'tmt_jabatan' => 'required|date',
	        'tmt_selesai' => 'nullable|date',
	        'surat_keputusan_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'jabatan_sementara' => "required",
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			],400);
	    }
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		$satuanJabatan=null;
		if($request->satuan_jabatan_id!='other'){
			if($request->satuan_jabatan_id!=null){
				$satuanJabatan = SatuanJabatan::find($request->satuan_jabatan_id);
				if($satuanJabatan == null){
					return response()->json([
						'errors' => 'Satuan Jabatan Tidak Ditemukan'
					],400);
				}
			}
		}
		$fileName=null;
		$personelNRP = $personel->nrp;
		$random=str_random(10);


		if($request->jabatan_sementara=='true'){
			$requestKeterangan = 'PS. '.strtoupper($request->keterangan);
		} else {
			$requestKeterangan = strtoupper($request->keterangan);
		}

		$today=Carbon::now()->format('Ymd');
		$riwayatJabatan->update([
			'satuan_jabatan_id' => $satuanJabatan==null?null:$request->satuan_jabatan_id,
			'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
			'tmt_jabatan' => $request->tmt_jabatan,
			'status_aktif' => false,
			'keterangan' => $requestKeterangan,
			'jabatan_sementara' => $request->jabatan_sementara
		]);
		if($request->hasFile('surat_keputusan_file')){
			$riwayatJabatan->hapusFile();
			$satuanJabatanID = $satuanJabatan?$satuanJabatan->satuan_jabatan_id:'other';
	    	$temp = "$personelNRP-jabatan_personel-surat_keputusan_file-$satuanJabatanID-$today-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
			$fileName = $temp.'.'.$request->file('surat_keputusan_file')->extension();
			$riwayatJabatan->surat_keputusan_file=$fileName;
			$riwayatJabatan->save();
		}
		if($satuanJabatan){
			$riwayatJabatan->keterangan = $satuanJabatan->fullKeterangan();
			$riwayatJabatan->save();
		}
		$riwayatJabatan->buatStatusAktif();
		$riwayatJabatan->personel;
		$riwayatJabatan->satuanJabatan;
		return response()->json([
			'data' => $riwayatJabatan
		],200);
	}

	public function hapusRiwayatJabatan(RiwayatJabatan $riwayatJabatan)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Riwayat Jabatan '. $riwayatJabatan->riwayat_jabatan_id.' pada URL :'.url()->full());		
		$riwayatJabatan->hapusFile();
		if($riwayatJabatan->delete()){
			return response()->json([
				'data' => 'Data riwayat jabatan berhasil dihapus.'
			],200);
		} else {
			return response()->json([
				'data' => 'Data riwayat jabatan gagal dihapus.'
			],200);
		}
	}
}
