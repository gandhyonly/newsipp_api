<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Personel;
use App\Psikologi;
use App\RiwayatPsikologi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PsikologiController extends Controller
{
	public function daftarPsikologi()
	{
		return response()->json([
			'data' => Psikologi::get()
		],200);
	}

	public function tambahRiwayatPsikologi(Request $request)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
            'psikologi_id.required'    => 'Kolom psikologi tidak boleh dikosongkan.',
	        'nilai.required'    => 'Kolom nilai tidak boleh dikosongkan.',
		    'tanggal_tes.required'    => 'Kolom tanggal tes tidak boleh dikosongkan.',
		    'tanggal_tes.date'    => 'format tanggal tes salah.',
		];
		$validation = Validator::make($request->all(),[
            'psikologi_id' => 'required',
		    'nilai' => 'required|numeric',
	        'tanggal_tes' => 'required|date',
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			]);
	    }
		$personel = Personel::find(auth()->user()->personel_id);
		$psikologiID = $request->psikologi_id;
		if($request->psikologi_id == 'other'){
			$psikologi = Psikologi::create(['jenis' => $request->psikologi]);
			$psikologiID= $psikologi->psikologi_id;
		} else {
			$psikologi = Psikologi::find($psikologiID);
		}
		$nilaiFile = null;
		$personelNRP = $personel->nrp;
		if($request->hasFile('nilai_file')){
	    	$temp = $personelNRP.'-psikologi-nilai-'.str_random(10);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
			$nilaiFile = $temp.'.'.$request->file('nilai_file')->extension();
		}
		$riwayatPsikologi = RiwayatPsikologi::create([
			'personel_id' => $personelID,
			'psikologi_id' => $psikologi->psikologi_id,
			'nilai' => $request->nilai,
			'nilai_file' => $nilaiFile,
			'tanggal_tes' => $request->tanggal_tes
		]);
		$riwayatPsikologi->psikologi;
		$riwayatPsikologi->personel;
		return response()->json([
			'data' => $riwayatPsikologi
		],200);
	}
	public function hapusRiwayatPsikologi(RiwayatPsikologi $riwayatPsikologi)
	{
		$riwayatPsikologi->hapusFile();
		if($riwayatPsikologi->delete()){
			return response()->json([
				'message' => 'Psikologi berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus psikologi.',
				'status_code' => 400
			],400);
		}
	}

    public function editRiwayatPsikologi(Request $request, RiwayatPsikologi $riwayatPsikologi)
    {
        $error_message = [
            'psikologi_id.required'    => 'Kolom psikologi tidak boleh dikosongkan.',
            'nilai.required'    => 'Kolom nilai tidak boleh dikosongkan.',
            'tanggal_tes.required'    => 'Kolom tanggal tes tidak boleh dikosongkan.',
            'tanggal_tes.date'    => 'format tanggal tes salah.',
        ];
        $validation = Validator::make($request->all(),[
            'psikologi_id' => 'required',
            'nilai' => 'required|numeric',
            'tanggal_tes' => 'required|date',
        ],$error_message);
        if($validation->fails()){
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }

        $personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
        $psikologi = null;
        if($request->psikologi_id == 'other'){
            $psikologi = Psikologi::create(['jenis' => $request->psikologi]);
        } else {
            $psikologi = Psikologi::find($request->psikologi_id);
        }
        if($request->hasFile('nilai_file')){
            $temp = $personelNRP.'-psikologi-nilai-'.str_random(10);
            Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
            $nilaiFile = $temp.'.'.$request->file('nilai_file')->extension();
            $riwayatPsikologi->nilai_file=$nilaiFile;
            $riwayatPsikologi->save();
        }
        $riwayatPsikologi->update([
        	'psikologi_id' => $psikologi->psikologi_id,
            'nilai' => $request->nilai,
            'tanggal_tes' => $request->tanggal_tes,
        ]);
        $riwayatPsikologi->psikologi;
        $riwayatPsikologi->personel;
        return response()->json([
            'data' => $riwayatPsikologi
        ],200);
    }
    public function editPutRiwayatPsikologi(Request $request, RiwayatPsikologi $riwayatPsikologi)
    {
        $error_message = [
            'psikologi_id.required'    => 'Kolom psikologi tidak boleh dikosongkan.',
            'nilai.required'    => 'Kolom nilai tidak boleh dikosongkan.',
            'tanggal_tes.required'    => 'Kolom tanggal tes tidak boleh dikosongkan.',
            'tanggal_tes.date'    => 'format tanggal tes salah.',
        ];
        $validation = Validator::make($request->all(),[
            'psikologi_id' => 'required',
            'nilai' => 'required|numeric',
            'tanggal_tes' => 'required|date',
        ],$error_message);
        if($validation->fails()){
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }

        $personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
        $psikologi = null;
        if($request->psikologi_id == 'other'){
            $psikologi = Psikologi::create(['jenis' => $request->psikologi]);
        } else {
            $psikologi = Psikologi::find($request->psikologi_id);
        }
        if($request->hasFile('nilai_file')){
            $temp = $personelNRP.'-psikologi-nilai-'.str_random(10);
            Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
            $nilaiFile = $temp.'.'.$request->file('nilai_file')->extension();
            $riwayatPsikologi->nilai_file=$nilaiFile;
            $riwayatPsikologi->save();
        }
        $riwayatPsikologi->update([
        	'psikologi_id' => $psikologi->psikologi_id,
            'nilai' => $request->nilai,
            'tanggal_tes' => $request->tanggal_tes,
        ]);
        $riwayatPsikologi->psikologi;
        $riwayatPsikologi->personel;
        return response()->json([
            'data' => $riwayatPsikologi
        ],200);
    }
}