<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Personel;
use App\TandaKehormatan;
use App\TandaKehormatanPersonel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Cache;
use Log;

class TandaKehormatanController extends Controller
{
	public function daftarTandaKehormatan()
	{
		$tandaKehormatan = Cache::remember('daftar_tanda_kehormatan',60,function(){
			return TandaKehormatan::orderBy('nama_tanda_kehormatan')->get();
		});

		return response()->json([
			'data' => $tandaKehormatan
		],200);
	}

	public function tambahTandaKehormatan(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		$personelNRP = $personel->nrp;
	    $error_message = [
			'tanda_kehormatan_id.required' 	=> 'Kolom tanda kehormatan tidak boleh dikosongkan.',
			'tanggal_surat.required' 	=> 'Kolom tanggal surat tidak boleh dikosongkan.',
			'tanggal_surat.date' 	=> 'Format tanggal surat harus berupa yyyy-mm-dd.',
			'surat_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];

		$validation = Validator::make($request->all(),[
			'tanda_kehormatan_id' 	=> 'required',
			'tanggal_surat' 	=> 'required|date',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		if($request->tanda_kehormatan_id == 'other'){
			if($request->tanda_kehormatan == null){
				return response()->json([
					'errors' => 'Kolom tanda kehormatan tidak boleh dikosongkan.'
				],400);
			}
			$tandaKehormatan = TandaKehormatan::create(['nama_tanda_kehormatan' => strtoupper($request->tanda_kehormatan)]);
		} else {
			$tandaKehormatan = TandaKehormatan::find($request->tanda_kehormatan_id);
			if($tandaKehormatan == null){
				return response()->json([
					'errors' => 'Tanda Kehormatan yang dipilih tidak ditemukan.'
				],400);
			}
		}
		$suratFile=null;
		$tandaKehormatanPersonel = TandaKehormatanPersonel::create([
			'tanda_kehormatan_id' => $tandaKehormatan->tanda_kehormatan_id,
			'personel_id' => $personelID,
			'surat_nomor' => strtoupper($request->surat_nomor),
			'tanggal_surat' => $request->tanggal_surat,
		]);
		if($request->hasFile('surat_file')){
			$tandaKehormatanID=$tandaKehormatan->tanda_kehormatan_id;
			$namaTandaKehormatan=str_slug($tandaKehormatan->nama_tanda_kehormatan);
			$random=str_random(5);
	    	$temp = $personelNRP."-tanda_kehormatan_personel-surat_keputusan_file-$tandaKehormatanID-$namaTandaKehormatan-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_file'), $temp.'.'.$request->file('surat_file')->extension());
			$suratFile = $temp.'.'.$request->file('surat_file')->extension();
			$tandaKehormatanPersonel->surat_file=$suratFile;
			$tandaKehormatanPersonel->save();
		}
		$tandaKehormatanPersonel->personel;
		$tandaKehormatanPersonel->tandaKehormatan;

		return response()->json([
			'data' => $tandaKehormatanPersonel
		],200);
	}

    public function editTandaKehormatan(Request $request, TandaKehormatanPersonel $tandaKehormatanPersonel)
    {
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
        $personelID = auth()->user()->personel_id;
        $personel = Personel::find($personelID);
		$personelNRP = $personel->nrp;
        $error_message = [
            'tanda_kehormatan_id.required' 	=> 'Kolom tanda kehormatan tidak boleh dikosongkan.',
            'tanggal_surat.required' 	=> 'Kolom tanggal surat tidak boleh dikosongkan.',
            'tanggal_surat.date' 	=> 'Format tanggal surat harus berupa yyyy-mm-dd.',
			'surat_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
        ];

        $validation = Validator::make($request->all(),[
            'tanda_kehormatan_id' 	=> 'required',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
            'tanggal_surat' 	=> 'required|date',
        ],$error_message);


        if($validation->fails()){
            return response()->json([
                'errors' => $validation->errors(),
                'status_code' => 400
            ], 400);
        }
        if($request->tanda_kehormatan_id == 'other'){
            if($request->tanda_kehormatan == null){
                return response()->json([
                    'errors' => 'Kolom tanda kehormatan tidak boleh dikosongkan.'
                ],400);
            }
            $tandaKehormatan = TandaKehormatan::create(['nama_tanda_kehormatan' => strtoupper($request->tanda_kehormatan)]);
        } else {
            $tandaKehormatan = TandaKehormatan::find($request->tanda_kehormatan_id);
            if($tandaKehormatan == null){
                return response()->json([
                    'errors' => 'Tanda Kehormatan yang dipilih tidak ditemukan.'
                ],400);
            }
        }


        $suratFile=null;
        if($request->hasFile('surat_file')){
			$tandaKehormatanPersonel->hapusFile('surat_file');
			$tandaKehormatanID=$tandaKehormatan->tanda_kehormatan_id;
			$namaTandaKehormatan=str_slug($tandaKehormatan->nama_tanda_kehormatan);
			$random=str_random(5);
	    	$temp = $personelNRP."-tanda_kehormatan_personel-surat_keputusan_file-$tandaKehormatanID-$namaTandaKehormatan-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_file'), $temp.'.'.$request->file('surat_file')->extension());
			$suratFile = $temp.'.'.$request->file('surat_file')->extension();
			$tandaKehormatanPersonel->surat_file=$suratFile;
			$tandaKehormatanPersonel->save();
        }
        $tandaKehormatanPersonel->save();
		$tandaKehormatanPersonel->update([
            'tanda_kehormatan_id' => $tandaKehormatan->tanda_kehormatan_id,
            'surat_nomor' => strtoupper($request->surat_nomor),
            'tanggal_surat' => $request->tanggal_surat,
        ]);

        return response()->json([
            'data' => $tandaKehormatanPersonel
        ],200);
    }
	public function hapusTandaKehormatan(TandaKehormatanPersonel $tandaKehormatanPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Bahasa '. $tandaKehormatanPersonel->tanda_kehormatan_personel_id.' pada URL :'.url()->full());
		$tandaKehormatanPersonel->hapusFile('surat_file');
		if($tandaKehormatanPersonel->delete()){
			return response()->json([
				'message' => 'Tanda kehormatan berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus tanda kehormatan.',
				'status_code' => 400
			],400);
		}
	}
}
