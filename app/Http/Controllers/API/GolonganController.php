<?php

namespace App\Http\Controllers\API;

use App\Golongan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cache;

class GolonganController extends Controller
{
	public function daftarGolongan()
	{
		$daftarGolongan = Cache::remember('daftar_golongan',180,function(){
			return Golongan::get();
		});

		return response()->json([
			'data' => $daftarGolongan
		],200);
	}
}
