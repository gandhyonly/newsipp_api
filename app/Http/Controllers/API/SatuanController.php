<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Satuan;
use App\SatuanJabatan;
use Illuminate\Http\Request;
use Cache;

class SatuanController extends Controller
{
	public function listChild(Request $request)
	{
		$satuanID = $request->satuan_id ?? 1;
		$dataSatuan = Satuan::active()->where('parent_id',$satuanID)->select(['satuan_id','nama_satuan'])->orderBy('nama_satuan')->get();
		return response()->json([
			'data' => $dataSatuan->toArray()
		], 200);
	}
	public function listJabatan($satuanID)
	{
		$dataSatuanJabatan = SatuanJabatan::with('jabatan')->where('satuan_id',$satuanID)->orderBy('jabatan.nama_jabatan')->get();

		return response()->json([
			'data' => $dataSatuanJabatan->toArray()
		], 200);
	}

	public function daftarSatuanPertama()
	{
		$dataSatuan = Satuan::where('parent_id',1)->select(['satuan_id','nama_satuan'])->orderBy('nama_satuan')->get();
		return response()->json([
			'data' => $dataSatuan->toArray()
		], 200);
	}
	public function daftarSatuan(Satuan $satuan)
	{
		$satuanID=$satuan->satuan_id;
		$dataSatuan=Satuan::select(['satuan_id','nama_satuan'])->where('parent_id',$satuan->satuan_id)->orderBy('nama_satuan')->get();
		return response()->json([
			'data' => $dataSatuan->toArray()
		],200);
	}
	public function daftarSatuanAktif(Satuan $satuan)
	{
		$satuanID=$satuan->satuan_id;
		 $dataSatuan=Satuan::select(['satuan_id','nama_satuan'])->where('tipe','STRUKTUR')->whereNull('deleted_by')->where('parent_id',$satuan->satuan_id)->orderBy('nama_satuan')->get();
		return response()->json([
			'data' => $dataSatuan->toArray()
		],200);
	}
	public function daftarJabatan(Satuan $satuan)
	{
		return response()->json([
			'data' => $satuan->jabatan
		],200);
	}
}
