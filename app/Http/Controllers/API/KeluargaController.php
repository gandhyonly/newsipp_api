<?php

namespace App\Http\Controllers\API;

use App\Golongan;
use App\Http\Controllers\Controller;
use App\HubunganKeluarga;
use App\JenisPekerjaan;
use App\KeluargaPersonel;
use App\PekerjaanKeluarga;
use App\Personel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use DB;
use Log;

class KeluargaController extends Controller
{
	public function daftarHubunganKeluarga()
	{
		$hubunganKeluarga = HubunganKeluarga::orderBy('hubungan')->get();

		return response()->json([
			'data' => $hubunganKeluarga
		], 200);
	}
	public function sejarahPekerjaanKeluarga(Request $request)
	{
		$keluargaPersonelID = $request->keluarga_personel_id;
		$keluargaPersonel = KeluargaPersonel::find($keluargaPersonelID);
		if($keluargaPersonel==null){
			return response()->json([
				'error_message' => 'Data Keluarga Not Found.',
				'status_code' => 404,
			], 404);
		}
		$keluargaPersonel->pekerjaanKeluarga;

		return response()->json([
			'data' => $keluargaPersonel
		], 200);
	}
	public function daftarJenisPekerjaan()
	{
		$jenisPekerjaan = JenisPekerjaan::get();

		return response()->json([
			'data' => $jenisPekerjaan
		], 200);
	}
	public function daftarKeluarga(Personel $personel)
	{
		$personel->keluarga;
		return response()->json([
			'data' => $personel
		],200);
	}
	public function tambahKeluarga(Request $request)
	{
		DB::beginTransaction();
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
	    $error_message = [
			'nama_keluarga.required' 	=> 'Kolom nama tidak boleh dikosongkan.',
			'status.required' 			=> 'Kolom status tidak boleh dikosongkan.',
			'jenis_kelamin.required' 	=> 'Kolom jenis kelamin tidak boleh dikosongkan.',
			'tempat_lahir.required' 	=> 'Kolom tempat lahir tidak boleh dikosongkan.',
			'tanggal_lahir.required' 	=> 'Kolom tanggal lahir tidak boleh dikosongkan.',
			'hubungan_keluarga_id.required' 	=> 'Kolom hubungan keluarga tidak boleh dikosongkan.',
			'tanggal_lahir.date' 		=> 'Format tanggal lahir harus berupa yyyy-mm-dd.',
		];

		$validation = Validator::make($request->all(),[
			'nama_keluarga' 		=> 'required',
			'status' 				=> 'required',
			'jenis_kelamin' 		=> 'required',
			'tempat_lahir' 			=> 'required',
			'tanggal_lahir' 		=> 'required|date',
			'hubungan_keluarga_id' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    $jenisPekerjaan=null;
		if(strtolower($request->status)!='meninggal'){
		    $error_message = [
				'telepon.required' 			=> 'Kolom telepon tidak boleh dikosongkan.',
				'alamat.required' 			=> 'Kolom alamat tidak boleh dikosongkan.',
				'jenis_pekerjaan_id.required' 	=> 'Kolom jenis pekerjaan tidak boleh dikosongkan.',
			];

			$validation = Validator::make($request->all(),[
				'telepon' 				=> 'required',
				'alamat' 				=> 'required',
				'jenis_pekerjaan_id' 	=> 'required',
		    ],$error_message);


		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				], 400);
		    }
			if($request->jenis_pekerjaan_id == 'other'){
				if($request->jenis_pekerjaan == null){
					return response()->json([
						'errors' => 'Kolom jenis pekerjaan tidak boleh dikosongkan.'
					],400);
				}
				$jenisPekerjaan = JenisPekerjaan::create(['jenis' => strtoupper($request->jenis_pekerjaan)]);
			} else {
				$jenisPekerjaan = JenisPekerjaan::find($request->jenis_pekerjaan_id);

				if($jenisPekerjaan == null){
					return response()->json([
						'errors' => 'Jenis pekerjaan tidak ditemukan.'
					],400);
				}else if($jenisPekerjaan->jenis_pekerjaan_id==1){
					$golongan = Golongan::find($request->golongan_id);
					if($golongan == null){
						return response()->json([
							'errors' => 'Golongan Pekerjaan tidak ditemukan.'
						],400);
					}
				}
			}
		}

		$hubunganKeluarga = HubunganKeluarga::find($request->hubungan_keluarga_id);
		if($hubunganKeluarga == null){
			return response()->json([
				'errors' => 'Hubungan Keluarga tidak ditemukan.'
			],400);
		}


		$keluargaPersonel = KeluargaPersonel::create([
			'kpis_nomor' => strtoupper($request->kpis_nomor),
			'personel_id' => $personelID,
			'parent_keluarga_personel_id' => $request->parent_keluarga_personel_id,
			'hubungan_keluarga_id' => $request->hubungan_keluarga_id,
			'golongan_id' => $request->golongan_id,
			'personel_keluarga_id' => $request->personel_keluarga_id,
			'status' => strtoupper($request->status),
			'status_personel' => $request->status_personel,
			'tempat_lahir' => strtoupper($request->tempat_lahir),
			'jenis_kelamin' => strtoupper($request->jenis_kelamin),
			'tanggal_nikah' => $request->tanggal_nikah,
			'tanggal_lahir' => $request->tanggal_lahir,
			'nama_keluarga' => strtoupper($request->nama_keluarga),
			'status_nikah' => $request->status_nikah,
			'nrp_keluarga' => $request->nrp_keluarga,
			'buku_nikah_nomor' => strtoupper($request->buku_nikah_nomor),
			'telepon' => $request->telepon,
			'alamat' => strtoupper($request->alamat)
		]);

		$pekerjaanKeluarga = PekerjaanKeluarga::create([
			'jenis_pekerjaan_id' => $jenisPekerjaan==null?null:$jenisPekerjaan->jenis_pekerjaan_id,
			'keluarga_personel_id' => $keluargaPersonel->keluarga_personel_id,
			'nama_institusi' => $request->nama_institusi,
			'telepon' => $request->pekerjaan_telepon,
			'alamat' => $request->pekerjaan_alamat,
			'status_terakhir' => 1,
		]);
		
		$personelNRP = $personel->nrp;
		$random=str_random(5);
		$keluargaPersonelID=$keluargaPersonel->keluarga_personel_id;
	    if($request->hasFile('buku_nikah_file')){
		    $error_message = [
				'buku_nikah_file.mimes' => "Pastikan file berupa jpegg/pdf",
				'buku_nikah_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'buku_nikah_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
	    	$temp="$personelNRP-keluarga_personel-buku_nikah_file-$keluargaPersonelID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('buku_nikah_file'), $temp.'.'.$request->file('buku_nikah_file')->extension());
			$keluargaPersonel->buku_nikah_file = $temp.'.'.$request->file('buku_nikah_file')->extension();
	    }
	    if($request->hasFile('kpis_file')){
		    $error_message = [
				'kpis_file.mimes' => "Pastikan file berupa jpegg/pdf",
				'kpis_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'kpis_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
	    	$temp="$personelNRP-keluarga_personel-kpis_file-$keluargaPersonelID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('kpis_file'), $temp.'.'.$request->file('kpis_file')->extension());
			$keluargaPersonel->kpis_file = $temp.'.'.$request->file('kpis_file')->extension();
	    }
	    if($request->hasFile('foto_file')){
		    $error_message = [
				'foto_file.mimes' => "Pastikan file berupa jpeg",
				'foto_file.max' => "Pastikan file berupa jpeg dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'foto_file' => "nullable|mimes:jpg,jpeg|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
	    	$temp="$personelNRP-keluarga_personel-foto_file-$keluargaPersonelID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('foto_file'), $temp.'.'.$request->file('foto_file')->extension());
			$keluargaPersonel->foto_file = $temp.'.'.$request->file('foto_file')->extension();
	    	$keluargaPersonel->save();
	    }
	    $keluargaPersonel->save();
	    if($keluargaPersonel->hubungan_keluarga_id==3){
	    	$personel->nama_ibu=strtoupper($keluargaPersonel->nama_keluarga);
	    	$personel->save();
	    }
	    DB::commit();
	    $personel->keluarga;
		return response()->json([
			'data' => $personel
		],200);
	}
	public function editKeluarga(Request $request,KeluargaPersonel $keluargaPersonel)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		DB::beginTransaction();
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		if($keluargaPersonel->personel_id != $personelID){
			return response()->json([
				'message' => 'Edit anggota keluarga gagal.',
				'status_code' => '400'
			],400);
		}
	    $error_message = [
			'nama_keluarga.required' 	=> 'Kolom nama tidak boleh dikosongkan.',
			'status.required' 			=> 'Kolom status tidak boleh dikosongkan.',
			'jenis_kelamin.required' 	=> 'Kolom jenis kelamin tidak boleh dikosongkan.',
			'tempat_lahir.required' 	=> 'Kolom tempat lahir tidak boleh dikosongkan.',
			'tanggal_lahir.required' 	=> 'Kolom tanggal lahir tidak boleh dikosongkan.',
			'hubungan_keluarga_id.required' 	=> 'Kolom hubungan keluarga tidak boleh dikosongkan.',
			'tanggal_lahir.date' 		=> 'Format tanggal lahir harus berupa yyyy-mm-dd.',
		];

		$validation = Validator::make($request->all(),[
			'nama_keluarga' 		=> 'required',
			'status' 				=> 'required',
			'jenis_kelamin' 		=> 'required',
			'tempat_lahir' 			=> 'required',
			'tanggal_lahir' 		=> 'required|date',
			'hubungan_keluarga_id' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

	    $jenisPekerjaan=null;
		if(strtolower($request->status)!='meninggal'){
		    $error_message = [
				'telepon.required' 			=> 'Kolom telepon tidak boleh dikosongkan.',
				'alamat.required' 			=> 'Kolom alamat tidak boleh dikosongkan.',
				'jenis_pekerjaan_id.required' 	=> 'Kolom jenis pekerjaan tidak boleh dikosongkan.',
			];

			$validation = Validator::make($request->all(),[
				'telepon' 				=> 'required',
				'alamat' 				=> 'required',
				'jenis_pekerjaan_id' 	=> 'required',
		    ],$error_message);


		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				], 400);
		    }
			if($request->jenis_pekerjaan_id == 'other'){
				if($request->jenis_pekerjaan == null){
					return response()->json([
						'errors' => 'Kolom jenis pekerjaan tidak boleh dikosongkan.'
					],400);
				}
				$jenisPekerjaan = JenisPekerjaan::create(['jenis' => strtoupper($request->jenis_pekerjaan)]);
			} else {
				$jenisPekerjaan = JenisPekerjaan::find($request->jenis_pekerjaan_id);

				if($jenisPekerjaan == null){
					return response()->json([
						'errors' => 'Jenis pekerjaan tidak ditemukan.'
					],400);
				}else if($jenisPekerjaan->jenis_pekerjaan_id==1){
					$golongan = Golongan::find($request->golongan_id);
					if($golongan == null){
						return response()->json([
							'errors' => 'Golongan Pekerjaan tidak ditemukan.'
						],400);
					}
				}
			}
		}
		$hubunganKeluarga = HubunganKeluarga::find($request->hubungan_keluarga_id);
		if($hubunganKeluarga == null){
			return response()->json([
				'errors' => 'Hubungan Keluarga tidak ditemukan.'
			],400);
		}


		$keluargaPersonel->update([
			'kpis_nomor' => strtoupper($request->kpis_nomor),
			'personel_id' => $personelID,
			'parent_keluarga_personel_id' => $request->parent_keluarga_personel_id,
			'hubungan_keluarga_id' => $request->hubungan_keluarga_id,
			'golongan_id' => $request->golongan_id,
			'personel_keluarga_id' => $request->personel_keluarga_id,
			'status_personel' => $request->status_personel,
			'status' => strtoupper($request->status),
			'tempat_lahir' => strtoupper($request->tempat_lahir),
			'jenis_kelamin' => strtoupper($request->jenis_kelamin),
			'tanggal_lahir' => $request->tanggal_lahir,
			'tanggal_nikah' => $request->tanggal_nikah,
			'nama_keluarga' => strtoupper($request->nama_keluarga),
			'buku_nikah_nomor' => strtoupper($request->buku_nikah_nomor),
			'nrp_keluarga' => $request->nrp_keluarga,
			'status_nikah' => $request->status_nikah,
			'telepon' => $request->telepon,
			'alamat' => strtoupper($request->alamat)
		]);
		$keluargaPersonel->pekerjaanKeluarga()->update([
			'jenis_pekerjaan_id' => $jenisPekerjaan ? $jenisPekerjaan->jenis_pekerjaan_id : null,
			'keluarga_personel_id' => $keluargaPersonel->keluarga_personel_id,
			'nama_institusi' => strtoupper($request->nama_institusi),
			'telepon' => $request->pekerjaan_telepon,
			'alamat' => strtoupper($request->pekerjaan_alamat),
			'status_terakhir' => 1,
		]);
		$personelNRP = $personel->nrp;
		$keluargaPersonelID=$keluargaPersonel->keluarga_personel_id;
    	$random=str_random(5);
	    if($request->hasFile('buku_nikah_file')){
		    $error_message = [
				'buku_nikah_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'buku_nikah_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'buku_nikah_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
	    	$temp="$personelNRP-keluarga_personel-buku_nikah_file-$keluargaPersonelID-$random";
	    	$temp = $personelNRP.'-buku-nikah-'.$keluargaPersonel->keluarga_personel_id.str_random(10);
			$storeSuccess = Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('buku_nikah_file'), $temp.'.'.$request->file('buku_nikah_file')->extension());
			if($storeSuccess){
				$keluargaPersonel->buku_nikah_file = $temp.'.'.$request->file('buku_nikah_file')->extension();
			}
	    }
	    if($request->hasFile('kpis_file')){
		    $error_message = [
				'kpis_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'kpis_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'kpis_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
	    	$temp="$personelNRP-keluarga_personel-kpis_file-$keluargaPersonelID-$random";
			$storeSuccess = Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('kpis_file'), $temp.'.'.$request->file('kpis_file')->extension());
			if($storeSuccess){
				$keluargaPersonel->kpis_file = $temp.'.'.$request->file('kpis_file')->extension();
			}
	    }
	    if($request->hasFile('foto_file')){
		    $error_message = [
				'foto_file.mimes' => "Pastikan file berupa jpeg",
				'foto_file.max' => "Pastikan file berupa jpeg dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'foto_file' => "nullable|mimes:jpg,jpeg|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
	    	$temp="$personelNRP-keluarga_personel-foto_file-$keluargaPersonelID-$random";
			$storeSuccess = Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('foto_file'), $temp.'.'.$request->file('foto_file')->extension());
			if($storeSuccess){
				$keluargaPersonel->foto_file = $temp.'.'.$request->file('foto_file')->extension();
				$keluargaPersonel->save();
			}
	    }
		if($request->status_nikah == 0){
			$keluargaPersonel->buku_nikah_nomor=null;
			$keluargaPersonel->buku_nikah_file=null;
			$keluargaPersonel->kpis_nomor=null;
			$keluargaPersonel->kpis_file=null;
			$keluargaPersonel->save();
		}
	    $keluargaPersonel->save();
	    DB::commit();
	    $personel->keluarga;
		return response()->json([
			'data' => $personel
		],200);
	}
	public function hapusKeluarga(KeluargaPersonel $keluargaPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel=Personel::find($personelID);
		$personelNRP = $personel->nrp;
		Log::info($personel->nrp.' : Hapus Daftar Keluarga '. $keluargaPersonel->keluarga_personel_id.' pada URL :'.url()->full());
		if($keluargaPersonel->personel_id == $personelID){
			if($keluargaPersonel->kpis_file != null){
				$fileLocation = 'personel/'.$personelNRP.'/'.$keluargaPersonel->kpis_file;
				$fileExist = Storage::disk('local')->exists($fileLocation);
				if($fileExist){
					Storage::disk('local')->delete($fileLocation);
				}
			}
			if($keluargaPersonel->foto_file != null){
				$fileLocation = 'personel/'.$personelNRP.'/'.$keluargaPersonel->foto_file;
				$fileExist = Storage::disk('local')->exists($fileLocation);
				if($fileExist){
					Storage::disk('local')->delete($fileLocation);
				}
			}
			if($keluargaPersonel->buku_nikah_file != null){
				$fileLocation = 'personel/'.$personelNRP.'/'.$keluargaPersonel->buku_nikah_file;
				$fileExist = Storage::disk('local')->exists($fileLocation);
				if($fileExist){
					Storage::disk('local')->delete($fileLocation);
				}
			}
			$keluargaPersonel->delete();
			return response()->json([
				'message' => 'Hapus Anggota Keluarga berhasil.',
				'status_code' => '200'
			],200);
		} else {
			return response()->json([
				'message' => 'Hapus anggota keluarga gagal.',
				'status_code' => '400'
			],400);
		}
	}
}
