<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\JenisPekerjaan;
use Illuminate\Http\Request;
use Cache;

class JenisPekerjaanController extends Controller
{
	public function daftarJenisPekerjaan()
	{

		$daftarJenisPekerjaan = Cache::remember('daftar_jenis_pekerjaan',180,function(){
			return JenisPekerjaan::get();
		});

		return response()->json([
			'data' => $daftarJenisPekerjaan
		],200);
	}
}
