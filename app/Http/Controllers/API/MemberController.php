<?php

namespace App\Http\Controllers\API;

use App\Fisik;
use App\Helpdesk;
use App\Http\Controllers\Controller;
use App\Member;
use App\Personel;
use App\PersonelData;
use App\Satuan;
use App\StatusForm;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;

class MemberController extends Controller
{
	public function checkNRP(Request $request)
	{    
		$test=[];
	    $error_message = [
		    'nrp.required'    => 'Kolom NRP lahir tidak boleh dikosongkan.',
		    'nrp.numeric'    => 'Kolom nrp harus berupa angka.',
		    'tanggal_lahir.required'    => 'Kolom tanggal lahir tidak boleh dikosongkan.',
		    'satker_pertama.required'    => 'Kolom satker pertama tidak boleh dikosongkan.',
		    'nama_ibu.required'    => 'Kolom nama ibu tidak boleh dikosongkan.',
		    'tmt_jabatan_pertama.required'    => 'Kolom tmt jabatan pertama tidak boleh dikosongkan.',
		    'satker_pertama.required'    => 'Kolom satker pertama tidak boleh dikosongkan.',
		];
		$validation = Validator::make($request->all(),[
	        'nrp' => 'required|numeric',
	        'tanggal_lahir' => 'required|date',
	        'satker_pertama' => 'required',
	        'nama_ibu' => 'required',
	        'tmt_jabatan_pertama' => 'required|date',
	        'satker_pertama' => 'required',
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			]);
	    }


		$namaIbu=$request->nama_ibu;

		$data = [
			'nrp' => $request->nrp,
			'tanggal_lahir' => $request->tanggal_lahir,
			'nama_ibu' => $namaIbu,
			'satker_pertama' => $request->satker_pertama,
			'tmt_jabatan_pertama' => $request->tmt_jabatan_pertama,
		];
	    // Check NRP
	    // 1001 = NRP DITEMUKAN
	    // 1002 = NRP TIDAK DITEMUKAN
	    // 1003 = NRP GANDA

    	if(Member::where('nrp',$request->nrp)->count()>0){
	    	$status='Anda sudah terdaftar. Silahkan Login.';
	    	$status_code=5;

			return response()->json([
				'status' => $status,
				'status_code' => $status_code,
				'data' => $data,
			], 200);
    	} else if(Personel::where('nrp',$request->nrp)->count()>1){
	    	$status='NRP GANDA';
	    	$status_code=3;

			return response()->json([
				'status' => $status,
				'status_code' => $status_code,
				'data' => $data,
			], 200);
	    } elseif(Personel::where('nrp',$request->nrp)->count()==1){
	    	$personel = Personel::where('nrp', $request->nrp)->first();
	    	$date=$personel->tanggal_lahir?$personel->tanggal_lahir->format('Y-m-d'):null;
	    	$dateTMT=$personel->tmt_pertama?$personel->tmt_pertama->format('Y-m-d'):null;
	    	$dateTMTJabatan=$personel->tmt_jabatan_pertama?$personel->tmt_jabatan_pertama->format('Y-m-d'):null;
	    	if(strlen($request->nama_ibu)<5){
		    	$namaIbuMatch = strtolower($request->nama_ibu) == strtolower($personel->nama_ibu);
	    	} else {
		    	$namaIbuMatch = preg_match("/".strtolower(substr($request->nama_ibu,0,5))."/i",strtolower($personel->nama_ibu));
	    	}
	    	if($namaIbuMatch){
	    		$namaIbu=$personel->nama_ibu;
	    	}
	    	//
	    	if($date==$request->tanggal_lahir){
		    	if($namaIbuMatch || $dateTMT == $request->tmt_jabatan_pertama || $dateTMTJabatan == $request->tmt_jabatan_pertama){
			    	$status='NRP DITEMUKAN';
			    	$status_code=1;

					return response()->json([
						'status' => $status,
						'status_code' => $status_code,
						'data' => $data,
					], 200);
		    	} else {
		    		$status_code=4;
			    	$status='Nama Ibu/tmt pertama salah.';

					return response()->json([
						'status' => $status,
						'status_code' => $status_code,
						'data' => $data,
					], 200);
		    	}
	    	} else {
		    	$status='NRP DITEMUKAN TAPI INFORMASI SALAH.';
		    	$status_code=2;
				return response()->json([
					'status' => $status,
					'status_code' => $status_code,
					'data' => $data,
				], 200);
	    	}
	    } else {
	    	$status='NRP TIDAK DITEMUKAN.';
	    	$status_code=2;
			return response()->json([
				'status' => $status,
				'status_code' => $status_code,
				'data' => $data,
			], 200);
	    }
	}


	public function testingPost(Request $request)
	{
		return response()->json([
			'data' => $request->all()
		],200);
	}

	public function registrasi(Request $request)
	{
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'nrp.numeric'    => 'Kolom nrp harus berupa angka.',
		    'nrp.unique'    => 'NRP sudah terdaftar.',
		    'password.required'    => 'Kolom kata kunci tidak boleh dikosongkan.',
		    'password_confirmation.required'    => 'Kolom konfirmasi kata kunci  tidak boleh dikosongkan.',
		    'password_confirmation.same'    => 'Konfirmasi password salah.',
		    'satuan_3.required'    => 'Kolom satuan tidak boleh dikosongkan.',
		    'satuan_2.required'    => 'Kolom satuan tidak boleh dikosongkan.',
		    'satuan_1.required'    => 'Kolom satuan tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' => 'required|unique:members|numeric',
	        'password' => 'required',
	        'satuan_3' => 'required',
	        'satuan_2' => 'required',
	        'satuan_1' => 'required',
	        'password_confirmation' => 'required|same:password',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

	    $personel = Personel::where('nrp',$request->nrp)->first();
	    if($personel==null){
			return response()->json([
				'errors' => 'NRP tidak terdaftar.'
			], 400);
	    } else {
		    DB::beginTransaction();
		    if($personel->data == null){
       			PersonelData::create(['personel_id' => $personel->personel_id]);
		    }
		    if($personel->fisik==null){
		        Fisik::create(['personel_id' => $personel->personel_id]);
		    }
		    $member = Member::create($request->only('nrp','email','handphone','nama_ibu','satker_pertama','tanggal_lahir','satuan_3','satuan_2','satuan_1'));
		    $member->satuan_4=1;
		    $member->api_token=str_random(60);
		    $member->password=bcrypt($request->password);
		    $member->tmt_jabatan_pertama=$personel->tmt_jabatan_pertama;
		    $member->personel_id = $personel->personel_id;
		    $member->save();
	        $member->newStatusForm();
		    $member->satker_sekarang=Satuan::find($member->satuan_3)->nama_satuan.' > '.Satuan::find($member->satuan_2)->nama_satuan.' > '.Satuan::find($member->satuan_1)->nama_satuan;
		    $member->save();
	        DB::commit();

		    


			return response()->json([
				'data' => $member,
				'personel' => $personel
			], 200);
	    }
	}

	public function login(Request $request)
	{
		DB::disableQueryLog();
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'nrp.numeric'    => 'Kolom nrp harus berupa angka.',
		    'password.required'    => 'Kolom kata kunci tidak boleh dikosongkan.',
		    'tanggal_lahir.required'    => 'Kolom tanggal lahir tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' => 'required|numeric',
			'password' => 'required',
			'tanggal_lahir' => 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    if(auth('member')->attempt(['nrp'=>$request->nrp,'password' => $request->password,'tanggal_lahir' => $request->tanggal_lahir])){
	    	$member=Member::find(auth('member')->user()->member_id);
		    $member->satker_sekarang=Satuan::find($member->satuan_3)->nama_satuan.' > '.Satuan::find($member->satuan_2)->nama_satuan.' > '.Satuan::find($member->satuan_1)->nama_satuan;
		    $member->save();

			$personelID = auth('member')->user()->personel_id;
			return response()->json([
				'token' => auth('member')->user()->api_token,
				'nrp' => $request->nrp,
				'personel_id' => $personelID,
				'pangkat' => 'KOMPOL', #NOTE
				'satker_sekarang' => $member->satker_sekarang
			], 200);
	    } else{
	    	if(Member::where('nrp',$request->nrp)->first()){
				return response()->json([
					'message' => 'NRP/NIP, Password atau Tanggal Lahir anda salah.',
					'status_code' => 400
				],400);
	    	} else {
				return response()->json([
					'message' => 'NRP/NIP anda belum terdaftar',
					'status_code' => 400
				],400);	
	    	}
	    }
	}

	public function registrasiDataTidakDitemukan(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		DB::disableQueryLog();
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'nrp.numeric'    => 'Kolom nrp harus berupa angka.',
		    'nama_ibu.required'    => 'Kolom nama ibu tidak boleh dikosongkan.',
		    'tanggal_lahir.required'    => 'Kolom tanggal lahir tidak boleh dikosongkan.',
		    'tanggal_lahir.date'    => 'Kolom tanggal lahir bukan berupa tanggal.',
		    'tmt_jabatan_pertama.required'    => 'Kolom tmt jabatan pertama tidak boleh dikosongkan.',
		    'tmt_jabatan_pertama.date'    => 'Kolom tmt jabatan pertama bukan berupa tanggal.',
		    'satker_pertama.required'    => 'Kolom satker pertama tidak boleh dikosongkan.',
		    'riwayat_hidup.required'    => 'Data riwayat hidup tidak diinput.',
			'riwayat_hidup.mimes' => "Pastikan file berupa jpeg/pdf",
			'riwayat_hidup.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		    'petikan_pertama.required'    => 'Data petikan pertama tidak diinput.',
			'petikan_pertama.mimes' => "Pastikan file berupa jpeg/pdf",
			'petikan_pertama.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		    'handphone.numeric'    => 'Kolom nomor hp harus berupa angka.',
		    'satker_sekarang_id.required'    => 'Kolom satker sekarang tidak boleh dikosongkan.',
		    'pangkat_id.required'    => 'Kolom pangkat tidak boleh dikosongkan.',
		    'nama_lengkap.required'    => 'Kolom nama lengkap tidak boleh dikosongkan.',
		    'tmt_jabatan_terakhir.required'    => 'Kolom tmt jabatan terakhir tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' => 'required|numeric',
			'nama_ibu' => 'required',
			'tanggal_lahir' => 'required|date',
			'tmt_jabatan_pertama' => 'required|date',
			'satker_pertama' => 'required',
			'riwayat_hidup' => "required|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
			'petikan_pertama' => "required|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
	        'handphone' => 'numeric',
			'satker_sekarang_id' => 'required',
			'pangkat_id' => 'required',
			'nama_lengkap' => 'required',
			'tmt_jabatan_terakhir' => 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    $satuan = Satuan::find($request->satker_sekarang_id);
	    if($satuan == null){
			return response()->json([
				'errors' => 'Satuan Tidak Ditemukan.',
				'status_code' => 400
			], 400);
	    }
	    $helpdesk = Helpdesk::where('handphone',$request->handphone)->where('nrp',$request->nrp)->where('satker_pertama',$request->satker_pertama)->first();
	    if($helpdesk){
			return response()->json([
				'errors' => 'Data anda sdh masuk helpdesk. Mohon tunggu konfirmasi dari helpdesk.',
				'status_code' => 400
			], 400);
	    }
	    DB::beginTransaction();
	    $helpdesk = Helpdesk::create($request->only(['nrp','nama_ibu','tanggal_lahir','tmt_jabatan_pertama','satker_pertama','email','handphone','satker_sekarang_id','pangkat_id','nama_lengkap','tmt_jabatan_terakhir']) + ['status' => 'NOTFOUND']);
		$helpdesk->satuan_sekarang=$helpdesk->satuanSekarang->fullPrint();
		$helpdesk->save();

	    $satuanIDTerkecil = Satuan::satuanTerkecil($request->satker_sekarang_id);
	    $arraySatuan = Satuan::fullArraySatuan($satuanIDTerkecil);
    	$helpdesk->email_from=Helpdesk::emailFrom($arraySatuan[1]);
    	$helpdesk->save();
		$temp='petikan-pertama-'.str_random(10);
	    if($request->hasFile('petikan_pertama')){
			$helpdesk->petikan_pertama = Storage::putFileAs('file/',$request->file('petikan_pertama'), $helpdesk->id.$temp.'.'.$request->file('petikan_pertama')->extension());
			$helpdesk->petikan_pertama = $helpdesk->id.$temp.'.'.$request->file('petikan_pertama')->extension();
	    }
		$temp='riwayat-hidup-'.str_random(10);
	    if($request->hasFile('riwayat_hidup')){
			$helpdesk->riwayat_hidup = Storage::putFileAs('file/',$request->file('riwayat_hidup'), $helpdesk->id.$temp.'.'.$request->file('riwayat_hidup')->extension());
			$helpdesk->riwayat_hidup = $helpdesk->id.$temp.'.'.$request->file('riwayat_hidup')->extension();
		}
		$helpdesk->save();
		DB::commit();



		return response()->json([
			'data' => $helpdesk
		], 200);
	}
	public function registrasiDataDuplikat(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		DB::disableQueryLog();
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'nrp.numeric'    => 'Kolom nrp harus berupa angka.',
		    'nama_ibu.required'    => 'Kolom nama ibu tidak boleh dikosongkan.',
		    'tanggal_lahir.required'    => 'Kolom tanggal lahir tidak boleh dikosongkan.',
		    'tanggal_lahir.date'    => 'Kolom tanggal lahir bukan berupa tanggal.',
		    'satker_pertama.required'    => 'Kolom satker pertama tidak boleh dikosongkan.',
		    'handphone.numeric'    => 'Kolom nomor hp hanya berupa angka.',
		    'riwayat_hidup.required'    => 'Data riwayat hidup tidak diinput.',
			'riwayat_hidup.mimes' => "Pastikan file berupa jpeg/pdf",
			'riwayat_hidup.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		    'petikan_pertama.required'    => 'Data petikan pertama tidak diinput.',
			'petikan_pertama.mimes' => "Pastikan file berupa jpeg/pdf",
			'petikan_pertama.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		    'satker_sekarang_id.required'    => 'Kolom satker sekarang tidak boleh dikosongkan.',
		    'pangkat_id.required'    => 'Kolom pangkat tidak boleh dikosongkan.',
		    'nama_lengkap.required'    => 'Kolom nama lengkap tidak boleh dikosongkan.',
		    'tmt_jabatan_terakhir.required'    => 'Kolom tmt jabatan terakhir tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' => 'required|numeric',
			'nama_ibu' => 'required',
			'tanggal_lahir' => 'required|date',
			'tmt_jabatan_pertama' => 'required|date',
			'riwayat_hidup' => "required|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
			'petikan_pertama' => "required|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
			'satker_pertama' => 'required',
	        'handphone' => 'numeric',
			'satker_sekarang_id' => 'required',
			'pangkat_id' => 'required',
			'nama_lengkap' => 'required',
			'tmt_jabatan_terakhir' => 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    $satuan = Satuan::find($request->satker_sekarang_id);
	    if($satuan == null){
			return response()->json([
				'errors' => 'Satuan Tidak Ditemukan.',
				'status_code' => 400
			], 400);
	    }
	    $helpdesk = Helpdesk::where('handphone',$request->handphone)->where('nrp',$request->nrp)->where('satker_pertama',$request->satker_pertama)->first();
	    
	    if($helpdesk){
			return response()->json([
				'errors' => 'Data anda sdh masuk helpdesk. Mohon tunggu konfirmasi dari helpdesk.',
				'status_code' => 400
			], 400);
	    }
	    DB::beginTransaction();
	    $helpdesk = Helpdesk::create($request->only(['nrp','nama_ibu','tanggal_lahir','tmt_jabatan_pertama','satker_pertama','email','handphone','satker_sekarang_id','pangkat_id','nama_lengkap','tmt_jabatan_terakhir']) + ['status' => 'DUPLICATE']);
		$helpdesk->satuan_sekarang=$helpdesk->satuanSekarang->fullPrint();
		$helpdesk->save();

	    $satuanIDTerkecil = Satuan::satuanTerkecil($request->satker_sekarang_id);
	    $arraySatuan = Satuan::fullArraySatuan($satuanIDTerkecil);
    	$helpdesk->email_from=Helpdesk::emailFrom($arraySatuan[1]);
    	$helpdesk->save();
		$temp='petikan-pertama-'.str_random(10);
	    if($request->hasFile('petikan_pertama')){
			Storage::putFileAs('file/',$request->file('petikan_pertama'), $helpdesk->id.$temp.'.'.$request->file('petikan_pertama')->extension());
			$helpdesk->petikan_pertama = $helpdesk->id.$temp.'.'.$request->file('petikan_pertama')->extension();
		}
		$temp='riwayat-hidup-'.str_random(10);
	    if($request->hasFile('riwayat_hidup')){
			$helpdesk->riwayat_hidup = Storage::putFileAs('file/',$request->file('riwayat_hidup'), $helpdesk->id.$temp.'.'.$request->file('riwayat_hidup')->extension());
			$helpdesk->riwayat_hidup = $helpdesk->id.$temp.'.'.$request->file('riwayat_hidup')->extension();
		}
		$helpdesk->save();
		DB::commit();

		return response()->json([
			'data' => $helpdesk,
		], 200);
	}

	public function getStatusForm()
	{
		$member = Member::find(auth()->user()->member_id);
		$member->statusForm;
		return response()->json([
			'data' => $member,
		], 200);
	}

	public function ajukanVerifikator()
	{
		DB::disableQueryLog();
		$member = auth()->user();
		$member->updated=true;
		$member->save();

		foreach(StatusForm::get() as $statusForm){
			$member->statusForm()->syncWithoutDetaching([$statusForm->status_form_id => ['keterangan' => null]]);
		}
        return response()->json([
            'data' => [ 'message' => 'Berhasil diajukan ke Verifikator.' ]
        ],200);
	}

	public function gantiPassword(Request $request)
	{
		$memberID = auth('api')->user()->member_id;
		$member = Member::find($memberID);
	    $error_message = [
		    'old_password.required'    => 'Kolom kata kunci lama tidak boleh dikosongkan.',
		    'password.required'    => 'Kolom kata kunci tidak boleh dikosongkan.',
		    'password_confirmation.required'    => 'Kolom konfirmasi kata kunci  tidak boleh dikosongkan.',
		    'password_confirmation.same'    => 'Konfirmasi password lama anda tidak sama.',
		];

		$validation = Validator::make($request->all(),[
	        'old_password' => 'required',
	        'password' => 'required',
	        'password_confirmation' => 'required|same:password',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    if(Hash::check($request->old_password, $member->password)){
			$member->password=bcrypt($request->password);
			$member->save();
	        return response()->json([
	            'data' => [ 'message' => 'Ganti password berhasil.' ]
	        ],200);		
	    }else {
	        return response()->json([
	            'data' => [ 'message' => 'Password anda salah.' ]
	        ],400);		
	    }
	}

	public function lupaPassword(Request $request)
	{
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'nrp.numeric'    => 'Kolom nrp harus berupa angka.',
		    'nama_ibu.required'    => 'Kolom nama ibu tidak boleh dikosongkan.',
		    'tanggal_lahir.required'    => 'Kolom tanggal lahir tidak boleh dikosongkan.',
		    'tanggal_lahir.date'    => 'Kolom tanggal lahir bukan berupa tanggal.',
		    'tmt_jabatan_pertama.required'    => 'Kolom tmt jabatan pertama tidak boleh dikosongkan.',
		    'tmt_jabatan_pertama.date'    => 'Kolom tmt jabatan pertama bukan berupa tanggal.',
		    'satker_pertama.required'    => 'Kolom satker pertama tidak boleh dikosongkan.',
		];
		$validation = Validator::make($request->all(),[
			'nrp' => 'required|numeric',
			'nama_ibu' => 'required',
			'tanggal_lahir' => 'required|date',
			'tmt_jabatan_pertama' => 'required|date',
			'satker_pertama' => 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    $member = Member::where('nrp',$request->nrp)->first();
	    if($member==null){
			return response()->json([
				'errors' => 'NRP tidak ditemukan.',
				'status_code' => 400
			], 400);
	    } else {
    		$memberTanggalLahir=$member->tanggal_lahir->format('Y-m-d');
    		$tglLahir=$request->tanggal_lahir;

	    	if(strlen($request->nama_ibu)<5){
		    	$namaIbuMatch = strtolower($request->nama_ibu) == strtolower($member->nama_ibu);
	    	} else {
		    	$namaIbuMatch = preg_match("/".strtolower($request->nama_ibu)."/i",strtolower($member->nama_ibu));
	    	}

	    	$tmtJabatanMatch=false;
	    	if($member->tmt_jabatan_pertama!=null){
	    		if($member->tmt_jabatan_pertama->format('Y-m-d') == $request->tmt_jabatan_pertama){
	    			$tmtJabatanMatch=true;
	    		}
	    	}
	    	$tmtPertamaMatch=false;
	    	if($member->tmt_pertama!=null){
	    		if($member->tmt_pertama->format('Y-m-d') == $request->tmt_jabatan_pertama){
	    			$tmtPertamaMatch=true;
	    		}
	    	}
	    	if($namaIbuMatch && $memberTanggalLahir==$tglLahir && ($tmtJabatanMatch || $tmtPertamaMatch) && strtolower($member->satker_pertama) == strtolower($request->satker_pertama) ){
	    		$member->password=bcrypt($member->tanggal_lahir->format('dmY'));
	    		$member->save();
				return response()->json([
					'data' => 'Reset password berhasil. Password anda adalah '.$member->tanggal_lahir->format('dmY'),
				], 200);
	    	} else {
				return response()->json([
					'errors' => 'Pastikan data anda benar.',
					'status_code' => 400
				], 400);
	    	}

	    }
	}
	public function resetSatuanVerifikator(Request $request)
	{
		$member = Member::find(auth()->user()->member_id);
	    $error_message = [
		    'satuan_1.required'    => 'Kolom satuan tidak boleh dikosongkan.',
		    'satuan_2.required'    => 'Kolom satuan tidak boleh dikosongkan.',
		    'satuan_3.required'    => 'Kolom satuan tidak boleh dikosongkan.',
		];
		$validation = Validator::make($request->all(),[
			'satuan_1' => 'required',
			'satuan_2' => 'required',
			'satuan_3' => 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

	    $member->satuan_1=$request->satuan_1;
	    $member->satuan_2=$request->satuan_2;
	    $member->satuan_3=$request->satuan_3;
	    $member->satker_sekarang=Satuan::find($member->satuan_3)->nama_satuan.' > '.Satuan::find($member->satuan_2)->nama_satuan.' > '.Satuan::find($member->satuan_1)->nama_satuan;
	    $member->save();
	    $member->save();

		return response()->json([
			'satuan_sekarang' => $member->satker_sekarang,
			'data' => 'Reset data satuan verifikator berhasil',
		], 200);
	}
}
