<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Kesehatan;
use App\KesehatanPersonel;
use App\Personel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class KesehatanController extends Controller
{
	public function daftarKesehatan()
	{
		return response()->json([
			'data' => Kesehatan::get()
		],200);
	}
	public function tambahKesehatan(Request $request)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			'diagnosa.required' 		=> 'Kolom diagnosa tidak boleh dikosongkan.',
			'tanggal_tes.required' 	=> 'Kolom tanggal tes tidak boleh dikosongkan.',
			'tanggal_tes.date' 	=> 'Format tanggal tes harus berupa tanggal.',
			'kesehatan_id.required' 	=> 'Kolom kesehatan tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'diagnosa' 		=> 'required',
			'tanggal_tes' 				=> 'required|date',
			'kesehatan_id' 		=> 'required',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$kesehatanID = $request->kesehatan_id;
		if($request->kesehatan_id == 'other'){
			$kesehatan = Kesehatan::create(['jenis' => $request->jenis]);
			$kesehatanID= $kesehatan->kesehatan_id;
		} else {
			$kesehatan = Kesehatan::find($kesehatanID);
		}
		$personel = Personel::find($personelID);
		$fileName=null;
		$personelNRP = $personel->nrp;
		
		if($request->hasFile('file')){
	    	$temp = $personelNRP.'-kesehatan-'.str_random(10);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('file'), $temp.'.'.$request->file('file')->extension());
			$fileName = $temp.'.'.$request->file('file')->extension();
		}
		$kesehatanPersonel = KesehatanPersonel::create([
			'personel_id' => $personelID,
			'kesehatan_id' => $kesehatan->kesehatan_id,
			'diagnosa' => $request->diagnosa,
			'file' => $fileName,
			'tanggal_tes' => $request->tanggal_tes,
		]);
		$kesehatanPersonel->kesehatan;
		$kesehatanPersonel->personel;
		return response()->json([
			'data' => $kesehatanPersonel
		],200);
	}
	public function hapusKesehatan(KesehatanPersonel $kesehatanPersonel)
	{
		if($kesehatanPersonel->delete()){
			return response()->json([
				'message' => 'Kesehatan personel berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus kesehatan personel.',
				'status_code' => 400
			],400);
		}
	}

    public function editKesehatan(Request $request, KesehatanPersonel $kesehatanPersonel)
    {

        $error_message = [
            'diagnosa.required' 		=> 'Kolom diagnosa tidak boleh dikosongkan.',
            'tanggal_tes.required' 	=> 'Kolom tanggal tes tidak boleh dikosongkan.',
            'tanggal_tes.date' 	=> 'Format tanggal tes harus berupa tanggal.',
            'kesehatan_id.required' 	=> 'Kolom kesehatan tidak boleh dikosongkan.',
        ];

        $validation = Validator::make($request->all(),[
            'diagnosa' 		=> 'required',
            'tanggal_tes' 				=> 'required|date',
            'kesehatan_id' 		=> 'required',
        ],$error_message);

        if($validation->fails()){
            return response()->json([
                'errors' => $validation->errors()
            ]);
        }


        $personel = Personel::find(auth()->user()->personel_id);

        $kesehatanID = $request->kesehatan_id;
        if($request->kesehatan_id == 'other'){
            $kesehatan = kesehatan::create(['jenis' => $request->kesehatan]);
            $kesehatanID= $kesehatan->kesehatan_id;
        } else {
            $kesehatan = kesehatan::find($kesehatanID);
        }

        $kesehatanPersonel->update([
        	'kesehatan_id' => $request->kesehatan_id,
            'diagnosa' => $request->diagnosa,
            'tanggal_tes' => $request->tanggal_tes,
        ]);
		$personelNRP = $personel->nrp;
        if($request->hasFile('file')){
			if($kesehatanPersonel->file != null){
				$fileLocation = 'personel/'.$personelNRP.'/'.$kesehatanPersonel->file;
				$fileExist = Storage::disk('local')->exists($fileLocation);
				if($fileExist){
					Storage::disk('local')->delete($fileLocation);
				}
			}
            $temp = $personelNRP.'-kesehatan-diagnosa-'.str_random(10);
            Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('file'), $temp.'.'.$request->file('file')->extension());
            $nilaiFile = $temp.'.'.$request->file('file')->extension();
        }
        $kesehatanPersonel->kesehatan;
        $kesehatanPersonel->personel;
        return response()->json([
            'data' => $kesehatanPersonel
        ],200);
    }
}
