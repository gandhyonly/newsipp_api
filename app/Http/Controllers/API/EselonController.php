<?php

namespace App\Http\Controllers\API;

use App\Eselon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EselonController extends Controller
{
	public function daftarEselon()
	{
		return response()->json([
			'data' => Eselon::get()
		],200);
	}
}
