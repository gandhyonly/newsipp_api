<?php

namespace App\Http\Controllers\API;

use App\Brevet;
use App\Http\Controllers\Controller;
use App\Personel;
use App\RiwayatBrevet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Cache;
use DB;
use Log;

class BrevetController extends Controller
{
	public function daftarBrevet()
	{
		$daftarBrevet = Cache::remember('daftar_brevet',60,function(){
			return Brevet::orderBy('nama_brevet')->get();
		});
		return response()->json([
			'data' => $daftarBrevet
		],200);
	}

	public function tambahRiwayatBrevet(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
        $personelID = auth()->user()->personel_id;
		$random = str_random(5);
	    $error_message = [
			'brevet_id.required' 		=> 'Kolom brevet tidak boleh dikosongkan.',
			'tmt_brevet.required' 	=> 'Kolom tmt brevet tidak boleh dikosongkan.',
			'tmt_brevet.date' 	=> 'Format tmt brevet harus berupa tanggal.',
			'surat_keputusan_nomor.required' 		=> 'Kolom nomor surat keputusan tidak boleh dikosongkan.',
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];

		$validation = Validator::make($request->all(),[
			'brevet_id' 			=> 'required',
			'tmt_brevet' 	=> 'required|date',
			'surat_keputusan_nomor' 	=> 'required',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		if($request->brevet_id == 'other'){
		    $error_message = [
				'nama_brevet.required' 		=> 'Kolom nama brevet tidak boleh dikosongkan.',
				'asal_brevet.required' 		=> 'Kolom asal brevet tidak boleh dikosongkan.',
			];

			$validation = Validator::make($request->all(),[
				'nama_brevet' 			=> 'required',
				'asal_brevet' 	=> 'required',
		    ],$error_message);

		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				], 400);
		    }
			$brevet = Brevet::create(['nama_brevet' => strtoupper($request->nama_brevet),'asal_brevet' => strtoupper($request->asal_brevet)]);
		} else {
			$brevet = Brevet::find($request->brevet_id);
		}
		$personel = Personel::find($personelID);
		$personelNRP = $personel->nrp;
		$random=str_random(5);
		$suratKeputusanFile=null;
		$riwayatBrevet = RiwayatBrevet::create([
			'personel_id' => $personelID,
			'brevet_id'		=> $brevet->brevet_id,
			'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
			'surat_keputusan_file' => $suratKeputusanFile,
			'tmt_brevet' => $request->tmt_brevet,
		]);
		if($request->hasFile('surat_keputusan_file')){
			$brevetID=$brevet->brevet_id;
			$temp = "$personelNRP-brevet_personel-surat_keputusan_file-$brevetID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
			$suratKeputusanFile = $temp.'.'.$request->file('surat_keputusan_file')->extension();
			$riwayatBrevet->surat_keputusan_file=$suratKeputusanFile;
			$riwayatBrevet->save();
		}
		$riwayatBrevet->brevet;
		$riwayatBrevet->personel;
		return response()->json([
			'data' => $riwayatBrevet
		],200);
	}

    public function editRiwayatBrevet(Request $request, RiwayatBrevet $riwayatBrevet)
    {
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$random = str_random(5);
        $personelID = auth()->user()->personel_id;
        $error_message = [
            'brevet_id.required' 		=> 'Kolom brevet tidak boleh dikosongkan.',
            'tmt_brevet.required' 	=> 'Kolom tmt brevet penghargaan tidak boleh dikosongkan.',
            'tmt_brevet.date' 	=> 'Format tmt brevet penghargaan harus berupa tanggal.',
            'surat_keputusan_nomor.required' 		=> 'Kolom nomor surat keputusan tidak boleh dikosongkan.',
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
        ];

        $validation = Validator::make($request->all(),[
            'brevet_id' 			=> 'required',
            'tmt_brevet' 	=> 'required|date',
            'surat_keputusan_nomor' 	=> 'required',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
        ],$error_message);



        if($validation->fails()){
            return response()->json([
                'errors' => $validation->errors(),
                'status_code' => 400
            ], 400);
        }

        $personel = Personel::find($personelID);

        if($request->brevet_id == 'other'){
            $error_message = [
                'nama_brevet.required' 		=> 'Kolom nama brevet tidak boleh dikosongkan.',
                'asal_brevet.required' 		=> 'Kolom asal brevet tidak boleh dikosongkan.',
            ];

            $validation = Validator::make($request->all(),[
                'nama_brevet' 			=> 'required',
                'asal_brevet' 	=> 'required',
            ],$error_message);

            if($validation->fails()){
                return response()->json([
                    'errors' => $validation->errors(),
                    'status_code' => 400
                ], 400);
            }
            $brevet = Brevet::create(['nama_brevet' => strtoupper($request->nama_brevet),'asal_brevet' => strtoupper($request->asal_brevet)]);
        } else {
            $brevet = Brevet::find($request->brevet_id);

        }

		$personelNRP = $personel->nrp;
        $riwayatBrevet->update([
        	'brevet_id'	=> $brevet->brevet_id,
            'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
            'tmt_brevet' => $request->tmt_brevet,
        ]);
        if($request->hasFile('surat_keputusan_file')){
        	$riwayatBrevet->hapusFile();
			$brevetID=$brevet->brevet_id;
			$temp = "$personelNRP-brevet_personel-surat_keputusan_file-$brevetID-$random";
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
			$suratKeputusanFile = $temp.'.'.$request->file('surat_keputusan_file')->extension();
			$riwayatBrevet->surat_keputusan_file=$suratKeputusanFile;
			$riwayatBrevet->save();
        }
        $riwayatBrevet->brevet;
        $riwayatBrevet->personel;
        return response()->json([
            'data' => $riwayatBrevet
        ],200);
    }

	public function hapusRiwayatBrevet(RiwayatBrevet $riwayatBrevet)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Riwayat Brevet '. $riwayatBrevet->riwayat_brevet_id.' pada URL :'.url()->full());
		$riwayatBrevet->hapusFile();
		if($riwayatBrevet->delete()){
			return response()->json([
				'message' => 'Data riwayat brevet berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus data riwayat brevet.',
				'status_code' => 400
			],400);
		}
	}
}
