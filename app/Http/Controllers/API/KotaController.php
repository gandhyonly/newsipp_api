<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Kota;
use Illuminate\Http\Request;
use Cache;

class KotaController extends Controller
{
	public function daftarKota()
	{
		$kota = Cache::remember('kota_baru',60,function(){
		 	return Kota::orderBy('nama_kota')->get();
		});
		return response()->json([
			'data' => $kota
		],200);
	}
}
