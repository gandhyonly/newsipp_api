<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Jurusan;
use Illuminate\Http\Request;
use App\Dikum;

class JurusanController extends Controller
{
	public function daftarJurusan()
	{
		$daftarJurusan = Jurusan::where('jurusan_dikpol',false)->orderBy('nama_jurusan')->get();
		return response()->json([
			'data' => $daftarJurusan
		],200);
	}
	public function daftarJurusanByDikum($dikumID)
	{
		$dikum=Dikum::find($dikumID);
		if($dikum==null){
			return response()->json([
				'errors' => 'Dikum tidak ditemukan'
			],400);
		}
		$daftarJurusan = Jurusan::where('dikum_id',$dikum->dikum_id)->where('jurusan_dikpol',false)->orderBy('nama_jurusan')->get();
		return response()->json([
			'data' => $daftarJurusan
		],200);
	}
	public function daftarJurusanDikpol()
	{
		$daftarJurusan = Jurusan::where('jurusan_dikpol',true)->orderBy('nama_jurusan')->get();
		return response()->json([
			'data' => $daftarJurusan
		],200);
	}
}
