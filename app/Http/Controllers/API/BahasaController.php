<?php

namespace App\Http\Controllers\API;

use App\Bahasa;
use App\BahasaPersonel;
use App\Http\Controllers\Controller;
use App\Personel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Log;

class BahasaController extends Controller
{
	public function daftarBahasaDaerah()
	{
		$bahasa = Bahasa::daerah()->orderBy('bahasa')->get();
		return response()->json([
			'data' => $bahasa
		],200);
	}
	public function daftarBahasaInternational()
	{
		$bahasa = Bahasa::international()->orderBy('bahasa')->get();
		return response()->json([
			'data' => $bahasa
		],200);
	}

	public function tambahBahasaInternational(Request $request)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			'bahasa_id.required' 	=> 'Kolom bahasa tidak boleh dikosongkan.',
			'aktif.required' 	=> 'Kolom aktif tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'bahasa_id' 	=> 'required',
			'aktif' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

		if($request->bahasa_id == 'other'){
			if($request->bahasa == null){
				return response()->json([
					'errors' => 'Kolom bahasa tidak boleh dikosongkan.'
				],400);
			}
			$bahasa = Bahasa::create(['bahasa' => strtoupper($request->bahasa), 'lokal' => false]);
		} else {
			$bahasa = Bahasa::find($request->bahasa_id);
			if($bahasa == null){
				return response()->json([
					'errors' => 'Bahasa yang dipilih tidak ditemukan.'
				],400);
			}
		}
		$personel = Personel::find($personelID);
		$bahasaPersonel = BahasaPersonel::create([
			'personel_id' => $personel->personel_id,
			'bahasa_id' => $bahasa->bahasa_id,
			'aktif' => $request->aktif,
		]);
		return response()->json([
			'data' => [
				'bahasa' => $bahasa,
				'personel' => $personel,
				'bahasaPersonel' => $bahasaPersonel,
			]
		],200);
	}

	public function tambahBahasaDaerah(Request $request)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			'bahasa_id.required' 	=> 'Kolom bahasa tidak boleh dikosongkan.',
			'aktif.required' 	=> 'Kolom aktif tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'bahasa_id' 	=> 'required',
			'aktif' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		if($request->bahasa_id == 'other'){
			if($request->bahasa == null){
				return response()->json([
					'errors' => 'Kolom bahasa tidak boleh dikosongkan.'
				],400);
			}
			$bahasa = Bahasa::create(['bahasa' => strtoupper($request->bahasa), 'lokal' => true]);
		} else {
			$bahasa = Bahasa::find($request->bahasa_id);
			if($bahasa == null){
				return response()->json([
					'errors' => 'Bahasa yang dipilih tidak ditemukan.'
				],400);
			}
		}
		$personel = Personel::find($personelID);
		$bahasaPersonel = BahasaPersonel::create([
			'personel_id' => $personel->personel_id,
			'bahasa_id' => $bahasa->bahasa_id,
			'aktif' => $request->aktif,
		]);
		return response()->json([
			'data' => [
				'bahasa' => $bahasa,
				'personel' => $personel,
				'bahasaPersonel' => $bahasaPersonel,
			]
		],200);
	}
	public function editBahasaDaerah(Request $request, BahasaPersonel $bahasaPersonel)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			'bahasa_id.required' 	=> 'Kolom bahasa tidak boleh dikosongkan.',
			'aktif.required' 	=> 'Kolom aktif tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'bahasa_id' 	=> 'required',
			'aktif' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		if($request->bahasa_id == 'other'){
			if($request->bahasa == null){
				return response()->json([
					'errors' => 'Kolom bahasa tidak boleh dikosongkan'
				],400);
			}
			$bahasa = Bahasa::create(['bahasa' => strtoupper($request->bahasa), 'lokal' => true]);
		} else {
			$bahasa = Bahasa::find($request->bahasa_id);
			if($bahasa == null){
				return response()->json([
					'errors' => 'Bahasa yang dipilih tidak ditemukan.'
				],400);
			}
		}
		$personel = Personel::find($personelID);
		$bahasaPersonel->update([
			'personel_id' => $personel->personel_id,
			'bahasa_id' => $bahasa->bahasa_id,
			'aktif' => $request->aktif,
		]);
		return response()->json([
			'data' => [
				'bahasa' => $bahasa,
				'personel' => $personel,
				'bahasaPersonel' => $bahasaPersonel,
			]
		],200);
	}

    public function editBahasaAsing(Request $request, BahasaPersonel $bahasaPersonel)
    {
        $personelID = auth()->user()->personel_id;
        $error_message = [
            'bahasa_id.required' 	=> 'Kolom bahasa tidak boleh dikosongkan.',
            'aktif.required' 	=> 'Kolom aktif tidak boleh dikosongkan.',
        ];

        $validation = Validator::make($request->all(),[
            'bahasa_id' 	=> 'required',
            'aktif' 	=> 'required',
        ],$error_message);

        if($validation->fails()){
            return response()->json([
                'errors' => $validation->errors(),
                'status_code' => 400
            ], 400);
        }
        if($request->bahasa_id == 'other'){
            if($request->bahasa == null){
                return response()->json([
                    'errors' => 'Kolom bahasa tidak boleh dikosongkan'
                ],400);
            }
            $bahasa = Bahasa::create(['bahasa' => strtoupper($request->bahasa), 'lokal' => false]);
        } else {
            $bahasa = Bahasa::find($request->bahasa_id);
            if($bahasa == null){
                return response()->json([
                    'errors' => 'Bahasa yang dipilih tidak ditemukan.'
                ],400);
            }
        }
        $personel = Personel::find($personelID);
        $bahasaPersonel->update([
            'personel_id' => $personel->personel_id,
            'bahasa_id' => $bahasa->bahasa_id,
            'aktif' => $request->aktif,
        ]);
        return response()->json([
            'data' => [
                'bahasa' => $bahasa,
                'personel' => $personel,
                'bahasaPersonel' => $bahasaPersonel,
            ]
        ],200);
    }


	public function hapusBahasa(BahasaPersonel $bahasaPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Bahasa '. $bahasaPersonel->bahasa_personel_id.' pada URL :'.url()->full());
        if($bahasaPersonel->personel_id == $personelID){
			if($bahasaPersonel->delete()){
				return response()->json([
					'message' => 'Kemampuan Bahasa berhasil dihapus.',
					'status_code' => 200
				],200);
			} else {
				return response()->json([
					'message' => 'Gagal menghapus kemampuan bahasa.',
					'status_code' => 400
				],400);
			}
        } else {
			return response()->json([
				'message' => 'Gagal menghapus kemampuan bahasa. StatusCode : 04318748',
				'status_code' => 400
			],400);
        }
	}

}
