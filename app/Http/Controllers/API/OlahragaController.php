<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Olahraga;
use App\OlahragaPersonel;
use App\Personel;
use Illuminate\Http\Request;
use Log;

class OlahragaController extends Controller
{
	public function daftarOlahraga()
	{
		return response()->json([
			'data' => Olahraga::orderBy('nama_olahraga')->get()
		],200);
	}

	public function tambahOlahraga(Request $request)
	{
		$personelID = auth()->user()->personel_id;
		$olahraga = Olahraga::find($request->olahraga_id);
		if($olahraga==null){
			return response()->json([
				'message' => 'Olahraga tidak ditemukan.',
				'status_code' => 400
			],400);
		}
		$olahragaPersonel = OlahragaPersonel::create([
			'personel_id' => $personelID,
			'olahraga_id' => $olahraga->olahraga_id
		]);
		$olahragaPersonel->personel;
		$olahragaPersonel->olahraga;
		return response()->json([
			'data' => $olahragaPersonel
		],200);
	}
    public function editOlaharaga(Request $request, OlahragaPersonel $olahragaPersonel)
    {
        $olahraga = Olahraga::find($request->olahraga_id);
		if($olahraga==null){
			return response()->json([
				'message' => 'Olahraga tidak ditemukan.',
				'status_code' => 400
			],400);
		}
		$olahragaPersonel->update([
			'olahraga_id' => $olahraga->olahraga_id
		]);
        $olahragaPersonel->olahraga;
        $olahragaPersonel->personel;
        return response()->json([
            'data' => $olahragaPersonel
        ],200);
    }
	public function hapusOlahraga(OlahragaPersonel $olahragaPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Hobi '. $olahragaPersonel->olahraga_personel_id.' pada URL :'.url()->full());
		if($olahragaPersonel->delete()){
			return response()->json([
				'message' => 'Olahraga berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus olahraga.',
				'status_code' => 400
			],400);
		}
	}
}
