<?php

namespace App\Http\Controllers\API;

use App\Alamat;
use App\Http\Controllers\Controller;
use App\Personel;
use Illuminate\Http\Request;
use Validator;
use Log;

class AlamatController extends Controller
{
	public function daftarAlamat()
	{
		$personel = Personel::find(auth()->user()->personel_id);

		return response()->json([
			'data' => $personel->alamat
		],200);
	}
	public function alamatTerakhir()
	{
		$personel = Personel::find(auth()->user()->personel_id);
		return response()->json([
			'data' => $personel->alamat->where('status_terakhir',1)
		],200);
	}
	public function tambahAlamat(Request $request)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			'alamat.required' 	=> 'Kolom alamat tidak boleh dikosongkan.',
			'telepon.max' 	=> 'Maksimal digit nomor telepon adalah 20 digit.',
			'tanggal_mulai.required' 	=> 'Kolom tempat mulai tidak boleh dikosongkan.',
			'tanggal_mulai.date' 	=> 'Format tanggal mulai salah.',
			'tanggal_selesai.date' 	=> 'Format tanggal selesai salah.',
		];

		$validation = Validator::make($request->all(),[
			'alamat' 	=> 'required',
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_selesai' 	=> 'nullable|date',
			'telepon' 	=> 'nullable|max:20',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

		$personel = Personel::find(auth()->user()->personel_id);

    	$alamat = Alamat::create([
    		'personel_id' => $personel->personel_id,
    		'alamat' => strtoupper($request->alamat),
    		'telepon' => $request->telepon,
    		'status_terakhir' => false,
    		'tanggal_mulai' => $request->tanggal_mulai,
    		'tanggal_selesai' => $request->tanggal_selesai,
    	]);
    	Alamat::where('personel_id',$personel->personel_id)->update(['status_terakhir' => false]);
    	$alamatSekarang = Alamat::where('personel_id',$personel->personel_id)->orderBy('tanggal_mulai','DESC')->first();
    	$alamatSekarang->status_terakhir=true;
    	$alamatSekarang->save();

		return response()->json([
			'data' => $alamat 
		],200);
	}

	public function editAlamat(Request $request,Alamat $alamat)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
			'alamat.required' 	=> 'Kolom alamat tidak boleh dikosongkan.',
			'telepon.required' 	=> 'Kolom telepon tidak boleh dikosongkan.',
			'tanggal_mulai.required' 	=> 'Kolom tempat mulai tidak boleh dikosongkan.',
			'tanggal_mulai.date' 	=> 'Format tanggal mulai salah.',
			'tanggal_selesai.date' 	=> 'Format tanggal selesai salah.',
		];

		$validation = Validator::make($request->all(),[
			'alamat' 	=> 'required',
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_mulai' 	=> 'nullable|date',
			'telepon' 	=> 'required',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

		$personel = Personel::find(auth()->user()->personel_id);

    	$alamat->personel_id = $personel->personel_id;
    	$alamat->alamat = strtoupper($request->alamat);
		$alamat->telepon = $request->telepon;
		$alamat->status_terakhir = false;
		$alamat->tanggal_mulai = $request->tanggal_mulai;
		$alamat->tanggal_selesai = $request->tanggal_selesai;
		$alamat->save();
    	Alamat::where('personel_id',$personel->personel_id)->update(['status_terakhir' => false]);
    	$alamatSekarang = Alamat::where('personel_id',$personel->personel_id)->orderBy('tanggal_mulai','DESC')->first();
    	$alamatSekarang->status_terakhir=true;
    	$alamatSekarang->save();

		return response()->json([
			'data' => $alamat 
		],200);
	}

	public function hapusAlamat(Alamat $alamat)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Alamat '. $alamat->alamat_id.' pada URL :'.url()->full());
		if($alamat->delete()){
			return response()->json([
				'message' => 'Hapus data alamat berhasil.',
				'status_code' => '200'
			],200);
		} else {
			return response()->json([
				'message' => 'Hapus data alamat gagal.',
				'status_code' => '400'
			],400);
		}
	}
}
