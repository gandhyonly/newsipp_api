<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\TipePendidikan;
use Illuminate\Http\Request;

class TipePendidikanController extends Controller
{
	public function daftarTipePendidikan()
	{
		$tipePendidikan = TipePendidikan::get();
		return response()->json([
			'data' => $tipePendidikan
		],200);
	}

	public function daftarPendidikan(Request $request)
	{
		$tipePendidikan = TipePendidikan::find($request->tipe_pendidikan_id);
		$tipePendidikan->pendidikan;
		return response()->json([
			'data' => $tipePendidikan
		],200);
	}
}
