<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Negara;
use Illuminate\Http\Request;
use Cache;

class NegaraController extends Controller
{
	public function daftarNegara()
	{
		$daftarNegara = Cache::remember('daftar_negara',180,function(){
			return Negara::get();
		});
		return response()->json([
			'data' => $daftarNegara
		],200);
	}
}
