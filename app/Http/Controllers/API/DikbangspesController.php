<?php

namespace App\Http\Controllers\API;

use App\Dikbangspes;
use App\Http\Controllers\Controller;
use App\Personel;
use App\PendidikanPersonel;
use App\TipeDikbangspes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Cache;
use Log;

class DikbangspesController extends Controller
{
	public function daftarTipeDikbangspes()
	{
		$daftarTipeDikbangspes = Cache::remember('daftar_tipe_dikbangspes',60,function(){
			return TipeDikbangspes::orderBy('tipe')->get();
		});
		return response()->json([
			'data' => $daftarTipeDikbangspes
		],200);
	}
	public function daftarDikbangspes(TipeDikbangspes $tipeDikbangspes)
	{
		return response()->json([
			'data' => $tipeDikbangspes->dikbangspes
		],200);
	}

	public function tambahDikbangspes(Request $request)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
	    $error_message = [
			'tanggal_mulai.required' 	=> 'Kolom tanggal mulai tidak boleh dikosongkan.',
			'tanggal_mulai.date' 	=> 'Format tanggal mulai salah.',
			'tanggal_lulus.date' 	=> 'Format tanggal lulus salah.',
			'dikbangspes_id.required' 	=> 'Kolom dikbangspes tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_lulus' 	=> 'nullable|date',
			'dikbangspes_id' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$dikbangspes = Dikbangspes::find($request->dikbangspes_id);
		if($dikbangspes == null){
			return response()->json([
				'errors' => 'Dikbangspes tidak ditemukan.'
			],400);
		}

		$pendidikanPersonel = PendidikanPersonel::create([
			'dikbangspes_id' => $dikbangspes->dikbangspes_id,
			'personel_id' => $personelID,
			'tanggal_mulai' => $request->tanggal_mulai,
			'tanggal_lulus' => $request->tanggal_lulus,
			'nilai_akhir' => $request->nilai_akhir,
			'ranking' => $request->ranking,
			'total_siswa' => $request->total_siswa,
			'status_terakhir' => 1,
			'surat_kelulusan_nomor' => strtoupper($request->surat_kelulusan_nomor),
		]);

		$personelNRP = $personel->nrp;
		$random = str_random(5);
		$dikbangspesNama = str_slug($dikbangspes->nama_dikbangspes);
		$dikbangspesID=$dikbangspes->dikbangspes_id;
	    if($request->hasFile('nilai_file')){
	    	$temp = "$personelNRP-dikbangspes_personel-nilai_file-$dikbangspesID-$dikbangspesNama-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
			$pendidikanPersonel->nilai_file = $temp.'.'.$request->file('nilai_file')->extension();
	    }
	    if($request->hasFile('surat_kelulusan_file')){
	    	$temp = "$personelNRP-dikbangspes_personel-surat_kelulusan_file-$dikbangspesID-$dikbangspesNama-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_kelulusan_file'), $temp.'.'.$request->file('surat_kelulusan_file')->extension());
			$pendidikanPersonel->surat_kelulusan_file = $temp.'.'.$request->file('surat_kelulusan_file')->extension();
	    }
	    $pendidikanPersonel->save();
	    $pendidikanPersonel->dikbangspes;

		return response()->json([
			'data' => $pendidikanPersonel
		],200);
	}
	public function editDikbangspes(Request $request,PendidikanPersonel $pendidikanPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
	    $error_message = [
			'tanggal_mulai.required' 	=> 'Kolom tanggal mulai tidak boleh dikosongkan.',
			'tanggal_mulai.date' 	=> 'Format tanggal mulai salah.',
			'tanggal_lulus.date' 	=> 'Format tanggal lulus salah.',
			'dikbangspes_id.required' 	=> 'Kolom dikbangspes tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_lulus' 	=> 'nullable|date',
			'dikbangspes_id' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$dikbangspes = Dikbangspes::find($request->dikbangspes_id);
		if($dikbangspes == null){
			return response()->json([
				'errors' => 'Dikbangspes tidak ditemukan.'
			],400);
		}

		$pendidikanPersonel->update([
			'dikbangspes_id' => $dikbangspes->dikbangspes_id,
			'personel_id' => $personelID,
			'tanggal_mulai' => $request->tanggal_mulai,
			'tanggal_lulus' => $request->tanggal_lulus,
			'nilai_akhir' => $request->nilai_akhir,
			'ranking' => $request->ranking,
			'total_siswa' => $request->total_siswa,
			'status_terakhir' => 1,
			'surat_kelulusan_nomor' => strtoupper($request->surat_kelulusan_nomor),
		]);

		$personelNRP = $personel->nrp;
		$random = str_random(5);
		$dikbangspesID = $dikbangspes->dikbangspes_id;
		$dikbangspesNama = str_slug($dikbangspes->nama_dikbangspes);
	    if($request->hasFile('nilai_file')){
			$pendidikanPersonel->hapusFile('nilai_file');
	    	$temp = "$personelNRP-dikbangspes_personel-nilai_file-$dikbangspesID-$dikbangspesNama-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
			$pendidikanPersonel->nilai_file = $temp.'.'.$request->file('nilai_file')->extension();
	    }
	    if($request->hasFile('surat_kelulusan_file')){
			$pendidikanPersonel->hapusFile('surat_kelulusan_file');
	    	$temp = "$personelNRP-dikbangspes_personel-surat_kelulusan_file-$dikbangspesID-$dikbangspesNama-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_kelulusan_file'), $temp.'.'.$request->file('surat_kelulusan_file')->extension());
			$pendidikanPersonel->surat_kelulusan_file = $temp.'.'.$request->file('surat_kelulusan_file')->extension();
	    }
	    $pendidikanPersonel->save();
	    $pendidikanPersonel->dikbangspes;

		return response()->json([
			'data' => $pendidikanPersonel
		],200);
	}
	public function hapusDikbangspes(PendidikanPersonel $pendidikanPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Dikbangspes '. $pendidikanPersonel->pendidikan_personel_id.' pada URL :'.url()->full());
		$pendidikanPersonel->hapusFile('nilai_file');
		$pendidikanPersonel->hapusFile('surat_kelulusan_file');
		$pendidikanPersonel->delete();
		return response()->json([
			'data' => 'Data berhasil dihapus'
		],200);
	}
}
