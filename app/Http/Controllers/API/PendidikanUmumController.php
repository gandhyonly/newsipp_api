<?php

namespace App\Http\Controllers\API;

use App\Dikum;
use App\Gelar;
use App\Http\Controllers\Controller;
use App\Personel;
use App\Jurusan;
use App\Konsentrasi;
use App\PendidikanPersonel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Cache;
use DB;
use Log;

class PendidikanUmumController extends Controller
{

	public function daftarGelar(Request $request)
	{
		$jurusan = Jurusan::find($request->jurusan_id);
	    if($jurusan==null){
			return response()->json([
				'errors' => 'Jurusan Tidak Ditemukan.',
				'status_code' => 400
			], 400);
	    }
		$jurusan->gelar;
		return response()->json([
			'data' => $jurusan
		],200);
	}
	public function daftarDikum()
	{
		$daftarDikum = Cache::remember('daftar_dikum',60,function(){
			return Dikum::get();
		});
		return response()->json([
			'data' => $daftarDikum
		],200);
	}
	public function tambahPendidikanUmum(Request $request)
	{
		$today=Carbon::now()->format('Ymd');
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
	    $error_message = [
			'nama_institusi.required' 	=> 'Kolom nama institusi tidak boleh dikosongkan.',
			'tanggal_mulai.required' 	=> 'Kolom tanggal mulai tidak boleh dikosongkan.',
			'tanggal_mulai.date' 	=> 'Format tanggal mulai salah.',
			'tanggal_lulus.date' 	=> 'Format tanggal lulus salah.',
			'dikum_id.required' 	=> 'Kolom tingkat tidak boleh dikosongkan.',
			'dinas.required' 	=> 'Kolom dinas tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nama_institusi' 	=> 'required',
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_lulus' 	=> 'nullable|date',
			'dikum_id' 	=> 'required',
			'dinas' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$dikum = Dikum::find($request->dikum_id);
		if($dikum == null){
			return response()->json([
				'errors' => 'Pendidikan Umum tidak ditemukan.'
			],400);
		}
		$jurusan=null;
		$gelar=null;
		$konsentrasi=null;
		DB::beginTransaction();
		if($request->jurusan_id == null){
 
		} else if($request->jurusan_id == 'other'){
				return response()->json([
					'errors' => 'Harap Melakukan Update Aplikasi',
					'status_code' => 400
				], 400);
		} else {
			$jurusan = Jurusan::find($request->jurusan_id);
			if($jurusan == null){
				return response()->json([
					'errors' => 'Jurusan tidak ditemukan.'
				],400);
			}

			if($request->konsentrasi_id == 'other'){

				return response()->json([
					'errors' => 'Harap Melakukan Update Aplikasi',
					'status_code' => 400
				], 400);
			} else {
				$konsentrasi = Konsentrasi::find($request->konsentrasi_id);
				if($konsentrasi == null){
					return response()->json([
						'errors' => 'Konsentrasi tidak ditemukan.'
					],400);
				}
			}
			if($request->gelar_id == 'other'){
			    $error_message = [
					'gelar.required' 	=> 'Kolom gelar tidak boleh dikosongkan.',
					'gelar_posisi_sebelum.required' 	=> 'Kolom gelar posisi sebelum tidak boleh dikosongkan.',
					'jurusan_id.required' 	=> 'Kolom jurusan tidak boleh dikosongkan.',
				];
				$validation = Validator::make($request->all(),[
					'gelar' 	=> 'required',
					'gelar_posisi_sebelum' 	=> 'required',
					'jurusan_id' 	=> 'required',
			    ],$error_message);

			    if($validation->fails()){
					return response()->json([
						'errors' => $validation->errors(),
						'status_code' => 400
					], 400);
			    }

				$gelar = Gelar::create(['gelar' => $request->gelar,'gelar_posisi_sebelum' => $request->gelar_posisi_sebelum,'jurusan_id' => $jurusan->jurusan_id]);
			} else {
				$gelar = Gelar::find($request->gelar_id);
				if($gelar == null){
					return response()->json([
						'errors' => 'Gelar tidak ditemukan.'
					],400);
				}
			}
		}


		$pendidikanPersonel = PendidikanPersonel::create([
			'dikum_id' => $dikum->dikum_id,
			'konsentrasi_id' => $konsentrasi == null ? null : $konsentrasi->konsentrasi_id,
			'personel_id' => $personelID,
			'nama_institusi' => strtoupper($request->nama_institusi),
			'tanggal_mulai' => $request->tanggal_mulai,
			'tanggal_lulus' => $request->tanggal_lulus,
			'nilai_akhir' => $request->nilai_akhir,
			'ranking' => $request->ranking,
			'gelar_id' => $gelar == null ? null : $gelar->gelar_id,
			'total_siswa' => $request->total_siswa,
			'status_terakhir' => 1,
			'akreditasi' => strtoupper($request->akreditasi),
			'surat_kelulusan_nomor' => strtoupper($request->surat_kelulusan_nomor),
			'dinas'	=> $request->dinas
		]);

    	$random =str_random(10);
		$personelNRP = $personel->nrp;
		$dikumID = $dikum->dikum_id;
		$namaDikum = str_slug($dikum->tingkat);
		$pendidikanPersonelID = $pendidikanPersonel->id;
	    if($request->hasFile('nilai_file')){
		    $error_message = [
				'nilai_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'nilai_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'nilai_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$temp = "$personelNRP-dikum_personel-nilai_file-$dikumID-$namaDikum-$today-$pendidikanPersonelID-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
			$pendidikanPersonel->nilai_file = $temp.'.'.$request->file('nilai_file')->extension();
	    }
	    if($request->hasFile('surat_kelulusan_file')){
		    $error_message = [
				'surat_kelulusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'surat_kelulusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'surat_kelulusan_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$temp = "$personelNRP-dikum_personel-surat_kelulusan_file-$dikumID-$namaDikum-$today-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_kelulusan_file'), $temp.'.'.$request->file('surat_kelulusan_file')->extension());
			$pendidikanPersonel->surat_kelulusan_file = $temp.'.'.$request->file('surat_kelulusan_file')->extension();
	    }
	    $pendidikanPersonel->save();
	    DB::commit();
	    $pendidikanPersonel->gelar;
	    $pendidikanPersonel->dikum;
	    $pendidikanPersonel->konsentrasi;

		return response()->json([
			'data' => $pendidikanPersonel
		],200);
	}

	public function editPendidikanUmum(Request $request,PendidikanPersonel $pendidikanPersonel)
	{
		$today=Carbon::now()->format('Ymd');
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
	    $error_message = [
			'nama_institusi.required' 	=> 'Kolom nama institusi tidak boleh dikosongkan.',
			'tanggal_mulai.required' 	=> 'Kolom tanggal mulai tidak boleh dikosongkan.',
			'tanggal_mulai.date' 	=> 'Format tanggal mulai salah.',
			'tanggal_lulus.date' 	=> 'Format tanggal lulus salah.',
			'dikum_id.required' 	=> 'Kolom tingkat tidak boleh dikosongkan.',
			'dinas.required' 	=> 'Kolom dinas tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nama_institusi' 	=> 'required',
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_lulus' 	=> 'nullable|date',
			'dikum_id' 	=> 'required',
			'dinas' 	=> 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
		$dikum = Dikum::find($request->dikum_id);
		if($dikum == null){
			return response()->json([
				'errors' => 'Pendidikan Umum tidak ditemukan.'
			],400);
		}
		$jurusan=null;
		$gelar=null;
		$konsentrasi=null;
		DB::beginTransaction();
		if($request->jurusan_id == null){
 
		} else if($request->jurusan_id == 'other'){
			return response()->json([
				'errors' => 'Harap Melakukan Update Aplikasi',
				'status_code' => 400
			], 400);
		} else {
			$jurusan = Jurusan::find($request->jurusan_id);
			if($jurusan == null){
				return response()->json([
					'errors' => 'Jurusan tidak ditemukan.'
				],400);
			}

			if($request->konsentrasi_id == 'other'){
				return response()->json([
					'errors' => 'Harap Melakukan Update Aplikasi',
					'status_code' => 400
				], 400);
			} else {
				$konsentrasi = Konsentrasi::find($request->konsentrasi_id);
				if($konsentrasi == null){
					return response()->json([
						'errors' => 'Konsentrasi tidak ditemukan.'
					],400);
				}
			}
			if($request->gelar_id == 'other'){
			    $error_message = [
					'gelar.required' 	=> 'Kolom gelar tidak boleh dikosongkan.',
					'gelar_posisi_sebelum.required' 	=> 'Kolom gelar posisi sebelum tidak boleh dikosongkan.',
					'jurusan_id.required' 	=> 'Kolom jurusan tidak boleh dikosongkan.',
				];
				$validation = Validator::make($request->all(),[
					'gelar' 	=> 'required',
					'gelar_posisi_sebelum' 	=> 'required',
					'jurusan_id' 	=> 'required',
			    ],$error_message);

			    if($validation->fails()){
					return response()->json([
						'errors' => $validation->errors(),
						'status_code' => 400
					], 400);
			    }

				$gelar = Gelar::create(['gelar' => $request->gelar,'gelar_posisi_sebelum' => $request->gelar_posisi_sebelum,'jurusan_id' => $jurusan->jurusan_id]);
			} else {
				$gelar = Gelar::find($request->gelar_id);
				if($gelar == null){
					return response()->json([
						'errors' => 'Gelar tidak ditemukan.'
					],400);
				}
			}
		}


	    
		$pendidikanPersonel->update([
			'dikum_id' => $dikum->dikum_id,
			'konsentrasi_id' => $konsentrasi == null ? null : $konsentrasi->konsentrasi_id,
			'personel_id' => $personelID,
			'nama_institusi' => strtoupper($request->nama_institusi),
			'tanggal_mulai' => $request->tanggal_mulai,
			'tanggal_lulus' => $request->tanggal_lulus,
			'nilai_akhir' => $request->nilai_akhir,
			'ranking' => $request->ranking,
			'gelar_id' => $gelar == null ? null : $gelar->gelar_id,
			'total_siswa' => $request->total_siswa,
			'status_terakhir' => 1,
			'akreditasi' => strtoupper($request->akreditasi),
			'surat_kelulusan_nomor' => strtoupper($request->surat_kelulusan_nomor),
			'dinas'	=> $request->dinas
		]);
    	$random =str_random(5);
		$personelNRP = $personel->nrp;
		$dikumID = $dikum->dikum_id;
		$namaDikum = str_slug($dikum->tingkat);
	    if($request->hasFile('nilai_file')){
		    $error_message = [
				'nilai_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'nilai_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'nilai_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$pendidikanPersonel->hapusFile('nilai_file');
			$temp = "$personelNRP-dikum_personel-nilai_file-$dikumID-$namaDikum-$today-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('nilai_file'), $temp.'.'.$request->file('nilai_file')->extension());
			$pendidikanPersonel->nilai_file = $temp.'.'.$request->file('nilai_file')->extension();
	    	$pendidikanPersonel->save();
	    }
	    if($request->hasFile('surat_kelulusan_file')){
		    $error_message = [
				'surat_kelulusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
				'surat_kelulusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
			];
			$validation = Validator::make($request->all(),[
		        'surat_kelulusan_file' => "nullable|mimes:jpg,jpeg,pdf|max:$maxFileUpload",
		    ],$error_message);
		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				],400);
		    }
			$pendidikanPersonel->hapusFile('surat_kelulusan_file');
			$temp = "$personelNRP-dikum_personel-surat_kelulusan_file-$dikumID-$namaDikum-$today-".str_random(5);
			Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_kelulusan_file'), $temp.'.'.$request->file('surat_kelulusan_file')->extension());
			$pendidikanPersonel->surat_kelulusan_file = $temp.'.'.$request->file('surat_kelulusan_file')->extension();
	    	$pendidikanPersonel->save();
	    }
	    DB::commit();

	    $pendidikanPersonel->gelar;
	    $pendidikanPersonel->dikum;
	    $pendidikanPersonel->konsentrasi;

		return response()->json([
			'data' => $pendidikanPersonel
		],200);
	}

	public function hapusPendidikanUmum(PendidikanPersonel $pendidikanPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Riwayat Pendidikan Polisi '. $pendidikanPersonel->pendidikan_personel_id.' pada URL :'.url()->full());
		$pendidikanPersonel->hapusFile('nilai_file');
		$pendidikanPersonel->hapusFile('surat_kelulusan_file');
		$pendidikanPersonel->delete();
		return response()->json([
			'data' => 'Data berhasil dihapus'
		],200);
	}
	public function daftarKonsentrasi(Jurusan $jurusan)
	{
		$jurusan->konsentrasi;
		return response()->json([
			'data' => $jurusan
		],200);
	}
}
