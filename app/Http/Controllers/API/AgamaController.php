<?php

namespace App\Http\Controllers\API;

use App\Agama;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cache;

class AgamaController extends Controller
{
	public function daftarAgama()
	{
		$daftarAgama = Cache::remember('daftar_agama',180,function(){
			return Agama::get();
		});
		return response()->json([
			'data' => $daftarAgama
		],200);
	}
}
