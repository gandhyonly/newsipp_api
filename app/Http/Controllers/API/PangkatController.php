<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Pangkat;
use App\Personel;
use App\RiwayatPangkat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Validator;
use Log;

class PangkatController extends Controller
{
	public function tambahRiwayatPangkat(Request $request)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
	    $error_message = [
		    'pangkat_id.required'    => 'Kolom pangkat tidak boleh dikosongkan.',
		    'surat_keputusan_nomor.required'    => 'Kolom surat keputusan nomor tidak boleh dikosongkan.',
		    'tmt_pangkat.required'    => 'Kolom tmt pangkat tidak boleh dikosongkan.',
		    'tmt_pangkat.date'    => 'Kolom tmt pangkat tidak boleh dikosongkan.',
		    'tmt_pangkat.date_format'    => 'Format tmt pangkat salah.',
		    'tmt_pangkat.after'    => 'Tahun tmt pangkat harus lebih dari 1800.',
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];
		$validation = Validator::make($request->all(),[
	        'pangkat_id' => 'required',
	        'surat_keputusan_nomor' => 'required',
	        'tmt_pangkat' => 'required|date|date_format:Y-m-d|after:1800-01-01',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			],400);
	    }
		$personelID = auth()->user()->personel_id;
		$pangkat = Pangkat::find($request->pangkat_id);
		if($pangkat == null){
			return response()->json([
				'errors' => 'Pangkat Tidak Ditemukan.'
			],400);
		}
		$personel = Personel::find($personelID);
		$fileName=null;
		$personelNRP = $personel->nrp;
		DB::beginTransaction();
			$riwayatPangkat = RiwayatPangkat::create([
				'personel_id' => $personelID,
				'pangkat_id' => $request->pangkat_id,
				'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
				'tmt_pangkat' => $request->tmt_pangkat,
			]);
			if($request->hasFile('surat_keputusan_file')){
				$pangkatID = $riwayatPangkat->pangkat_id;
				$pangkatSingkat = $riwayatPangkat->pangkat->singkatan;
				$riwayatPangkatID=$riwayatPangkat->riwayat_pangkat_id;
		    	$temp = "$personelNRP-pangkat_personel-surat_keputusan_file-$pangkatID-$pangkatSingkat-$riwayatPangkatID-".str_random(5);
				Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
				$fileName = $temp.'.'.$request->file('surat_keputusan_file')->extension();
			}
			$riwayatPangkat->surat_keputusan_file=$fileName;
			$riwayatPangkat->save();
		DB::commit();

		$riwayatPangkat->pangkat;
		$riwayatPangkat->personel;
		return response()->json([
			'data' => $riwayatPangkat
		],200);
	}
	public function editRiwayatPangkat(Request $request,RiwayatPangkat $riwayatPangkat)
	{
		$maxFileUpload=env('MAX_FILE_UPLOAD','512');
	    $error_message = [
		    'pangkat_id.required'    => 'Kolom pangkat tidak boleh dikosongkan.',
		    'surat_keputusan_nomor.required'    => 'Kolom surat keputusan nomor tidak boleh dikosongkan.',
		    'tmt_pangkat.required'    => 'Kolom tmt pangkat tidak boleh dikosongkan.',
		    'tmt_pangkat.date'    => 'Kolom tmt pangkat tidak boleh dikosongkan.',
		    'tmt_pangkat.date_format'    => 'Format tmt pangkat salah.',
		    'tmt_pangkat.after'    => 'Tahun tmt pangkat harus lebih dari 1800.',
			'surat_keputusan_file.mimes' => "Pastikan file berupa jpeg/pdf",
			'surat_keputusan_file.max' => "Pastikan file berupa jpeg/pdf dengan ukuran maksimum $maxFileUpload KB",
		];
		$validation = Validator::make($request->all(),[
	        'pangkat_id' => 'required',
	        'surat_keputusan_nomor' => 'required',
	        'tmt_pangkat' => 'required|date|date_format:Y-m-d|after:1800-01-01',
	        'surat_keputusan_file' => "nullable|mimes:jpeg,pdf|max:$maxFileUpload",
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			],400);
	    }
		$personelID = auth()->user()->personel_id;
		$pangkat = Pangkat::find($request->pangkat_id);
		if($pangkat == null){
			return response()->json([
				'errors' => 'Pangkat Tidak Ditemukan.'
			],400);
		}
		DB::beginTransaction();
		$personel = Personel::find($personelID);
		$fileName=null;
		$personelNRP = $personel->nrp;
			if($request->hasFile('surat_keputusan_file')){
				$riwayatPangkat->hapusFile();
				$pangkatID = $riwayatPangkat->pangkat_id;
				$pangkatSingkat = str_slug($riwayatPangkat->pangkat->singkatan);
				$riwayatPangkatID=$riwayatPangkat->riwayat_pangkat_id;
		    	$temp = "$personelNRP-pangkat_personel-surat_keputusan_file-$pangkatID-$pangkatSingkat-$riwayatPangkatID-".str_random(5);
				Storage::putFileAs('personel/'.$personelNRP.'/',$request->file('surat_keputusan_file'), $temp.'.'.$request->file('surat_keputusan_file')->extension());
				$fileName = $temp.'.'.$request->file('surat_keputusan_file')->extension();
				$riwayatPangkat->surat_keputusan_file=$fileName;
				$riwayatPangkat->save();
			}
			$riwayatPangkat->update([
				'pangkat_id' => $request->pangkat_id,
				'surat_keputusan_nomor' => strtoupper($request->surat_keputusan_nomor),
				'tmt_pangkat' => $request->tmt_pangkat,
			]);
		DB::commit();
		$riwayatPangkat->pangkat;
		$riwayatPangkat->personel;
		return response()->json([
			'data' => $riwayatPangkat
		],200);
	}
	public function hapusRiwayatPangkat(RiwayatPangkat $riwayatPangkat)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Riwayat Pangkat '. $riwayatPangkat->pangkat_personel_id.' pada URL :'.url()->full());
		$riwayatPangkat->hapusFile();
		if($riwayatPangkat->delete()){
			return response()->json([
				'data' => 'Data riwayat pangkat berhasil dihapus.'
			],200);
		}else {

			return response()->json([
				'data' => 'Data riwayat pangkat gagal dihapus.'
			],200);
		}
	}

	public function daftarPangkat()
	{
		return response()->json([
			'data' => Pangkat::get()
		],200);
	}
}
