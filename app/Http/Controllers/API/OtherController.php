<?php

namespace App\Http\Controllers\API;

use App\OtherGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cache;

class OtherController extends Controller
{
	public function daftarJenisKelamin()
	{
		$daftarJenisKelamin = Cache::remember('daftar_jenis_kelamin',1440,function(){
			return OtherGroup::find(1)->others;
		});
		return response()->json([
			'data' => $daftarJenisKelamin
		],200);
	}
	public function daftarGolonganDarah()
	{
		$daftarGolonganDarah = Cache::remember('daftar_golongan_darah',1440,function(){
			return OtherGroup::find(2)->others;
		});
		return response()->json([
			'data' => $daftarGolonganDarah
		],200);
	}
	public function daftarStatusPernikahan()
	{
		$daftarStatusPernikahan = Cache::remember('daftar_status_pernikahan',1440,function(){
			return OtherGroup::find(3)->others;
		});
		return response()->json([
			'data' => $daftarStatusPernikahan
		],200);
	}
	public function daftarWarnaKulit()
	{
		$daftarWarnaKulit = Cache::remember('daftar_warna_kulit',60,function(){
			return OtherGroup::find(4)->others;
		});
		return response()->json([
			'data' => $daftarWarnaKulit
		],200);
	}
	public function daftarSukuBangsa()
	{
		$daftarSukuBangsa = Cache::remember('daftar_suku_bangsa',60,function(){
			return OtherGroup::find(5)->others;
		});
		return response()->json([
			'data' => $daftarSukuBangsa
		],200);
	}
	public function daftarJenisRambut()
	{
		$daftarJenisRambut = Cache::remember('daftar_jenis_rambut',60,function(){
			return OtherGroup::find(6)->others;
		});
		return response()->json([
			'data' => $daftarJenisRambut
		],200);
	}
	public function daftarWarnaRambut()
	{
		$daftarWarnaRambut = Cache::remember('daftar_warna_rambut',60,function(){
			return OtherGroup::find(7)->others;
		});
		return response()->json([
			'data' => $daftarWarnaRambut
		],200);
	}
	public function daftarWarnaMata()
	{
		$daftarWarnaMata = Cache::remember('daftar_warna_mata',60,function(){
			return OtherGroup::find(8)->others;
		});
		return response()->json([
			'data' => $daftarWarnaMata
		],200);
	}
	public function daftarAkreditasi()
	{
		$daftarAkreditasi = Cache::remember('daftar_akreditasi',60,function(){
			return OtherGroup::find(9)->others;
		});
		return response()->json([
			'data' => $daftarAkreditasi
		],200);
	}
	public function daftarStatusKeluarga()
	{
		$daftarStatusKeluarga = Cache::remember('daftar_status_keluarga',60,function(){
			return OtherGroup::find(10)->others;
		});
		return response()->json([
			'data' => $daftarStatusKeluarga
		],200);
	}
}
