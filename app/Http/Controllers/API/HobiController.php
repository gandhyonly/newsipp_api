<?php

namespace App\Http\Controllers\API;

use App\Hobi;
use App\HobiPersonel;
use App\Http\Controllers\Controller;
use App\Personel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Log;

class HobiController extends Controller
{
	public function daftarHobi()
	{
		return response()->json([
			'data' => Hobi::orderBy('hobi')->get()
		],200);
	}

	public function tambahHobi(Request $request)
	{
		$personelID = auth()->user()->personel_id;
	    $error_message = [
            'hobi_id.required'    => 'Kolom hobi tidak boleh dikosongkan.',
		];
		$validation = Validator::make($request->all(),[
            'hobi_id' => 'required',
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			]);
	    }
	    DB::beginTransaction();
		$hobi = Hobi::find($request->hobi_id);
		if($hobi==null){
			return response()->json([
				'message' => 'Hobi tidak ditemukan.',
				'status_code' => 400
			],400);
		}
		$hobiPersonel = HobiPersonel::create([
			'personel_id' => $personelID,
			'hobi_id' => $hobi->hobi_id
		]);
		DB::commit();
		$hobiPersonel->hobi;
		$hobiPersonel->personel;
		return response()->json([
			'data' => $hobiPersonel
		],200);
	}
    public function editHobi(Request $request, HobiPersonel $hobiPersonel)
    {
		$personelID = auth()->user()->personel_id;
	    $error_message = [
            'hobi_id.required'    => 'Kolom hobi tidak boleh dikosongkan.',
		];
		$validation = Validator::make($request->all(),[
            'hobi_id' => 'required',
	    ],$error_message);
	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors()
			]);
	    }
        $hobi = Hobi::find($request->hobi_id);
		$hobiPersonel->update([
			'hobi_id' => $hobi->hobi_id
		]);
        $hobiPersonel->hobi;
        $hobiPersonel->personel;
        return response()->json([
            'data' => $hobiPersonel
        ],200);
    }
	public function hapusHobi(HobiPersonel $hobiPersonel)
	{
		$personelID = auth()->user()->personel_id;
		$personel = Personel::find($personelID);
		Log::info($personel->nrp.' : Hapus Hobi '. $hobiPersonel->hobi_personel_id.' pada URL :'.url()->full());
		if($hobiPersonel->delete()){
			return response()->json([
				'message' => 'Hobi berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus hobi.',
				'status_code' => 400
			],400);
		}
	}
}
