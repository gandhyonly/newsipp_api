<?php

namespace App\Http\Controllers\API;

use App\Helpdesk;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HelpdeskController extends Controller
{
	public function getData(Request $request)
	{
		if($request->status=="ALL" || $request->status==null){
			$helpdesk = Helpdesk::with('satuan')->with('verifikator')->latest();
		}elseif($request->status=="PROBLEM"){
			$helpdesk = Helpdesk::with('satuan')->with('verifikator')->where('status',"NOTFOUND")->orWhere('status','DUPLICATE')->latest();
		} else{
			$helpdesk = Helpdesk::with('satuan')->with('verifikator')->where('status',$request->status)->latest();
		}
		return datatables($helpdesk)->toJson();
	}
	public function detail(Request $request)
	{
		$helpdesk = Helpdesk::find($request->id);
		$helpdesk->satuan;
		if($helpdesk==null){
			return response()->json([
				'error' => 'NOT FOUND'
			],404);
		}
		return response()->json([
			'data' => $helpdesk
		],200);
	}
	public function verified(Request $request)
	{
		$helpdesk = Helpdesk::find($request->id);
		if($helpdesk==null){
			return response()->json([
				'error' => 'NOT FOUND'
			],404);
		}
		$helpdesk->update(['status' => 'VERIFIED']);
		$helpdesk->save();
		return response()->json([
			'data' => $helpdesk
		],200);
	}
}
