<?php

namespace App\Http\Controllers\Admin;

use App\HelpdeskUser;
use App\Http\Controllers\Controller;
use App\Satuan;
use App\VerifikatorUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class AdminController extends Controller
{
	public function login(Request $request)
	{
	    $error_message = [
		    'nrp.required'    => 'Kolom nrp tidak boleh dikosongkan.',
		    'password.required'    => 'Kolom kata kunci tidak boleh dikosongkan.',
		];

		$validation = Validator::make($request->all(),[
			'nrp' => 'required',
			'password' => 'required',
	    ],$error_message);

	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }
	    if(auth('admin')->attempt(['nrp'=>$request->nrp,'password' => $request->password])){
	    	$admin=auth('admin')->user();
			return response()->json([
				'token' => auth('admin')->user()->api_token,
				'admin' => $admin
			], 200);
	    } else{
			return response()->json([
				'message' => 'NRP atau Password anda salah.',
				'status_code' => 400
			],400);
	    }
	}

	public function tambahVerifikator(Request $request)
	{
		$verifikator = auth()->user();
	    $error_message = [
			'nama.required' 				=> 'Kolom nama tidak boleh dikosongkan.',
			'nrp.required' 				=> 'Kolom nrp tidak boleh dikosongkan.',
			'nrp.unique' 				=> 'NRP telah terdaftar sebagai verifikator.',
			'password.required' 				=> 'Kolom password tidak boleh dikosongkan.',
			'password.confirmed' 				=> 'Kolom password dan konfirmasi password tidak sama.',
		];

		$validation = Validator::make($request->all(),[
			'nama' 		=> 'required',
			'nrp' 		=> 'required|unique:verifikator_user',
			'password' 		=> 'required|confirmed',
	    ],$error_message);


	    if($validation->fails()){
			return response()->json([
				'errors' => $validation->errors(),
				'status_code' => 400
			], 400);
	    }

	    $verifikatorBaru = VerifikatorUser::create([
	    	'nama' => $request->nama,
	    	'nrp' => $request->nrp,
	    	'satuan_id' => 1,
	    	'tingkat' => 4,
	    	'password' => bcrypt($request->password),
	    	'api_token' => str_random(60),
	    ]);

		return response()->json([
			'message' => "Verifikator dengan nama $verifikatorBaru->nama (NRP $verifikatorBaru->nrp) tingkat 4 berhasil terdaftar",
			'status_code' => 200
		], 200);
	}

	public function tambahHelpdesk(Request $request)
	{
		$verifikator = auth()->user();
		if($verifikator->tingkat==1){
			return response()->json([
				'message' => 'Unauthenticated.',
				'status_code' => 402
			],402);
		} else {

		    $error_message = [
				'nama.required' 				=> 'Kolom nama tidak boleh dikosongkan.',
				'nrp.required' 				=> 'Kolom nrp tidak boleh dikosongkan.',
				'nrp.unique' 				=> 'NRP telah terdaftar sebagai helpdesk.',
				'password.required' 				=> 'Kolom password tidak boleh dikosongkan.',
				'password.confirmed' 				=> 'Kolom password dan konfirmasi password tidak sama.',
			];

			$validation = Validator::make($request->all(),[
			'nama' 		=> 'required',
				'nrp' 		=> 'required|unique:helpdesk_users',
				'password' 		=> 'required|confirmed',
		    ],$error_message);


		    if($validation->fails()){
				return response()->json([
					'errors' => $validation->errors(),
					'status_code' => 400
				], 400);
		    }

		    $helpdesk = HelpdeskUser::create([
	    		'nama' => $request->nama,
		    	'nrp' => $request->nrp,
		    	'password' => bcrypt($request->password),
		    	'api_token' => str_random(60),
		    ]);

			return response()->json([
				'message' => 'Helpdesk dengan NRP '.$helpdesk->nrp.' berhasil didaftarkan.',
				'status_code' => 200
			], 200);

		}
		
	}
	public function daftarHelpdesk()
	{
		return datatables(HelpdeskUser::latest()->get())->toJson();
	}
	public function daftarVerifikator()
	{
		$verifikator=DB::table('verifikator_user')->join('satuan','satuan.satuan_id','=','verifikator_user.satuan_id')->select('verifikator_user_id','nrp','nama','tingkat','dibuat_oleh','verifikator_user.aktif as aktif','satuan.nama_satuan as satuan.nama_satuan','updated_at');
		return datatables($verifikator)->toJson();
	}
	
	public function hapusVerifikator(VerifikatorUser $verifikatorUser)
	{
		$verifikatorUser->aktif=false;
		if($verifikatorUser->save()){
			return response()->json([
				'message' => 'Verifikator berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus verifikator.',
				'status_code' => 400
			],400);
		}
	}
	
	public function aktifkanVerifikator(VerifikatorUser $verifikatorUser)
	{
		$verifikatorUser->aktif=true;
		if($verifikatorUser->save()){
			return response()->json([
				'message' => 'Verifikator berhasil diaktifkan.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal mengaktifkan verifikator.',
				'status_code' => 400
			],400);
		}
	}
	public function resetPasswordVerifikator(VerifikatorUser $verifikatorUser)
	{
		$verifikatorUser->password=bcrypt('verifikatorsipp');
		if($verifikatorUser->save()){
			return response()->json([
				'message' => 'Verifikator berhasil direset menjadi password: verifikatorsipp',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal mereset password verifikator.',
				'status_code' => 400
			],400);
		}
	}
	public function hapusHelpdeskUser(HelpdeskUser $helpdeskUser)
	{
		$helpdeskUser->aktif=false;
		if($helpdeskUser->save()){
			return response()->json([
				'message' => 'Helpdesk berhasil dihapus.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal menghapus helpdesk.',
				'status_code' => 400
			],400);
		}
	}
	public function aktifkanHelpdeskUser(HelpdeskUser $helpdeskUser)
	{
		$helpdeskUser->aktif=true;
		if($helpdeskUser->save()){
			return response()->json([
				'message' => 'Helpdesk berhasil diaktifkan.',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal mengaktifkan helpdesk.',
				'status_code' => 400
			],400);
		}
	}
	public function resetPasswordHelpdeskUser(HelpdeskUser $helpdeskUser)
	{
		$helpdeskUser->password=bcrypt('helpdesksipp');
		if($helpdeskUser->save()){
			return response()->json([
				'message' => 'Helpdesk berhasil direset menjadi password: helpdesksipp',
				'status_code' => 200
			],200);
		} else {
			return response()->json([
				'message' => 'Gagal mereset password helpdesk.',
				'status_code' => 400
			],400);
		}
	}
}
