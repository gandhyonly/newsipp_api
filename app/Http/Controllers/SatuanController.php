<?php

namespace App\Http\Controllers;

use App\Jabatan;
use App\JenisSatuan;
use App\Nivelering;
use App\Pangkat;
use App\Satuan;
use App\SatuanJabatan;
use App\StatusJabatan;
use Illuminate\Http\Request;
use Auth;
use DB;

class SatuanController extends Controller
{

	function __construct()
	{
		$this->limit=7000;
		$this->tipeDuplikasi='DUPLIKASI RUMKIT TINGKAT 1';
		$this->sumberDuplikasi=7182048;//DUPLIKASI RUMKIT1
		$this->targetDuplikasi=Satuan::where('jenis_satuan_id',59)->get();
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}
	public function redirectToIndex()
	{
		return redirect()->route('satuan.index');
	}
	public function index()
	{
		$dataSatuan = Satuan::root()->active()->orderBy('nama_satuan')->paginate(100);
		$jumlahJabatan = Jabatan::active()->count();

		$jumlahJenisSatuan = JenisSatuan::count();
		$dataJenisSatuan = JenisSatuan::get();
		return view('satuan.index',compact([
				'dataSatuan',
				'jumlahJenisSatuan',
				'jumlahJabatan',
				'dataJenisSatuan'
			]));
	}
	public function getDuplikasi()
	{
		return view('duplication.satuan');
	}
	public function postDuplikasi(Request $request)
	{
		$this->limit=7000;
		$this->tipeDuplikasi='DUPLIKASI';
		$this->sumberDuplikasi=$request->sumber_duplikasi_id;//DUPLIKASI RUMKIT1
		$this->targetDuplikasi=Satuan::where('satuan_id',$request->tujuan_duplikasi_id)->get();
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
		$this->jabatanID=$request->jabatan_id;
		$this->kepalaJabatanID=SatuanJabatan::find($request->jabatan_id)->parent_satuan_jabatan_id;

    	$this->duplication();
	}
	public function duplication()
	{
		DB::beginTransaction();
		$jabatanSementara = SatuanJabatan::find($this->jabatanID);
		$jabatanSementara->parent_satuan_jabatan_id=null;
		$jabatanSementara->save();


		$satuanSumberDuplikasi = Satuan::find($this->sumberDuplikasi);
		echo "$this->tipeDuplikasi Sumber: $satuanSumberDuplikasi->nama_satuan \n";
		echo "Removing Atasan\n";
		$this->arraySatuan[$satuanSumberDuplikasi->satuan_id]=$satuanSumberDuplikasi;
		foreach($satuanSumberDuplikasi->childsSatker as $satuan){
			$this->arraySatuan[$satuan->satuan_id]=$satuan;
			$this->satuanChild($satuan);
		}
		foreach($satuanSumberDuplikasi->jabatan as $jabatan){
			$this->arraySatuanJabatan[$jabatan->satuan_jabatan_id]=$jabatan;
			$this->jabatanChild($jabatan);
		}
		foreach($this->targetDuplikasi as $test => $satuan){
			echo "$this->tipeDuplikasi Tujuan: $satuan->nama_satuan ($satuan->satuan_id)\n";
			echo "remove All Satker, ";
			Satuan::where('parent_id',$satuan->satuan_id)->category('SATUAN KERJA')->delete();
			// echo "remove All Jabatan\n";
			// SatuanJabatan::where('satuan_id',$satuan->satuan_id)->delete();

			if($test==$this->limit){
				break;
			}
			$jumlahSatuan=0;
			foreach($this->arraySatuan as $key => $satuanBaru){
				if($key==$this->sumberDuplikasi){
					$this->arraySatuanBaru[$key]=Satuan::find($satuan->satuan_id);
				}else{
					$this->arraySatuanBaru[$key] = Satuan::create([
						'nama_satuan' => $satuanBaru->nama_satuan,
						'tipe' => $satuanBaru->tipe,
						'jenis_satuan_id' => $satuanBaru->jenis_satuan_id,
						'parent_id' => $this->arraySatuanBaru[$satuanBaru->parent_id]->satuan_id,
					]);
				}
				$jumlahSatuan++;
			}
			echo "jumlah satuan : $jumlahSatuan\n";
			$jumlahJabatan=0;
			foreach($this->arraySatuanJabatan as $key => $satuanJabatan){
					if($satuanJabatan->parent_satuan_jabatan_id==26){
						dd($satuanJabatan);
					}
					$this->arraySatuanJabatanBaru[$key] = SatuanJabatan::create([
						'satuan_id' => $this->arraySatuanBaru[$satuanJabatan->satuan_id]->satuan_id,
						'jabatan_id' => $satuanJabatan->jabatan_id,
						'parent_satuan_jabatan_id' => $satuanJabatan->parent_satuan_jabatan_id==null?null:$this->arraySatuanJabatanBaru[$satuanJabatan->parent_satuan_jabatan_id]->satuan_jabatan_id, 
						'nivelering_id' => $satuanJabatan->nivelering_id,
						'dsp' => $satuanJabatan->dsp,
						'status_jabatan_id' => $satuanJabatan->status_jabatan_id
					]);
					$this->arraySatuanJabatanBaru[$key]->keterangan=$this->arraySatuanJabatanBaru[$key]->fullKeterangan();
					$this->arraySatuanJabatanBaru[$key]->save();
					$this->arraySatuanJabatanBaru[$key]->pangkat()->sync($satuanJabatan->pangkat);
					$jumlahJabatan++;
			}
			echo "jumlah jabatan : $jumlahJabatan\n";
			foreach($this->arraySatuanJabatanBaru as $key => $satuanJabatanBaru){
				$temp = $satuanJabatan->parent_satuan_jabatan_id==null?null:$this->arraySatuanJabatanBaru[$satuanJabatan->parent_satuan_jabatan_id]->satuan_jabatan_id;
			}
		}
		$jabatanSementara = SatuanJabatan::find($this->jabatanID);
		$jabatanSementara->parent_satuan_jabatan_id=$this->kepalaJabatanID;
		$jabatanSementara->save();
		DB::commit();

		return redirect()->route('satuan.duplication.get')->with('success','Duplikasi Berhasil');
	}
	public function satuanChild($paramSatuan)
	{
		foreach($paramSatuan->childsSatker as $satuan){
			$this->arraySatuan[$satuan->satuan_id]=Satuan::find($satuan->satuan_id);
			if($satuan->childsSatker==null){
				return null;
			}
			$this->satuanChild($satuan);
		}
	}
	public function jabatanChild($paramJabatan)
	{
		if($paramJabatan->childs==null){
			return null;
		}
		foreach($paramJabatan->childs as $jabatan){
			$this->arraySatuanJabatan[$jabatan->satuan_jabatan_id]=SatuanJabatan::find($jabatan->satuan_jabatan_id);
			$this->jabatanChild($jabatan);
		}
	}
	public function show(Satuan $satuan)
	{
		$dataSatuanKerja = Satuan::sorted()->active()->where('parent_id',$satuan->satuan_id)->active()->category('SATUAN KERJA')->get();
		$dataSatuanWilayah = Satuan::sorted()->active()->where('parent_id',$satuan->satuan_id)->active()->category('SATUAN WILAYAH')->get();
		$dataSatuanJabatan = SatuanJabatan::active()->where('satuan_id',$satuan->satuan_id)->paginate(10);
		$jumlahSatker = Satuan::where('parent_id',$satuan->satuan_id)->category('SATUAN KERJA')->active()->count();
		$jumlahSatwil = Satuan::where('parent_id',$satuan->satuan_id)->category('SATUAN WILAYAH')->active()->count();
		return view('satuan.view',compact([
				'satuan',
				'dataSatuanKerja',
				'dataSatuanWilayah',
				'jumlahSatker',
				'jumlahSatwil',
				'dataSatuanJabatan',
			]));
	}

	public function APIdataWidget(Satuan $satuan)
	{
		$total['totalVerifiedSatuan']=0;
		$total['totalSatuan']=0;
		$total['totalJabatan']=0;
		$total['totalVerifiedJabatan']=0;
		$total = Satuan::widgetSatuan($satuan);

		return response()->json([
			'data' => $total
		],200);
	}

	public function create()
	{
		$selectDataJenisSatuan = JenisSatuan::get();
		return view('satuan.create',compact(['selectDataJenisSatuan']));
	}

	public function store(Request $request)
	{
		$satuan = Satuan::create($request->except('_token'));
		if($satuan)
			return redirect()->back()->with('success','Buat satuan berhasil dilakukan.');
		else 
			return redirect()->back()->with('error','Buat satuan gagal dilakukan.');
	}

	public function destroy(Satuan $satuan)
	{
		$satuan->deleted_by=auth('user')->user()->id;
		if($satuan->save())
			return redirect()->back()->with('success','Hapus satuan berhasil dilakukan.');
		else 
			return redirect()->back()->with('error','Hapus satuan gagal dilakukan.');
	}
	public function createSatuan(Satuan $satuan)
	{
		$selectDataJenisSatuan = JenisSatuan::get();
		return view('satuan.create-satuan',compact(['satuan','selectDataJenisSatuan']));
	}

	public function storeSatuan(Request $request,Satuan $satuan)
	{
		$jumlahSatuan=$request->jumlah_satuan;
		if($jumlahSatuan>1){
			for($i=1;$i<=$jumlahSatuan;$i++){
				$satuanBaru = Satuan::create([
					'nama_satuan' => $request->nama_satuan.'-'.$i,
					'tipe' => $request->tipe,
					'jenis_satuan_id' => $request->jenis_satuan_id,
					'parent_id' => $satuan->satuan_id
				]);
			}
			if($satuanBaru){
				return redirect()->route('satuan.show',$satuanBaru->parent_id)->with('success','Buat satuan berhasil dilakukan.');
			}
			else {
				return redirect()->route('satuan.show',$satuanBaru->parent_id)->with('error','Buat satuan gagal dilakukan.');
			}
		} else {
			$satuanBaru = Satuan::create($request->except(['_token','jumlah_satuan']));
			if($satuanBaru){
				return redirect()->route('satuan.show',$satuanBaru->parent_id)->with('success','Buat satuan berhasil dilakukan.');
			}
			else {
				return redirect()->route('satuan.show',$satuanBaru->parent_id)->with('error','Buat satuan gagal dilakukan.');
			}
		}
	}

	public function createJabatan(Satuan $satuan)
	{
		$selectDataJabatan = Jabatan::active()->select('jabatan_id','nama_jabatan')->orderBy('nama_jabatan')->get();
		if($satuan->parent()->count()>0){
			$selectDataAtasanJabatan = SatuanJabatan::active()->where('satuan_id',$satuan->satuan_id)->orWhere('satuan_id',$satuan->parent->satuan_id)->get();
		} else {
			$selectDataAtasanJabatan = SatuanJabatan::active()->where('satuan_id',$satuan->satuan_id)->get();
		}

		$selectDataNivelering = Nivelering::get();
		$selectDataPangkat = Pangkat::get();
		$selectDataStatusJabatan = StatusJabatan::get();
		

		return view('satuan-jabatan.create',compact([
			'selectDataJabatan','selectDataNivelering',
			'selectDataPangkat','selectDataAtasanJabatan','selectDataStatusJabatan','satuan'
		]));
	}

	public function storeJabatan(Request $request,Satuan $satuan)
	{
		$input = $request->except(['_token','pangkat_id','nama_satuan','pangkat']);
		$arrayStringPangkat = $request->input('pangkat'); 
		$pangkatPolisi = true;
		$pangkatID=[];
		foreach($arrayStringPangkat as $pangkat){
			switch($pangkat){
				case 'BA': 
					$pangkatID = array_merge($pangkatID,[19,20,21,22,23,24,25,26,27,28,29,30]);
					break;
				case 'IP':
					$pangkatID = array_merge($pangkatID,[31,32]);
					break;
				case 'AKP':
					$pangkatID = array_merge($pangkatID,[33]);
					break;
				case 'KP':
					$pangkatID = array_merge($pangkatID,[34]);
					break;
				case 'AKBP':
					$pangkatID = array_merge($pangkatID,[35]);
					break;
				case 'KBP':
					$pangkatID = array_merge($pangkatID,[36]); 
					break;
				case 'BRIGJEN':
					$pangkatID = array_merge($pangkatID,[37]); 
					break;
				case 'IRJEN':
					$pangkatID = array_merge($pangkatID,[38]); 
					break;
				case 'KOMJEN':
					$pangkatID = array_merge($pangkatID,[39]); 
					break;
				case 'JENDERAL':
					$pangkatID = array_merge($pangkatID,[40]); 
					break;
				case 'null':
					$pangkatPolisi = false;
					break;
				default : 
					$pangkatPolisi = false;
					break;
			}
		}

		$input['parent_satuan_jabatan_id']=$request->input('parent_satuan_jabatan_id') == 'null' ? null : $request->input('parent_satuan_jabatan_id');
		$input['nivelering_id']=$request->input('nivelering_id') == 'null' ? null : $request->input('nivelering_id');
		$input['keterangan'] = Jabatan::find($input['jabatan_id'])->nama_jabatan.' '.Satuan::find($input['satuan_id'])->fullName();
		$satuanJabatan = SatuanJabatan::create($input);
		if($request->has('pangkat_id')){
			foreach($request->pangkat_id as $pangkat_id){
				array_push($pangkatID,intval($pangkat_id));
			}
		}
		$satuanJabatan->pangkat()->sync($pangkatID);
		if($satuanJabatan){
			return redirect()->route('satuan.show',$satuan->satuan_id)->with('success','Buat satuan berhasil dilakukan.');
		}
		else {
			return redirect()->route('satuan.show',$satuan->satuan_id)->with('error','Buat satuan gagal dilakukan.');
		}
	}

	public function createJenisSatuan()
	{
		return view('satuan.create-jenis-satuan');
	}

	public function storeJenisSatuan()
	{
		# code...
	}

	public function edit(Satuan $satuan)
	{
		$selectDataJenisSatuan = JenisSatuan::get();
		$selectDataSatuanAtasan = $satuan->parent->childs()->get();
		return view('satuan.edit',compact(['satuan','selectDataJenisSatuan','selectDataSatuanAtasan']));
	}

	public function update(Request $request, Satuan $satuan)
	{
		if($satuan->update($request->except(['_token']))){
			$satuan->setSatuanJabatanKeterangan();
			return redirect()->route('satuan.show',$satuan->satuan_id)->with('success','Update satuan berhasil dilakukan.');
		}
		else {
			return redirect()->route('satuan.show',$satuan->satuan_id)->with('error','Update satuan gagal dilakukan.');
		}
	}

	public function verification(Satuan $satuan)
	{
		$satuan->is_verified=!$satuan->is_verified;
		$satuan->verified_by=auth()->user()->username;

		if($satuan->save())
			return redirect()->back()->with('success','Verifikasi berhasil.');
		else
			return redirect()->back()->with('error','Verifikasi gagal.');
	}

	public function filter(Request $request)
	{
		$namaSatuan = $request->input('nama_satuan');
		$jenisSatuanID = $request->input('jenis_satuan_id');

		if($request->input('jenis_satuan_id')=='all'){
			$dataSatuanKerja = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->category('SATUAN KERJA')->orderBy('nama_satuan')->paginate(50);
			$dataSatuanWilayah = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->category('SATUAN WILAYAH')->orderBy('nama_satuan')->paginate(50);
			$jumlahSatuan = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->count();
			$jumlahSatuanKerja = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->category('SATUAN KERJA')->count();
			$jumlahSatuanWilayah = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->category('SATUAN WILAYAH')->count();
		}
		else{
			$dataSatuanKerja = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->where('jenis_satuan_id',$jenisSatuanID)->category('SATUAN KERJA')->orderBy('nama_satuan')->paginate(50);
			$dataSatuanWilayah = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->where('jenis_satuan_id',$jenisSatuanID)->category('SATUAN WILAYAH')->orderBy('nama_satuan')->paginate(50);
			$jumlahSatuan = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->where('jenis_satuan_id',$jenisSatuanID)->count();
			$jumlahSatuanKerja = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->where('jenis_satuan_id',$jenisSatuanID)->category('SATUAN KERJA')->count();
			$jumlahSatuanWilayah = Satuan::active()->where('nama_satuan','LIKE','%'.$namaSatuan.'%')->where('jenis_satuan_id',$jenisSatuanID)->category('SATUAN WILAYAH')->count();
		}
		$dataJenisSatuan = JenisSatuan::get();

		return view('satuan.filter',compact([
				'dataSatuanKerja',
				'dataSatuanWilayah',
				'dataJenisSatuan',
				'jumlahSatuan',
				'jumlahSatuanWilayah',
				'jumlahSatuanKerja'
			]));
	}
	
	public function masalahPolsubsektor()
	{
		$i=0;
		foreach(Satuan::active()->where('jenis_satuan_id',48)->get() as $satuan){
			if($satuan->parent->jenis_satuan_id == 3 || $satuan->parent->jenis_satuan_id == 43 || $satuan->parent->jenis_satuan_id == 6 || $satuan->parent->jenis_satuan_id == 4){
				$i++;
				$parent=$satuan->parent->nama_satuan;
				echo "$i = (ID:$satuan->satuan_id) $satuan->nama_satuan > $parent <br>";
			}
		}
	}
	public function tambahKeteranganSatuan()
	{
		foreach(Satuan::where('keterangan')->get() as $satuan){
			$satuan->keterangan=$satuan->fullPrint();
			$satuan->save();
			echo $satuan->keterangan.'<br>';
		}
	}
	public function masalahSatuanDouble()
	{
		echo 'test';
		$i=0;
		$temp = null;
		foreach(Satuan::active()->orderBy('nama_satuan')->withCount('childs')->orderBy('satuan_id')->get() as $satuan){
			if($satuan->keterangan == $temp){
				$i++;
				echo "$i = (ID:$satuan->satuan_id)==(ID:$tempSatuan->satuan_id) $satuan->keterangan ($jumlah < $satuan->childs_count)<br>";
			}
			$jumlah = $satuan->childs_count;
			$tempSatuan=$satuan;
			$temp=$satuan->keterangan;
		}
	}
}
