<?php

namespace App\Http\Controllers;

use App\Pangkat;
use Illuminate\Http\Request;

class PangkatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataPangkat = Pangkat::paginate(60);
        $pangkatCount = Pangkat::count();
        return view('pangkat.index')
            ->with('pangkatCount',$pangkatCount)
            ->with('dataPangkat',$dataPangkat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectGolongan = Golongan::get();
        return view('pangkat.create')
            ->with('selectGolongan',$selectGolongan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pangkat $pangkat)
    {
        if($pangkat->delete())
            return redirect()->route('pangkat.index')->with('success','Hapus pangkat berhasil.');
        else
            return redirect()->route('pangkat.index')->with('error','Hapus pangkat gagal.');
    }
}
