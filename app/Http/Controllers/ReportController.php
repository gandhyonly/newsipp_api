<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PersonelData;
use App\Fisik;
use App\RiwayatJabatan;
use App\RiwayatPangkat;
use App\PendidikanPersonel;
use App\BahasaPersonel;
use App\Penugasan;

class ReportController extends Controller
{
	public function reportHarian()
	{
		echo "Personel Data : <br>";
		echo "==================<br>";
		echo "Jumlah : ".PersonelData::count();
		echo "<br>MIN ID : ".PersonelData::orderBy('personel_data_id','ASC')->first()->personel_data_id;
		echo "<br>MAX ID : ".PersonelData::orderBy('personel_data_id','DESC')->first()->personel_data_id;
		echo "<br><br>";
		echo "Fisik : <br>";
		echo "==================<br>";
		echo "Jumlah : ".Fisik::count();
		echo "<br>MIN ID : ".Fisik::orderBy('fisik_personel_id','ASC')->first()->fisik_personel_id;
		echo "<br>MAX ID : ".Fisik::orderBy('fisik_personel_id','DESC')->first()->fisik_personel_id;
		echo "<br><br>";
		echo "Riwayat Jabatan : <br>";
		echo "==================<br>";
		echo "Jumlah : ".RiwayatJabatan::count();
		echo "<br>MIN ID : ".RiwayatJabatan::orderBy('riwayat_jabatan_id','ASC')->first()->riwayat_jabatan_id;
		echo "<br>MAX ID : ".RiwayatJabatan::orderBy('riwayat_jabatan_id','DESC')->first()->riwayat_jabatan_id;
		echo "<br><br>";
		echo "Riwayat Pangkat : <br>";
		echo "==================<br>";
		echo "Jumlah : ".RiwayatPangkat::count();
		echo "<br>MIN ID : ".RiwayatPangkat::orderBy('riwayat_pangkat_id','ASC')->first()->riwayat_pangkat_id;
		echo "<br>MAX ID : ".RiwayatPangkat::orderBy('riwayat_pangkat_id','DESC')->first()->riwayat_pangkat_id;
		echo "<br><br>";
		echo "Pendidikan Personel : <br>";
		echo "==================<br>";
		echo "Jumlah : ".PendidikanPersonel::count();
		echo "<br>MIN ID : ".PendidikanPersonel::orderBy('pendidikan_personel_id','ASC')->first()->pendidikan_personel_id;
		echo "<br>MAX ID : ".PendidikanPersonel::orderBy('pendidikan_personel_id','DESC')->first()->pendidikan_personel_id;
		echo "<br><br>";
		echo "Bahasa : <br>";
		echo "==================<br>";
		echo "Jumlah : ".BahasaPersonel::count();
		echo "<br>MIN ID : ".BahasaPersonel::orderBy('bahasa_personel_id','ASC')->first()->bahasa_personel_id;
		echo "<br>MAX ID : ".BahasaPersonel::orderBy('bahasa_personel_id','DESC')->first()->bahasa_personel_id;
		echo "<br><br>";
		echo "Penugasan : <br>";
		echo "==================<br>";
		echo "Jumlah : ".Penugasan::count();
		echo "<br>MIN ID : ".Penugasan::orderBy('penugasan_id','ASC')->first()->penugasan_id;
		echo "<br>MAX ID : ".Penugasan::orderBy('penugasan_id','DESC')->first()->penugasan_id;
		echo "<br><br>";
	}
}
