<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PolsekJabatanMigration extends Controller
{
	
	public function SIUM($POLSEKRURAL,$KAPOLSEK)
	{
		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$SIUM = $POLSEKRURAL->childs()->where('nama_satuan','SIUM')->first();

		// JABATAN PADA SIUM
		$KASIUM = SatuanJabatan::createJabatan($SIUM,$KAPOLSEK,'KASIUM',['IP'],'IV/B',1);
		$BANUM = SatuanJabatan::createJabatan($SIUM,$KASIUM,'BANUM',['PNS II','PNS I'],'-',2);

		// SATUAN PADA SIUM, URRENMIN
		$URRENMIN = $SIUM->childs()->where('nama_satuan','URRENMIN')->first();

			// JABATAN PADA URRENMIN
			$BAMIN = SatuanJabatan::createJabatan($URRENMIN,$KASIUM,'BAMIN',['BA'],'-',1);

		// SATUAN PADA SIUM, URTAUD
		$URTAUD = $SIUM->childs()->where('nama_satuan','URTAUD')->first();

			// JABATAN PADA URRENMIN
			$BAMIN = SatuanJabatan::createJabatan($URTAUD,$KASIUM,'BAMIN',['BA'],'-',1);
			
		// SATUAN PADA SIUM, URTAHTI
		$URTAHTI = $SIUM->childs()->where('nama_satuan','URTAHTI')->first();

		// JABATAN PADA URTAHTI
		$BAMIN = SatuanJabatan::createJabatan($URTAHTI,$KASIUM,'BAMIN',['BA'],'-',1);
	}
	public function SIHUMAS($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja UNIT SIHUMAS
		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$SIHUMAS = $POLSEKRURAL->childs()->where('nama_satuan','SIHUMAS')->first();

		// JABATAN PADA SIHUMAS
		$KASIHUMAS = SatuanJabatan::createJabatan($SIHUMAS,$KAPOLSEK,'KASIHUMAS',['IP'], 'IV/B',1);
		$BAMIN = SatuanJabatan::createJabatan($SIHUMAS,$KASIHUMAS,'BAMIN',['BA'],'-',2);
	}
	public function SPKT($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja SPKT
		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$SPKT = $POLSEKRURAL->childs()->where('nama_satuan','SPKT')->first();

		// JABATAN PADA SPKT
		$KASPKT = SatuanJabatan::createJabatan($SPKT,$KAPOLSEK,'KASPKT',['IP'], 'IV/B',3);
		$BASPKT = SatuanJabatan::createJabatan($SPKT,$KASPKT,'BASPKT',['BA'],'-',3);
	}
	public function UNIT_BINMAS($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja UNIT BINMAS
		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$UNITBINMAS = $POLSEKRURAL->childs()->where('nama_satuan','UNITBINMAS')->first();

		// JABATAN PADA UNITBINMAS
		$KANITBINMAS = SatuanJabatan::createJabatan($UNITBINMAS,$KAPOLSEK,'KANITBINMAS',['IP'], 'IV/B',1);
		$BANIT = SatuanJabatan::createJabatan($UNITBINMAS,$KANITBINMAS,'BANIT',['BA'],'-',4);
	}
	public function UNIT_INTELKAM($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja UNIT INTELKAM 
			// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
			$UNITINTELKAM = $POLSEKRURAL->childs()->where('nama_satuan','UNITINTELKAM')->first();

			// JABATAN PADA UNITINTELKAM
			$KANITINTELKAM = SatuanJabatan::createJabatan($UNITINTELKAM,$KAPOLSEK,'KANITINTELKAM',['IP'], 'IV/B',1);
			$BANIT = SatuanJabatan::createJabatan($UNITINTELKAM,$KANITINTELKAM,'BANIT',['BA'],'-',4);
	}
	public function UNIT_LANTAS($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja UNIT LANTAS 
		//PM

		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$UNITLANTAS = $POLSEKRURAL->childs()->where('nama_satuan','UNITLANTAS')->first();
	}
	public function UNIT_POLAIR($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja UNIT POLAIR
		//PM

		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$UNITPOLAIR = $POLSEKRURAL->childs()->where('nama_satuan','UNITPOLAIR')->first();
	}
	public function UNIT_PROVOS($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja UNIT PROVOS

		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$UNITPROVOS = $POLSEKRURAL->childs()->where('nama_satuan','UNITPROVOS')->first();

		// JABATAN PADA UNIT PROVOS
		$KANITPROVOS = SatuanJabatan::createJabatan($UNITPROVOS,$KAPOLSEK,'KANITPROVOS',['IP'],'IV/B',1);
		$BANIT = SatuanJabatan::createJabatan($UNITPROVOS,$KANITPROVOS,'BANIT',['BA'],'-',2);
	}
	public function UNIT_RESKRIM($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja UNIT RESKRIM 

		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$UNITRESKRIM = $POLSEKRURAL->childs()->where('nama_satuan','UNITRESKRIM')->first();

		// JABATAN PADA UNITRESKRIM
		$KANITRESKRIM = SatuanJabatan::createJabatan($UNITRESKRIM,$KAPOLSEK,'KANITRESKRIM',['IP'], 'IV/B',1);
		$BANIT = SatuanJabatan::createJabatan($UNITRESKRIM,$KANITRESKRIM,'BANIT',['BA'],'-',6);
	}
	public function UNIT_SABHARA($POLSEKRURAL,$KAPOLSEK)
	{
		//Kapolsek rural, Satuan Kerja UNIT SABHARA
		// Urutan, satuan, atasan, nama jabatan, pangkat, nivelering, dsp
		$UNITSABHARA = $POLSEKRURAL->childs()->where('nama_satuan','UNITSABHARA')->first();

		// JABATAN PADA UNITSABHARA
		$KANITSABHARA = SatuanJabatan::createJabatan($UNITSABHARA,$KAPOLSEK,'KANITSABHARA',['IP'], 'IV/B',1);
		$BANIT = SatuanJabatan::createJabatan($UNITSABHARA,$KANITSABHARA,'BANIT',['BA'],'-',12);
	}
}
