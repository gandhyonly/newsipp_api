<?php

namespace App\Http\Controllers\Duplication;

use App\Http\Controllers\Controller;
use App\SatuanJabatan;
use Illuminate\Http\Request;

class NiveleringController extends Controller
{
	public function niveleringNormalisasi()
	{
		SatuanJabatan::where('nivelering_id',5)->update(['nivelering_id' => 4]);
		SatuanJabatan::where('nivelering_id',6)->update(['nivelering_id' => 5]);
		SatuanJabatan::where('nivelering_id',7)->update(['nivelering_id' => 6]);
		SatuanJabatan::where('nivelering_id',9)->update(['nivelering_id' => 7]);
		SatuanJabatan::where('nivelering_id',10)->update(['nivelering_id' => 8]);
		SatuanJabatan::where('nivelering_id',12)->update(['nivelering_id' => 9]);
		SatuanJabatan::where('nivelering_id',13)->update(['nivelering_id' => 10]);
		SatuanJabatan::where('nivelering_id',14)->update(['nivelering_id' => 11]);
		SatuanJabatan::where('nivelering_id',17)->update(['nivelering_id' => 12]);
	}
}
