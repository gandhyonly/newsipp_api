<?php

namespace App\Http\Controllers\Duplication;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DuplikasiRumkitTingkat1Controller extends Controller
{
	function __construct()
	{
		$this->tipeDuplikasi='DUPLIKASI RUMKIT TINGKAT 1';
		// $this->sumberDuplikasi=13137;//POLDA SUMUT > POLRESTABES MEDAN > POLSEK MEDAN KOTA (urban)
		$this->sumberDuplikasi=7182048;//DUPLIKASI POLSEK URBAN
		// $this->targetDuplikasi=Satuan::where('jenis_satuan_id',45)->get();
		$this->targetDuplikasi=1;
		$this->arraySatuanJabatan=[];
		$this->arraySatuan=[];
		$this->arraySatuanBaru=[];
		$this->arraySatuanJabatanBaru=[];
	}
	public function run(Satuan)
	{
		$this->duplication;
	}
	public function duplication()
	{
		$this->removeAtasanJabatan();
		$satuanSumberDuplikasi = Satuan::find($this->sumberDuplikasi);
		echo "$this->tipeDuplikasi Sumber: $satuanSumberDuplikasi->nama_satuan \n";
		$this->arraySatuan[$satuanSumberDuplikasi->satuan_id]=$satuanSumberDuplikasi;
		foreach($satuanSumberDuplikasi->childsSatker as $satuan){
			$this->arraySatuan[$satuan->satuan_id]=$satuan;
			$this->satuanChild($satuan);
		}
		foreach($satuanSumberDuplikasi->jabatan as $jabatan){
			$this->arraySatuanJabatan[$jabatan->satuan_jabatan_id]=$jabatan;
			$this->jabatanChild($jabatan);
		}
		$satuan = Satuan::find($this->targetDuplikasi);
		Satuan::where('parent_id',$satuan->satuan_id)->category('SATUAN KERJA')->delete();
		SatuanJabatan::where('satuan_id',$satuan->satuan_id)->delete();

		// Pencarian Kapolda
		echo "$this->tipeDuplikasi Tujuan: $satuan->nama_satuan ($satuan->satuan_id)\n";
		$jumlahSatuan=0;
		foreach($this->arraySatuan as $key => $satuanBaru){
			if($key==$this->sumberDuplikasi){
				$this->arraySatuanBaru[$key]=Satuan::find($satuan->satuan_id);
			}else{
				$this->arraySatuanBaru[$key] = Satuan::create([
					'nama_satuan' => $satuanBaru->nama_satuan,
					'tipe' => $satuanBaru->tipe,
					'jenis_satuan_id' => $satuanBaru->jenis_satuan_id,
					'parent_id' => $this->arraySatuanBaru[$satuanBaru->parent_id]->satuan_id,
				]);
			}
			$jumlahSatuan++;
		}
		echo "jumlah satuan : $jumlahSatuan\n";
		$jumlahJabatan=0;
		foreach($this->arraySatuanJabatan as $key => $satuanJabatan){
				$this->arraySatuanJabatanBaru[$key] = SatuanJabatan::create([
					'satuan_id' => $this->arraySatuanBaru[$satuanJabatan->satuan_id]->satuan_id,
					'jabatan_id' => $satuanJabatan->jabatan_id,
					'parent_satuan_jabatan_id' => $satuanJabatan->parent_satuan_jabatan_id==null?null:$this->arraySatuanJabatanBaru[$satuanJabatan->parent_satuan_jabatan_id]->satuan_jabatan_id, 
					'nivelering_id' => $satuanJabatan->nivelering_id,
					'dsp' => $satuanJabatan->dsp,
					'status_jabatan_id' => $satuanJabatan->status_jabatan_id
				]);
				$this->arraySatuanJabatanBaru[$key]->keterangan=$this->arraySatuanJabatanBaru[$key]->fullKeterangan();
				$this->arraySatuanJabatanBaru[$key]->save();
				$this->arraySatuanJabatanBaru[$key]->pangkat()->sync($satuanJabatan->pangkat);
				$jumlahJabatan++;
		}
		echo "jumlah jabatan : $jumlahJabatan\n";
		foreach($this->arraySatuanJabatanBaru as $key => $satuanJabatanBaru){
			$temp = $satuanJabatan->parent_satuan_jabatan_id==null?null:$this->arraySatuanJabatanBaru[$satuanJabatan->parent_satuan_jabatan_id]->satuan_jabatan_id;
		}
	}
	public function satuanChild($paramSatuan)
	{
		foreach($paramSatuan->childsSatker as $satuan){
			$this->arraySatuan[$satuan->satuan_id]=Satuan::find($satuan->satuan_id);
			if($satuan->childsSatker==null){
				return null;
			}
			$this->satuanChild($satuan);
		}
	}
	public function jabatanChild($paramJabatan)
	{
		if($paramJabatan->childs==null){
			return null;
		}
		foreach($paramJabatan->childs as $jabatan){
			$this->arraySatuanJabatan[$jabatan->satuan_jabatan_id]=SatuanJabatan::find($jabatan->satuan_jabatan_id);
			$this->jabatanChild($jabatan);
		}
	}
	public function removeAtasanJabatan()
	{
		SatuanJabatan::whereIn('jabatan_id',[276,280,281,282,283,286,843,2193,2208])->where('satuan_id',$this->sumberDuplikasi)->update(['parent_satuan_jabatan_id' => null]);
	}
}
