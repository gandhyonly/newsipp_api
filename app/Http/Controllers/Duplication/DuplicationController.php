<?php

namespace App\Http\Controllers\Duplication;

use App\Eselon;
use App\Http\Controllers\Controller;
use App\Nivelering;
use App\Satuan;
use App\SatuanJabatan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class DuplicationController extends Controller
{
	function __construct()
	{
		$this->limit=20000;
	}
	public function duplication()
	{
		$this->removeAtasanJabatan();
		$satuanSumberDuplikasi = Satuan::find($this->sumberDuplikasi);
		echo "$this->tipeDuplikasi Sumber: $satuanSumberDuplikasi->nama_satuan \n";
		$this->arraySatuan[$satuanSumberDuplikasi->satuan_id]=$satuanSumberDuplikasi;
		foreach($satuanSumberDuplikasi->childsSatker as $satuan){
			$this->arraySatuan[$satuan->satuan_id]=$satuan;
			$this->satuanChild($satuan);
		}
		foreach($satuanSumberDuplikasi->jabatan as $jabatan){
			$this->arraySatuanJabatan[$jabatan->satuan_jabatan_id]=$jabatan;
			$this->jabatanChild($jabatan);
		}
		foreach($this->targetDuplikasi as $test => $satuan){
			Satuan::where('parent_id',$satuan->satuan_id)->category('SATUAN KERJA')->delete();
			SatuanJabatan::where('satuan_id',$satuan->satuan_id)->delete();

			if($test==$this->limit){
				break;
			}
			// Pencarian Kapolda
			echo "$this->tipeDuplikasi Tujuan: $satuan->nama_satuan ($satuan->satuan_id)\n";
			$jumlahSatuan=0;
			foreach($this->arraySatuan as $key => $satuanBaru){
				if($key==$this->sumberDuplikasi){
					$this->arraySatuanBaru[$key]=Satuan::find($satuan->satuan_id);
				}else{
					$this->arraySatuanBaru[$key] = Satuan::create([
						'nama_satuan' => $satuanBaru->nama_satuan,
						'tipe' => $satuanBaru->tipe,
						'jenis_satuan_id' => $satuanBaru->jenis_satuan_id,
						'parent_id' => $this->arraySatuanBaru[$satuanBaru->parent_id]->satuan_id,
					]);
				}
				$jumlahSatuan++;
			}
			echo "jumlah satuan : $jumlahSatuan\n";
			$jumlahJabatan=0;
			foreach($this->arraySatuanJabatan as $key => $satuanJabatan){
					$this->arraySatuanJabatanBaru[$key] = SatuanJabatan::create([
						'satuan_id' => $this->arraySatuanBaru[$satuanJabatan->satuan_id]->satuan_id,
						'jabatan_id' => $satuanJabatan->jabatan_id,
						'parent_satuan_jabatan_id' => $satuanJabatan->parent_satuan_jabatan_id==null?null:$this->arraySatuanJabatanBaru[$satuanJabatan->parent_satuan_jabatan_id]->satuan_jabatan_id, 
						'nivelering_id' => $satuanJabatan->nivelering_id,
						'dsp' => $satuanJabatan->dsp,
						'status_jabatan_id' => $satuanJabatan->status_jabatan_id
					]);
					$this->arraySatuanJabatanBaru[$key]->keterangan=$this->arraySatuanJabatanBaru[$key]->fullKeterangan();
					$this->arraySatuanJabatanBaru[$key]->save();
					$this->arraySatuanJabatanBaru[$key]->pangkat()->sync($satuanJabatan->pangkat);
					$jumlahJabatan++;
			}
			echo "jumlah jabatan : $jumlahJabatan\n";
			foreach($this->arraySatuanJabatanBaru as $key => $satuanJabatanBaru){
				$temp = $satuanJabatan->parent_satuan_jabatan_id==null?null:$this->arraySatuanJabatanBaru[$satuanJabatan->parent_satuan_jabatan_id]->satuan_jabatan_id;
			}
		}
	}
	public function satuanChild($paramSatuan)
	{
		foreach($paramSatuan->childsSatker as $satuan){
			$this->arraySatuan[$satuan->satuan_id]=Satuan::find($satuan->satuan_id);
			if($satuan->childsSatker==null){
				return null;
			}
			$this->satuanChild($satuan);
		}
	}
	public function jabatanChild($paramJabatan)
	{
		if($paramJabatan->childs==null){
			return null;
		}
		foreach($paramJabatan->childs as $jabatan){
			$this->arraySatuanJabatan[$jabatan->satuan_jabatan_id]=SatuanJabatan::find($jabatan->satuan_jabatan_id);
			$this->jabatanChild($jabatan);
		}
	}
	public function removeAtasanJabatan()
	{
		SatuanJabatan::whereIn('jabatan_id',[276,280,281,282,283,286,843,2193,2208])->where('satuan_id',$this->sumberDuplikasi)->update(['parent_satuan_jabatan_id' => null]);
	}
}
