<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penghargaan extends Model
{
	protected $table = 'penghargaan';
	protected $primaryKey = 'penghargaan_id';
	protected $fillable = ['nama_penghargaan'];
	public $timestamps = false;

	public function personel()
	{
		return $this->belongsToMany(Personel::class,'penghargaan_personel','penghargaan_id','personel_id')->withPivot(['nama_penghargaan','tanggal_penghargaan','surat_nomor','surat_file','pemberi_penghargaan','tanggal_perubahan']);
	}
}
