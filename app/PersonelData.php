<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonelData extends Model
{

	protected $table = 'personel_data';
	protected $primaryKey = 'personel_data_id';
	protected $guarded = [];

	public $timestamps=false;

	const CREATED_AT = null;
	const UPDATED_AT = 'tanggal_perubahan';


	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
}
