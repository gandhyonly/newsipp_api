<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonelBersih extends Model
{
	protected $primaryKey = 'personel_id';
	protected $table = 'personel_bersih';
	protected $guarded = [];
	protected $dates = ['tmt_jabatan','tmt_pertama','tanggal_lahir'];
	public $timestamps = false;

	public function statusList()
	{
		if($this->is_duplicate || $this->is_error){
			$icon = $this->is_duplicate==true ? "<span class='label bg-orange'>data duplikat</span><br>" :'';
			$icon.= $this->is_error==true ? "<span class='label bg-red'>data error</span>" :'';	
		} else {
			$icon = "<span class='label bg-blue'>data bersih</span>";
		}
		return $icon;
	}
	public function scopeActive($query)
    {
        return $query->where('is_removed',false);
    }
	public function verificationButton()
	{
		if($this->is_duplicate){
			$icon = "<button type='submit' class='btn btn-xs bg-green'><i class='material-icons'>check</i></button>";
			return "<form method='POST' action='".route('personel.verification',$this->personel_id)."'>".
			csrf_field().
			"<input type='hidden' name='_method' value='PATCH'>".
			$icon.
			"</form>";
		}
		return '';
	}

	public function errors()
	{
		return $this->belongsToMany(Error::class,'personel_error','personel_id','error_id')->withPivot('error_message');
	}

	public function agama()
	{
		return $this->belongsTo(Agama::class);
	}

	public function jenisKelamin()
	{
		return $this->belongsTo(JenisKelamin::class);
	}

	public function statusPersonel()
	{
		return $this->belongsTo(StatusPersonel::class);
	}
}
