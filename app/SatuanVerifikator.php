<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SatuanVerifikator extends Model
{
    protected $table = 'satuan_verifikator';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
