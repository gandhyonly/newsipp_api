<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
	protected $table = 'kota';
	protected $primaryKey = 'kota_id';
	protected $fillable = ['nama_kota'];
	public $timestamps = false;
}
