<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherGroup extends Model
{
	protected $table = 'other_groups';
	protected $primaryKey = 'id';
	protected $fillable = ['name'];
	public $timestamps = false;
	public function others()
	{
		return $this->hasMany(Other::class)->orderBy('value'); 
	}
}
