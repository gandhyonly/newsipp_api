<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Negara extends Model
{
	protected $table = 'negara';
	protected $primaryKey = 'negara_id';
	protected $fillable = ['nama_negara'];
	public $timestamps = false;

	public function misi()
	{
		return $this->hasMany(Misi::class,'negara_id')->orderBy('nama_misi');
	}
	public function lokasiPenugasan()
	{
		return $this->hasMany(LokasiPenugasan::class,'negara_id')->orderBy('kota')->orderBy('nama_lembaga');
	}
}
