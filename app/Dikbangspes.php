<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dikbangspes extends Model
{
	protected $table = 'dikbangspes';
	protected $primaryKey = 'dikbangspes_id';
	protected $fillable = ['tipe_dikbangspes_id','nama_dikbangspes'];
	public $timestamps = false;

	public function tipeDikbangspes()
	{
		return $this->belongsTo(TipeDikbangspes::class,'tipe_dikbangspes_id');
	}
}
