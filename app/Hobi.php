<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hobi extends Model
{
	protected $table = 'hobi';
	protected $primaryKey = 'hobi_id';
	protected $fillable = ['hobi'];
	public $timestamps = false;

	public function personel()
	{
		return $this->belongsToMany(Personel::class,'hobi_personel','personel_id','hobi_id');
	}

}
