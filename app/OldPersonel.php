<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldPersonel extends Model
{
	protected $connection = 'newsipp_jateng';
	protected $table = 't_personil';
}
