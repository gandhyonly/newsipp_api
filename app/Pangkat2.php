<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pangkat2 extends Model
{
    protected $connection = 'mysql-satjab';
	protected $table = 'pangkat';
	protected $primaryKey = 'pangkat_id';
	protected $fillable = ['nama_pangkat','tipe','golongan_id','singkatan','singkatan_tentara'];
	public $timestamps = false;

	public function golongan()
	{
		return $this->belongsTo(Golongan::class,'golongan_id','golongan_id');
	}

	public function jabatan()
	{
		return $this->belongsToMany(SatuanJabatan2::class,'pangkat_jabatan','pangkat_id','satuan_jabatan_id');
	}
	public function personel()
	{
		return $this->belongsToMany(Personel::class,'pangkat_personel','pangkat_id','personel_id')->withPivot(['status_terakhir','tmt_pangkat','surat_keputusan_nomor','surat_keputusan_file','tanggal_perubahan']);
	}

	public function getNamaGolongan()
	{
		if($this->golongan == null){
			return null;
		}
		return $this->golongan->nama_golongan;
	}
}
