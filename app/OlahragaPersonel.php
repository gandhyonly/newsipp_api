<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OlahragaPersonel extends Model
{
	protected $table = 'olahraga_personel';
	protected $primaryKey = 'olahraga_personel_id';
	protected $fillable = ['olahraga_id','personel_id'];
	public $timestamps = false;
	public function olahraga()
	{
		return $this->belongsTo(Olahraga::class,'olahraga_id');
	}
	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
}
