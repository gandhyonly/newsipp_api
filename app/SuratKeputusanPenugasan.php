<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuratKeputusanPenugasan extends Model
{
	protected $table = 'surat_keputusan_penugasan';
	protected $primaryKey = 'surat_keputusan_penugasan_id';
	protected $fillable = ['surat_keputusan_nomor','surat_keputusan_file','tanggal_mulai','tanggal_selesai','perpanjang','parent_id'];
	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';
	public function skepPerpanjangan()
	{
		return $this->belongsTo(SuratKeputusanPenugasan::class,'surat_keputusan_penugasan_id');
	}
}
