<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SIM extends Model
{
	protected $primaryKey = 'sim_id';
	protected $table = 'sim';
	protected $fillable = ['sim_nama'];
	public $timestamps = false;

	public function personel()
	{
		return $this->belongsToMany(Personel::class,'sim_personel','sim_id','personel_id')->withPivot(['file','sim_nomor']);
	}
}
