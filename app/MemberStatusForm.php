<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberStatusForm extends Model
{
	protected $table = 'member_status_form';
	protected $fillable = ['member_id','status_form_id','verifikator_1','verifikator_2','verifikator_3','verifikator_4'];

	public $timestamps = false;
}
