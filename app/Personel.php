<?php

namespace App;

use App\StatusForm;
use Illuminate\Database\Eloquent\Model;

class Personel extends Model
{
    protected $primaryKey = 'personel_id';
    protected $table = 'personel';
    protected $fillable = ['agama_id','status_personel_id','nama_lengkap','jenis_kelamin','tanggal_lahir','tmt_jabatan_pertama','nrp','nrp_file','nip','nip_file','foto_file','ktp_no','ktp_file','anak_ke','status_pernikahan','tanggal_perubahan','tanggal_pembuatan','nama_ibu','satker_pertama','polisi','tmt_pertama','database','tempat_lahir_id','tempat_lahir'];
    protected $dates = ['tmt_jabatan','tmt_jabatan_pertama','tanggal_lahir','tmt_pertama'];
    const CREATED_AT = 'tanggal_pembuatan';
    const UPDATED_AT = 'tanggal_perubahan';

    public static function createPersonel($data)
    {
        $personel = self::create($data);
        Fisik::create(['personel_id' => $personel->personel_id]);
        PersonelData::create(['personel_id' => $personel->personel_id]);
        $personel->fisik;
        $personel->data;
        return $personel;
    }

    public function statusList()
    {
        if ($this->is_duplicate || $this->is_error) {
            $icon = $this->is_duplicate==true ? "<span class='label bg-orange'>data duplikat</span><br>" :'';
            $icon.= $this->is_error==true ? "<span class='label bg-red'>data error</span>" :'';
        } else {
            $icon = "<span class='label bg-blue'>data bersih</span>";
        }
        return $icon;
    }
    public function scopeActive($query)
    {
        return $query->where('is_removed', false);
    }
    public function verificationButton()
    {
        if ($this->is_duplicate) {
            $icon = "<button type='submit' class='btn btn-xs bg-green'><i class='material-icons'>check</i></button>";
            return "<form method='POST' action='".route('personel.verification', $this->personel_id)."'>".
            csrf_field().
            "<input type='hidden' name='_method' value='PATCH'>".
            $icon.
            "</form>";
        }
        return '';
    }

    public function errors()
    {
        return $this->belongsToMany(Error::class, 'personel_error', 'personel_id', 'error_id')->withPivot('error_message');
    }

    public function agama()
    {
        return $this->belongsTo(Agama::class);
    }

    public function jenisKelamin()
    {
        return $this->belongsTo(JenisKelamin::class);
    }

    public function keluarga()
    {
        return $this->hasMany(KeluargaPersonel::class, 'personel_id');
    }

    public function statusPersonel()
    {
        return $this->belongsTo(StatusPersonel::class, 'status_personel_id');
    }
    public function bahasa()
    {
        return $this->belongsToMany(Bahasa::class, 'bahasa_personel', 'personel_id', 'bahasa_id')->using('App\BahasaPersonel')->withPivot(['aktif']);
    }
    public function bahasaDaerah()
    {
        return $this->belongsToMany(Bahasa::class, 'bahasa_personel', 'personel_id', 'bahasa_id')->using('App\BahasaPersonel')->withPivot(['aktif'])->wherePivot('lokal', 1);
    }
    public function bahasaInternational()
    {
        return $this->belongsToMany(Bahasa::class, 'bahasa_personel', 'personel_id', 'bahasa_id')->using('App\BahasaPersonel')->withPivot(['aktif'])->wherePivot('lokal', 0);
    }

    public function tandaKehormatan()
    {
        return $this->belongsToMany(TandaKehormatan::class, 'tanda_kehormatan_personel', 'personel_id', 'tanda_kehormatan_id')->using('App\TandaKehormatanPersonel')->withPivot('surat_nomor', 'surat_file', 'tanggal_pemberian', 'tanggal_perubahan');
    }

    public function hobi()
    {
        return $this->belongsToMany(Hobi::class, 'hobi_personel', 'personel_id', 'hobi_id');
    }

    public function olahraga()
    {
        return $this->belongsToMany(Olahraga::class, 'olahraga_personel', 'personel_id', 'olahraga_id');
    }
    public function alamat()
    {
        return $this->hasMany(Alamat::class, 'personel_id');
    }
    public function riwayatPsikologi()
    {
        return $this->hasMany(PsikologiPersonel::class, 'personel_id');
    }
    public function fisik()
    {
        return $this->hasOne(Fisik::class,'personel_id');
    }
    public function sim()
    {
        return $this->belongsToMany(SIM::class, 'sim_personel', 'personel_id', 'sim_id')->withPivot(['file','sim_nomor']);
    }
    public function simPersonel()
    {
        return $this->hasMany(SIMPersonel::class,'personel_id');
    }
    public function data()
    {
        return $this->hasOne(PersonelData::class, 'personel_id');
    }
    public function statusForms()
    {
        return $this->belongsToMany(StatusForm::class, 'personel_status_form', 'personel_id', 'status_form_id')->withPivot(['verified_1','verified_2','verified_3','verified_4','verified_5']);
    }

    public function newStatusForm()
    {
        // $this->statusForms()->sync(StatusForm::pluck('status_form_id'));
    }

    public function member()
    {
        return $this->hasOne(Member::class, 'personel_id');
    }

    public function penghargaan()
    {
        return $this->hasMany(PenghargaanPersonel::class, 'personel_id');
    }

    public function pendidikanUmum()
    {
        return $this->hasMany(PendidikanPersonel::class, 'personel_id')->whereNotNull('dikum_id');
    }

    public function brevet()
    {
        return $this->belongsToMany(Brevet::class, 'brevet_personel', 'personel_id', 'brevet_id')->withPivot(['tanggal_pengesahan','surat_keputusan_nomor','surat_keputusan_file','tanggal_perubahan']);
    }
    public function jasmani()
    {
        return $this->belongsToMany(Jasmani::class, 'jasmani_personel', 'personel_id', 'jasmani_id')->withPivot(['nilai','tanggal_tes','tanggal_perubahan','jasmani_personel_id']);
    }
    public function kesehatan()
    {
        return $this->belongsToMany(Kesehatan::class, 'kesehatan_personel', 'personel_id', 'kesehatan_id')->withPivot(['diagnosa','file','tanggal_tes','tanggal_perubahan','kesehatan_personel_id']);
    }
    public function pangkat()
    {
        return $this->belongsToMany(Pangkat::class, 'pangkat_personel', 'personel_id', 'pangkat_id')->withPivot(['status_terakhir','tmt_pangkat','surat_keputusan_nomor','surat_keputusan_file','tanggal_perubahan']);
    }

    public function riwayatPangkat()
    {
        return $this->hasMany(RiwayatPangkat::class, 'personel_id');
    }

    public function riwayatJabatan()
    {
        return $this->hasMany(RiwayatJabatan::class, 'personel_id');
    }

    public function tempatLahir()
    {
        return $this->belongsTo(Kota::class,'tempat_lahir_id');
    }
}
