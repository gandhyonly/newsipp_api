<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brevet extends Model
{
	protected $primaryKey = 'brevet_id';
	protected $table = 'brevet';
	protected $fillable = ['nama_brevet','asal_brevet','verified'];
	public $timestamps = false;

	public function personel()
	{
		return $this->belongsToMany(Personel::class,'brevet_personel','brevet_id','personel_id')->withPivot(['tanggal_pengesahan','surat_keputusan_nomor','surat_keputusan_file','tanggal_perubahan']);
	}
}
