<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kesehatan extends Model
{
	protected $table = 'kesehatan';
	protected $primaryKey = 'kesehatan_id';
	protected $fillable = ['jenis'];
	public $timestamps = false;
}
   