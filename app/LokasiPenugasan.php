<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LokasiPenugasan extends Model
{
	protected $table = 'lokasi_penugasan';
	protected $primaryKey = 'lokasi_penugasan_id';
	protected $fillable = ['nama_lembaga','kota','negara_id','verified','status_aktif'];
	public $timestamps = false;
	
	public function negara()
	{
		return $this->belongsTo(Negara::class,'negara_id');
	}
	public function jabatanPenugasan()
	{
		return $this->hasMany(JabatanPenugasan::class,'lokasi_penugasan_id')->orderBy('nama_jabatan_penugasan');
	}
}
