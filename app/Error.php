<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Error extends Model
{
	protected $primaryKey = 'error_id';
	protected $table = 'errors';
	public $timestamps = false;

	public function personels()
	{
		return $this->belongsToMany(Personel::class,'personel_error');
	}
}
