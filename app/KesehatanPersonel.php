<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KesehatanPersonel extends Model
{
	protected $table = 'kesehatan_personel';
	protected $primaryKey = 'kesehatan_personel_id';
	protected $fillable = ['personel_id','kesehatan_id','diagnosa','tanggal_tes','file'];
	public $dates = ['tanggal_tes'];
	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';

	public function kesehatan()
	{
		return $this->belongsTo(Kesehatan::class,'kesehatan_id');
	}
	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
}
