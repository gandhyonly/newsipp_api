<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanPenugasan extends Model
{
	protected $table = 'jabatan_penugasan';
	protected $primaryKey = 'jabatan_penugasan_id';
	protected $fillable = ['nama_jabatan_penugasan','eselon','misi_id','lokasi_penugasan_id'];
	public $timestamps = false;

	public function misi()
	{
		return $this->belongsTo(Misi::class,'misi_id');
	}

	public function lokasiPenugasan()
	{
		return $this->belongsTo(LokasiPenugasan::class,'lokasi_penugasan_id');
	}
}
