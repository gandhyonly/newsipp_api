<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
	protected $primaryKey = 'jabatan_id';
	protected $table = 'jabatan';
	protected $fillable = ['nama_jabatan'];
	public $timestamps = false;


	public function scopeActive($query)
	{
		return $query->where('deleted_by',null);
	}
	public function satuanJabatan()
	{
		return $this->hasMany(SatuanJabatan::class,'jabatan_id');
	}

	static public function deleteSatuanJabatanEmpty()
	{
		foreach(self::get() as $jabatan){
			if($jabatan->satuanJabatan()->count() == 0){
				$jabatan->delete();
			}
		}
	}
}
