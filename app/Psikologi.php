<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Psikologi extends Model
{
	protected $table = 'psikologi';
	protected $primaryKey = 'psikologi_id';
	protected $fillable = ['jenis'];
	public $timestamps = false;

	public function personel()
	{
		return $this->belongsToMany(Personel::class,'psikologi_personel','personel_id','psikologi_id')->withPivot('nilai','nilai_file','tanggal_tes','tanggal_perubahan');
	}
}
