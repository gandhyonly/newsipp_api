<?php

namespace App;

use App\Jabatan;
use App\Nivelering;
use App\Pangkat;
use Illuminate\Database\Eloquent\Model;

class SatuanJabatan2 extends Model
{
    protected $connection = 'mysql-satjab';
	protected $primaryKey = 'satuan_jabatan_id';
	protected $table = 'satuan_jabatan';
	protected $guarded = [];
	public $timestamps = false;


	public function scopeActive($query)
	{
		return $query->where('deleted_by',null);
	}
	public function scopeVerified($query)
	{
		return $query->whereNotNull('verified_by');
	}

	public function scopeLeader($query)
	{
		return $query->where('is_leader',true);
	}
	
	public function jabatan()
	{
		return $this->belongsTo(Jabatan::class,'jabatan_id');
	}
	public function nivelering()
	{
		return $this->belongsTo(Nivelering::class,'nivelering_id');
	}
	public function pangkat()
	{
		return $this->belongsToMany(Pangkat2::class,'pangkat_jabatan','satuan_jabatan_id','pangkat_id');
	}
	public function satuan()
	{
		return $this->belongsTo(Satuan2::class,'satuan_id');
	}
	public function childs()
	{
		return $this->hasMany(SatuanJabatan2::class,'parent_satuan_jabatan_id','satuan_jabatan_id');
	}
	public function parent()
	{
		return $this->belongsTo(SatuanJabatan2::class,'parent_satuan_jabatan_id');
	}
	public function printStatus()
	{
		if($this->verified_by!=null)
			return "<span class='label bg-green'>Terverifikasi</span>";
		else
			return "<span class='label bg-red'>Belum diverifikasi</span>";
	}

	public function listPangkat()
	{
		$temp = '';
		foreach($this->pangkat()->get() as $key => $pangkat){
			if($key!=0)
				$temp.=', ';
			$temp.=$pangkat->singkatan;
		}
		return $temp;
	}
	public function listPangkatID()
	{
		$temp = '';
		foreach($this->pangkat()->get() as $key => $pangkat){
			if($key!=0)
				$temp.=',';
			$temp.='"'.$pangkat->pangkat_id.'"';
		}
		return $temp;
	}
	public function listPangkatPNSID()
	{
		$temp = '';
		foreach($this->pangkat()->where('jenis','PNS')->get() as $key => $pangkat){
			if($key!=0)
				$temp.=',';
			$temp.='"'.$pangkat->pangkat_id.'"';
		}
		return $temp;
	}
	public function fullJabatan()
	{
		return $this->jabatan->nama_jabatan.' ('.$this->satuan()->first()->fullPrint().')';
	}
	public function fullKeterangan()
	{
		return $this->jabatan->nama_jabatan.' '.$this->satuan()->first()->fullName();
	}
}
