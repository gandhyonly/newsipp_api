<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HobiPersonel extends Model
{
	protected $table = 'hobi_personel';
	protected $primaryKey = 'hobi_personel_id';
	protected $fillable = ['hobi_id','personel_id'];
	public $timestamps = false;
	public function hobi()
	{
		return $this->belongsTo(Hobi::class,'hobi_id');
	}
	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
}
