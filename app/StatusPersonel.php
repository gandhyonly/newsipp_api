<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusPersonel extends Model
{
	protected $primaryKey = 'status_personel_id';
	protected $table = 'status_personel';
	protected $fillable =['status','status_aktif'];
	public $timestamps = false;

	public function personel()
	{
		return $this->hasMany(Personel::class);
	}
}
