<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nivelering extends Model
{
	protected $table = 'nivelering';
	protected $primaryKey = 'nivelering_id';
	protected $fillable = ['nivelering_id','nama_nivelering'];
	public $timestamps = false;
}
