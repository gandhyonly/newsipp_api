<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alamat extends Model
{
	protected $table = 'alamat';
	protected $primaryKey = 'alamat_id';
	protected $fillable = ['personel_id','alamat','telepon','tanggal_mulai','tanggal_selesai','status_terakhir'];

	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';

	public function scopeTerakhir($query)
	{
		return $query->where('status_terakhir',true);
	}

	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
}
