<?php

namespace App;

use App\MemberStatusForm;
use App\Satuan;
use App\StatusForm;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Member extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['nrp','','tanggal_lahir','tmt_jabatan_pertama','satker_pertama','api_token'];
    protected $primaryKey = 'member_id';
    protected $dates = ['tmt_jabatan_pertama','tmt_pertama','tanggal_lahir'];
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','api_token'
    ];

    public function personel()
    {
        return $this->belongsTo(Personel::class,'personel_id');
    }
    public function statusForm()
    {
        return $this->belongsToMany(StatusForm::class,'member_status_form','member_id','status_form_id')->withPivot(['verifikator_1','verifikator_2','verifikator_3','verifikator_4','tanggal_perubahan','keterangan']);
    }
    public function newStatusForm()
    {
        foreach(StatusForm::get() as $statusForm){
            $this->statusForm()->syncWithoutDetaching([$statusForm->status_form_id => ['tanggal_perubahan' => Carbon::now()]]);
        }
    }
    public function pesanMember()
    {
        return $this->hasMany(PesanMember::class,'member_id');
    }

    public function saveFullArraySatuan($satuanID)
    {
        $satuanIDTerkecil = Satuan::satuanTerkecil($satuanID);
        $arraySatuan = Satuan::fullArraySatuan($satuanIDTerkecil);
        $this->satuan_4=$arraySatuan[0];
        $this->satuan_3=$arraySatuan[1];
        $this->satuan_2=$arraySatuan[2];
        $this->satuan_1=empty($arraySatuan[3])?$arraySatuan[2]:$arraySatuan[3];
        return $this->save();
    }
    public function satkerTerkecil()
    {
        return $this->belongsTo(Satuan::class,'satuan_1');
    }
    public function satkerSekarang()
    {
        return $this->belongsTo(Satuan::class,'satker_sekarang_id');
    }
    public function resetStatusVerifikasi()
    {
        MemberStatusForm::where('member_id',$this->member_id)->update([
            'verifikator_1' => null,
            'verifikator_2' => null,
            'verifikator_3' => null,
            'verifikator_4' => null
        ]);
        $this->tingkat=1;
        $this->updated=false;
        $this->save();
    }
    
}
