<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Member;
use App\MemberStatusForm;

class RepairMemberStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repair:memberstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perbaiki Status Member yang kurang dari 20';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $i=0;
        foreach(Member::get() as $member){
            $count = MemberStatusForm::where('member_id',$member->member_id)->count();
            if($count != 20){
                echo $member->member_id.' '.$count."\n";
                // $member->newStatusForm();
                $i++;
            }
        }
        echo "Total = $i";
    }
}
