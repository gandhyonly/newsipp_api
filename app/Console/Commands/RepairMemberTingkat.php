<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Member;
use App\MemberStatusForm;

class RepairMemberTingkat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repair:memberTingkat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perbaiki member yang sudah diverifikasi tapi belum naik tingkatnya.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $i=0;
        $tingkat=0;
        foreach(Member::where('tingkat',2)->get() as $member){
            if(MemberStatusForm::where('member_id',$member->member_id)->whereNotNull('verifikator_2')->count()==20){
                $member->tingkat=3;
                $member->save();
                $tingkat++;
                $i++;
            }
        }
        echo "Tingkat 2: $tingkat\n";
        $tingkat=0;
        foreach(Member::where('tingkat',3)->get() as $member){
            if(MemberStatusForm::where('member_id',$member->member_id)->whereNotNull('verifikator_3')->count()==20){
                $member->tingkat=4;
                $member->save();
                $tingkat++;
                $i++;
            }
        }
        echo "Tingkat 3: $tingkat\n";
        $tingkat=0;
        foreach(Member::where('tingkat',4)->get() as $member){
            if(MemberStatusForm::where('member_id',$member->member_id)->whereNotNull('verifikator_4')->count()==20){
                $member->tingkat=5;
                $member->save();
                $i++;
            }
        }
        echo "Tingkat 4: $tingkat\n";
        echo $i;
        $memberCount=0;
        foreach(Member::where('tingkat',5)->get() as $member){
            // $member->updated=true;
            // $member->save();
            $memberCount++;
        }
        echo "\nError Tingkat 5 tapi False: $memberCount";
    }
}
