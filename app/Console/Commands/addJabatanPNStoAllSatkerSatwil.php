<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Pangkat;
use App\Satuan;
use App\SatuanJabatan;

class addJabatanPNStoAllSatkerSatwil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:PNS';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tambah PNS dan BHABINKAMTIBMAS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $jabatanIDs=[138,791];
        $pangkatIDs = Pangkat::where('jenis','PNS')->pluck('pangkat_id');
        foreach($jabatanIDs as $jabatanID){
            echo "Duplikasi PNS $jabatanID \n";
            // Polda
            $satuanPolda = Satuan::pluck('satuan_id')->toArray();
            $this->duplikasi($satuanPolda,$jabatanID,$pangkatIDs);
        }
        
    }
        
    public function duplikasi($satuanArray,$jabatanID,$pangkatIDs)
    {
        foreach($satuanArray as $satuanID){
            echo $satuanID."\n";
            $satuanJabatan = SatuanJabatan::create([
                'dsp' => 0,
                'satuan_id' => $satuanID,
                'jabatan_id' => $jabatanID,
                'status_jabatan_id' => 1,
                'parent_satuan_jabatan_id' => SatuanJabatan::where('satuan_id',$satuanID)->orderBy('nivelering_id')->orderBy('satuan_jabatan_id')->first() ? SatuanJabatan::where('satuan_id',$satuanID)->orderBy('nivelering_id')->orderBy('satuan_jabatan_id')->first()->satuan_jabatan_id : null
            ]);
            $satuanJabatan->pangkat()->sync($pangkatIDs);
        }
    }
}
