<?php

namespace App\Console\Commands;

use App\Member;
use App\Satuan;
use App\SatuanVerifikator;
use Illuminate\Console\Command;

class ReportSatuanVerifikator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:satuanverifikator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        SatuanVerifikator::truncate();
        $satuanChild = $mabes = Satuan::find(1);
        $tingkat=4;
        $totalPersonel=$mabes->total_personel??0;
        $jumlahPersonelTerdaftar=Member::where("satuan_$tingkat",$mabes->satuan_id)->count();
        $jumlahPersonelTerverifikasi=Member::where('updated',1)->where("satuan_$tingkat",$mabes->satuan_id)->where('tingkat','>',$tingkat)->count();
        $jumlahPersonelTingkat1=Member::where('updated',1)->where("satuan_$tingkat",$mabes->satuan_id)->where('tingkat','=',1)->count();
        $jumlahPersonelTingkat2=Member::where('updated',1)->where("satuan_$tingkat",$mabes->satuan_id)->where('tingkat','=',2)->count();
        $jumlahPersonelTingkat3=Member::where('updated',1)->where("satuan_$tingkat",$mabes->satuan_id)->where('tingkat','=',3)->count();
        $jumlahPersonelTingkat4=Member::where('updated',1)->where("satuan_$tingkat",$mabes->satuan_id)->where('tingkat','=',4)->count();
        $jumlahPersonelTingkat5=Member::where('updated',1)->where("satuan_$tingkat",$mabes->satuan_id)->where('tingkat','=',5)->count();
        $jumlahPersonelBelumMengirim = Member::where("satuan_$tingkat",$mabes->satuan_id)->where('updated',0)->count();
        $jumlahPersonelBelumTerverifikasi=$jumlahPersonelTerdaftar-$jumlahPersonelBelumMengirim-$jumlahPersonelTingkat5;
        $mabes = SatuanVerifikator::create([
            'nama_satuan' => $mabes->nama_satuan,
            'satuan_id' => $mabes->satuan_id,
            'tingkat' => 4,
            'jenis_satuan' => $mabes->jenisSatuan->kategori,
            'aktif' => $mabes->tipe == 'STRUKTUR' ? true : false,
            'position' => $mabes->posisi ? $mabes->posisi : $mabes->satuan_id,
            'total_personel' => $totalPersonel,
            'jumlah_personel_terdaftar' => $jumlahPersonelTerdaftar,
            'jumlah_personel_terverifikasi' => $jumlahPersonelTerverifikasi,
            'jumlah_personel_belum_mengirim' => $jumlahPersonelBelumMengirim,
            'jumlah_personel_belum_terverifikasi' => $jumlahPersonelBelumTerverifikasi,
            'jumlah_personel_tingkat_1' => $jumlahPersonelTingkat1,
            'jumlah_personel_tingkat_2' => $jumlahPersonelTingkat2,
            'jumlah_personel_tingkat_3' => $jumlahPersonelTingkat3,
            'jumlah_personel_tingkat_4' => $jumlahPersonelTingkat4,
            'jumlah_personel_tingkat_5' => $jumlahPersonelTingkat5,
        ]);

        $tingkat=3;
        $satuanTingkat = SatuanVerifikator::where('tingkat',$tingkat+1)->get();
        foreach($satuanTingkat as $satuanVerifikator){
            $parent = Satuan::find($satuanVerifikator->satuan_id);
            foreach($parent->childs as $satuan){
                $totalPersonel=$satuan->total_personel??0;
                $jumlahPersonelTerdaftar=Member::where("satuan_$tingkat",$satuan->satuan_id)->count();
                $jumlahPersonelTerverifikasi=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','>',$tingkat)->count();
                $jumlahPersonelTingkat1=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',1)->count();
                $jumlahPersonelTingkat2=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',2)->count();
                $jumlahPersonelTingkat3=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',3)->count();
                $jumlahPersonelTingkat4=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',4)->count();
                $jumlahPersonelTingkat5=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',5)->count();
                $jumlahPersonelBelumMengirim = Member::where("satuan_$tingkat",$satuan->satuan_id)->where('updated',0)->count();
                $jumlahPersonelBelumTerverifikasi=$jumlahPersonelTerdaftar-$jumlahPersonelBelumMengirim-$jumlahPersonelTingkat5;
                SatuanVerifikator::create([
                    'nama_satuan' => $satuan->nama_satuan,
                    'satuan_id' => $satuan->satuan_id,
                    'parent_id' => $satuanVerifikator->id,
                    'total_personel' => $totalPersonel,
                    'jumlah_personel_terdaftar' => $jumlahPersonelTerdaftar,
                    'jumlah_personel_terverifikasi' => $jumlahPersonelTerverifikasi,
                    'jumlah_personel_belum_mengirim' => $jumlahPersonelBelumMengirim,
                    'jumlah_personel_belum_terverifikasi' => $jumlahPersonelBelumTerverifikasi,
                    'jumlah_personel_tingkat_1' => $jumlahPersonelTingkat1,
                    'jumlah_personel_tingkat_2' => $jumlahPersonelTingkat2,
                    'jumlah_personel_tingkat_3' => $jumlahPersonelTingkat3,
                    'jumlah_personel_tingkat_4' => $jumlahPersonelTingkat4,
                    'jumlah_personel_tingkat_5' => $jumlahPersonelTingkat5,
                    'tingkat' => $tingkat,
                    'jenis_satuan' => $satuan->jenisSatuan->kategori,
                    'aktif' => $satuan->tipe == 'STRUKTUR' ? true : false,
                    'position' => $satuan->posisi ? $satuan->posisi : $satuan->satuan_id,
                ]);
            }
        }

        $tingkat=2;
        $satuanTingkat = SatuanVerifikator::where('tingkat',$tingkat+1)->get();
        foreach($satuanTingkat as $satuanVerifikator){
            $parent = Satuan::find($satuanVerifikator->satuan_id);
            foreach($parent->childs as $satuan){
                $totalPersonel=$satuan->total_personel??0;
                $jumlahPersonelTerdaftar=Member::where("satuan_$tingkat",$satuan->satuan_id)->count();
                $jumlahPersonelTerverifikasi=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','>',$tingkat)->count();
                $jumlahPersonelTingkat1=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',1)->count();
                $jumlahPersonelTingkat2=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',2)->count();
                $jumlahPersonelTingkat3=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',3)->count();
                $jumlahPersonelTingkat4=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',4)->count();
                $jumlahPersonelTingkat5=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',5)->count();
                $jumlahPersonelBelumMengirim = Member::where("satuan_$tingkat",$satuan->satuan_id)->where('updated',0)->count();
                $jumlahPersonelBelumTerverifikasi=$jumlahPersonelTerdaftar-$jumlahPersonelBelumMengirim-$jumlahPersonelTingkat5;
                SatuanVerifikator::create([
                    'nama_satuan' => $satuan->nama_satuan,
                    'satuan_id' => $satuan->satuan_id,
                    'parent_id' => $satuanVerifikator->id,
                    'total_personel' => $totalPersonel,
                    'jumlah_personel_terdaftar' => $jumlahPersonelTerdaftar,
                    'jumlah_personel_terverifikasi' => $jumlahPersonelTerverifikasi,
                    'jumlah_personel_belum_mengirim' => $jumlahPersonelBelumMengirim,
                    'jumlah_personel_belum_terverifikasi' => $jumlahPersonelBelumTerverifikasi,
                    'jumlah_personel_tingkat_1' => $jumlahPersonelTingkat1,
                    'jumlah_personel_tingkat_2' => $jumlahPersonelTingkat2,
                    'jumlah_personel_tingkat_3' => $jumlahPersonelTingkat3,
                    'jumlah_personel_tingkat_4' => $jumlahPersonelTingkat4,
                    'jumlah_personel_tingkat_5' => $jumlahPersonelTingkat5,
                    'tingkat' => $tingkat,
                    'jenis_satuan' => $satuan->jenisSatuan->kategori,
                    'aktif' => $satuan->tipe == 'STRUKTUR' ? true : false,
                    'position' => $satuan->posisi ? $satuan->posisi : $satuan->satuan_id,
                ]);
            }
        }
        $tingkat=1;
        $satuanTingkat = SatuanVerifikator::where('tingkat',$tingkat+1)->get();
        foreach($satuanTingkat as $satuanVerifikator){
            $parent = Satuan::find($satuanVerifikator->satuan_id);
            foreach($parent->childs as $satuan){
                $totalPersonel=$satuan->total_personel??0;
                $jumlahPersonelTerdaftar=Member::where("satuan_$tingkat",$satuan->satuan_id)->count();
                $jumlahPersonelTerverifikasi=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','>',$tingkat)->count();
                $jumlahPersonelTingkat1=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',1)->count();
                $jumlahPersonelTingkat2=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',2)->count();
                $jumlahPersonelTingkat3=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',3)->count();
                $jumlahPersonelTingkat4=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',4)->count();
                $jumlahPersonelTingkat5=Member::where('updated',1)->where("satuan_$tingkat",$satuan->satuan_id)->where('tingkat','=',5)->count();
                $jumlahPersonelBelumMengirim = Member::where("satuan_$tingkat",$satuan->satuan_id)->where('updated',0)->count();
                $jumlahPersonelBelumTerverifikasi=$jumlahPersonelTerdaftar-$jumlahPersonelBelumMengirim-$jumlahPersonelTingkat5;
                SatuanVerifikator::create([
                    'nama_satuan' => $satuan->nama_satuan,
                    'satuan_id' => $satuan->satuan_id,
                    'parent_id' => $satuanVerifikator->id,
                    'total_personel' => $totalPersonel,
                    'jumlah_personel_terdaftar' => $jumlahPersonelTerdaftar,
                    'jumlah_personel_terverifikasi' => $jumlahPersonelTerverifikasi,
                    'jumlah_personel_belum_mengirim' => $jumlahPersonelBelumMengirim,
                    'jumlah_personel_belum_terverifikasi' => $jumlahPersonelBelumTerverifikasi,
                    'jumlah_personel_tingkat_1' => $jumlahPersonelTingkat1,
                    'jumlah_personel_tingkat_2' => $jumlahPersonelTingkat2,
                    'jumlah_personel_tingkat_3' => $jumlahPersonelTingkat3,
                    'jumlah_personel_tingkat_4' => $jumlahPersonelTingkat4,
                    'jumlah_personel_tingkat_5' => $jumlahPersonelTingkat5,
                    'tingkat' => $tingkat,
                    'jenis_satuan' => $satuan->jenisSatuan->kategori,
                    'aktif' => $satuan->tipe == 'STRUKTUR' ? true : false,
                    'position' => $satuan->posisi ? $satuan->posisi : $satuan->satuan_id,
                ]);
            }
        }
    }
}
