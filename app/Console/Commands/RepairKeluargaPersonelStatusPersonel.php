<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\KeluargaPersonel;

class RepairKeluargaPersonelStatusPersonel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repair:keluarga-status-personel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jika memiliki nrp_keluarga maka status_personel = true;';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        KeluargaPersonel::whereNotNull('nrp_keluarga')->update(['status_personel'=>true]);
    }
}
