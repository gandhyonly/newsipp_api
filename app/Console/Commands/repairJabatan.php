<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SatuanJabatan;

class repairJabatan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'repair:jabatan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perbaiki contoh KAPOLDA POLDA A menjadi KAPOLDA A';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arrayString=['KAPOL','KARO'];
        foreach($arrayString as $stringJabatan){
            foreach(SatuanJabatan::where('keterangan','LIKE',$stringJabatan.'%')->get() as $satuanJabatan){
                $satuanJabatan->keterangan = $temp = $satuanJabatan->jabatan->nama_jabatan.' '.$satuanJabatan->satuan->fullNameUntukJabatan();
                $satuanJabatan->save();
                echo $temp."\n";
            }
        }
    }
}
