<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('report:satuanverifikator')->dailyAt('6:00');
        $schedule->command('report:satuanverifikator')->dailyAt('12:00');
        $schedule->command('report:satuanverifikator')->dailyAt('18:00');
        $schedule->command('repair:memberTingkat')->dailyAt('6:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
        $this->load(__DIR__.'/Commands');
    }
    protected $middleware = [
        \App\Http\Middleware\Cors::class,
        // \Barryvdh\Cors\HandleCors::class,
    ];
}
