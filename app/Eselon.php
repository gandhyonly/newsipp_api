<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eselon extends Model
{
	protected $table = 'eselon';
	protected $primaryKey = 'eselon_id';
	protected $fillable = ['nama_eselon'];
	public $timestamps = false;
}
