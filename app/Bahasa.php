<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bahasa extends Model
{
	protected $table = 'bahasa';
	protected $primaryKey = 'bahasa_id';
	protected $fillable = ['bahasa','lokal'];
	public $timestamps = false;

	public function scopeDaerah($query)
	{
		return $query->where('lokal',true);
	}
	public function scopeInternational($query)
	{
		return $query->where('lokal',false);
	}
	public function personel()
	{
		return $this->belongsToMany(Personel::class,'bahasa_personel','bahasa_id','personel_id')->using('App\BahasaPersonel')->withPivot('aktif');
	}
}
