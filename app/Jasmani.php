<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jasmani extends Model
{
	protected $table = 'jasmani';
	protected $primaryKey = 'jasmani_id';
	protected $guarded = [];

	public $timestamps=false;
	public function personel()
	{
		return $this->belongsToMany(Personel::class,'jasmani_personel','jasmani_id','personel_id')->withPivot(['nilai','tanggal_tes','tanggal_perubahan']);
	}

}
