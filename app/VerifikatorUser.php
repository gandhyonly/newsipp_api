<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class VerifikatorUser extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = ['nrp','','tanggal_lahir','tmt_jabatan_pertama','satker_pertama','api_token'];
    protected $primaryKey = 'verifikator_user_id';
    protected $guarded = [];
    protected $table = 'verifikator_user';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','api_token','is_superadmin'
    ];

    public function personel()
    {
        return $this->belongsTo(Personel::class,'personel_id');
    }
    public function statusForms()
    {
        return $this->belongsToMany(StatusForm::class,'member_status_form','member_id','status_form_id')->withPivot(['verifikator_1','verifikator_2','verifikator_3','verifikator_4','verifikator_5','tanggal_perubahan']);
    }
    public function newStatusForm()
    {
        foreach(StatusForm::get() as $statusForm){
            $this->statusForms()->sync($statusForm->status_form_id,['tanggal_perubahan' => Carbon::now()]);
        }
    }
    public function satuan()
    {
        return $this->belongsTo(Satuan::class,'satuan_id');
    }
    public function scopeCreatedByMe($query)
    {
        return $query->where('dibuat_oleh',auth()->user()->verifikator_user_id);
    }
    public function scopeActive($query)
    {
        return $query->where('aktif',1);
    }
}
