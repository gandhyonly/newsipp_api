<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipePendidikan extends Model
{
	protected $table = 'tipe_pendidikan';
	protected $primaryKey = 'tipe_pendidikan_id';
	protected $fillable = ['tipe'];
	public $timestamps = false;

	public function pendidikan()
	{
		return $this->hasMany(Pendidikan::class,'tipe_pendidikan_id');
	}
}
