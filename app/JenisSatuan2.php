<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisSatuan2 extends Model
{
    protected $connection = 'mysql-satjab';
	protected $primaryKey = 'jenis_satuan_id';
	protected $table = 'jenis_satuan';
	protected $guarded = [];
	public $timestamps = false;

	public function satuan()
	{
		return $this->hasMany(Satuan2::class,'jenis_satuan_id');
	}
}
