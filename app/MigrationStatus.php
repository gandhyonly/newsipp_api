<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MigrationStatus extends Model
{
    protected $table = 'migration_statuses';
    protected $filable = ['category','name','migration_date','route'];
    public $dates = ['migration_date'];
    public $timestamps = false;

    public function getRoute()
    {
    	return route($this->route);
    }

    public function getStatus()
    {
    	if($this->migration_date==null){
    		return '<small class="label bg-red">Belum dimigrasi</small>';
    	}else{
    		return '<small class="label bg-green">Sudah dimigrasi</small>';
    	}
    }
}
