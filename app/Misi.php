<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Misi extends Model
{
	protected $table = 'misi';
	protected $primaryKey = 'misi_id';
	public $dates = ['tanggal_mulai']; 
	protected $fillable = ['nama_misi','kota','verified','negara_id'];
	public $timestamps = false;

	public function negara()
	{
		return $this->belongsTo(Negara::class,'negara_id');
	}

	public function jabatanPenugasan()
	{
		return $this->hasMany(JabatanPenugasan::class,'misi_id')->orderBy('nama_jabatan_penugasan');
	}
}
