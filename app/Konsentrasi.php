<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konsentrasi extends Model
{
	protected $table = 'konsentrasi';
	protected $primaryKey = 'konsentrasi_id';
	protected $fillable = ['nama_konsentrasi','jurusan_id'];
	public $timestamps = false;

	public function jurusan()
	{
		return $this->belongsTo(Jurusan::class,'jurusan_id');
	}
	public function personel()
	{
		return $this->hasMany(PendidikanPersonel::class,'konsentrasi_id');
	}
	public static function echoTable()
	{
		foreach(self::withCount('personel')->orderBy('jurusan_id','nama_konsentrasi','konsentrasi_id')->get() as $konsentrasi){
			echo $konsentrasi->konsentrasi_id.' , '.$konsentrasi->nama_konsentrasi.' ,'.$konsentrasi->jurusan_id.','.$konsentrasi->personel_count."\n";
		}
	}
}
