<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonelKotor extends Model
{
    protected $connection = 'dbkotor';
	protected $primaryKey = 'personel_id';
	protected $table = 'personel';
	protected $guarded = [];
	public $timestamps = false;
}
