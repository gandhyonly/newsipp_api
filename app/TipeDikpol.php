<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeDikpol extends Model
{
	protected $table = 'tipe_dikpol';
	protected $primaryKey = 'tipe_dikpol_id';
	protected $fillable = ['tipe'];
	public $timestamps = false;

	public function jenisDikpol()
	{
		return $this->hasMany(JenisDikpol::class,'tipe_dikpol_id');
	}
}
