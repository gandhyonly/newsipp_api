<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KeluargaPersonel extends Model
{
	protected $primaryKey = 'keluarga_personel_id';
	protected $table = 'keluarga_personel';
	protected $guarded = [];
	protected $dates = ['tanggal_lahir','tanggal_nikah'];

	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';

	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}

	public function pekerjaanKeluarga()
	{
		return $this->hasMany(PekerjaanKeluarga::class,'keluarga_personel_id');
	}
	public function hubunganKeluarga()
	{
		return $this->belongsTo(HubunganKeluarga::class,'hubungan_keluarga_id');
	}
	public function golongan()
	{
		return $this->belongsTo(Golongan::class,'golongan_id');
	}
}
