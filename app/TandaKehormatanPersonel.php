<?php

namespace App;

use App\Personel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class TandaKehormatanPersonel extends Model
{
	protected $table = 'tanda_kehormatan_personel';
	protected $primaryKey = 'tanda_kehormatan_personel_id';
	protected $fillable = ['tanda_kehormatan_id','personel_id','surat_nomor','surat_file','tanggal_surat'];
	protected $dates = ['tanggal_surat'];

	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';
	public function tandaKehormatan()
	{
		return $this->belongsTo(TandaKehormatan::class,'tanda_kehormatan_id');
	}
	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}

	public function hapusFile($file)
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->$file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->$file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}
}
