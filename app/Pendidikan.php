<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
	protected $table = 'pendidikan';
	protected $primaryKey = 'pendidikan_id';
	protected $fillable = ['tipe_pendidikan_id','tingkat'];
	public $timestamps = false;
	
	public function tipePendidikan()
	{
		return $this->belongsTo(TipePendidikan::class,'tipe_pendidikan_id');
	}
}
