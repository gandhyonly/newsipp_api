<?php

namespace App;

use App\Personel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PenghargaanPersonel extends Model
{
	protected $table = 'penghargaan_personel';
	protected $primaryKey = 'penghargaan_personel_id';
	protected $fillable = ['personel_id','penghargaan_id','tingkat_penghargaan_id','tanggal_penghargaan','surat_nomor','surat_file','penghargaan_file'];
	protected $dates = ['tanggal_penghargaan'];
	public $timestamps=true;

	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';
	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}
	public function penghargaan()
	{
		return $this->belongsTo(Penghargaan::class,'penghargaan_id');
	}
	public function tingkatPenghargaan()
	{
		return $this->belongsTo(TingkatPenghargaan::class,'tingkat_penghargaan_id');
	}
	
	public function hapusFile($file)
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->$file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->$file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}
}
