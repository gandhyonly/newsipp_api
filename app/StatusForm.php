<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusForm extends Model
{
	protected $primaryKey = 'status_form_id';
	protected $table = 'status_form';
	protected $guarded = [];
	public $timestamps = false;
}
