<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TingkatPenghargaan extends Model
{
	protected $table = 'tingkat_penghargaan';
	protected $primaryKey = 'tingkat_penghargaan_id';
	protected $fillable = ['tingkat_penghargaan'];
	public $timestamps = false;
}
