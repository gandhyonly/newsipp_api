<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dikpol extends Model
{
	protected $table = 'dikpol';
	protected $primaryKey = 'dikpol_id';
	protected $fillable = ['tingkat','jenis_dikpol_id'];
	public $timestamps = false;

	public function jenisDikpol()
	{
		return $this->belongsTo(JenisDikpol::class,'jenis_dikpol_id');
	}
}
