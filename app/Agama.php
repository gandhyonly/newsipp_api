<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
	protected $table = 'agama';
	protected $primaryKey = 'agama_id';
	protected $fillable = ['nama'];
	public $timestamps = false;

	public function personels()
	{
		return $this->hasMany(Personel::class);
	}
}
