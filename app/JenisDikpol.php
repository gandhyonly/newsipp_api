<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisDikpol extends Model
{
	protected $table = 'jenis_dikpol';
	protected $primaryKey = 'jenis_dikpol_id';
	protected $fillable = ['nama_jenis_pendidikan'];
	public $timestamps = false;

	public function dikpol()
	{
		return $this->hasMany(Dikpol::class,'jenis_dikpol_id')->orderBy('tingkat');
	}
}
