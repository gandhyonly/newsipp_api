<?php

namespace App;

use App\Jabatan;
use App\Nivelering;
use App\Pangkat;
use App\Satuan;
use Illuminate\Database\Eloquent\Model;

class SatuanJabatan extends Model
{
	protected $primaryKey = 'satuan_jabatan_id';
	protected $table = 'satuan_jabatan';
	protected $guarded = [];
	public $timestamps = false;


	public function scopeActive($query)
	{
		return $query->where('deleted_by',null);
	}
	public function scopeVerified($query)
	{
		return $query->whereNotNull('verified_by');
	}

	public function urutanSatuan()
	{
		return Satuan::arraySatuanBaru($this->satuan_id);
	}

	public function scopeLeader($query)
	{
		return $query->where('is_leader',true);
	}
	
	public function jabatan()
	{
		return $this->belongsTo(Jabatan::class,'jabatan_id');
	}
	public function nivelering()
	{
		return $this->belongsTo(Nivelering::class,'nivelering_id');
	}
	public function pangkat()
	{
		return $this->belongsToMany(Pangkat::class,'pangkat_jabatan','satuan_jabatan_id','pangkat_id');
	}
	public function satuan()
	{
		return $this->belongsTo(Satuan::class,'satuan_id');
	}
	public function childs()
	{
		return $this->hasMany(SatuanJabatan::class,'parent_satuan_jabatan_id','satuan_jabatan_id')->orderBy('satuan_jabatan_id');
	}
	public function parent()
	{
		return $this->belongsTo(SatuanJabatan::class,'parent_satuan_jabatan_id');
	}

	public function setKeterangan()
	{
		return $this->update(['keterangan' => $this->fullKeterangan()]);
	}
	public function printStatus()
	{
		if($this->verified_by!=null)
			return "<span class='label bg-green'>Terverifikasi</span>";
		else
			return "<span class='label bg-red'>Belum diverifikasi</span>";
	}
	public static function createJabatan($satuan,$atasan,$namaJabatan,$arrayPangkat,$namaNivelering,$dsp)
	{
		$errorMessage='';
		$niveleringID = null;
		if(Jabatan::where('nama_jabatan',$namaJabatan)->count()>0){
			$jabatan = Jabatan::where('nama_jabatan',$namaJabatan)->first();
		} else {
			$jabatan = Jabatan::create(['nama_jabatan' => $namaJabatan]);
		}
		if($namaNivelering=='-'){
			$niveleringID=null;
		} elseif(Nivelering::where('nama_nivelering',$namaNivelering)->count()>0){
			$niveleringID = Nivelering::where('nama_nivelering',$namaNivelering)->first()->nivelering_id;
		} else{
			$errorMessage.="Nivelering : '".$namaNivelering."' tidak ditemukan.<br>";
		}
		$atasanID=$atasan==null?null:$atasan->satuan_jabatan_id;
		$satuanJabatan = self::create([
			'satuan_id' => $satuan->satuan_id,
			'jabatan_id' => $jabatan->jabatan_id,
			'parent_satuan_jabatan_id' => $atasanID,
			'nivelering_id' => $niveleringID,
			'status_jabatan_id' => 1,
			'deskripsi' => $jabatan->nama_jabatan.' '.$satuan->fullName(),
			'dsp'	=> $dsp,
			'error_message' => $errorMessage
		]);
		$pangkatIDs = [];
		if($arrayPangkat!=null){
			foreach($arrayPangkat as $pangkat){
				if(Pangkat::where('nama_pangkat',$pangkat)->count()>0){
					$pangkatID = Pangkat::where('nama_pangkat',$pangkat)->first()->pangkat_id;
					array_push($pangkatIDs,$pangkatID);
				}
				else {
					$pangkatID=[];
					switch($pangkat){
						case 'BA': 
							$pangkatID = [19,20,21,22,23,24,25,26,27,28,29,30];
							break;
						case 'IP':
							$pangkatID = [31,32];
							break;
						case 'AKP':
							$pangkatID = [33];
							break;
						case 'KP':
							$pangkatID = [34];
							break;
						case 'AKBP':
							$pangkatID = [35];
							break;
						case 'KBP':
							$pangkatID = [36]; 
							break;
						case 'BRIGJEN':
							$pangkatID = [37]; 
							break;
						case 'IRJEN':
							$pangkatID = [38]; 
							break;
						case 'KOMJEN':
							$pangkatID = [39]; 
							break;
						case 'JENDERAL':
							$pangkatID = [40]; 
							break;
						default : 
							$errorMessage.="Pangkat : '".$pangkat."' tidak ditemukan.<br>";
							break;
					}
					array_merge($pangkatIDs,$pangkatID);
				}
			}
			$satuanJabatan->pangkat()->sync($pangkatIDs);
		}
		$satuanJabatan->error_message=$errorMessage;
		$satuanJabatan->save();
		return $satuanJabatan;
	}

	public function listPangkat()
	{
		$temp = '';
		foreach($this->pangkat()->get() as $key => $pangkat){
			if($key!=0)
				$temp.=', ';
			$temp.=$pangkat->singkatan;
		}
		return $temp;
	}
	public function listPangkatID()
	{
		$temp = '';
		foreach($this->pangkat()->get() as $key => $pangkat){
			if($key!=0)
				$temp.=',';
			$temp.='"'.$pangkat->pangkat_id.'"';
		}
		return $temp;
	}
	public function listPangkatPNSID()
	{
		$temp = '';
		foreach($this->pangkat()->where('jenis','PNS')->get() as $key => $pangkat){
			if($key!=0)
				$temp.=',';
			$temp.='"'.$pangkat->pangkat_id.'"';
		}
		return $temp;
	}
	public function fullJabatan()
	{
		return $this->jabatan->nama_jabatan.' ('.$this->satuan()->first()->fullPrint().')';
	}
	public function fullKeterangan()
	{
		return $this->jabatan->nama_jabatan.' '.$this->satuan()->first()->fullName();
	}
}
