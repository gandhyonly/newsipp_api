<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisBKO extends Model
{
	protected $table = 'jenis_bko';
	protected $primaryKey = 'jenis_bko_id';
	protected $fillable = ['nama_jenis_bko'];
	public $timestamps = false;
}
