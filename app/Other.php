<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Other extends Model
{
	protected $table = 'others';
	protected $primaryKey = 'id';
	protected $fillable = ['value','other_group_id'];
	public $timestamps = false;

	public function otherGroups()
	{
		return $this->belongsTo(OtherGroup::class)->orderBy('value'); 
	}
}
