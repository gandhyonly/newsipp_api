<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Olahraga extends Model
{
	protected $table = 'olahraga';
	protected $primaryKey = 'olahraga_id';
	protected $fillable = ['nama_olahraga'];
	public $timestamps = false;

	public function personel()
	{
		return $this->belongsToMany(Personel::class,'olahraga_personel','personel_id','olahraga_id');
	}
}
