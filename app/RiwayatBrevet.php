<?php

namespace App;

use App\Personel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RiwayatBrevet extends Model
{
	protected $table = 'riwayat_brevet';
	protected $primaryKey = 'riwayat_brevet_id';
	protected $fillable = ['brevet_id','personel_id','tmt_brevet','surat_keputusan_nomor','surat_keputusan_file'];
	public $dates = ['tmt_brevet'];
	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';

	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}

	public function brevet()
	{
		return $this->belongsTo(Brevet::class,'brevet_id');
	}
	public function hapusFile()
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->surat_keputusan_file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->surat_keputusan_file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}
}
