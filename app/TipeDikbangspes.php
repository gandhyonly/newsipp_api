<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipeDikbangspes extends Model
{
	protected $table = 'tipe_dikbangspes';
	protected $primaryKey = 'tipe_dikbangspes_id';
	protected $fillable = ['tipe'];
	public $timestamps = false;

	public function dikbangspes()
	{
		return $this->hasMany(Dikbangspes::class,'tipe_dikbangspes_id')->orderBy('nama_dikbangspes');
	}
}
