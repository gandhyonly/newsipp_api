<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TandaKehormatan extends Model
{
	protected $table = 'tanda_kehormatan';
	protected $primaryKey = 'tanda_kehormatan_id';
	protected $fillable = ['nama_tanda_kehormatan'];
	public $timestamps = false;

	public function personel()
	{
		return $this->belongsToMany(Personel::class,'tanda_kehormatan_personel','personel_id','tanda_kehormatan_id')->using('App\TandaKehormatan')->withPivot('surat_nomor','surat_file','tanggal_pemberian','tanggal_perubahan');
	}
}
