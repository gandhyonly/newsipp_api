<?php

namespace App;

use App\Personel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class PendidikanPersonel extends Model
{

	protected $table = 'pendidikan_personel';
	protected $primaryKey = 'pendidikan_personel_id';
	protected $fillable = ['nama_institusi','nilai_akhir','nilai_file','ranking','total_siswa','gelar_id','personel_id','pendidikan_id','status_terakhir','akreditasi','surat_kelulusan_nomor','surat_kelulusan_file','tanggal_mulai','tanggal_lulus','akreditasi','dinas','dikum_id','dikpol_id','dikbangspes_id','angkatan','konsentrasi_id'];
	protected $dates = ['tanggal_mulai','tanggal_lulus'];

	public $timestamps=false;

	const CREATED_AT = null;
	const UPDATED_AT = 'tanggal_perubahan';

	public function hapusFile($file)
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->$file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->$file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}

	public function gelar()
	{
		return $this->belongsTo(Gelar::class,'gelar_id');
	}
	public function dikum()
	{
		return $this->belongsTo(Dikum::class,'dikum_id');
	}
	public function konsentrasi()
	{
		return $this->belongsTo(Konsentrasi::class,'konsentrasi_id');
	}
	public function dikpol()
	{
		return $this->belongsTo(Dikpol::class,'dikpol_id');
	}
	public function dikbangspes()
	{
		return $this->belongsTo(Dikbangspes::class,'dikbangspes_id');
	}
}
