<?php

namespace App;

use App\Personel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class BKO extends Model
{
	protected $table = 'bko';
	protected $primaryKey = 'bko_id';
	protected $fillable = ['personel_id','satuan_penugasan_id','jenis_bko_id','jabatan_bko','satuan_asal_id','surat_keputusan_nomor','surat_keputusan_file','tmt_bko'];
	public $timestamps = false;

	public function jenisBko()
	{
		return $this->belongsTo(JenisBKO::class,'jenis_bko_id','jenis_bko_id');
	}
	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id','personel_id');
	}
	public function satuanAsal()
	{
		return $this->belongsTo(Satuan::class,'satuan_asal_id','satuan_id');
	}
	public function satuanPenugasan()
	{
		return $this->belongsTo(Satuan::class,'satuan_penugasan_id','satuan_id');
	}
	public function hapusFile()
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->surat_keputusan_file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->surat_keputusan_file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}
}
