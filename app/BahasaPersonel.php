<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;
class BahasaPersonel extends Pivot
{
	protected $table = 'bahasa_personel';
	protected $primaryKey = 'bahasa_personel_id';
	protected $fillable = ['bahasa_id','personel_id','aktif'];
	public $timestamps = false;
	public function bahasa()
	{
		return $this->belongsTo(Bahasa::class,'bahasa_id');
	}
}
