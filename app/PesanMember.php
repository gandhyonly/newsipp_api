<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PesanMember extends Model
{
	protected $table = 'pesan_member';
	protected $primaryKey = 'pesan_member_id';
	protected $fillable = ['judul','pesan','verifikator_user_id','member_id','tingkat','bermasalah','tanggal_dibuat'];

	protected $dates= ['tanggal_dibuat'];

	public $timestamps = false;

	public function verifikator()
	{
		return $this->belongsTo(VerifikatorUser::class,'verifikator_user_id');
	}
}
