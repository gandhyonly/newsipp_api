<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
	protected $primaryKey = 'golongan_id';
	protected $table = 'golongan';
	protected $fillable = ['nama_golongan'];
	public $timestamps = false;

	public function pangkat()
	{
		return $this->hasMany(Pangkat::class);
	}
}
