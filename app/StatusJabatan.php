<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusJabatan extends Model
{
	protected $primaryKey = 'status_jabatan_id';
	protected $table = 'status_jabatan';
	protected $guarded = [];
	public $timestamps = false;
}
