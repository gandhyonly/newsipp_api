<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dikum extends Model
{
	protected $table = 'dikum';
	protected $primaryKey = 'dikum_id';
	protected $fillable = ['tingkat'];
	public $timestamps = false;
}
