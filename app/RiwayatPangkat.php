<?php

namespace App;

use App\Personel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class RiwayatPangkat extends Model
{
	protected $table = 'riwayat_pangkat';
	protected $primaryKey = 'riwayat_pangkat_id';
	protected $fillable = ['pangkat_id','personel_id','status_terakhir','tmt_pangkat','surat_keputusan_nomor','surat_keputusan_file'];
	protected $dates = ['tmt_pangkat'];
	public $timestamps = true;
	const CREATED_AT = 'tanggal_perubahan';
	const UPDATED_AT = 'tanggal_perubahan';

	public function personel()
	{
		return $this->belongsTo(Personel::class,'personel_id');
	}

	public function pangkat()
	{
		return $this->belongsTo(Pangkat::class,'pangkat_id');
	}
	public function hapusFile()
	{
		$personel = Personel::find(auth()->user()->personel_id);
		$personelNRP = $personel->nrp;
		if($this->surat_keputusan_file != null){
			$fileLocation = 'personel/'.$personelNRP.'/'.$this->surat_keputusan_file;
			$fileExist = Storage::disk('local')->exists($fileLocation);
			if($fileExist){
				Storage::disk('local')->delete($fileLocation);
			}
		}
	}
}
