<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'dbkotor' => [
            'driver' => 'pgsql',
            'host' => env('DBKOTOR_HOST', '127.0.0.1'),
            'port' => env('DBKOTOR_PORT', '3306'),
            'database' => env('DBKOTOR_DATABASE', 'forge'),
            'username' => env('DBKOTOR_USERNAME', 'forge'),
            'password' => env('DBKOTOR_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'mysql' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],
        'mysql-satjab' => [
            'driver' => 'mysql',
            'host' => env('DB_MYSQL_SATJAB_HOST', '127.0.0.1'),
            'port' => env('DB_MYSQL_SATJAB_PORT', '3306'),
            'database' => env('DB_MYSQL_SATJAB_DATABASE', 'forge'),
            'username' => env('DB_MYSQL_SATJAB_USERNAME', 'forge'),
            'password' => env('DB_MYSQL_SATJAB_PASSWORD', ''),
            'unix_socket' => env('DB_MYSQL_SATJAB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],
        'newsipp_mabes' => [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'port' => '3306',
            'database' => 'newsipp_mabes',
            'username' => 'root',
            'password' => '',
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
        ],
        'LOCAL_JATEN' => ['driver' => 'mysql', 'host' => env('DB_HOST', '127.0.0.1'), 'port' => env('DB_PORT', '3306'), 'database' => 'newsipp_jateng', 'username' => 'root', 'password' => 'lhdSi4EPRy3IgRI6', 'unix_socket' => env('DB_SOCKET', ''), 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci', 'prefix' => '', 'strict' => true, 'engine' => null, ], 'newsipp_ssdm' => ['driver' => 'mysql', 'host' => env('DB_HOST', '127.0.0.1'), 'port' => env('DB_PORT', '3306'), 'database' => 'oldsipp_ssdm', 'username' => 'root', 'password' => 'lhdSi4EPRy3IgRI6', 'unix_socket' => env('DB_SOCKET', ''), 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci', 'prefix' => '', 'strict' => true, 'engine' => null, ],
        'local_ssdm' => ['driver' => 'mysql', 'host' => env('DB_HOST', '127.0.0.1'), 'port' => env('DB_PORT', '3306'), 'database' => 'local_ssdm', 'username' => 'root', 'password' => '', 'unix_socket' => env('DB_SOCKET', ''), 'charset' => 'utf8mb4', 'collation' => 'utf8mb4_unicode_ci', 'prefix' => '', 'strict' => true, 'engine' => null, ],
        
        'M_baharkam' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_baharkam','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_baintelkam' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_baintelkam','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_bareskrim' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_bareskrim','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_densus' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_densus','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_divhubinter' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_divhubinter','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_divhumas' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_divhumas','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_divkum' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_divkum','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_divpropam' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_divpropam','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_divti' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_divti','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_itwasum' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_itwasum','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_korbrimob' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_korbrimob','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_korlantas' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_korlantas','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_lemdikpol' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_lemdikpol','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_litbang' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_litbang','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_pusdokkes' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_pusdokkes','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_pusjarah' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_pusjarah','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_puskeu' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_puskeu','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_sahli' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_sahli','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_setum' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_setum','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_sops' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_sops','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_spripim' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_spripim','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_srena' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_srena','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_ssarpras' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_ssarpras','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_ssdm' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_ssdm','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'M_yanma' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'M_yanma','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_ACEH' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_ACEH','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_BABEL' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_BABEL','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_BALI' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_BALI','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_BANTEN' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_BANTEN','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_BENGKULU' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_BENGKULU','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_DIY' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_DIY','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_GORONTALO' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_GORONTALO','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_JABAR' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_JABAR','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_JAMBI' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_JAMBI','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_JATENG' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_JATENG','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_JATIM' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_JATIM','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_KALBAR' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_KALBAR','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_KALSEL' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_KALSEL','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_KALTIM' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_KALTIM','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_KALTENG' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_KALTENG','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_KALTIM' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_KALTIM','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_KEPRI' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_KEPRI','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_LAMPUNG' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_LAMPUNG','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_MALUKU' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_MALUKU','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_MALUT' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_MALUT','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_METRO' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_METRO','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_NTB' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_NTB','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_NTT' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_NTT','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_PAPUA' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_PAPUA','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_PAPUABARAT' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_PAPUABARAT','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_RIAU' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_RIAU','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_SULSEL' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_SULSEL','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_SULTENG' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_SULTENG','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_SULTRA' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_SULTRA','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_SULUT' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_SULUT','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_SUMBAR' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_SUMBAR','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_SUMSEL' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_SUMSEL','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
        'P_SUMUT' => ['driver' => 'mysql','host' => env('MYSQL_OLD_DB_HOST','192.168.212.150'),'port' => env('MYSQL_OLD_DB_PORT','3306'),'database' => 'P_SUMUT','username' => env('MYSQL_OLD_DB_USERNAME','sipp'),'password' => env('MYSQL_OLD_DB_PASSWORD',''),'charset' => 'utf8','collation' => 'utf8_general_ci','prefix' => '','strict' => false,'engine' => null,],
    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => 'predis',

        'default' => [
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', 6379),
            'database' => 0,
        ],
    ],
];
